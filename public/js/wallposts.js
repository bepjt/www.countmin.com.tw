var wallposts_renew_box=function(wuid,wdata){

	var tobj=$(".wallposts .wbox[data-wuid="+wuid+"]");

	///目前只更新一些資訊,到時開放編輯功能,再換上對應欄位
	if(tobj.html()){
		tobj.after(wdata.box);
		tobj.remove();
		return 1;
	}
	else{
		return 0;
	}
	return 0;
}

var wallposts_renew=function(wuid,wdata){

	var tobj=$(".wallposts .wbox[data-wuid="+wuid+"]");

	///目前只更新一些資訊,到時開放編輯功能,再換上對應欄位
	if(tobj.html()){
		$(tobj).find(".box-foot .info span:eq(0)").html(wdata.wallpostNumLikes);
		$(tobj).find(".box-foot .info span:eq(1)").html(wdata.wallpostNumDislikes);
		$(tobj).find(".box-foot .info span:eq(2)").html(wdata.wallpostNumComments);
		$(tobj).find(".box-foot .info span:eq(3)").html(wdata.wallpostNumShares);
		if(typeof wdata.uwallpost_track.like!="undefined"){$(tobj).find(".like-this").addClass("did");}
		if(typeof wdata.uwallpost_track.dislike!="undefined"){$(tobj).find(".dislike-this").addClass("did");}
		if(typeof wdata.uwallpost_track.comment!="undefined"){$(tobj).find(".comment-this").addClass("did");}
		if(typeof wdata.uwallpost_track.share!="undefined"){$(tobj).find(".share-this").addClass("did");}
		if(typeof wdata.uwallpost_track.favor!="undefined"){$(tobj).find(".favor-this").html("已收藏");}
		return 1;
	}
	else{
		return 0;
	}
	return 0;
}


var wallposts_like=function(tobj,exe,wuid){
	$.post('/user/wallpost_like',{exe:exe,wuid:wuid},function(mesg){
			mesg=$.parseJSON(mesg);
			if(mesg.error==0){
				$(tobj).addClass('did');
				wallposts_renew(mesg.data.wallpostUid,mesg.data);
			}
	});
}

var wallposts_favor=function(tobj,wuid){
	$.post('/user/wallpost_favor',{wuid:wuid},function(mesg){
			mesg=$.parseJSON(mesg);
			if(mesg.error==0){
				if(mesg.data==true){$(tobj).html('已收藏');}
				else{$(tobj).html('加入收藏');}
			}
	});
}


var wallposts_after=function(tobj,timeset){
	
	var timebefore=0;
	
	var ttype=$(".wallposts_after").attr('data-wtype');
	var interests=$(".wallposts_after").attr('data-winterests');
	
	$.post('/user/wallposts?t='+ttype+'&i='+interests,{timeset:timeset,timebefore:timebefore,box:true},function(mesg){
			mesg=$.parseJSON(mesg);
			$(mesg.data).each(function(inx,elem){
				if(wallposts_renew_box(elem.wuid,elem)!=1){
					$(".wallposts_after").after(elem.box);
				}
			});
			$('.datetime').each(function(){
				timezone_reset($(this));	
			});
	});
}
var wallposts_before=function(tobj,timeset){
	
	var timebefore=1;
	
	var ttype=$(".wallposts_before").attr('data-wtype');
	var interests=$(".wallposts_before").attr('data-winterests');
	
	$.post('/user/wallposts?t='+ttype+'&i='+interests,{timeset:timeset,timebefore:timebefore,box:true},function(mesg){
			mesg=$.parseJSON(mesg);
			$(mesg.data).each(function(inx,elem){
				if(wallposts_renew_box(elem.wuid,elem)!=1){
					$(".wallposts_before").before(elem.box);
				}
			});
			$('.datetime').each(function(){
				timezone_reset($(this));	
			});
	});
}

var my_wallposts_after=function(tobj,uid,timeset){
	
	var timebefore=0;
	
	$.post('/user/mywalls?p='+uid,{timeset:timeset,timebefore:timebefore},function(mesg){
			mesg=$.parseJSON(mesg);
			$(mesg.d).each(function(inx,elem){
				if(wallposts_renew(elem.wallpostUid,elem)!=1){
					
					//var txt=wallposts_add(elem.wallpostUid,elem);
					$(".my_wallposts_after").after(txt);
				}
			});
	});
}
var my_wallposts_before=function(tobj,uid,timeset){
	
	var timebefore=1;
	
	$.post('/user/mywalls?p='+uid,{timeset:timeset,timebefore:timebefore},function(mesg){
			mesg=$.parseJSON(mesg);
			$(mesg.d).each(function(inx,elem){
				if(wallposts_renew(elem.wallpostUid,elem)!=1){
					//var txt=wallposts_add(elem.wallpostUid,elem);
					$(".my_wallposts_before").before(txt);
				}
			});
	});
}


 var wallposts_renew_counter=function(){
	this.born=0;
	this.count=1; 
	if(this.born==0){
		$("body").append("<span class='wallposts_renew_counter'></span>");
		this.born=1;
	}
	
	this.run=function(){
		
		if(this.count>=300){
			this.count=0;
		}//reset count	
		if(this.count%10==0){
			var d=new Date();
			var tmin=d.getMinutes();
			var info=$(".wallposts .wbox");
			var wuid_dot='';
			info.each(function(inx,elem){
				wuid_dot+=(wuid_dot=='') ? $(elem).attr('data-wuid') : (","+$(elem).attr('data-wuid'));
			});
			
			$.post('/user/wallposts_renew',{wuid_dot:wuid_dot},function(mesg){
				mesg=$.parseJSON(mesg);
				if((mesg).error!=1){
					$(mesg.data.d).each(function(inx,elem){
						wallposts_renew(elem.wallpostUid,elem);
					});
				}
				$(".wallposts_after").trigger('click');/////自動讀取最新的興趣牆
			});
		}
		++this.count;

		var obj=this;
		$(".wallposts_renew_counter").hide().delay(1000).attr('count',this.count).hide(0,obj,function(){
			obj.run();
		});
	}
	
	this.run();

}



var relation_build=function(tobj,model){
	var view_block=$(tobj).parent().parent();
	var taction=$(tobj).attr('data-action');
	var need_confirm=$(tobj).attr('data-confirm');
	var tobj2=$(tobj).closest('.countmin_relation');
	var user_uid2=tobj2.attr('data-uid');
	
	var flag=1;
	if(need_confirm){
		flag=confirm('確定解除此關係!?');
	}
	
	if(flag==1){
		$.get('/user/relation_build',{'user_uid2':user_uid2,'action':taction,'model':model},function(mesg){
				mesg=$.parseJSON(mesg);
				if((mesg).error!=1){
					var relation=(mesg).data;
					var txt="";
					if(model==1){
						if(relation.relation_status.now=='stranger'){
							txt+="<div class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='r-btn' data-action='follow'><i class='fa' ></i>追蹤</div>";
							txt+="</div>";
						}
						if(relation.relation_status.now=='interest'){
							txt+="<div class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='r-btn' data-action='follow'><i class='fa' ></i>追蹤</div>";
							txt+="</div>";
						}
						
						if(relation.relation_status.now=='follow'){
							txt+="<div class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='r-btn' data-action='not_follow'><i class='fa' ></i>取消追蹤</div>";
							txt+="</div>";
						}
						
						if(relation.relation_status.now=='inviting'){
							txt+="<div class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='r-btn' data-action='not_inviting'><i class='fa' ></i>取消邀請</div>";
							txt+="</div>";
						}
						
						if(relation.relation_status.now=='friend'){
							txt+="<div class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='r-btn' data-action='not_friend' data-confirm='1'><i class='fa' ></i>取消好友</div>";
							txt+="</div>";
						}
					}
					else if(model==2){
						if(relation.relation_status.now=='stranger'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							if(typeof(relation.relation_status.follow)!='undefined'){
							txt+="<div class='s-btn s-btn-normal' data-action='not_follow'>取消追蹤</div>";
							}
							else{
							txt+="<div class='s-btn s-btn-normal' data-action='follow'>追蹤</div>";
							}
							if(typeof(relation.relation_status.inviting)!='undefined'){
							txt+="<div class='s-btn s-btn-normal' data-action='not_inviting'>取消邀請</div>";
							}
							else{
							txt+="<div class='s-btn s-btn-normal' data-action='inviting'>加好友</div>";
							}
							txt+="</span>";
						}
						if(relation.relation_status.now=='interest'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='s-btn s-btn-normal' data-action='follow'>追蹤</div>";
							txt+="<div class='s-btn s-btn-normal' data-action='inviting'>加好友</div>";
							txt+="</span>";
						}
						
						if(relation.relation_status.now=='follow'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='s-btn s-btn-normal' data-action='not_follow'>取消追蹤</div>";
							txt+="<div class='s-btn s-btn-normal' data-action='inviting'>加好友</div>";
							txt+="</span>";
						}
						
						if(relation.relation_status.now=='inviting'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							if(typeof(relation.relation_status.follow)!='undefined'){
							txt+="<div class='s-btn s-btn-normal' data-action='not_follow'>取消追蹤</div>";
							}
							else{
							txt+="<div class='s-btn s-btn-normal' data-action='follow'>追蹤</div>";
							}
							txt+="<div class='s-btn s-btn-normal' data-action='not_inviting'>取消邀請</div>";
							txt+="</span>";
						}
						
						if(relation.relation_status.now=='friend'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<div class='s-btn s-btn-normal' data-action='not_friend' data-confirm='1'>取消好友</div>";
							txt+="</span>";
						}
						txt+="<script>top.location.reload();</script>";
					}
					else if(model==3){
						if(relation.relation_status.now=='stranger'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							if(typeof(relation.relation_status.follow)!='undefined'){
							txt+="<span class='motion motion-btn' data-action='not_follow'>取消追蹤</span>";
							}
							else{
							txt+="<span class='motion motion-btn' data-action='follow'>追蹤</span>";
							}
							if(typeof(relation.relation_status.inviting)!='undefined'){
							txt+="<span class='motion motion-btn' data-action='not_inviting'>取消邀請</span>";
							}
							else{
							txt+="<span class='motion motion-btn' data-action='inviting'>加好友</span>";
							}
							txt+="</span>";
						}
						if(relation.relation_status.now=='interest'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<span class='motion motion-btn' data-action='follow'>追蹤</span>";
							txt+="<span class='motion motion-btn' data-action='inviting'>加好友</span>";
							txt+="</span>";
						}
						
						if(relation.relation_status.now=='follow'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<span class='motion motion-btn' data-action='not_follow'>取消追蹤</span>";
							txt+="<span class='motion motion-btn' data-action='inviting'>加好友</span>";
							txt+="</span>";
						}
						
						if(relation.relation_status.now=='inviting'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							if(typeof(relation.relation_status.follow)!='undefined'){
							txt+="<span class='motion motion-btn' data-action='not_follow'>取消追蹤</span>";
							}
							else{
							txt+="<span class='motion motion-btn' data-action='follow'>追蹤</span>";
							}
							txt+="<span class='motion motion-btn' data-action='not_inviting'>取消邀請</span>";
							txt+="</span>";
						}
						
						if(relation.relation_status.now=='friend'){
							txt+="<span class='countmin_relation' data-uid='"+user_uid2+"'>";
							txt+="<span class='motion motion-btn' data-action='not_friend' data-confirm='1'>取消好友</span>";
							txt+="</span>";
						}
					}
					
					
					
					
					view_block.html(txt);
					
					if(model==1){
						view_block.find('.r-btn').on('click',function(){
							relation_build(this,model);
						});
						view_block.find('.comment-relation-btn').on('click',function(){
							relation_build(this,model);
						});
					}
					else if(model==2){
						view_block.find('.s-btn').on('click',function(){
							relation_build(this,model);
						});
					}
					else if(model==3){
						view_block.find('.motion-btn').on('click',function(){
							relation_build(this,model);
						});
					}

				}
			});
	}
}


var wallpost_join=function(wuid,tobj){
	$.post('/user/wallpost_join',{'wuid':wuid},function(mesg){
		mesg=$.parseJSON(mesg);
		if(mesg.error==1){
			alert(mesg.msg);
		}
		else{
			alert('已參加');
			top.location.reload();
		}
	});
}



$(document).ready(function(){
	$('body').on('click','.like-this,.dislike-this,.share-this,.favor-this,.share-fb-this,.cshare-this',function(){
		
				
		var login=$('body').attr('data-login');
		if(login!=true){top.location='/index.php';return 0;}
		
		var tobj=this;
		var tobj1=$(tobj).closest('.wbox');
		var wuid=tobj1.attr("data-wuid");
		var exe="";
		
				 if($(tobj).hasClass('like-this')){exe="like";}
		else if($(tobj).hasClass('dislike-this')){exe="dislike";}
		else if($(tobj).hasClass('comment-this ')){exe="comment";}
		else if($(tobj).hasClass('share-this') || $(tobj).hasClass('cshare-this')){exe="share";}
		else if($(tobj).hasClass('favor-this')){exe="favor";}
		else if($(tobj).hasClass('share-fb-this')){exe="share_fb";}
		

		//+1,-1
				 if(exe=="like" || exe=="dislike" ){wallposts_like(tobj,exe,wuid);}
		else if(exe=="comment"){}
		else if(exe=="share"){wallposts_share(tobj,wuid);}
		else if(exe=="favor"){wallposts_favor(tobj,wuid);}
		else if(exe=="share_fb"){top.location='http://www.countmin.com/user/wallpost_comment?w='+wuid+'&way=fbshare';}
			
	});
	
	$(".wallposts_after").on('click',function(){
		var tobj=this;
		var tobj1=$(tobj).closest('.wallposts');
		var timeset=$(tobj1).find('.wbox:first').attr('data-wtime');
		wallposts_after(tobj,timeset);
	});
	$(".wallposts_before").on('click',function(){
		var tobj=this;
		var tobj1=$(tobj).closest('.wallposts');
		var timeset=$(tobj1).find('.wbox:last').attr('data-wtime');
		wallposts_before(tobj,timeset);
	});
	$(".my_wallposts_after").on('click',function(){
		var tobj=this;
		var tobj1=$(tobj).closest('.wallposts');
		var timeset=$(tobj1).find('.wbox:first').attr('data-wtime');
		var uid=$('.wallpost:first').attr('data-uid');
		my_wallposts_after(tobj,uid,timeset);
	});
	$(".my_wallposts_before").on('click',function(){
		var tobj=this;
		var tobj1=$(tobj).closest('.wallposts');
		var timeset=$(tobj1).find('.wbox:last').attr('data-wtime');
		var uid=$('.wallpost:first').attr('data-uid');
		my_wallposts_before(tobj,uid,timeset);
	});
	
	
	$("body .countmin_relation .s-btn").on('click',function(){
		var login=$('body').attr('data-login');
		if(login!=true){top.location='/index.php';}
		else{
			relation_build(this,2);
		}
	});
	
	$("body .countmin_relation .r-btn").on('click',function(){
		var login=$('body').attr('data-login');
		if(login!=true){top.location='/index.php';}
		else{
			relation_build(this,1);
		}
	});
	$("body .countmin_relation .motion-btn").on('click',function(){
		
		var login=$('body').attr('data-login');
		if(login!=true){top.location='/index.php';}
		else{
			relation_build(this,3);
		}
	});
	
	
	
	
	
	//參加任何mission,event
	$('body').on('click','.wjoin', function(){
		var wjoin_box=$(this).closest('.wjoin_box');
		var wuid=wjoin_box.attr('data-wuid');
		wallpost_join(wuid,wjoin_box);
		
		
	});
	
	
	/*
	// 顯示邀請名單
	$('body').on('click','.invite', function(){
		var tobj=$(this);
		$.post('/user/socially?status=friend',{'format':'json'},function(mesg){
			mesg=$.parseJSON(mesg);
			tobj.parent().find(".nostyle").html('');
			if(mesg.data.args.num>0){
				tobj.parent().find(".invite-submit").show();
				$(mesg.data.d).each(function(inx,elem){
					tobj.parent().find(".nostyle").append("<li><div class='invite-group'><input type='checkbox' id='invite_friend[]' name='invite_friend[]' value='"+elem.userUid+"' /><span class='checkbox'></span>"+elem.userRealname+"</div></li>");
				});
			}
			else{
				tobj.parent().find(".nostyle").append("<li>沒有朋友，快去加個朋友吧!?</li>");
			}
			tobj.siblings(".invite-nav").toggle();
		});
	});
	
	$('body').on('click','.invite-submit', function(){
		var tobj=$(this).closest('.wjoin_box');
		var wuid=tobj.attr('data-wuid');
		var val=new Array();
		tobj.find('input:checked').each(function(inx,elem){
			val[inx]=$(elem).val()
		});
		$.post('/user/wallpost_invite',{'wuid':wuid,'invite_user':val},function(mesg){
			
			mesg=$.parseJSON(mesg);
			if(mesg.error==1){
				alert(mesg.msg);
			}
			else{
				alert('已送出邀請');
				top.location.reload();
			}
		});
	});
	*/
	$('body').on('click','.wfinish', function(){
		var tobj=$(this).closest('.wjoin_box');
		var wuid=tobj.attr('data-wuid');
		$.get('/user/wallpost_over',{'wuid':wuid},function(mesg){
			mesg=$.parseJSON(mesg);
			if(mesg.error==1){
				alert(mesg.msg);
			}
			else{
				alert(mesg.msg);
				top.location.reload();
			}
		});
	});
	
	$('body').on('click','.wlist', function(){
		var tobj=$(this).closest('.wjoin_box');
		var wuid=tobj.attr('data-wuid');
		var ttype=$(this).attr('data-type');
		
		$.post('/user/wallpost_join_list',{'wuid':wuid,'type':ttype},function(mesg){
			mesg=$.parseJSON(mesg);
			if(mesg.error==1){
				alert(mesg.msg);
			}
			else{
				var list='';
				$(mesg.data.d).each(function(inx,elem){
					list+=elem.xuser.basic.userRealname+'\n';
				});
				alert(list);
			}
		});
	});
	
	
	
	
	
	$('body').on('click','.invite-group input+span.checkbox', function(){
		$this = $(this);
		if( $this.prev("input").attr("checked") ){
			$this.prev("input").attr("checked", false);
		}
		else{
			$this.prev("input").attr("checked", true);
		}
	});
	
	
  var clipboard = new Clipboard('.copy-link');
	clipboard.on('success', function(e,c,o) {
		var obj=e.trigger;
		$(obj).parent().parent().parent().hide();
	});

	clipboard.on('error', function(e) {
		//console.log(e);
	});
	
	
	
	

});
