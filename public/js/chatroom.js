$("#page-header .mailbox").addClass("selected");
var user_profile_image_path=function(user_uid,size){
	if(user_uid.length<10){
		return '/css/img/default_profile_pic_'+size+'.png';
	}
	var dir1=user_uid.substr(0,3);
	var dir2=user_uid.substr(user_uid.length-3,3);
	return ['/images/',dir1,'/',dir2,'/',user_uid,'_',size,'.png'].join('');
}
var user_mywall_path=function(user_uid){
	if(user_uid.length<10){
		return '/user/mywall/';
	}
	return ['/user/mywall/',user_uid].join('');
}

$(document).ready(function(){
	var chatroom_uid=$('#chatroom-uid').val();
	var xuser_uid=$('#xuser-uid').val();
	var myuid=$('body').data('myuid');
	alert(chatroom_uid);
	var adjust_chat_height=function(){
		var window_height = $(window).height();
		var mailbox_private_page_height = (( ( window_height - 50 ) > 472 ) ? ( window_height - 50 ) : 472 );
		var private_list_height = mailbox_private_page_height - 93;
		var list_content_body_height = mailbox_private_page_height - 297 -18 -18;
		$(".mailbox-private-page").css("height", mailbox_private_page_height +"px");
		$("#private-list").css("height", private_list_height +"px");
		$("#list_content .body").css("height", list_content_body_height);
	}
	adjust_chat_height();

	$(window).resize(function() {
		adjust_chat_height();
	});


	// 聊天視窗
	$("#chat-content").focus(function(){
		if($(this).html().match('輸入訊息...')){
			$(this).html('').removeClass('typing');
		}
	});

	$("#chat-content").blur(function(){
		if($(this).html()=='' || $(this).html().match(/^[ \r\n]+$/)){
			$(this).html('輸入訊息...').addClass('typing');
		}
	});

	$('#message-content-submit').click(function(){
		var data={
			user:$('body').data('myuid'),
			chatroom:$('#chatroom-uid').val(),
			xuser:$('#xuser-uid').val(),
			content:$('#chat-content').html(),
			lasttime:$('#lasttime').val()
		}
		$('#chat-content').html('');
		$.post('/mailbox/message_add',data,function(mesg){
			mesg=parse_json(mesg);
			if(mesg.chatroom_uid){
				$('#chatroom-uid').val(mesg.chatroom_uid);
				data.chatroom=mesg.chatroom_uid;
			}
			message_getter(data.chatroom,$('#fetch-lasttime').val(),0);
		});
		return false;
	});

	/**
	 * 取得聊天室列表
	 * 
	 */
	var chatrooms_getter=function(){
		var	tpl=['<div><a class="focusbar" href="">',
					'<div class="user-list">',
						'<img src="" />',
						'<div class="list-info">',
							'<div class="user-name"></div>',
							'<span class="last-call reltime" data-reltime=""></span>',
						'</div>',
					'</div>',
				'</a></div>'].join('');
		$.post('/mailbox/chatroom_getter/',{},function(data){
			var i;
			var $t=$(tpl);
			data=parse_json(data);
			if(data.args.num){
				for(i=0;i<data.args.num;i++){
					$('#private-list ul li#chatroom-'+data.d[i].uid).remove();
					$t.find('.focusbar').attr('href','/mailbox/one/'+data.d[i].uid);
					$t.find('.user-list > img').attr('src',user_profile_image_path(data.d[i].user_lastupdate,'s'));
					$t.find('.user-name').html(data.d[i].name);
					$t.find('.last-call').data('reltime',data.d[i].time_lastupdate).html(data.d[i].time_lastupdate);
					$('#private-list ul').prepend('<li id="chatroom-'+data.d[i].uid+'">'+$t.html()+'</li>');
					if(!chatroom_uid.length && !xuser_uid.length){
						$('#chatroom-'+data.d[i].uid).addClass('selected');
						$('#chatroom-uid').val(data.d[i].uid);
						if(data.d[i].name.length){
							$('#chatroom-title').html(data.d[i].name);
						}
						else{
							var title=[];
							for(var j=0;j<data.d[i].userListing.length;j++){
								if(data.d[i].userListing[j].userUid==myuid){
									continue;
								}
								else{
									title.push(data.d[i].userListing[j].userRealname);
								}
							}
							$('#chatroom-title').html(title.join(','));
						}
						chatroom_uid=data.d[i].uid;
					}
				}
			}
		});
	}
	chatrooms_getter();

	/**
	 * 取得聊天訊息
	 *
	 * @param uuid chatroom_uid 聊天室UUID
	 * @param timestamp lasttime 和timebefore搭配，timebefore=0表示取較新的，ASC排序；timebefore=1表示取得較舊的，DESC排序
	 *  
	 */
	var message_getter=function(chatroom_uid,lasttime,timebefore){
		var tpl=['<div><div class="chat-win" id="">',
					'<div class="chat-info">',
						'<img src="" />',
						'<div class="list-info">',
							'<span class="user-name"></span>',
							'<span class="last-call"></span>',
						'</div>',
					'</div>',
					'<div class="clearfix"></div>',
					'<div class="chat-content"></div>',
				'</div></div>'].join('');
		var $t=$(tpl);
		var $message_list=$('#list_content .body');
		var data={
			user:$('body').data('myuid'),
			chatroom:chatroom_uid,
			timeup:lasttime,
			timebefore:timebefore
		};
		if(data.chatroom.length){
			$.post('/mailbox/message_getter',data,function(mesg){
				var i;
				$('#fetch-lasttime').val(lasttime);
				mesg=parse_json(mesg);
				if(mesg.args.num){
					for(i=0;i<mesg.args.num;i++){
						if($message_list.find('#message-id-'+mesg.d[i]['chatmessageUid']).size()<1){
							$t.find('.chat-win').attr('id','message-id-'+mesg.d[i].chatmessageUid).addClass(data.user==mesg.d[i].userUid?'self':'others');
							$t.find('.chat-info > img').attr('src',user_profile_image_path(mesg.d[i].userUid,'s'));
							$t.find('.user-name').html(mesg.d[i].userRealname);
							$t.find('.last-call').html(mesg.d[i].chatmessageTime);
							$t.find('.chat-content').html(mesg.d[i].chatmessage);
							$message_list.append($t.html());
						}
						console.log(mesg.d[i].chatmessageTime);
					}
				}
			});
		}
	}
	message_getter(chatroom_uid, $('#fetch-lasttime').val(), 0);

	var chat_timer=function(){
		var timeup=$('#fetch-lasttime').val();
		console.log(chatroom_uid);
		if(chatroom_uid.length){
			message_getter(chatroom_uid, timeup, 0);
		}
		setTimeout(chat_timer,5000);
	}
	setTimeout(chat_timer,5000);

	$('#chat-create').click(function(){
		$('#list_content').hide().addClass('hidden');
		$('#mailbox-new-chat').show().removeClass('hidden');
	});

	$('#new-chat-close').click(function(){
		$('#list_content').show().removeClass('hidden');
		$('#mailbox-new-chat').hide().addClass('hidden');
	});

	$.fn.choosepicker= function(){
			var $cp=$(this);
			$cp.find('.item-list .item').each(function(){
				var $item=$(this);
				var id=$item.data('id');
			});
			$cp.on('click','.option-list .item',function(){
				var $item=$(this);
				if(!$item.hasClass('choosepicker-selected')){
					$cp.find('.item-list').append('<div class="item" data-id="'+$(this).data('id')+'"><i class="fa fa-close" style="cursor: pointer; color: #f00;"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="item[]" value="'+$(this).data('id')+'" />'+$(this).find('.cp-name').get(0).innerHTML+'</div>');
					$cp.find('.filter-input').val('');
					$cp.find('.option-list .item').removeClass('hidden');
					$(this).addClass('choosepicker-selected');
				}
			});
			$cp.on('click','.item-list .item',function(){
				var id=$(this).data('id');
				$(this).remove();
				$cp.find('.filter-input').val('');
				$cp.find('.option-list .item').removeClass('hidden');
				$cp.find('.option-list .item[data-id="'+id+'"]').removeClass('choosepicker-selected');
			});
			$cp.on('keyup','.filter-input',function(){
				cp_filter($(this).val());
			});
			$cp.on('click','.filter-button',function(){
				cp_filter($cp.find('.filter-input').val());
			});
			cp_filter=function(keyword){
				if(keyword.length>0){
					$cp.find('.option-list .item .cp-filter-keyword').each(function(){
						if(this.innerHTML.indexOf(keyword)>-1){
							$(this).closest('.item').removeClass('hidden');
						}
						else{
							$(this).closest('.item').addClass('hidden');
						}
					});	
				}
			}
	};
	$('#choose-picker').choosepicker();
	$('#chatroom-create').click(function(){
		var data={};
		data['name']=$('#chatroom-create-name').val();
		data['users']=[];
		$('#choose-picker .item-list input').each(function(){
			data['users'].push(this.value);
		});
		if(data['users'].length>0){
			$.post('/mailbox/chatroom_add',data,function(mesg){




			});
		}
	});
});

