/**
<div class="wallpost" data-uid="<?php value($wallposts['d'][$i]['wallpostUid']); ?>">
	<div class="wallpost-user">
		<div class="profile-image-s">
			<img src="/user_profiles/profile_<?php echo $wallposts['d'][$i]['userUid']; ?>_s.png" />
		</div>
		<div class="user-name"><a href="/user/mywalls/<?php echo $wallposts['d'][$i]['userUid']; ?>"><?php HTML($wallposts['d'][$i]['userRealname']); ?></a></div>
		<div class="post-time"><?php echo $wallposts['d'][$i]['wallpostTimeCreate']; ?></div>
		<div class="more-arrow"></div>
	</div>
	<div class="wallpost-content">
		<img src="/wallpost_images/post_<?php echo $wallposts['d'][$i]['wallpostUid']; ?>.png" />
		<div class="text">
			<div class="topic"><?php echo $wallposts['d'][$i]['wallpostTopic']; ?></div>
			<div class="content"><?php echo $wallposts['d'][$i]['wallpostBrief']; ?></div>
		</div>
	</div>
	<div class="social">
		<div class="func-counter row-fluid">
			<div class="co12-2 co12-xs-3 like-counter">喜歡 <span class="num"><?php numeric($wallposts['d'][$i]['wallpostNumLikes']); ?></span></div>
			<div class="co12-3 co12-xs-3 dislike-counter">不喜歡 <span class="num"><?php numeric($wallposts['d'][$i]['wallpostNumDislikes']); ?></span></div>
			<div class="co12-3 hidden-xs">&nbsp;</div>
			<div class="co12-2 co12-xs-3 comment-counter">留言 <span class="num"><?php numeric($wallposts['d'][$i]['wallpostNumComments']); ?></span></div>
			<div class="co12-2 co12-xs-3 share-counter">分享 <span class="num"><?php numeric($wallposts['d'][$i]['wallpostNumShares']); ?></span></div>
		</div>
		<div class="func-buttons">
			<div class="co12-2 co12-xs-3"><span class="like-this"><span class="txt">+1</span></span></div>
			<div class="co12-2 co12-xs-3"><span class="dislike-this"><span class="txt">-1</span></span></div>
			<div class="co12-4 hidden-xs">&nbsp;</div>
			<div class="co12-2 co12-xs-3"><span class="comment-this"><span class="txt">留言</span></span></div>
			<div class="co12-2 co12-xs-3"><span class="share-this"><span class="txt">分享</span></span></div>
		</div>
	</div>
</div>
**/
var templates={
	wallpost:'<div class="wallpost"><div class="wallpost-user"><div class="profile-image-s"><img src="" /></div><div class="user-name"><a href=""></a></div><div class="post-time"></div><div class="more-arrow"></div></div><div class="wallpost-content"><img src="" class="hidden" /><div class="text"><div class="topic"></div><div class="content"></div></div></div><div class="social"><div class="func-counter row-fluid"><div class="co12-2 co12-xs-3 like-counter">喜歡 <span class="num"></span></div><div class="co12-3 co12-xs-3 dislike-counter">不喜歡 <span class="num"></span></div><div class="co12-3 hidden-xs">&nbsp;</div><div class="co12-2 co12-xs-3 comment-counter">留言 <span class="num"></span></div><div class="co12-2 co12-xs-3 share-counter">分享 <span class="num"></span></div></div><div class="func-buttons"><div class="co12-2 co12-xs-3"><span class="like-this"><span class="txt">+1</span></span></div><div class="co12-2 co12-xs-3"><span class="dislike-this"><span class="txt">-1</span></span></div><div class="co12-4 hidden-xs">&nbsp;</div><div class="co12-2 co12-xs-3"><span class="comment-this"><span class="txt">留言</span></span></div><div class="co12-2 co12-xs-3"><span class="more"><span class="txt">more</span></span></div></div></div></div>'
};
var timezone=0;
var jwords={
	time_just_now:'剛剛',
	time_minute_ago:'[#min] 分鐘前',
	time_hour_ago:'[#hour] 小時前',
	time_yesterday:'昨天',
	time_day_ago:'[#day] 天前',
	time_tomorrow:'明天',
	time_day_later:'[#day] 天後',
	time_hour_later:'[#hour] 小時後',
	time_minute_later:'[#min] 分鐘後',
	time_soon:'即將到期'
};

var cookie_get=function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

var padLeft=function (tstr1,tlength){
   
    var tstr1=""+tstr1;
    while(tstr1.length < tlength)
    {
    	tstr1="0"+tstr1;
    }
    return tstr1;
}
/**
 * tpl2dom
 * 將樣板根據 o 轉成 html string
 * o 的結構為 [{s:'jquery選擇器',value:'要帶入的值'},{s:'jquery選擇器',src:'要帶入的圖片路徑}',....]
 * ex:
 *		$('.wallposts').prepend(
 *			tpl2dom('wallpost',[
 *				{s:'.wallpost-user img',src:data['usr-profile']},
 *				{s:'.user-name',html:data['user-name']},
 *				....
 *			])
 *		);
 * ajax return json format: 
 *		array(
 *			'user-profile'=>'.....',
 *			'user-name'=>'王大頭'
 *		);
 *
 * @param string tpl
 * @param array o 
 *
 * @return string
 */



var tpl2dom=function(tpl,o){
	var $tpl;
	var i;
	switch(tpl){
		case 'wallpost':
			tpl=templates[tpl];
			break;
	}
    $tpl=$(tpl);
	for(i=0;i<o.length;i++){
		$tpl.find(o[i]['s']).each(function(){
			switch(this.tagName.toLowerCase()){
				case 'input':
				case 'textarea':
					this.value=o[i]['value'];
					break;
				case 'img':
					this.src=o[i]['src'];
					$(this).removeClass('hidden');
					break;
				case 'a':
					if(o[selector]['href']){
						this.href=o[i]['href'];
					}
					if(o[selector]['html']){
						this.innerHTML=o[i]['html'];
					}
					break;
				default:
					this.innerHTML=o[i]['html'];
			}
		});
	}
	return $tpl.html();
}



/**
 * html
 * escape html special char
 *
 * @param string str
 *
 * @return string 
 */
var html=function(str){
	return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
}

var parse_json=function(str){
	str=str.trim();
	if(str.substr(0,12)=='while(1){/* ' && str.substr(str.length-5,5)==' */};'){
		return $.parseJSON(str.substr(12,str.length-17));
	}
	return {};
}

/**
 * 將 utc 時間轉換成 locale 時間，並回傳 Date 
 */
var time_shift_timezone=function(utc_time){
	if(utc_time.match(/^\d{4}([\-\.\/])\d{1,2}\1\d{1,2}[\sTt]\d{1,2}(:\d{1,2}(:\d{1,2})?)?$/)){
		utc_time=utc_time.replace(/[\-\.]/g,'/').replace(/[Tt]/,' ');
	}
	else if(utc_time.match(/^(19|20)\d{12}$/)){
		utc_time=[utc_time.substr(0,4),utc_time.substr(4,2),utc_time.substr(6,2)].join('/')+' '+[utc_time.substr(8,2),utc_time.substr(10,2),utc_time.substr(12,2)].join(':');
	}
	var d=new Date(utc_time);
	d.setTime(d.getTime()+timezone);
	return d;
}

// 分享至興趣牆關閉
var closeBlockUI = function(){
	document.body.style.overflow = 'auto';
	$.unblockUI();
}

// 展開 "分享至興趣牆"的公開設定
var sw_share = function(myself){
	$this = $(myself);
	$this.siblings(".nostyle").show();
}

// "分享至興趣牆"的公開設定
var set_share = function(myself, val){
	$this = $(myself);
	var myHtml    = $this.html();
	var $shareBtn = $this.parent("li").parent(".nostyle").siblings(".share-btn");
	$shareBtn.html( myHtml +' <i class="fa fa-caret-down"></i>').val( val );
	$this.closest(".wbox").data("public",val);
	$this.parent("li").parent(".nostyle").hide();
}

// "分享至興趣牆"的分享
var wallposts_share=function(myself){
	var $this = $(myself);
	var $wbox=$this.closest(".wbox");
	
	var wuid=$wbox.data("wuid");
	var msg=$wbox.find("#share-wall-msg").val();
	var public=$wbox.data("public");

	$.post('/user/wallpost_share',{wuid:wuid,public:public,msg:msg},function(mesg){
			mesg=$.parseJSON(mesg);
			if(mesg.error==0){
				top.location.reload();
			}
	});
}

// 邀請（全選）
var invite_all_func=function(myself){
	
	var invite_box=$('.invite_box');
	
	if($('#invite-all').prop('checked')==true){
		$(myself).removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
		$('#invite-all').prop('checked',false);
		//////
		invite_box.find('.invite_i').each(function(inx,elem){
			var tobji=$(elem).find('i');
			var tinput=$(elem).find('input');
			if(tinput.prop('checked')==true){
				tobji.removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
				tinput.prop('checked',false);
			}
			else{
				//tobji.removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
				//tinput.prop('checked',true);
			}
		});

	}
	else{
		$(myself).removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
		$('#invite-all').prop('checked',true);
		//////
		invite_box.find('.invite_i').each(function(inx,elem){
			var tobji=$(elem).find('i');
			var tinput=$(elem).find('input');
			if(tinput.prop('checked')==true){
				//tobji.removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
				//tinput.prop('checked',false);
			}
			else{
				tobji.removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
				tinput.prop('checked',true);
			}
		});
	}
}

// 訊息邀請（全選）
var chat_all_func=function(myself){

	var chat_box = $('.chat_box');

	if( $('#chat-all').prop('checked') == true ){
		$(myself).removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
		$('#chat-all').prop('checked',false);

		chat_box.find('.chat_i').each(function(i,elem){
			var ci     = $(elem).find('i');
			var cinput = $(elem).find('input');
			if( cinput.prop('checked') == true ){
				ci.removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
				cinput.prop('checked',false);
			}
		});
	}
	else{
		$(myself).removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
		$('#chat-all').prop('checked',true);

		chat_box.find('.chat_i').each(function(i,elem){
			var ci     = $(elem).find('i');
			var cinput = $(elem).find('input');
			if( !cinput.prop('checked') ){
				ci.removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
				cinput.prop('checked',true);
			}
		});
	}

//	var invite_box=$('.invite_box');
	
	/*
	if($('#invite-all').prop('checked')==true){
		$(myself).removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
		$('#invite-all').prop('checked',false);
		//////
		invite_box.find('.invite_i').each(function(inx,elem){
			var tobji=$(elem).find('i');
			var tinput=$(elem).find('input');
			if(tinput.prop('checked')==true){
				tobji.removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
				tinput.prop('checked',false);
			}
			else{
				//tobji.removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
				//tinput.prop('checked',true);
			}
		});

	}
	else{
		$(myself).removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
		$('#invite-all').prop('checked',true);
		//////
		invite_box.find('.invite_i').each(function(inx,elem){
			var tobji=$(elem).find('i');
			var tinput=$(elem).find('input');
			if(tinput.prop('checked')==true){
				//tobji.removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
				//tinput.prop('checked',false);
			}
			else{
				tobji.removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
				tinput.prop('checked',true);
			}
		});
	}
	*/
}



$(document).ready(function(){
	var common={};

	is_req=function(req){
		if(req=='y' || req=='Y' || req=='1' || req=='true' || req=='req'){
			return true;
		}
		return false;
	}
	common.radio_confirm=function($form){
		$form.find('.radio-confirm:radio').each(function(){
			var $radio_confirm=$(this);
			var radio_req=$radio_confirm.attr('req');
			if(is_req(radio_req)){
				var radio_name=$radio_confirm.attr('name');
				alert(radio_name);
				var radio_checked=false;
				$form.find(':radio[name="'+radio_name+'"]').filter(':checked').each(function(){
					alert('pp');
				});
				if($form.find(':radio[name="'+radio_name+'"]').filter(':checked').size()<1){
					$radio_confirm.closest('label').addClass('err');
				}
			}
		});
	}

	common.checkbox_confirm=function($form){
		$form.find('.checkbox-confirm:checkbox').each(function(){
			var $checkbox_confirm=$(this);
			var checkbox_req=$checkbox_confirm.attr('req');
			if(is_req(checkbox_req)){
				var checkbox_name=$checkbox_confirm.data('name');
				alert(checkbox_name);
				var checkbox_checked=false;
				$form.find(':checkbox[data-name="'+checkbox_name+'"]').filter(':checked').each(function(){
					alert('xx');
				});
				if($form.find(':checkbox[data-name="'+checkbox_name+'"]').filter(':checked').size()<1){
					$checkbox_confirm.closest('label').addClass('err');
				}
			}
		});
	}

	common.el_text_confirm=function($el){
		var req=$el.attr('req');
		if(is_req(req)){
			var val=$el.val();
			var range=$el.data('range');
			if(val.length<1){
				$el.addClass('err');
			}
			if(range.match(/^(\d+)?~(\d+)?$/)){
				var r=range.split('~');
				r[0]=parseInt(r[0],10);
				r[1]=parseInt(r[1],10);
				if(r[0]>0 && val.length<r[0]){
					$el.addClass('err');
				}
				if(r[1]>0 && val.length>r[1]){
					$el.addClass('err');
				}
			}
		}
	}

	common.text_confirm=function($form){
		
	}

	$('body').on('click','.sw',function(event){
		var $this=$(this);
		var swfor=$this.attr('sw-for');
		var i;
		if($this.is('a')){
			event.preventDefault();
		}
		swfor=swfor.split(/\s*,\s*/);
		for(i=0;i<swfor.length;i++){
			$(swfor[i]).toggleClass('hidden');
		}
	});

	$('body').on('blur','input:text',function(){
		var $this=$(this);
		var req=$this.attr('req');
		if(is_req(req)){
			if($this.val().length<1){
				$this.addClass('err');
			}
		}
	});
	

	$('body').on('submit','form.form-validate',function(){
		alert('form validate');
		var $this=$(this);
		var num_errors=0;
		common.radio_confirm($this);
		common.checkbox_confirm($this);
		$this.find('[name]').filter('.err').each(function(){
			num_errors++;
		});
		if(num_errors>0){
			$this.find('.err:eq(0)').focus();
			return false;
		}
		return false;
	});

	/**
	 * 按下+1 
	 */
	/*
	$('body').on('click','.like-this:not(.did)',function(){
			var wallpost_uid = $(this).parent("div").parent("div").siblings(".wuid").val();
		alert(wallpost_uid);

		$.post('/user/wallpost_like',{exe:"like",wuid:wallpost_uid},function(mesg){
			console.log(mesg);
			mesg=$.parseJSON(mesg);
			if(mesg.error==0){
				$this.addClass('did');
			}
		});
	});
*/
	/**
	 * 按下-1
	 */
	 /*
	$('body').on('click','.dislike-this:not(.did)',function(){
		var $this=$(this);
		var $wallpost=$this.closest('.wallpost');
		var wallpost_uid=$wallpost.data('uid');
		alert( wallpost_uid);
		$.post('/user/wallpost_like',{exe:"dislike",wuid:wallpost_uid},function(mesg){
			console.log(mesg);
		});
	});
*/
	/**
	 * 按下+好友->好友
	 */
	$('body').on('click','.motion:has(i.add)',function(){
		var $this=$(this);
		$this.children("i.add").remove();
		$this.html("好友");
		$this.css("cursor", "default");
	});

	/**
	 * 按下+追蹤->追衝
	 */
	$('body').on('click','.motion:has(i.trace)',function(){
		var $this=$(this);
		$this.children("i.trace").remove();
		$this.html("追蹤");
		$this.css("cursor", "default");
	});
	
	
	/**
	 * 選擇邀請對象
	 */
	$('body').on('click','.invite_i',function(){
		var tobji=$(this).find('i');
		var tinput=$(this).find('input');
		
		if(tinput.prop('checked')==true){
			tobji.removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
			tinput.prop('checked',false);
			
			$('#invite-all-i').removeClass("fa-check-square").addClass("fa-square-o").css("color", "#b2b2b2");
			$('#invite-all').prop('checked',false);
		}
		else{
			tobji.removeClass("fa-square-o").addClass("fa-check-square").css("color", "#ffcc35");
			tinput.prop('checked',true);
		}
	});

	/**
	 * 邀請搜索
	 */
	$('body').on('keyup','#invite-search', function(e){
		$('.invite-info .name').each(function(inx,elem){
			var patt1=new RegExp($('#invite-search').val(),'i');
			if(patt1.exec($(elem).html())){
				$(elem).closest('li').show();
			}
			else{
				$(elem).closest('li').hide();
			}
		});
	});
	
	
	/**
	 * 邀請送出
	 */
	$('body').on('click','.invite-submit', function(){
		var tobj=$('.invite_box');
		var wuid=tobj.attr('data-wuid');
		
	
		var val=new Array();
		tobj.find('.invite_i input:checked').each(function(inx,elem){
			val[inx]=$(elem).val()
		});
		
		$.post('/user/wallpost_invite',{'wuid':wuid,'invite_user':val},function(mesg){
			
			mesg=$.parseJSON(mesg);
			if(mesg.error==1){
				alert(mesg.msg);
			}
			else{
				alert('已送出邀請');
				top.location.reload();
			}
		});
	});


	/**
	 * 按下comment
	 */
	$('body').on('click','.comment-this,.comment-this2',function(){
	 	var tobj1 = $(this).closest('.wbox');
	  	var wuid  = tobj1.attr('data-wuid');
	  	//$.get('/user/wallpost_comment?w='+wuid+'&ui=1','',function(txt){
	   		//comment_content = txt;
	   		
	   		var hh=$(window).height()*80/100;
	   		
	   		comment_content = '<iframe src="/user/wallpost_comment?w='+wuid+'&ui=1" width="100%" height="'+hh+'"> </iframe>';
	   		$.blockUI({ 
	   			message: '<div class="scroll-wrapper">'+ comment_content +"</div>",
	   			onBlock: function(){
	   				document.body.style.overflow = 'hidden';
	   				$('.scroll-wrapper').jScrollPane({
	   					showArrows: true
	   				});
	   			}
	   		 });
	   		
	   		$('body').one('click', '.blockOverlay:not(.blockPage)', function(){ $.unblockUI({
	   				onUnblock: function(){
	   					document.body.style.overflow = 'auto';
	   				}
	   			}); 
	   		});
	   		
	   		
	  	//});
	});

	$('body').on('click','.comment-this-submit',function(){

	});

	$('body').on('click','.comment-this-cancel',function(){

	});

	$('body').on('click','.share-this',function(){

	});

	$('body').on('click','.share-wall-this',function(type){
		var $this = $(this);
		var thisWallUid= $this.closest(".wbox").data("wuid");
		
		$.post("/user/wallpost_comment?w="+thisWallUid+"&ui=1",{w:thisWallUid,json:1},function(mesg){
		
			mesg=$.parseJSON(mesg);
			if((mesg).error!=1){
				var wallpost=(mesg).data;
				var thisWallType = wallpost.wallpost_view.wallpostType;
				var thisShareTxt = ( thisWallType < 2 ) ? "動態":"活動";
				var thisImgSrc = wallpost.wallpost_view.imageUrl;
				var thisName = wallpost.wallpost_view.xuser.basic.userRealname;
				var thisTitle = wallpost.wallpost_view.wallpostTopic;
				var thisContent = wallpost.wallpost_view.wallpostBrief;
				
				var share_content = '<div class="head">分享這則'+ thisShareTxt +'<span class="pull-right" onclick="closeBlockUI();">&times;</span></div>'+
							'<div class="body" >'+
								'<textarea id="share-wall-msg" name="share-wall-msg" class="share-wall-msg" placeholder="留個話吧...." rows="5"></textarea>'+
								'<img src="'+ thisImgSrc +'">'+
								'<h4 class="share-wall-who">'+ thisName +'</h4>'+
								'<p class="share-wall-title">'+ thisTitle +'</p>'+
								'<div class="share-wall-content">'+ thisContent.trim() +'</div>'+
							'</div>'+
							'<div class="foot">'+
								'<div class="action-buttons pull-right">'+
									'<button class="share-btn" type="button" onclick="sw_share(this);" value="0"><i class="fa fa-globe"></i> <span class="txt">公開</span> <i class="fa fa-caret-down"></i></button>'+
									'<button type="button" onclick="closeBlockUI();">取消</button>'+
									'<button type="button" onclick="wallposts_share(this);">分享</button>'+
									'<ul class="nostyle" style="display:none;">'+
										'<li><a href="javascript:void(0);" onclick="set_share(this,0);"><i class="fa fa-globe"></i> <span class="txt">公開</span></a></li>'+
										'<li><a href="javascript:void(0);" onclick="set_share(this,1);"><i class="fa fa-group"></i> <span class="txt">僅限好友</span></a></li>'+
									'</ul>'+
								'</div>'+
							'</div>';

				$.blockUI({
					message: '<div class="scroll-wrapper share-wall-block"><div class="scroll-wall-share-wrapper"><div class="share-wrapper wbox" data-wuid="'+thisWallUid+'" data-public="0" >'+ share_content +'</div></div></div>',
					onBlock: function(){
						document.body.style.overflow = 'hidden';
							$('.scroll-wrapper').jScrollPane({
							showArrows: true
						});
					}
				});
				
			}
		
		});
	});

	// 0121 邀請 winvite
	$('body').on('click','.winvite',function(){
		var wjoin_box=$(this).closest('.wjoin_box');
		var wuid=wjoin_box.attr('data-wuid');

		$.post('/user/socially?status=friend',{'format':'json'},function(mesg){
			mesg=$.parseJSON(mesg);
		
			var friend_txt='';
			var foot_css='';
			if(mesg.data.args.num>0){
				for(var i=0;i<mesg.data.args.num;i++){
				var tuser=mesg.data.d[i];
				var invited_txt='';
				if(tuser.inviting==true){invited_txt='已邀請';}
				friend_txt+='<li><div class="img-cover">'+
									'<img src="'+tuser.xuser.userImgUrl+'" />'+	// 朋友頭像
								'</div>'+
								'<div class="invite-info">'+
									'<div class="name">'+tuser.userRealname+
									'</div>'+
									'<div class="level">'+tuser.xuser.userTitle+
									'</div>'+
								'</div>'+
								'<div class="pull-right invite_i">'+
									'<span class="invited">'+invited_txt+'</span>'+
									'<input type="checkbox" id="invite_friend[]" name="invite_friend[]" value="'+tuser.userUid+'" /><i class="fa fa-square-o"></i>'+
								'</div>'+
							'</li>';
				}
			}
			else{
				friend_txt+='沒有朋友，快去加個朋友吧~~';
				foot_css='display:none';
			}
			
			var invite_content = '<div class="head ">邀請朋友 '+
								'<div class="invite-search"><input type="text" placeholder="搜尋好友" id="invite-search" /></div>'+
							 	'<span class="pull-right" onclick="closeBlockUI();">&times;</span>'+
							 '</div>'+
							 '<div class="body">'+
							 	'<div class="title">朋友列表 '+mesg.data.args.num+
							 	'<div class="pull-right"><span class="all-txt">全選</span> <input id="invite-all" type="checkbox" /><i id="invite-all-i" class="fa fa-square-o" onclick="invite_all_func(this);"></i></div></div>'+
								'<div class="share-wall-content">'+
									'<ul class="nostyle invite_box wjoin_box" data-wuid="'+wuid+'">'+friend_txt+
									'</ul>'+
								'</div>'+
							 '</div>'+
							 '<div class="foot" style="'+foot_css+'">'+
								'<div class="action-buttons pull-right">'+
									'<button type="button" onclick="closeBlockUI();">取消</button>'+
									'<button type="button" class="invite-submit">邀請</button>'+
								'</div>'+
							 '</div>';
			$.blockUI({
				message: '<div class="scroll-wrapper share-wall-block"><div class="scroll-wall-share-wrapper"><div class="share-wrapper invite">'+ invite_content +'</div></div></div>',
				onBlock: function(){
					document.body.style.overflow = 'hidden';
						/*$('.scroll-wrapper').jScrollPane({
						showArrows: true
					});*/
				}
			});
		});
	});

	// 0127 參加名單
	$('body').on('click','.lottery-join',function(){
		var join_content =  '<div class="head">參加名單 <span class="pull-right" onclick="closeBlockUI();">&times;</span></div>'+
							'<div class="body join">'+
								'<div class="share-wall-content">'+
									'<ul class="nostyle">'+
										
										'<li>'+
											'<div class="img-cover">'+
												'<img src="/images/9ko/koo/9kok5vrz-leskcok4-0ko4swckoo_s.png" />'+	// 朋友頭像
											'</div>'+
											'<div class="join-name">'+
												'艾瑪汪汪'+ // 朋友名稱
											'</div>'+
										'</li>'+

										'<li>'+
											'<div class="img-cover">'+
												'<img src="/images/9ko/koo/9kok5vrz-leskcok4-0ko4swckoo_s.png" />'+	// 朋友頭像
											'</div>'+
											'<div class="join-name">'+
												'艾瑪汪汪'+ // 朋友名稱
											'</div>'+
										'</li>'+

									'</ul>'+
								'</div>'+
							'</div>';

		$.blockUI({
			message: '<div class="scroll-wrapper share-wall-block"><div class="scroll-wall-share-wrapper"><div class="share-wrapper invite">'+ join_content +'</div></div></div>',
			onBlock: function(){
				document.body.style.overflow = 'hidden';
				$('.scroll-wrapper').jScrollPane({
					showArrows: true
				});
			}
		});
	});

	// 關於（編輯）
	/*
	$('body').on('click','.btn-editable:not(.editable-executing)',function(){
		var $this=$(this);
		$this.addClass('editable-executing');
		$this.closest('.form-input').find('.editable').each(function(){
			var $el=$(this);
			var datatype=$el.data('type');
			var defval=$el.html();
			var el='';

		//	alert( datatype +"\n\n"+ defval );

		//	$el.html('');
			$el.hide();
			switch(datatype){
				default:
					$('<input type="text" value="" />').insertBefore($el).val(defval);
					$el.closest('.form-input').removeClass('form-text');
				//	$el.after('<div><button type="button"><span class="text">Cancel</span><button type="button"><span class="text"><span><span>Save</span></span></span></div>');
					$el.after('<div class="form-action"><button type="button" class="save">儲存變更</button> <button type="button" class="cancel">取消</button></div>');
					break;

			}
		});
	});
	*/

	/*
	$('body').on('click', '.form-about .form-action .save', function(){
		var $this=$(this);
		var val = $this.parent("div").siblings("input").val();
		$this.parent("div").siblings("span").html( val ).show();
		$this.parent("div").siblings("input").remove();

		$this.closest(".form-input").find(".editable-executing").removeClass("editable-executing");
		$this.closest(".form-input").addClass("form-text");
		$this.closest(".form-action").remove();

	});

	$('body').on('click', '.form-about .form-action .cancel', function(){
		var $this=$(this);
	//	var val = $this.parent("div").siblings("input").val();
		$this.parent("div").siblings("span").show();
		$this.parent("div").siblings("input").remove();

		$this.closest(".form-input").find(".editable-executing").removeClass("editable-executing");
		$this.closest(".form-input").addClass("form-text");
		$this.closest(".form-action").remove();
	});
	*/

	// 發表 - 第二步（ 興趣最多只能選３個 ）
	$('body').on('click','.modal-body .icon a', function(e){
		var icon_a = $(this);
		if( icon_a.parent("li").hasClass("selected") ){	icon_a.parent("li").removeClass("selected"); }
		else{
			var icon_selected_length = $(".modal-body .icon.selected").get().length;
			if( icon_selected_length > 2 ){
				alert("最多只能選３個");
			}
			else{
				icon_a.parent("li").addClass("selected");
			}
		}
		e.stopPropagation();
	});

	// 發表 - 公開設定
	$('body').on('change', '.modal-footer .dynamic .privacy', function(){
		if( $(this).val() > 0 ){	$(this).siblings("span").hide();	}
		else {						$(this).siblings("span").show();	}
	});


	$('.user-setting-web').click(function(){
		var $this=$(this);
		var of=$this.offset();
		var h=parseInt($this.height(),10);
		var w=parseInt($this.width(),10);
		var r=of.left;
		var t=of.top + h;
		var nav_w=parseInt($('#nav-more').width(),10);

		$('#nav-more').offset({top:[t+2,'px'].join(''), left:[r-nav_w,'px'].join('')});
		$('#nav-more').toggleClass('hidden');
	});

	$('.wallpost-write').click(function(){
		wallpost_write();
		$("#modal .modal-content").css({
			background: "transparent",
			border: 0
		});

		$("#modal .modal-close").hide();
		$('body').one('click', '.modal-mask:not(.modal-content)', function(){ $("#modal").remove(); });
		$('.btn-events button').one('click', function(){
			$("#modal .modal-content").css({
				background: "#fff",
				border: "1px solid #666"
			});
			$("#modal .modal-close").show();
		});
	});

	$('.leave-wallpost a').on('click', function(){

		if( !$(this).parent().attr("class").match("wallpost-plus-post") ){
			wallpost_write();
			$this = $(this);
			$("."+ $this.data('target')).click();
		}
	});

	// 通知 1206
	$("#notification").on("click", function(){
		$("#notification-msg").toggle("Blind");
	});
	// ./通知


	timezone=parseInt($('body').data('timezone'),10);
	if(isNaN(timezone)) timezone=0;

	wallpost_write=function(wid){
		modal();
		$('#modal').children('.modal-content').append('<div class="btn-events">'+
			'<button type="button" class="btn-new btn-wallpost-new" data-new-action="wallpost"><span class="fa fa-pencil"></span>發表動態</button>'+
			'<button type="button" class="btn-new btn-lottery-new" data-new-action="lottery"><span></span>抽獎活動</button>'+
			'<button type="button" class="btn-new btn-spread-new" data-new-action="spread"><span></span>揪團</button>'+
			'<button type="button" class="btn-new btn-pulling-new" data-new-action="pulling"><span></span>推廣活動</button>'+
		'</div>').parent().find('.modal-close').click(function(){
			var $this=$(this);
			if( confirm("您確定要關閉此視窗嗎？") ){
				// 清空 youtube
				var $Youtube_title = $(".cke_dialog_title:contains('嵌入 Youtube 影片')");

				if( $Youtube_title.closest(".cke_reset_all").get().length && $Youtube_title.closest(".cke_reset_all").is(":visible") ){
					var $Youtube_close = $Youtube_title.next(".cke_dialog_close_button");
					$Youtube_close.children(".cke_label").click();
				}

				// 清空 Gmap
				var $Gmap_title = $(".cke_dialog_title:contains('Insert Google Map')");

				if( $Gmap_title.closest(".cke_reset_all").get().length && $Gmap_title.closest(".cke_reset_all").is(":visible") ){
					var $Gmap_close = $Gmap_title.next(".cke_dialog_close_button");
					$Gmap_close.children(".cke_label").click();
				}

				$this.parent().parent().remove();
			}
		}).parent().find('.btn-new').click(function(){
			var	$this=$(this);
			$this.wallpost_editor($this.data('new-action'),wid);
		});
	}

	modal=function(){
		var h=$(document).height();
		if($('.modal').size()){
			$('.modal').remove();
		}
		$('body').append('<div id="modal" class="modal"><div class="modal-mask"></div><div class="modal-content"><div class="modal-close pull-right"><button type="button"><span class="fa fa-close"></span></button></div></div></div>');
		$('.modal').height(h*1.5);
	}

	/* 0127~ wallpost iframe */
	var if_mini_level = $("#user-level-name-mini").get();
	if( if_mini_level.length > 0 ){
		;
	}
});

$.fn.extend({
	wallpost_editor:function(action,wid){
		var $this=$(this);
		$this.closest('.modal').find('.modal-content').append('<div>'+
			'<div class="editor"></div>',
		'</div>');
		countmin_editor($this.closest('.modal').find('.editor'),action,wid);
		$this.closest('.btn-events').remove();

		if( action == "wallpost"){

		}
		if( action == "lottery" ){	// 抽獎
			$('input[name^=lottery-date], input[name=lottery-gotime]').datepicker({ dateFormat: "yy-mm-dd" });
			$('.lottery-time-text').timepicker({ timeFormat: 'H:i' });
			$('.lottery-time-text').val('00:00');
		}
		if( action == "spread" ){	//  揪團
			$('input[name^=spread-date]').datepicker({	dateFormat: "yy-mm-dd" });
			$('input[id^=spread-time-txt]').timepicker({ timeFormat: 'H:i' });
			$('input[id^=spread-time-txt]').val('00:00');
		}
		if( action == "pulling" ){	// 推廣活動
			$("input[name^=pulling-date]").datepicker({ dateFormat: "yy-mm-dd" });
			$('.pulling-time-text').timepicker({ timeFormat: 'H:i' });
			$('.pulling-time-text').val('00:00');
		}
	}
});

// for 27 interest item
var i_item    = new Array('movie', 'brand', 'trip', 'mkup', 'music', 'invest', 'trend', 'pet', 'learn', 'artist', 'fate', '3c', 'sci', 'acg', 'bg', 'pub', 'pi', 'food', 'parent', 'life', 'soul', 'mood', 'photo', 'car', 'health','art', 'sport');
var i_item_tw = new Array('電影戲劇', '品牌服務', '休閒旅遊', '美容彩妝', '音樂', '投資理財', '流行時尚', '寵物', '教育學習', '名人動態', '命理星座', '3c數位', '自然科普', '遊戲動漫', '感情話題', '公眾議題', '公益', '美食', '親子互動', '居家生活', '心靈勵志', '心情抒發', '攝影', '車友天地', '醫療保健', '文學藝術', '運動');

/**
 * 頁面載入時立刻執行取得通知
 */
(function(){
	var notification_pull=function(notify){
		//console.log(notify);
	}
	var notification_auto=function(){
		$(".unread-notifications-block").html('');
		
		$.post('/user/notification_realtime_get',{},function(data){
			mesg=parse_json(data);
			mesg.error=0;
			
			if(!mesg.error){
				// 通知訊息 count 顯示與否
				if( parseInt(mesg.counter) == 0 ){
					$('#notification-counter').parent(".note-count").hide();
				}
				else{
					$('#notification-counter').parent(".note-count").show();
				}

				$('#notification-counter').html(mesg.counter);
				if(mesg.realtimes.args.num){
					var i;
					var html = "";
					
					for(i=0;i<mesg.realtimes.args.num;i++){
						html+= "<div class='unread-notifications'>"+mesg.realtimes.d[i].msg+"</div>";
					}
					
					$(html).appendTo(".unread-notifications-block");
				}	
				
			}
		});

		setTimeout(notification_auto,10000);
	}
	if(me.myuid){
		setTimeout(notification_auto,5000);
	}
})();

// 興趣牆：收藏＆檢舉
function ul_sw(obj){
	$this = $(obj);
	$sw   = $this.siblings('.ul-bordered');

	$sw.toggle();
}

// 興趣牆：分享至 臉書＆興趣牆
function share_sw(obj){
	$this = $(obj);

	$sw = $this.parent('div').siblings('.share-bordered');

	$sw.css({"top": ($this.position().top + 24)+"px", "left": ($this.position().left - 95)+"px" }).toggle();
}

function share_sw_siblings(obj){
	$this = $(obj);
	$sw   = $this.siblings('.share-bordered');

	$sw.toggle();
}

timezone=$('body').data('timezone');
if(!timezone) timezone=0;
else timezone=parseInt(timezone,10)*60000;
var timezone_reset=function($this){
		var d=time_shift_timezone($this.html());
		$this.attr('datetime',$this.html());
		$this.attr('timestamp',d.getTime());
		$this.html([[padLeft(d.getFullYear(),4),padLeft(d.getMonth()+1,2),padLeft(d.getDate(),2)].join('-'),[padLeft(d.getHours(),2),padLeft(d.getMinutes(),2)].join(':')].join(' '));
		$this.removeClass('datetime');
}

$(document).ready(function() {
	var websocket_able=false;
	if(websocket_able){
		var server_url=document.location.href.replace(/^https?:\/\//,'').replace(/\/.*$/,'');
		server_url = 'ws://'+server_url+':8765/websocket';
		var socket;
		if (window.MozWebSocket) {
			socket = new MozWebSocket(server_url);
		} else if (window.WebSocket) {
			socket = new WebSocket(server_url);
		}
		socket.binaryType = 'blob';
		socket.onopen = function(msg) {
			$('#status').html('<span class="label label-info">Registering</span>');
			register_user();
			return true;
		};
		socket.onmessage = function(msg) {
			var response;
			response = JSON.parse(msg.data);
			checkJson(response);
			return true;
		};
		socket.onclose = function(msg) {
			$('#status').html('<span class="label label-danger">Disconnected</span>');
			setTimeout(function(){
				$('#status').html('<span class="label label-warning">Reconnecting</span>');
			}
			,5000);
			
		//	setTimeout(function(){
		//		location.reload();
		//	}
		//	,1000000);
		
			return true;
		};
	}
	function checkJson(res) {
		console.log(res);
		if(res.action=='registred'){
			$('#status').html('<span class="label label-success">Registred</span>');
			$('#chat_button').removeAttr('disabled');
			$('#text_message').removeAttr('disabled');
			$('#chat-head').html('<b>User-'+res.user_id+'</b> ('+res.users_online+' Users Online)');
			user_id = res.user_id;
		}else if(res.action=='chat_text'){
			if(res.user_id==user_id){
				$('#text_message').val('');
			}
			var new_entry = '<li><b>User-'+res.user_id+'&nbsp;&nbsp;</b>&nbsp;&nbsp;<span style="color: #DDD;width: 300px">'+res.date_time+' &nbsp;:&nbsp;</span>'+res.chat_text+'</li>'
			$("#chat_text_list").append(new_entry);
			$("#chat_text_list").animate({ scrollTop: 50000 }, "slow");
		}
		
	}
	function register_user(){
		payload = new Object();
		payload.action 	= 'register';
		payload.user_id = cookie_get('SSID');
		socket.send(JSON.stringify(payload));
	}
	$("#chat_button").click(function(){
		console.log('Triggred');
		payload = new Object();
		payload.action 		= 'chat_text';
		payload.user_id 	= cookie_get('SSID');
		payload.chat_text   = $('#text_message').val();
		socket.send(JSON.stringify(payload));
	});
	$("#text_message").on("keypress", function(e) {
		if (e.keyCode == 13){
			$("#chat_button").click();
		}
	});

	// 興趣分類（上層：全部/動態/活動)
	$("#wallpost-intersting .box .tab-group .refresh-wallposts").on("click", function(e){
		if( !$(this).hasClass("selected") ){
			$("#wallpost-intersting .box .tab-group .refresh-wallposts").removeClass("selected");
			$(this).addClass("selected");
			e.stopPropagation();
		}
	});

	// BxSlider : interest items
	$(".interest-item-mask-left .arrow").hide();
	var onload_bxslider = $(".slider1").get();
	
  	if( onload_bxslider.length > 0 ){
		var slider = $('.slider1').bxSlider({
		    slideWidth: 50,
		    minSlides: 12,
		    maxSlides: 12,
		    moveSlides: 3,
		    slideMargin: 10,
		    adaptiveHeight: true,
		    pager: false,
		    controls: false,
		    infiniteLoop: false,
		    onSlideAfter: function($slideElement, oldIndex, newIndex){
		    	if(newIndex != 0){	$(".interest-item-mask-left .arrow").show(); }
		    	else{				$(".interest-item-mask-left .arrow").hide(); }

		    	if( newIndex != 5 ){	$(".interest-item-mask-right .arrow").show(); }
		    	else{					$(".interest-item-mask-right .arrow").hide(); }
		    }
		});

		// 興趣分類（下層：２７個選項)
		$(".slider1 .slide").on("click", function(e){
			if( !$(this).hasClass("active") ){
				$(".slider1 .slide").removeClass("active");
				$(this).addClass("active");
				e.stopPropagation();
			}
		});

		// 將slider1 移至active的位置
		if( urlindex != -1 ){
			$.each($(".slider1 li"), function(i,v){
				if( $(v).hasClass("active") ){
					active_li = (i+1);
					return false;
				}
			});
			var trunTo = ( active_li <= 12 ) ? 0 : Math.ceil( (active_li-12)/3 );
			slider.goToSlide( trunTo );
		}
	}

	var onload_explore_bxslide = $(".slider2").get();

	if( onload_explore_bxslide.length > 0 ){
		var slider = $('.slider2').bxSlider({
		    slideWidth: 200,
		    minSlides: 16,
		    maxSlides: 16,
		    moveSlides: 3,
		    slideMargin: 10,
		    pager: false,
		    controls: false,
		    infiniteLoop: false,
		    onSlideAfter: function($slideElement, oldIndex, newIndex){
		    	if(newIndex != 0){	$(".interest-item-mask-left .arrow").show(); }
		    	else{				$(".interest-item-mask-left .arrow").hide(); }

		    	if( newIndex != 4 ){	$(".interest-item-mask-right .arrow").show(); }
		    	else{					$(".interest-item-mask-right .arrow").hide(); }
		    }
		});

		// 興趣分類（下層：２７個選項)
		$(".slider2 .slide").on("click", function(e){
			if( !$(this).hasClass("active") ){
				$(".slider2 .slide").removeClass("active");
				$(this).addClass("active");
				e.stopPropagation();
			}
		});

		// 將 slider2 移至active的位置
		if( urlindex != -1 ){
			$.each($(".slider2 li"), function(i,v){
				if( $(v).hasClass("active") ){
					active_li = (i+1);
					return false;
				}
			});

			var trunTo = ( active_li <= 16 ) ? 0 : Math.ceil( (active_li-16)/3 );
			slider.goToSlide( trunTo );
		}
	}

	// 點數 ＆ Ｐro等級
	$("#user_pro_point .tab").on("click", function(e){
		if( !$(this).hasClass("selected") ){
			$("#user_pro_point .tab").removeClass("selected");
			$(this).addClass("selected");
			if( $("#user_pro_point .tab:first-child").hasClass("selected") ){
				$(this).parent("div").siblings(".tab-content").hide();
				$(this).parent("div").siblings(".tab-content").eq(0).show();
			}
			else{
				$(this).parent("div").siblings(".tab-content").hide();
				$(this).parent("div").siblings(".tab-content").eq(1).show();
			}
			e.stopPropagation();
		}
	});

	$(".interest-item-mask-right").on("click", function(e){
		slider.goToNextSlide();
		e.stopPropagation();
	});

	$(".interest-item-mask-left").on("click", function(e){
		slider.goToPrevSlide();
		e.stopPropagation();
	});

	// 參與的互動 ＆ 參與的活動
	$(".activity-wrapper .box .tab-group .tab").on("click", function(){
		if( $(this).hasClass("participation-interactivity") ){
			if( !$(".participation-interactivity").hasClass("selected") ){
				$(this).addClass("selected");
				$(".participation-activity").removeClass("selected");

				$(".activity-body").hide();
				$(".activity-body.inter").show();
			}
		}
		else{
			if( !$(".participation-activity").hasClass("selected") ){
				$(this).addClass("selected");
				$(".participation-interactivity").removeClass("selected");

				$(".activity-body").show();
				$(".activity-body.inter").hide();
			}
		}
	});

	$('.datetime').each(function(){
		timezone_reset($(this));	
	});
});
