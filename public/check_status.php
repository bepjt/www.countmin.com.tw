<?php
if(!in_array($_SERVER['REMOTE_ADDR'],array('203.79.177.151','127.0.0.1','::1')) &&
	strcmp(substr($_SERVER['REMOTE_ADDR'],0,10),'10.10.100.') &&
	strcmp(substr($_SERVER['REMOTE_ADDR'],0,8),'192.168.') 
){ die('Reject.'); }
define('_DIR_DOCS','/srv/web/countmin/');
define('_DIR_FRAMEWORK','/srv/web/CoreBase/');

$checks=array(
	array(
		'type'=>'dir',
		'id'=>'images',
		'path'=>_DIR_DOCS.'public/images/',
		'perm'=>7
	),
	array(
		'type'=>'dir',
		'id'=>'image_temps',
		'path'=>_DIR_DOCS.'public/image_temps/',
		'perm'=>7
	),
	array(
		'type'=>'file',
		'id'=>'root',
		'path'=>_DIR_FRAMEWORK.'Firm/root.php',
		'perm'=>5
	),
	array(
		'type'=>'file',
		'id'=>'config',
		'path'=>_DIR_DOCS.'etc/config.php',
		'perm'=>5
	),
	array(
		'type'=>'file',
		'id'=>'db',
		'path'=>_DIR_DOCS.'etc/db.php',
		'perm'=>5
	),
	array(
		'type'=>'file',
		'id'=>'jscss',
		'path'=>_DIR_DOCS.'etc/css_js_timeup.php',
		'perm'=>7
	),
	array(
		'type'=>'class',
		'id'=>'pdo',
		'name'=>'PDO',
	),
	array(
		'type'=>'function',
		'id'=>'mb',
		'name'=>'mb_substr',
	),
	array(
		'type'=>'function',
		'id'=>'gd-jpeg',
		'name'=>'imagecreatefromjpeg',
	),
	array(
		'type'=>'function',
		'id'=>'gd-png',
		'name'=>'imagecreatefrompng',
	),
	array(
		'type'=>'max_size',
		'id'=>'max size',
		'upload_max_filesize'=>'100M',
		'file_uploads'=>1,
		'post_max_size'=>'100M',
	),
);
function get_fileperms($perms){
	$info='';
	// Owner
	$info .= (($perms & 0x0100) ? 'r' : '-');
	$info .= (($perms & 0x0080) ? 'w' : '-');
	$info .= (($perms & 0x0040) ?
			(($perms & 0x0800) ? 's' : 'x' ) :
			(($perms & 0x0800) ? 'S' : '-'));

	// Group
	$info .= (($perms & 0x0020) ? 'r' : '-');
	$info .= (($perms & 0x0010) ? 'w' : '-');
	$info .= (($perms & 0x0008) ?
			(($perms & 0x0400) ? 's' : 'x' ) :
			(($perms & 0x0400) ? 'S' : '-'));

	// Others
	$info .= (($perms & 0x0004) ? 'r' : '-');
	$info .= (($perms & 0x0002) ? 'w' : '-');
	$info .= (($perms & 0x0001) ?
			(($perms & 0x0200) ? 't' : 'x' ) :
			(($perms & 0x0200) ? 'T' : '-'));
	return $info;
}

?>
<!doctype html>
<html>
<head>
</head>
<style type="text/css" media="screen">
table#checks tr:nth-child(odd) {background:#ffc;}
table#checks tr th {text-align:left;background:#ccc;}
.wran {color:#f00;}
</style>
<body>
<table id="checks" class="table table-striped table-bordered table-hover" style="width:80%;" align="center">
<caption></caption>
<colgroup>
	<col class="" />
	<col class="" />
</colgroup>
<thead>
	<tr>
		<th>ID</th>
		<th>Type</th>
		<th>Exists</th>
		<th>Condition</th>
		<th>Result</th>
	</tr>
</thead>
<tbody>
<?php
for($i=0,$n=count($checks);$i<$n;$i++){
	$exists=0;
	$result='';
	$condition='';
	$wran=0;
	switch($checks[$i]['type']){
		case 'dir':
			if(is_dir($checks[$i]['path'])){
				$exists=1;
			}
			$perms=fileperms($checks[$i]['path']);
			$r=is_readable($checks[$i]['path']);
			$w=is_writable($checks[$i]['path']);
			$x=is_executable($checks[$i]['path']);
			$condition=($checks[$i]['perm']&4?'r':'-').($checks[$i]['perm']&2?'w':'-').($checks[$i]['perm']&1?'x':'-');
			$rwx_check=($r?'r':'-').($w?'w':'-').($x?'x':'-');
			$info=get_fileperms($perms);
			if(strcmp($condition,$rwx_check)){ $wran=1; }
			$result='<div>'.($rwx_check).'</div><div>'.$info.'</div>';

			break;
		case 'file':
			if(is_file($checks[$i]['path'])){
				$exists=1;
			}
			$perms=fileperms($checks[$i]['path']);
			$r=is_readable($checks[$i]['path']);
			$w=is_writable($checks[$i]['path']);
			$x=is_executable($checks[$i]['path']);
			$condition=($checks[$i]['perm']&4?'r':'-').($checks[$i]['perm']&2?'w':'-').($checks[$i]['perm']&1?'x':'-');
			$rwx_check=($r?'r':'-').($w?'w':'-').($x?'x':'-');
			$info=get_fileperms($perms);
			if(strcmp($condition,$rwx_check)){ $wran=1; }
			$result='<div>'.($rwx_check).'</div><div>'.$info.'</div>';

			break;
		case 'class':
			if(class_exists($checks[$i]['name'])){
				$exists=1;
				$result='Exists';
			}
			break;
		case 'function':
			if(function_exists($checks[$i]['name'])){
				$exists=1;
				$result='Exists';
			}

			break;
		case 'max_size':
			$exists=1;
			$upload_max_filesize=ini_get('upload_max_filesize');
			$post_max_size=ini_get('post_max_size');
			$file_uploads=ini_get('file_uploads');
			if(!$file_uploads || $file_uploads=='Off' || $post_max_size<$checks[$i]['post_max_size'] || $upload_max_filesize<$checks[$i]['upload_max_filesize']){
				$wran=1;
			}
			$condition='<div>file_uploads: '.$checks[$i]['file_uploads'].'</div><div>upload_max_filesize: '.$checks[$i]['upload_max_filesize'].'</div><div>post_max_size: '.$checks[$i]['post_max_size'].'</div>';
			$result='<div>file_uploads: '.$file_uploads.'</div><div>upload_max_filesize: '.$upload_max_filesize.'</div><div>post_max_size: '.$post_max_size.'</div>';
			break;
	}
?>
<tr<?php echo $wran?' class="wran"':''; ?>>
	<td><?php echo htmlspecialchars($checks[$i]['id']); ?></td>
	<td><?php echo $checks[$i]['type']; ?></td>
	<td><?php echo $exists?'Y':'-'; ?></td>
	<td><?php echo $condition; ?></td>
	<td><?php echo $result; ?></td>
</tr><?php
}
?>
</tbody>
</table>
<br /><br />
<?php phpinfo(); ?>
</body>
</html>
