<?php

class NoticesModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='notices';
		$this->field_pk='';
		$this->field_pk_charset='';
		$this->fields=array(
			'noticeId'=>array('charset'=>'string','max'=>48,'default'=>'','req'=>0),	//訊息詞彙代碼
			'noticeDescription'=>array('charset'=>'string','max'=>64,'default'=>'','req'=>0),	//訊息詞彙說明
		);
		$this->relation_tables=array();
	}

	// sample method
	public function notices_add(...){
	}
}

