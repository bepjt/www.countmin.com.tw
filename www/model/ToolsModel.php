<?php

class ToolsModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='chatrooms';
		$this->field_pk='';
		$this->field_pk_charset='';
		$this->fields=array(
			'chatroomUid'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//聊天室UUID
			'chatroomName'=>array('charset'=>'string','max'=>16,'default'=>'','req'=>0),	//聊天室名稱
			'chatroomNumUsers'=>array('charset'=>'uint','max'=>255,'default'=>0,'req'=>0),	//成員人數(上限 100 人)
			'chatroomTimeLastUpdate'=>array('charset'=>'timestamp','req'=>0),	//最後新增訊息時間
			'userUidLastUpdate'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//最後發訊息的帳號UUID
			'chatroomMessageLastUpdate'=>array('charset'=>'string','max'=>20,'default'=>'','req'=>0),	//最後訊息的20個字
		);
		$this->relation_tables=array();
	}

	public function files_getter($type){
		$query=array(
			'select'=>'f.*,n.funcDescription',
			'from'=>'func_files f
				LEFT JOIN func_names n ON f.funcId=n.funcId',
			'order'=>array(
				'f.funcId',
			),
			'where'=>'funcIdRefer=""',
		);
		switch($type){
			case 'controller':
			case 'model':
			case 'js':
			case 'template':
				$query['where'].='AND f.funcType='.$type;
				break;
		}
		return DB::data($this->dbid,$query);
	}

	public function func_getter($func_id){
		$query=array(
			'select'=>'*',
			'from'=>'func_files',
			'where'=>array(
				'funcId=:func_id',
			),
			'bind'=>array(
				':func_id'=>$func_id,	
			)
		);
		$func=DB::row($this->dbid,$query);
		return $func;
	}

	public function func_relationship_from_getter($func_id){
		$query=array(
			'select'=>'f.*,n.funcDescription',
			'from'=>'func_relationships r
				LEFT JOIN func_files f ON r.funcIdCall=f.funcId
				LEFT JOIN func_names n ON f.funcId=n.funcId',
			'where'=>array(
				'funcIdFrom=:func_id',
			),
			'order'=>array(
				'funcIdCall',
			),
			'bind'=>array(
				':func_id'=>$func_id,
			)
		);
		$rels=DB::data($this->dbid,$query);
		return $rels;
	}

	public function func_relationship_call_getter($func_id){
		$query=array(
			'select'=>'f.*,n.funcDescription',
			'from'=>'func_relationships r
				LEFT JOIN func_files f ON r.funcIdFrom=f.funcId
				LEFT JOIN func_names n ON f.funcId=n.funcId',
			'where'=>array(
				'funcIdCall=:func_id',
			),
			'order'=>array(
				'funcIdCall',
			),
			'bind'=>array(
				':func_id'=>$func_id,
			)
		);
		$rels=DB::data($this->dbid,$query);
		return $rels;
	}

	public function methods_getter($type,$file_id){
		$query=array(
			'select'=>'*',
			'from'=>'func_methods m
				LEFT JOIN func_files f ON m.fileId=f.fileId',
			'order'=>array(
				'fileId',
			),
		);
		switch($type){
			case 'controller':
			case 'model':
			case 'js':
			case 'template':
				$query['where']='fileType='.$type;
				break;
		}
		return DB::data($this->dbid,$query);
	}

	public function model_add($models){
		$models['default']=array(
			'funcId'=>'',
			'funcType'=>'model',
			'funcIdRefer'=>'',
			'funcProtected'=>'',
		);
		DB::adds($this->dbid,'func_files',$models);
	}

	public function controller_add($controllers){
		$controllers['default']=array(
			'funcId'=>'',
			'funcType'=>'controller',
			'funcIdRefer'=>'',
			'funcProtected'=>'',
		);
		DB::adds($this->dbid,'func_files',$controllers);
	}

	public function template_add($templates){
		$templates['default']=array(
			'funcId'=>'',
			'funcType'=>'template',
			'funcIdRefer'=>'',
			'funcProtected'=>'',
		);
		DB::adds($this->dbid,'func_files',$templates);
	}

	public function relationship_add($relationships){
		$relationships['default']=array(
			'funcIdFrom'=>'',
			'funcIdCall'=>''
		);
		DB::adds($this->dbid,'func_relationships',$relationships);
	}
}

