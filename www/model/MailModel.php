<?php 
use Mailgun\Mailgun;
class MailModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='mail_queues';
		
		$this->mail_account='mailgun@sandbox76b99712de564f3b909cb1aa4e8f55c2.mailgun.org';
		$this->mail_key='key-c171575a3231adfa8b44ed0f577f6a4d';
		$this->mail_domain='sandbox76b99712de564f3b909cb1aa4e8f55c2.mailgun.org';
		
		$this->mail_level=array(
			1	=>	10000,	//regist,change password	,cron 建議2分跑1次 (開站1分跑1次,但要配合 send_hold_mail_query)
			2	=>	5000,		//user setting						,cron 建議3分跑1次
			3	=>	200,		//normal									,cron 建議5分跑1次
			//// 11 up  for edm
			11	=>	200,	//edm 1
			12	=>	200,	//edm 2
			13	=>	200,	//edm 3
		);
	}
	/**
	 * send_mail
	 * 直接寄信
	 * 
	 * 
	 * @param mixed $to - abc@gmail.com 
	 * @param mixed $subtitle	標題 
	 * @param mixed $content	內文
	 * @access public
	 * @return void
	 */
	public function send_mail($to,$subtitle,$content){
		require_once('/srv/web/CoreBase/Firm/include/vendor/autoload.php');
		//use Mailgun\Mailgun;
		# Instantiate the client.
		$mgClient = new Mailgun($this->mail_key);
		# Make the call to the client.
		$result = $mgClient->sendMessage($this->mail_domain, array(
			'from'    => '參一腳 <service@countmin.com>',
			'to'      => $to,
			'subject' => $subtitle,
			'html'    => $content
		));
	}
	
	/**
	 * add_mail_query
	 * 增加1則mail通知
	 * 
	 * 
	 * @param mixed $to - abc@gmail.com 
	 * @param mixed $subtitle	標題 
	 * @param mixed $content	內文
	 * @param mixed $mail_level	預設3, 1 註冊與更改密碼用,2 常用設定更改,3 一般使用,4-10 有必要再加開,11以上保留給edm用
	 * @access public
	 * @return void
	 */
	public function add_mail_queue($to,$subtitle,$content,$mail_level=3){
		
		$mail_uid=uid4();
		$data=array(
			'mailUid' => $mail_uid,
			'mailLevel' => $mail_level,
			'mailTo' => $to,
			'mailSubtitle' => $subtitle,
			'mailContent' => $content,
			'mailStatus' => 0,		//0-未寄出,1-可重寄,2-堆疊中,3-已寄出
			'mailCreateTime' => _SYS_DATETIME,
		);
		$exe=DB::add($this->dbid,$this->table_name,$data);
		return ($exe==1) ? $mail_uid : $exe ;
	}
	
	
		
	/**
	 * send_mail_query
	 * 從mail堆疊中消耗信件, for crontab
	 * 
	 * @param mixed $mail_level	預設3, 1 註冊與更改密碼用,2 常用的設定更改,3 一般使用,11以上保留給edm用
	 * @access public
	 * @return void
	 */
	public function send_mail_queue($mail_level=3){
		
		$query=array(
			'select ' => '*',
			'from'=>$this->table_name,
			'where' =>' mailLevel=:mail_level AND mailStatus <=1',
			'bind'	=> array(
				':mail_level' => $mail_level
				),
			'order'	=>	'mailCreateTime ASC ',
			'limit'	=> !empty($this->mail_level[$mail_level]) ? $this->mail_level[$mail_level] : 200	,
		);
		$data=DB::data($this->dbid,$query);
		
		
		if($data['args']['num']>0){
			foreach($data['d'] as $mail_data){
				
				///防止send_mail過慢而且造成中斷....把此mail記憶成堆疊中....
				$update_data=array(
					'mailStatus' => 2,		//0-未寄出,1-可重寄,2-堆疊中,3-已寄出
					'mailTimeQueue' => _SYS_DATETIME,
				);
				
				DB::update($this->dbid,$this->table_name,$update_data,array('WHERE mailUid=:mail_uid',array(':mail_uid'=>$mail_data['mailUid'])));
				
				
				$this->send_mail($mail_data['mailTo'],$mail_data['mailSubtitle'],$mail_data['mailContent']);
				
				///send_mail有可能卡住所以隨時中斷(kill指令)php時....所以寄完1封才更新成已寄出
				$update_data=array(
					'mailStatus' => 3,		//0-未寄出,1-可重寄,2-堆疊中,3-已寄出
					'mailTimeSent' => _SYS_DATETIME,
				);
				
				DB::update($this->dbid,$this->table_name,$update_data,array('WHERE mailUid=:mail_uid',array(':mail_uid'=>$mail_data['mailUid'])));
				
			}
		}
	}
}
?>
