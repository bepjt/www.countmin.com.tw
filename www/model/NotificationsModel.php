<?php

class NotificationsModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='unotifications';
        $this->notificationSetting_table_name='notificationsetting';
		$this->field_pk='';
		$this->field_pk_charset='';
		$this->fields=array();
		$this->relation_tables=array();
	}

	/**
	 * notifications_add 
	 * 增加通知
	 * 提供給 ajax 取得
	 * 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function notifications_add($from_uid,$to_uid,$target_url,$target_uid,$noticeId,$noticeData){
		
		$data=array(
			'unotificationUid' => uid4(),
			'userUidFrom' =>	$from_uid,
			'userUidTo'=>	$to_uid,
			'unotificationTarget'	 => $target_url ,
			'unotificationTargetUid' => $target_uid ,
			'noticeId' => $noticeId ,
			'unotificationData' => json_encode($noticeData),
			'unotificationTime'	 => _SYS_DATETIME ,
			'unotificationStatusRead' => 0 ,
		);
		return DB::add($this->dbid,$this->table_name,$data,FALSE);
	}
    public function notifications_byarray_add($data){
        return DB::add($this->dbid,$this->table_name,$data,FALSE);    
    }        

        


	/**
	 * unread_getter 
	 * 取得還未讀的通知，並設定為已讀 (左下角alarm)
	 * 提供給 ajax 取得
	 * 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function unread_getter($user_uid,$spage=0,$per=50){

		
		
		$query=array(
			'select'=>'*',
			'from'=>$this->table_name,
			'where'=>'userUidTo=:user_uid AND unotificationStatusRead=0   ',  //unotificationTime
			'order'=>'unotificationTime desc',
			'bind'=>array(
				':user_uid'=>$user_uid,
			),
            'spage'   => $spage ,
            'per'     => $per,
		);
		// 即時通知訊息(左下角)
		$realtimes=DB::data($this->dbid,$query);
		
		
		$cnt=$this->counter($user_uid);
		$realtimes['args']['cnt']=$cnt;
		

		$data=array(
			'unotificationStatusRead'=>1,
		);
		DB::update($this->dbid,$this->table_name,$data,array('WHERE userUidTo=:user_uid AND unotificationStatusRead=0',array(':user_uid'=>$user_uid)));
		return $realtimes;
	}

	/**
	 * counter_getter 
	 * 取得未讀取通知
	 * 提供給當使用者讀取 #notification-counter 時
	 * 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function counter($user_uid){
		$query=array(
			'select'=>'count(*)',
			'from'=>$this->table_name,
			'where'=>'userUidTo=:user_uid AND unotificationStatusRead<=1',
			'bind'=>array(
				':user_uid'=>$user_uid,	
			)
		);
		return DB::value($this->dbid,$query);
	}

    /**
     * list_getter 
     * 取得所有通知清單（查看全部)
     * 
     * @param mixed $user_uid 
     * @access public
     * @return void
     */
    public function list_all_getter($user_uid,$spage=0,$per=100){
        $query=array(
            'select'=>'*',
            'from'=>$this->table_name,
            'where'=>'userUidTo=:user_uid',
            'order'=>'unotificationTime desc',
            'bind'=>array(
                ':user_uid'=>$user_uid,
            ),
            'spage'   => $spage ,                                           
            'per'     => $per,
        );
        $lists=DB::data($this->dbid,$query);
        return $lists;  
    }


	/**
	 * list_getter 
	 * 取得通知清單，並更新為已讀取(右上角alarm)
	 * 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function list_getter($user_uid,$spage=0,$per=5){
		$query=array(
			'select'=>'*',
			'from'=>$this->table_name,
			'where'=>'userUidTo=:user_uid  ',
			'order'=>'unotificationTime desc',
			'bind'=>array(
				':user_uid'=>$user_uid,
			),
            'spage'   => $spage ,
            'per'     => $per,
		);
		// 通知(右上alarm)
		$lists=DB::data($this->dbid,$query);

		$data=array(
			'unotificationStatusRead'=>2,
		);
		DB::update($this->dbid,$this->table_name,$data,array('WHERE userUidTo=:user_uid AND unotificationStatusRead=1',array(':user_uid'=>$user_uid)));

		return $lists;
	}

	/**
	 * read 
	 * 讀取通知連結
	 * 
	 * @param mixed $notification_uid 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function read($notification_uid,$user_uid){
		$data=array(
			'unotificationStatusRead'=>3,
		);
		DB::update($this->dbid,$this->table_name,$data,array('WHERE userUidTo=:user_uid AND unotificationUid=:notification_uid',array(':user_uid'=>$user_uid,':notification_uid'=>$notification_uid)));
	}

    /**
     * notificationSetting_add
     * 新增通知設定                                                                                                                          
     * 
     * @param mixed $data
     * @access public
     * @return void
     */          
    public function notificationSetting_add($data){    
        return DB::add($this->dbid,$this->notificationSetting_table_name,$data,FALSE);   
    }

    /**
     * notificationSetting_getter 
     * 取得通知設定
     * 
     * @param mixed $user_uid 
     * @access public
     * @return void
     */
    public function notificationSetting_getter($user_uid){
        $query=array(
                'select' => '*' ,
                'from'   => $this->notificationSetting_table_name ,
                'where'  => array('userUid=:userUid',
                    array(
                        'userUid'=>$user_uid,
                        )
                    )
                );
        return DB::row($this->dbid,$query);
    }    

    /**
     * notificationSetting_update 
     * 更新通知設定
     * 
     * @param mixed $user_uid 
     * @param mixed $data
     * @access public
     * @return void
     */          
    public function notificationSetting_update($user_uid,$data){    
        return DB::update($this->dbid,$this->notificationSetting_table_name,$data,array('WHERE userUid=:userUid',array('userUid'=>$user_uid)));  
    }


	/**
	 * notification_sendmail 
	 * 建立 phpmailer 物件
	 * 
	 * @access public
	 * @return $phpmailer
	 */
	public function notification_mailer(){
		include(_DIR_FRAMEWORK.'include/phpmailer/class.phpmailer.php');
		$mailer=new PHPMailer();
		$mailer->IsSMTP();
		$mailer->SMTPAuth=TRUE;
		$mailer->SMTPSecure = "ssl"; // Gmail的SMTP主機需要使用SSL連線   
		$mailer->Host = "smtp.gmail.com"; //Gamil的SMTP主機        
		$mailer->Port = 465;  //Gamil的SMTP主機的SMTP埠位為465埠。        
		$mailer->CharSet = "utf-8"; //設定郵件編碼        
		
		$mailer_config=CZ::config_get('mailer');
		$mailer->Username = $mailer_config['account'];
		$mailer->Password = $mailer_config['password'];
		return $mailer;
	}


}



