<?php

class GoodsModel extends Model_Base {
    public function __construct(){
        $this->dbid='common';
        $this->table_name='goods';
        $this->order_table_name = 'goodsorders';
        $this->suppliers_table_name ='suppliers';
        $this->deliveryinfo_table_name ='deliveryinfo';
        $this->suppliers_table_name='suppliers';
        $this->brands_table_name='brands';
        $this->field_pk='';
        $this->field_pk_charset='';
        $this->fields=array(
                'goodUid'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//好康UUID
                'goodName'=>array('charset'=>'string','max'=>128,'default'=>'','req'=>0),	//名稱
                'goodTimeStart'=>array('charset'=>'datetime','req'=>0),	//上架時間(UTC)
                'goodTimeClose'=>array('charset'=>'datetime','req'=>0),	//下架時間(UTC)
                'goodDateLimitStart'=>array('charset'=>'date','default'=>'0000-00-00','req'=>0),	//使用期限開始
                'goodDateLimitClose'=>array('charset'=>'date','default'=>'0000-00-00','req'=>0),	//使用期限結束
                'goodType'=>array('charset'=>'uint','max'=>255,'default'=>0,'req'=>0),	//類別 0:實體好康 1:虛擬卡號
                'goodImage'=>array('charset'=>'uint','max'=>255,'default'=>0,'req'=>0),	//好康的展示圖示檔 0:沒有圖示 1:圖示檔名為 goodUid.png
                'goodPricePoint'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0),	//需要花費的點數
                'goodCost'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0),	//成本/進貨價
                'goodPriceMarket'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0),	//市價
                'goodNumStorage'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0),	//剩餘可兌換數量
                'goodNumExchange'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0),	//已兌換數量
                'goodIntro'=>array('charset'=>'string','max'=>65535,'default'=>'','req'=>0),	//好康說明
                'goodNote'=>array('charset'=>'string','max'=>65535,'default'=>'','req'=>0),	//注意事項
                'goodExchangeFlow'=>array('charset'=>'string','max'=>65535,'default'=>'','req'=>0),	//兌換流程
                'goodBrandInfo'=>array('charset'=>'string','max'=>65535,'default'=>'','req'=>0),	//品牌介紹
                'goodLink'=>array('charset'=>'string','max'=>128,'default'=>'','req'=>0),	//相關連結
                );
        $this->relation_tables=array();
    }

    // sample method

    public function goods_add(){
    }

    public function goods_getter($goodsUid,$column='*' ){
        $query=array(
                'select' => $column ,
                'from'   => $this->table_name ,
                'where'  => array('goodsUid=:goodsUid',
                    array(
                        'goodsUid'=>$goodsUid,
                        )
                    )
                );
        $goods=DB::row($this->dbid,$query);
        return $goods;
    }    

    public function goods_orderCount_getter($userUid)
    {
        $query=array(
                'select' => 'count(*)' ,
                'from'   => $this->table_name.' g ,'.$this->order_table_name.' o' , 
                'where'  => array('g.goodUid = o.goodUid AND o.userUid=:userUid',
                    array(
                        'userUid'=>$userUid,
                        )
                    ),
                ); 
        $goods=DB::data($this->dbid,$query); 
        return $goods;   
    }   

    public function goods_order_getter($goodsorderId,$type='admin'){

        switch($type == 'admin' ){
            case 'admin' :
                $column = '*';
                break;
            case 'index' ;
               $column = 'g.goodsName,g.goodsDateLimitClose,g.goodsType,g.imageUidMaster,g.goodsPricePoint,g.goodsIntro,g.goodsNote,o.goodsStatus,o.goodsorderId,o.goodsorderTime,o.goodsorderReciverName,o.goodsorderReciverTel,o.goodsorderReciverCity,o.goodsorderReciverDist,o.goodsorderReciverZip,o.goodsorderReciverAddress,o.goodsLogistics,o.goodsOtherLogistics,o.goodsLogisticsId,o.goodsorderNote';
                break;
            default :
                $column = '*';
        }    

        $query=array(
                'select' => $column ,
                'from'   => $this->table_name.' g ,'.$this->order_table_name.' o' ,
                'where'  => array('g.goodsUid = o.goodsUid AND goodsorderId=:goodsorderId',
                    array(
                        'goodsorderId'=>$goodsorderId,
                        )
                    ),
                );
        return DB::row($this->dbid,$query);
    }    

    public function goods_order_by_user_getter($userUid,$goodsType=0,$startDay='',$endDay='',$spage=0,$per=50 ){

        $column='o.goodsorderId,o.goodsorderTime,o.goodsorderStatus,o.goodsorderTimeDelivery,g.goodsName,g.goodsType,g.goodsPricePoint,g.imageUidMaster';

        $query=array(
                'select' => $column ,
                'from'   => $this->table_name.' g ,'.$this->order_table_name.' o' ,
                'where'  => array('g.goodsUid = o.goodsUid AND o.userUid=:userUid',
                    array(
                        'userUid'=>$userUid,
                        )
                    ),
                'order' => 'o.goodsorderTime desc'  ,
                'spage'   => $spage ,
                'per'     => $per
                );  

        if(!empty($startDay) && !empty($endDay) ){
            $query['where'][0] .= ' AND o.goodsorderTime <=:startDay AND o.goodsorderTime >=:endDay';
            $query['where'][1]['startDay'] = $startDay ;
            $query['where'][1]['endDay'] = $endDay ;
        }    

        if($goodsType != 3){
            $goodsType--;
            $query['where'][0] .= ' AND g.goodsType=:goodsType';
            $query['where'][1]['goodsType'] = $goodsType ;
        }                         

        $goods=DB::data($this->dbid,$query); 
        return $goods;
    }    

    public function goods_order_query($oid,$goodsName,$goodsType,$brands,$status,$stime,$etime,$desc='desc',$spage=0,$per=50,$type='select' ){      

        switch($type){
            case 'count' :
                $column = 'count(*)';
                break;
            case 'select' :
                $column = '*';   
                break;
        }   

        $query=array(
                'select' => $column ,
                'from'   => $this->table_name.' g ,'.$this->order_table_name.' o' ,
                'order' => 'o.goodsorderTime desc ',  
                );


        if( empty($oid) && empty($goodsName) && empty($status)  && empty($stime) && empty($stime) && $goodsType == 0  && empty($brands)  )
            $query['where'] = 'g.goodsUid = o.goodsUid ' ;
        else
            $query['where'] = array('g.goodsUid = o.goodsUid ') ;

        if(!empty($oid)){
            $query['where'][0] .= ' AND o.goodsorderId=:oid';
            $query['where'][1]['oid'] = $oid ;
        }    

        if(!empty($goodsName)){
            $query['where'][0] .= ' AND g.goodsName LIKE :goodsName';
            $query['where'][1]['goodsName'] = '%'.$goodsName.'%' ;
        }    
        
        if($goodsType){
            $goodsType--;
            $query['where'][0] .= ' AND g.goodsType=:goodsType';
            $query['where'][1]['goodsType'] = $goodsType ;
        }    

        if( !empty($status)   ){
            $status--; //與DB參數值對應
            $query['where'][0] .= ' AND o.goodsorderStatus=:status';
            $query['where'][1]['status'] = $status ;
        }   

        if( !empty($brands) ){
            $query['where'][0] .= ' AND g.brandUid IN (select brandUid from brands where brandName like :brands )' ;       
            $query['where'][1]['brands'] = '%'.$brands.'%' ;
        }

        if(!empty($stime)){
            $query['where'][0] .= ' AND o.goodsorderTime >=:stime';
            $query['where'][1]['stime'] = $stime ; 
        }

        if(!empty($etime)){
            $query['where'][0] .= ' AND o.goodsorderTime <=:etime';
            $query['where'][1]['etime'] = $etime ;                
        }

        switch($type){
            case 'select' :
                $query['spage'] = $spage ;
                $query['per']   = $per   ; 
                break; 
        }   
        $goods=DB::data($this->dbid,$query); 
        return $goods;
    }    

    public function goods_order_modify($goodsorderId,$data){ 
        $attach=array(
                        'WHERE goodsorderId=:goodsorderId',
                            array('goodsorderId'=>$goodsorderId)
                     );                                                                             

        return DB::update($this->dbid,$this->order_table_name,$data,$attach);  
    }

    public function goods_new($data){
        $goods=DB::add($this->dbid,$this->table_name,$data,FALSE);
        return $goods;
    }    

    public function goods_modify($goodsUid,$data){

        return DB::update($this->dbid,$this->table_name,$data,array('WHERE goodsUid=:goodsUid',array('goodsUid'=>$goodsUid)));  
    }

    public function goods_list($gid,$keyword,$brands,$status,$goodsType,$stime='',$etime='',$orderby,$desc='desc',$spage=0,$per=50,$type='select' ){

        switch($type){
            case 'count' :
                $column = 'count(*)';
                break;
            case 'select' :
                $column = '*';
                break;
        }

        if(!empty($orderby))
            $orderby = 'g.'.$orderby ;
        else
            $orderby = 'g.goodsTimeStart';

        $query=array(
                'select' => $column ,
                'from'   => $this->table_name .' g ,'.$this->brands_table_name.' b' ,
                'where'  => array( 'g.brandUid = b.brandUid '
                    ),
                'order' => $orderby." ".$desc ,  
                );

        if(!empty($keyword)){
            $query['where'][0] .= ' AND (g.goodsName LIKE :keyword )';
            $query['where'][1]['keyword'] = '%'.$keyword.'%' ;
        }

        if(!empty($brands)){
            $query['where'][0] .= ' AND (b.brandName LIKE :brands)';
            $query['where'][1]['brands'] = '%'.$brands.'%' ;
        }

        if(!empty($gid)){
           $query['where'][0] .= ' AND g.goodsUid=:gid';         
           $query['where'][1]['gid'] = $gid ;
        }

        if( !empty($status)   ){
            $status-- ;
            $query['where'][0] .= ' AND g.goodsStatus=:status';
            $query['where'][1]['status'] = $status ;
        }   

        if( !empty($goodsType)   ){
            $goodsType-- ;
            $query['where'][0] .= ' AND g.goodsType=:goodsType';
            $query['where'][1]['goodsType'] = $goodsType ;
        }   


        if(!empty($stime)){
            $query['where'][0] .= ' AND g.goodsTimeStart >=:stime';
            $query['where'][1]['stime'] = $stime ; 
        }

        if(!empty($etime)){
            $query['where'][0] .= ' AND g.goodsTimeClose <=:etime'; 
            $query['where'][1]['etime'] = $etime ;           
        }

        switch($type){
            case 'select' :
                $query['spage'] = $spage ;
                $query['per']   = $per   ;
                break;
        }   
     
        return DB::data($this->dbid,$query);
    }

    public function goods_indexlist($keyword,$orderby,$goodsType,$interest,$goodsUid='',$spage=0,$per=6){
       $status=1; 
        
       $column='g.goodsUid,g.brandUid,g.imageUidMaster,g.goodsName,g.goodsType,g.goodsPricePoint,g.goodsInterests,b.brandName,b.imageUid' ;

       if(empty($orderby))
         $orderby = 'g.goodsTimeStart desc';

        $query=array(
                'select' => $column ,
                'from'   => $this->table_name.' g ,'.$this->brands_table_name.' b' ,
                'where'  => array( 'g.brandUid=b.brandUid AND g.goodsStatus=:status AND g.goodsNumStorage > 0 AND g.goodsStatus = 1 ' ,
                                 array(
                                       'status'=>$status,
                                 )
                            ),
                'order'   => $orderby ,   
                'spage'   => $spage,
                'per'     => $per,
                );
        
        if(!empty($keyword)){
            $query['where'][0] .= ' AND (g.goodsName LIKE :keyword OR b.brandName LIKE :keyword)';          
            $query['where'][1]['keyword'] = '%'.$keyword.'%' ;        
        }

        if(!empty($goodsType)){
            $goodsType--;
            $query['where'][0] .= ' AND g.goodsType=:goodsType';
            $query['where'][1]['goodsType'] = $goodsType ;      
        }      

        if(!empty($interest)){
            $query['where'][0] .= ' AND g.goodsInterests LIKE :goodsInterests';
            $query['where'][1]['goodsInterests'] = '%'.$interest.'%' ;      
        }      

        if(!empty($goodsUid)){
            $query['where'][0] .= ' AND g.goodsUid IN( select wallpostUid from ufavors where userUid = "'.ME::user_uid().'" AND wallpostType = 5 )' ;
        }      

        $goods=DB::data($this->dbid,$query);
        return $goods;
    }    

     public function goods_order_new($data){
         $goods=DB::add($this->dbid,$this->order_table_name,$data,FALSE);
         return $goods;
     }    

    /**
    * goods_interest_getter 
    * 你可能有興趣的好康 
    * @param mixed $interest 興趣標籤 ex acg;car;photo 
    * @param mixed $column 欄位
    * @param mixed $limit  數量 
    * @access public
    * @return void
    */
    public function goods_interest_getter($interest,$column='goodsUid,imageUidMaster',$limit=3 ){
        $str=''; 
        $interestArray = explode(";",$interest);
        $query=array(
                'select' => $column ,
                'from'   => $this->table_name ,
                'where'  => array(),
                'order'  => 'RAND()',
                'limit'  => $limit,
                ); 
        $query['where'][0]='';
        $query['where'][1]='';

        for($i=0;$i<count($interestArray);$i++){
            if( $i==count($interestArray)-1)
                $query['where'][0] .= ' goodsInterests LIKE :'.$interestArray[$i]; 
            else
                $query['where'][0] .=  ' goodsInterests LIKE :'.$interestArray[$i].' OR ' ;

            $query['where'][1][$interestArray[$i]] = '%'.$interestArray[$i].'%';
        }
        $query['where'][0] = '('.$query['where'][0].')';
        
        return DB::data($this->dbid,$query);
    }    

    public function deliveryinfo_getter($userUid,$groupid=''){
        $query=array(
                'select' => '*' ,
                'from'   => $this->deliveryinfo_table_name ,
                'where'  => array('userUid=:userUid',
                    array(
                        'userUid'=>$userUid,
                        )
                    )
                );

        if(!empty($groupid)){
            $query['where'][0] .= ' AND groupid=:groupid ';
            $query['where'][1]['groupid'] = $groupid ;
        }           

        return DB::data($this->dbid,$query);
        
    } 
   
    public function deliveryinfo_update($userUid,$groupId,$data){ 
        $attach=array('WHERE userUid=:userUid AND groupId=:groupId',
                    array('userUid' => $userUid ,
                          'groupId' => $groupId ,
                        ),
                );  
        return DB::update($this->dbid,$this->deliveryinfo_table_name,$data,$attach);
    }

    public function deliveryinfo_add($data){
        return DB::add($this->dbid,$this->deliveryinfo_table_name,$data,FALSE);              
    }
 
    public function suppliers_add($data){
        return DB::add($this->dbid,$this->suppliers_table_name,$data,FALSE);
    }

    public function suppliers_update($supplierUid,$data){               
        $attach=array('WHERE supplierUid=:supplierUid',
                    array('supplierUid' => $supplierUid ),
                );  

        return DB::update($this->dbid,$this->suppliers_table_name,$data,$attach);
    }

    public function suppliers_getter($selectType='all',$supplierUid='',$supplierName='',$spage=0,$per=50 ){                                          

        $query=array(
                'select' => '' ,
                'from'   => $this->suppliers_table_name ,
                'order' => 'supplierUid',
                ); 
       
        if(!empty($supplierUid) || !empty($supplierName)   ) 
            $query['where'][0]='';

        switch($selectType){
            case 'all' :
                $query['select']='*';
                $query['spage']=$spage;
                $query['per']=$per;   
                break;
            case 'count' :
                $query['select']='count(*)';
                break;                    
            default :
                $query['select']='count(*)';
                break;                
        }

        if(!empty($supplierUid)){
            $query['where'][0] .= ' supplierUid=:supplierUid ';
            $query['where'][1]['supplierUid'] = $supplierUid ;
        }        

        if(!empty($supplierName)){
            if( !empty($supplierUid) )
                $query['where'][0] .= ' AND ';

            $query['where'][0] .= ' supplierName LIKE :supplierName ';
            $query['where'][1]['supplierName'] = '%'.$supplierName.'%' ;
        }
        return DB::data($this->dbid,$query); 
    }    

    public function brands_add($data){
       return DB::add($this->dbid,$this->brands_table_name,$data,FALSE);
    }    

    public function brands_update($brandUid,$data){               
        $attach=array('WHERE brandUid=:brandUid',
                    array('brandUid' => $brandUid ),
                );  

        return DB::update($this->dbid,$this->brands_table_name,$data,$attach);
    }

    public function brands_getter($selectType='all',$brandUid='',$brandName='',$spage=0,$per=50 ){                                          

        $query=array(
                'select' => '' ,
                'from'   => $this->brands_table_name ,
                'order' => 'brandUid',
                'per'     => $per
                ); 

        if(!empty($brandUid) || !empty($brandName)  )
            $query['where'][0]='';

        switch($selectType){
            case 'all' :
                $query['select']='*';
                $query['spage']=$spage;
                $query['per']=$per;   
                break;
            case 'count' :
                $query['select']='count(*)';
                break;                    
            default :
                $query['select']='count(*)';
                break;                
        }

        if(!empty($brandUid)){
            $query['where'][0]='';
            $query['where'][0] .= ' brandUid=:brandUid ';
            $query['where'][1]['brandUid'] = $brandUid ;
        }        

        if(!empty($brandName)){
            if( !empty($brandUid) )
                $query['where'][0] .= ' AND ';

            $query['where'][0] .= ' brandName LIKE :brandName ';
            $query['where'][1]['brandName'] = '%'.$brandName.'%' ;
        }
        return DB::data($this->dbid,$query); 
    }



                        
}

