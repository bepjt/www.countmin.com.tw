<?php
class WallpostsModel extends Model_Base {

	
	public function __construct(){
		$this->dbid='common';
		$this->table_name='wallposts';
        $this->wsocials_table_name='wsocials';
        $this->writings_table_name='writings';
        $this->articles_table_name='articles';
        $this->missions_table_name='missions';
        $this->events_table_name='events';
        
        $this->htmlpurifier_allow_youtube_map='%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|maps.google.com)%';

		$this->field_pk='uaccUid';
		$this->field_pk_charset='uid4';
		$this->fields=array(
			'wallpostUid'=>array('charset'=>'string','max'=>'32','req'=>0),
			'userUid'=>array('charset'=>'string','max'=>'32','req'=>0),
			'wallpostType'=>array('charset'=>'int','max'=>127,'min'=>-127,'req'=>0),
			'wallpostPublic'=>array('charset'=>'int','max'=>127,'min'=>-127,'req'=>0),
			'wallpostImage'=>array('charset'=>'string','max'=>'32','req'=>0),
			'wallpostBrief'=>array('charset'=>'string','max'=>'255','req'=>0),
			'wallpostInterests'=>array('charset'=>'string','max'=>'20','req'=>0),
			'wallpostAuthor'=>array('charset'=>'string','max'=>'32','req'=>0),
			'wallpostTimeCreate'=>array('charset'=>'timestamp','req'=>0),
		);
		$this->rels=array(
			'writings'=>array(
				'pk'=>'wallpostUid',
				'pk_charset'=>'uid4',
				'fields'=>array(
					'wallpostUid'=>array('charset'=>'string','max'=>'32','req'=>0),
					'writingContent'=>array('charset'=>'string','max'=>'255','req'=>0),
				)
			),
			'articles'=>array(
				'pk'=>'wallpostUid',
				'pk_charset'=>'uid4',
				'fields'=>array(
					'wallpostUid'=>array('charset'=>'string','max'=>'32','req'=>0),
					'articleTopic'=>array('charset'=>'string','max'=>'128','req'=>0),
					'articleContentSource'=>array('charset'=>'string','max'=>65535,'req'=>0),
					'articleContent'=>array('charset'=>'string','max'=>65535,'req'=>0),
				)
			),
			'missions'=>array(
				'pk'=>'wallpostUid',
				'pk_charset'=>'uid4',
				'fields'=>array(
					'wallpostUid'=>array('charset'=>'string','max'=>'32','req'=>0),
					'missionName'=>array('charset'=>'string','max'=>'128','req'=>0),
					'missionTimeRequest'=>array('charset'=>'timestamp','req'=>0),
					'missionTimeRegist'=>array('charset'=>'timestamp','req'=>0),
					'missionNumPerson'=>array('charset'=>'uint','max'=>4294967295,'req'=>0),
					'missionPointTotal'=>array('charset'=>'uint','max'=>4294967295,'req'=>0),
					'missionContent'=>array('charset'=>'string','max'=>65535,'req'=>0),
					'regionUid'=>array('charset'=>'string','max'=>'32','req'=>0),
				)
			),
			'events'=>array(
				'pk'=>'wallpostUid',
				'pk_charset'=>'uid4',
				'fields'=>array(
					'wallpostUid'=>array('charset'=>'string','max'=>'32','req'=>0),
					'missionName'=>array('charset'=>'string','max'=>'128','req'=>0),
					'missionTimeRequest'=>array('charset'=>'timestamp','req'=>0),
					'missionTimeRegist'=>array('charset'=>'timestamp','req'=>0),
					'missionNumPerson'=>array('charset'=>'uint','max'=>4294967295,'req'=>0),
					'missionContent'=>array('charset'=>'string','max'=>65535,'req'=>0),
					'regionUid'=>array('charset'=>'string','max'=>'32','req'=>0),
				)
			)
		);
	}

	/**
	 * post_add 
	 * 發表動態，由內容長度判斷要寫入 articles 或 writings(短於 255)
	 * 
	 * @param uid/NULL $user_uid 發表者userUid
	 * @param option $public 公開等級, 0:完全公開, 1: 限同好, 2:限好友, 3:限自己, (2,3, 暫時用不到)
	 * @param uid/NULL $cover_image 縮圖的圖檔UUID
	 * @param string $topic 標題
	 * @param mixed $content 使用者發表的內容
	 * @param mixed $interests 傳入陣列，限制數量依規格書而定，不過目前資料表結構的長度只足夠三個(2015.10)
	 * @param mixed $author_uid_source 轉貼最原始發表者的userUid
	 * @param mixed $wallpost_uid_source 轉貼最原始發表的wallpostUid
     * @param mixed $wallpost_temp 是否為草稿 0:否 1:是
	 * @access public
	 * @return void
	 */
	public function post_add($user_uid,$public,$cover_image,$topic,$content,$interests,$author_uid_source,$wallpost_uid_source,$wallpost_temp=0){
		$wallpost_uid=uid4();
		if(is_array($interests)){
			$interests=join(';',$interests);
		}
		$brief=strip_tags($content);
		if(mb_strlen($brief)>240){
			$brief=mb_substr($brief,0,239).'...';
		}
		if(mb_strlen($content)>250){
			$type=1;
		}
		else{
			$type=0;
		}
		$data=array(
			'wallpostUid'=>$wallpost_uid,
			'userUid'=>$user_uid,
			'wallpostType'=>$type,
			'wallpostPublic'=>$public,
			'imageUid'=>$cover_image,
			'wallpostTopic'=>$topic,
			'wallpostBrief'=>$brief,
			'wallpostInterests'=>$interests,
			'userUidOriginal'=>$author_uid_source,
			'wallpostUidOriginal'=>$wallpost_uid_source,
            'wallpostTemp'=>$wallpost_temp,
			'wallpostTimeCreate'=>_SYS_DATETIME,
		);
		DB::add($this->dbid,$this->table_name,$data,FALSE);
        error_log( "type= " . $type);
		if($type==1){
			$this->article_add($wallpost_uid,$topic,$content);
		}
		else{
			$this->writing_add($wallpost_uid,$content);
		}

        if( $wallpost_temp == 0){		
		    ///使用者個人紀錄 (先算event)
		    $record_data=array(
			    'uparameterNumPosts' => array('uparameterNumPosts+1'),
		    );
		    CZ::model('users')->uparameters_update($user_uid,$record_data);
        }
		
		return $wallpost_uid;
	}

    //動態修改
    public function post_update($wallpost_uid,$user_uid,$wallpostType,$public,$cover_image,$topic,$content,$interests){
        if(is_array($interests))
            $interests=join(';',$interests);
        
        $brief=strip_tags($content);
        if(mb_strlen($brief)>240)
            $brief=mb_substr($brief,0,239).'...';
        
        if(mb_strlen($content)>250)
            $newType=1; //articles
        else
            $newType=0;

        $data=array(
            'wallpostType'=>$newType,
            'wallpostPublic'=>$public,
            'imageUid'=>$cover_image,
            'wallpostTopic'=>$topic,
            'wallpostBrief'=>$brief,
            'wallpostInterests'=>$interests,
        );
        DB::update($this->dbid,$this->table_name,$data,array('WHERE wallpostUid=:wallpostUid AND userUid=:userUid',array('wallpostUid'=>$wallpost_uid,'userUid'=>$user_uid,))); 

        if($newType==1){
            if( $newType == $wallpostType) 
                $this->article_update($wallpost_uid,$topic,$content);
            else{
                $this->article_add($wallpost_uid,$topic,$content); //文章短變長
                $this->writing_delete($wallpost_uid);
            }    
        }
        else{
            if( $newType == $wallpostType)
                $this->writing_update($wallpost_uid,$content);
            else{
                $this->writing_add($wallpost_uid,$content); //文章長變短
                $this->article_delete($wallpost_uid);
            }
        }
    }

	public function writing_add($wallpost_uid,$content_source){

		include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
		$config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map);
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
		$purifier = new HTMLPurifier($config);
		$content = $purifier->purify($content_source);

		$data=array(
			'wallpostUid'=>$wallpost_uid,
			'writingContent'=>$content,
		);
		DB::add($this->dbid,'writings',$data,FALSE);
	}

    public function writing_update($wallpost_uid,$content_source){

        include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);        
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map); 
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
        $purifier = new HTMLPurifier($config);
        $content = $purifier->purify($content_source);

        $data=array(
            'writingContent'=>$content
        );
        DB::update($this->dbid,$this->writings_table_name,$data,array('WHERE wallpostUid=:wallpostUid',array('wallpostUid'=>$wallpost_uid)));
    }

    public function writing_delete($wallpost_uid){
        $where=array('WHERE wallpostUid=:wallpostUid',
                        array(  
                                'wallpostUid'=>$wallpost_uid,
                             ),
                    );

        return DB::del($this->dbid,$this->writings_table_name,$where );  
    }     

	public function article_add($wallpost_uid,$topic,$content_source){

		include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
		$config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);        
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map);
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
		$purifier = new HTMLPurifier($config);
		$content = $purifier->purify($content_source);

		$data=array(
			'wallpostUid'=>$wallpost_uid,
			'articleTopic'=>$topic,
			'articleContentSource'=>$content_source,
			'articleContent'=>$content
		);
		DB::add($this->dbid,'articles',$data,FALSE);
	}

    public function article_update($wallpost_uid,$topic,$content_source){

        include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);        
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map); 
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
        $purifier = new HTMLPurifier($config);
        $content = $purifier->purify($content_source);

        $data=array(
            'articleTopic'=>$topic,
            'articleContentSource'=>$content_source,
            'articleContent'=>$content
        ); 
        DB::update($this->dbid,$this->articles_table_name,$data,array('WHERE wallpostUid=:wallpostUid',array('wallpostUid'=>$wallpost_uid)));
    }

    public function article_delete($wallpost_uid){
        $where=array('WHERE wallpostUid=:wallpostUid',
                        array(  
                                'wallpostUid'=>$wallpost_uid,
                             ),
                    );

        return DB::del($this->dbid,$this->articles_table_name,$where );                               
    }



	/**
	 * mission_add 
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $public 
	 * @param mixed $cover_image 
	 * @param mixed $name 
	 * @param mixed $content 
	 * @param mixed $interests 
	 * @param mixed $type
	 * @param mixed $time_start 
	 * @param mixed $time_end 
	 * @param mixed $num_person 
	 * @param mixed $request 
	 * @param mixed $request_comment 
	 * @param mixed $award 
	 * @param mixed $award_image
	 * @param mixed $award_name 
	 * @param mixed $award_desc 
	 * @param mixed $time_lottery 
	 * @param mixed $link 
	 * @param uint $point missions.missionAwardPoint 獎項點數
	 * @access public
	 * @return void
	 */
	public function mission_add($user_uid,$public,$cover_image,$name,$content,$interests,$type,$time_start,$time_end,$num_person,$request,$request_comment,$award,$award_image,$award_name,$award_desc,$time_lottery,$link,$point=0,$wallpost_temp=0){
		if(!in_array($type,array(2,3,'lottery','pulling'))){
			return NULL;
		}
		$wallpost_uid=uid4();
		if(is_array($interests)){
			$interests=join(';',$interests);
		}
		$brief=strip_tags($content);
		if(mb_strlen($brief)>240){
			$brief=mb_substr($brief,0,239).'...';
		}
		if($type==2 || $type=='lottery'){$wallpostType=2;} 
		if($type==3 || $type=='pulling'){$wallpostType=3;}
		
		$data=array(
			'wallpostUid'=>$wallpost_uid,
			'userUid'=>$user_uid,
			'wallpostType'=>$wallpostType,
			'wallpostPublic'=>$public?1:0,
			'imageUid'=>$cover_image,
			'wallpostTopic'=>$name,
			'wallpostBrief'=>$brief,
			'wallpostInterests'=>$interests,
			'userUidOriginal'=>'',
			'wallpostUidOriginal'=>'',
            'wallpostTemp'=>$wallpost_temp,    
			'wallpostTimeCreate'=>_SYS_DATETIME,
		);
		DB::add($this->dbid,$this->table_name,$data,FALSE);

		include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
		$config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);        
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map); 
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
		$purifier = new HTMLPurifier($config);
		$content = $purifier->purify($content);

		$data=array(
			'wallpostUid'=>$wallpost_uid,
			'missionName'=>$name,
			'missionRequest'=>$request,
			'missionRequestComment'=>$request_comment,
			'missionNumPerson'=>$num_person,
			'missionAward'=>$award,
			'missionAwardPoint'=>$point,
			'imageUidAward'=>$award_image,
			'missionAwardName'=>$award_name,
			'missionAwardDescription'=>$award_desc,
			'missionTImeStart'=>$time_start,
			'missionTimeClose'=>$time_end,
			'missionTimeLottery'=>$time_lottery,
			'missionLink'=>$link,
			'missionContent'=>$content,
		);

		DB::add($this->dbid,'missions',$data,FALSE);
		
	    if( $wallpost_temp == 0){	
		    ///使用者個人紀錄 (先算event)
		    $record_data=array(
			    'uparameterNumPosts' => array('uparameterNumPosts+1'),
			    'uparameterNumEvents' => array('uparameterNumEvents+1'),
		    );
		    CZ::model('users')->uparameters_update($user_uid,$record_data);
        }

		return $wallpost_uid;
	}

    public function mission_update($wallpost_uid,$user_uid,$public,$cover_image,$name,$content,$interests,$type,$time_start,$time_end,$num_person,$request,$request_comment,$award,$award_image,$award_name,$award_desc,$time_lottery,$link,$point=0){
        if(!in_array($type,array(2,3,'lottery','pulling'))){
            return NULL;
        }
        if(is_array($interests)){
            $interests=join(';',$interests);
        }
        $brief=strip_tags($content);
        if(mb_strlen($brief)>240){
            $brief=mb_substr($brief,0,239).'...';
        }
        if($type==2 || $type=='lottery'){$wallpostType=2;} 
        if($type==3 || $type=='pulling'){$wallpostType=3;}
        
        $data=array(
            'wallpostType'=>$wallpostType,
            'wallpostPublic'=>$public,
            'imageUid'=>$cover_image,
            'wallpostTopic'=>$name,
            'wallpostBrief'=>$brief,
            'wallpostInterests'=>$interests,
        );

        DB::update($this->dbid,$this->table_name,$data,array('WHERE wallpostUid=:wallpostUid AND userUid=:userUid',array('wallpostUid'=>$wallpost_uid,'userUid'=>$user_uid   )));

        include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);        
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map); 
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
        $purifier = new HTMLPurifier($config);
        $content = $purifier->purify($content);

        $data=array(
            'missionName'=>$name,
            'missionRequest'=>$request,
            'missionRequestComment'=>$request_comment,
            'missionNumPerson'=>$num_person,
            'missionAward'=>$award,
            'missionAwardPoint'=>$point,
            'imageUidAward'=>$award_image,
            'missionAwardName'=>$award_name,
            'missionAwardDescription'=>$award_desc,
            'missionTImeStart'=>$time_start,
            'missionTimeClose'=>$time_end,
            'missionTimeLottery'=>$time_lottery,
            'missionLink'=>$link,
            'missionContent'=>$content,
        );
        DB::update($this->dbid,$this->missions_table_name,$data,array('WHERE wallpostUid=:wallpostUid',array('wallpostUid'=>$wallpost_uid )));    
    }

	/**
	 * event_add 
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $public 
	 * @param mixed $cover_image 
	 * @param mixed $name 
	 * @param mixed $content 
	 * @param mixed $interests 
	 * @param mixed $time_start 
	 * @param mixed $time_end 
	 * @param mixed $num_person 
	 * @param mixed $location 
	 * @param uint $price_personal 每個人應負擔金額
	 * @access public
	 * @return void
	 */
	public function event_add($user_uid,$public,$cover_image,$name,$content,$interests,$time_start,$time_end,$num_person,$location,$price_personal=0,$wallpost_temp=0){
		$wallpost_uid=uid4();
		if(is_array($interests)){
			$interests=join(';',$interests);
		}
		$brief=strip_tags($content);
		if(mb_strlen($brief)>240){
			$brief=mb_substr($brief,0,239).'...';
		}
		$data=array(
			'wallpostUid'=>$wallpost_uid,
			'userUid'=>$user_uid,
			'wallpostType'=>4,
			'wallpostPublic'=>$public?1:0,
			'imageUid'=>$cover_image,
			'wallpostTopic'=>$name,
			'wallpostBrief'=>$brief,
			'wallpostInterests'=>$interests,
			'userUidOriginal'=>'',
			'wallpostUidOriginal'=>'',
            'wallpostTemp'=>$wallpost_temp,    
			'wallpostTimeCreate'=>_SYS_DATETIME,
		);
		DB::add($this->dbid,$this->table_name,$data,FALSE);

		include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
		$config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);        
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map);         
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
		$purifier = new HTMLPurifier($config);
		$content = $purifier->purify($content);

		$data=array(
			'wallpostUid'=>$wallpost_uid,
			'eventName'=>$name,
			'eventTimeStart'=>$time_start,
			'eventTimeEnd'=>$time_end,
			'eventNumPerson'=>$num_person,
			'eventPricePersonal'=>$price_personal,
			'eventContent'=>$content,
			'eventLocation'=>$location,
		);
		DB::add($this->dbid,'events',$data,FALSE);
	
        if( $wallpost_temp == 0){	
		    ///使用者個人紀錄
		    $record_data=array(
			    'uparameterNumPosts' => array('uparameterNumPosts+1'),
			    'uparameterNumEvents' => array('uparameterNumEvents+1'),
		    );
		    CZ::model('users')->uparameters_update($user_uid,$record_data);
        }

		return $wallpost_uid;
	}

    public function event_update($wallpost_uid,$user_uid,$public,$cover_image,$name,$content,$interests,$time_start,$time_end,$num_person,$location,$price_personal=0){
        if(is_array($interests)){
            $interests=join(';',$interests);
        }
        $brief=strip_tags($content);
        if(mb_strlen($brief)>240){
            $brief=mb_substr($brief,0,239).'...';
        }
        $data=array(
            'wallpostType'=>4,
            'wallpostPublic'=>$public,
            'imageUid'=>$cover_image,
            'wallpostTopic'=>$name,
            'wallpostBrief'=>$brief,
            'wallpostInterests'=>$interests,
        );

        DB::update($this->dbid,$this->table_name,$data,array('WHERE wallpostUid=:wallpostUid AND userUid=:userUid',array('wallpostUid'=>$wallpost_uid,'userUid'=>$user_uid   )));

        include(_DIR_FRAMEWORK.'include/htmlpurifier/HTMLPurifier.auto.php');
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.SafeIframe', true);        
        $config->set('URI.SafeIframeRegexp',$this->htmlpurifier_allow_youtube_map); 
        $config->set('Attr.AllowedFrameTargets', array('_blank'));
        $purifier = new HTMLPurifier($config);
        $content = $purifier->purify($content);

        $data=array(
            'wallpostUid'=>$wallpost_uid,
            'eventName'=>$name,
            'eventTimeStart'=>$time_start,
            'eventTimeEnd'=>$time_end,
            'eventNumPerson'=>$num_person,
            'eventPricePersonal'=>$price_personal,
            'eventContent'=>$content,
            'eventLocation'=>$location,
        );
        DB::update($this->dbid,$this->events_table_name,$data,array('WHERE wallpostUid=:wallpostUid',array('wallpostUid'=>$wallpost_uid )));
    }	
	
	/**
	 * user_wallpost_eventover 
	 * 結算參加活動的名單(純結算不給獎勵)
	 * 
	 * @param mixed $wallpost_uid 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 *
	 * rule 1 先過濾符合的參加者
	 * rule 2 再過濾送參一腳點數的名額  (取剛好的名額數 然後送點數給他) 
	 * rule finsh   送參一腳點數的送剛好名額數即可,送商品的彩排名序 (因為有人不會領獎)
	 */
	public function user_wallpost_eventover($wallpost_uid,$active,$game_rule){
		
		$query=array(
			'select'=> '*',
			'from'=>	'userwallpostjoin',
			'where'=> array( 'wallpostUid=:wallpost_uid'
			),
			'bind'=> array(
				':wallpost_uid' => $wallpost_uid,
			),
			'order' => 'joinReward DESC',
		);
		
		if($active=='mission'){$query['where'][0].=' AND eventComplete=1 ';}
		
		$people=DB::data($this->dbid,$query);
		////亂數抽獎
		if($people['args']['num']>0){
			$list=$people['d'];
			shuffle($list);
		}
		////過濾抽獎名額
		if($people['args']['num']>0){
			if($active=='mission'){
				$i=0;
				foreach($list as $tmpinx => $new_data){
					if($game_rule['missionAward']==1){
						if($i >= $game_rule['missionNumPerson'])
						{
							unset($list[$tmpinx]);
						}
						$i++;
					}
				}
			}
		}
		$people['d']=$list;
		$people['args']['num']=count($list);
		
		
		if($people['args']['num']>0){
			
			$i=1;
			foreach($people['d'] as $new_data){
				$joinid=$new_data['joinid'];
				$sql_data=array(
					'joinReward' =>	$i,
				);
				DB::update($this->dbid,'userwallpostjoin',$sql_data,array('WHERE joinid=:joinid',array(':joinid'=>$joinid)));
				$i++;
			}
			return $people;
		}
	}
	/**
	 * user_wallpost_join_get 
	 * 
	 * 
	 * @param mixed $wallpost_uid 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function user_wallpost_join_get($user_uid,$wallpost_uid){
		
		$query=array(
			'select'=> '*',
			'from'=>	'userwallpostjoin',
			'where'=> array( 'userUid=:user_uid AND wallpostUid=:wallpost_uid'
			),
			'bind'=> array(
				':user_uid' => $user_uid,
				':wallpost_uid' => $wallpost_uid,
			),
		);
		
		
		return DB::row($this->dbid,$query);
	}
	
	/**
	 * user_wallpost_join_list 
	 * 
	 * 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function user_wallpost_join_list($wallpost_uid,$order=''){
		
		$query=array(
			'select'=> '*',
			'from'=>	'userwallpostjoin',
			'where'=> array( 'wallpostUid=:wallpost_uid'
			),
			'bind'=> array(
				':wallpost_uid' => $wallpost_uid,
			),
		);
		
		if($order=='reward'){
			$query['where'][0].=' AND joinReward>0 ';
			$query['order']=' joinReward ASC';
		}
		else{
			$query['order']=' RAND() ';
		}
		
	
		
		return DB::data($this->dbid,$query);
	}
	
	/**
	 * user_wallpost_join 
	 * 
	 * 
	 * @param mixed $wallpost_uid 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function user_wallpost_join($user_uid,$wallpost_uid){
		
		$data=array(
			'joinid'=>uid4(),
			'userUid'=>$user_uid,
			'wallpostUid'=>$wallpost_uid,
			'joinReward'=>0,
			'joinTime'=>_SYS_DATETIME,
	
		);
		return DB::add($this->dbid,'userwallpostjoin',$data);
	}
	
	/**
	 * user_wallpost_fbshare 
	 * 
	 * 
	 * @param mixed $wallpost_uid 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function user_wallpost_fbshare($user_uid,$wallpost_uid){
		
		$data=array(
			'fbshareid'=>uid4(),
			'userUid'=>$user_uid,
			'wallpostUid'=>$wallpost_uid,
			'share'=>1,
			'fbshareCreateTime'=>_SYS_DATETIME,
	
		);
		return DB::add($this->dbid,'fbshare_log',$data);
	}
	

	/**
	 * user_wallpost 
	 * 查詢 uwallpost 的 userUid 和 wallpostUid 狀態
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function user_wallpost($user_uid,$wallpost_uid){
		$query=array(
			'select'=>'*',
			'from'=>'uwallposts',
			'where'=>array(
				'userUid=:user_uid AND wallpostUid=:wallpost_uid',
			),
			'bind'=>array(
				':user_uid'=>ME::user_uid(),
				':wallpost_uid'=>$wallpost_uid,
			)
		);
		return DB::row($this->dbid,$query);
	}
	/**
	 * user_walltrack
	 * 使用者在單篇文上的足跡
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function user_walltrack($user_uid,$wallpost_uid){
		$query=array(
			'select'=>'*',
			'from'=>'uwallposts',
			'where'=>array(
				'userUid=:user_uid AND wallpostUid=:wallpost_uid',
			),
			'bind'=>array(
				':user_uid'=>ME::user_uid(),
				':wallpost_uid'=>$wallpost_uid,
			)
		);
		$data=DB::row($this->dbid,$query);
		$user_track=array();
		if($data){
			if($data['uwallpostStatus']&1){$user_track['dislike']=TRUE;}
			if($data['uwallpostStatus']&2){$user_track['like']=TRUE;}
			if($data['uwallpostStatus']&4){$user_track['comment']=TRUE;}
			if($data['uwallpostStatus']&8){$user_track['share']=TRUE;}
			if($data['uwallpostStatus']&16){$user_track['favor']=TRUE;}
			if($data['uwallpostStatus']&32){$user_track['abuse']=TRUE;}
		}
		
		
		return $user_track;
	}
	/**
	 * user_socials_track
	 * 使用者在指定日前7天留下的log紀錄
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function user_socials_track($user_uid,$assign_date){
		
		$assign_sdate=date("Y-m-d",strtotime($assign_date." -7days"));
		
		
		$query=array(
				'select'=>'*',
				'from'=>'wsocials',
				'where'=>array(
					'userUid=:user AND ( wsocialTime BETWEEN :assign_sdate AND :assign_date ) AND (wsocialType<>11 OR wsocialType<>12) ',
				),
				'order' => 'wsocialTime desc',
				'bind'=>array(
					':user'=>$user_uid,
					':assign_sdate'=>$assign_sdate,
					':assign_date'=>$assign_date,
				)
		);
		$uwallpost_socials=DB::data($this->dbid,$query);
		
		
		////先單純倒大資料流
		foreach($uwallpost_socials['d'] as $uwallpost_socials_data){
			if(empty($wallpost_data[$uwallpost_socials_data['wallpostUid']])){////減少sql去讀同篇文章
				$wallpost_data[$uwallpost_socials_data['wallpostUid']]=CZ::model('wallposts')->wallpost_get($uwallpost_socials_data['wallpostUid']);
			}
			$uwallpost_socials_data['wallpost']=$wallpost_data[$uwallpost_socials_data['wallpostUid']];
			$tdate=date("Y-m-d",strtotime($uwallpost_socials_data['wsocialTime']));
			$tmp[$tdate][]=$uwallpost_socials_data;
		}
		
		////細部處理
		if(!empty($tmp)){
			foreach($tmp as $date => $date_info){
				if(!empty($date_info)){
					foreach($date_info as $date_inx => $date_list){
						switch($date_list['wsocialType']){
							case 0:$wsocialType='comment';break;
							case 1:$wsocialType='like';break;
							case 2:$wsocialType='dislike';break;
							case 3:$wsocialType='share';break;
							case 4:$wsocialType='favor';break;
							case 5:$wsocialType='abuse';break;
							default:$wsocialType='';break;
						}
						$tmp[$date][$date_inx]['wsocialTypeText']=$wsocialType;
					}
				}
			}
		}
		
		$socials_track=&$tmp;
		
		return $socials_track;
	}
	
	
	
	/**
	 * give_user_event_join
	 * 使用者完成活動的進度
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function give_user_event_join($user_uid,$wallpost_uid){
		$wallpost_data=$this->user_join_progressing($user_uid,$wallpost_uid);
		
		if(!empty($wallpost_data['wallpost_join'])){
			if($wallpost_data['wallpost_join']['eventComplete']==0){
				////確認是否完成活動條件
				if($wallpost_data['wallpost_extend']['missionComplete']['pass']>=$wallpost_data['wallpost_extend']['missionComplete']['need']){
					////完成活動的給10點
					$give_point=10;
					$note_data=array(
						'wallpost_topic' => html($wallpost_data['wallpostTopic'],false),
						'point' => $give_point,
					);
					$note="達成活動條件，點數增加".$give_point."點";
					
					$edate=date('Y-12-31 23:59:59',strtotime(_SYS_DATETIME.' +1 year '));
					$data=array(
							'upointUid'	=> uid4(),
							'userUid' => $user_uid,
							'userPointEarn' => $give_point,
							'userPointUsed' => 0,
							'userPoint' => $give_point,
							'userPointTimeLimit' => $edate,
							'upointlogTimeEarn' => _SYS_DATETIME,
							'upointTable' => 'wallposts',
							'upointTableId' => $wallpost_uid,
							'upointlogNote' => $note,
					);
					CZ::model('users')->upointlogs_add($data);
					
					
					
					///登記使用者完成活動條件
					$sql_data=array(
						'eventComplete' =>	1,
					);
						
					DB::update('common','userwallpostjoin',$sql_data,array('WHERE userUid=:user_uid AND wallpostUid=:wallpost_uid',array(':user_uid'=>$user_uid,':wallpost_uid'=>$wallpost_uid)));
					
					
					///使用者點數紀錄
					$record_data=array(
						'userPoint' => array('userPoint+'.$give_point),
					);
					CZ::model('users')->user_update($user_uid,$record_data);
					
					////通知
					$from_uid='countmin-system';
					$to_uid=$user_uid;
					$target_wuid=$wallpost_data['wallpostUid'];
					$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
					$notice_id='Ly_arrive_event_condition';
					
					$notice_data=array(
							'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
							'point' => $give_point,
							
					);
					$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
					if($notificationSetting['recommendActivity']==1){
								$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
					}
				}
			}
		}
	
	}
	
	
	
	
	/**
	 * user_join_progressing
	 * 使用者完成活動的進度
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function user_join_progressing($user_uid,$wallpost_uid){
		
		
		$query=array(
			'select'=>'*',
			'from'=>'userwallpostjoin',
			'where'=>array(
				'userUid=:user_uid AND wallpostUid=:wallpost_uid  ',
			),
			'bind'=>array(
				':user_uid'=>$user_uid,
				':wallpost_uid'=>$wallpost_uid,
			)
		);
	
		$uwallpost_join=DB::row($this->dbid,$query);
				
		$wallpost_data=$this->wallpost_get($wallpost_uid);
		
		
		
		if(!empty($wallpost_data) &&  !empty($uwallpost_join)){
			
			
			$wallpost_data['wallpost_join']=$uwallpost_join;
			
			$join_tmp='';
			////missions 有複合的任務條件
			if($wallpost_data['wallpostType']>=2 && $wallpost_data['wallpostType']<=3){
				$max_need_count=1; /////任務條件,按下參加算1個
				$self_pass_count=1;	/////已完成任務條件,按下參加算1個
				if($wallpost_data['wallpost_extend']['missionRequest'] & 1){
					$query1=array(
							'select'=>'*',
							'from'=>'fbshare_log',
							'where'=>array(
								'userUid=:user_uid AND wallpostUid=:wallpost_uid ',
							),
							'bind'=>array(
								':user_uid'=>$user_uid,
								':wallpost_uid'=>$wallpost_data['wallpostUid'],
							)
					);
					$fb_join=DB::row($this->dbid,$query1);
					$max_need_count++;
					if(!empty($fb_join)){$self_pass_count++;$join_tmp['fb_share']=1;}
				}
				if($wallpost_data['wallpost_extend']['missionRequest'] & 2){
					$query1=array(
							'select'=>'*',
							'from'=>'wsocials',
							'where'=>array(
								'userUid=:user AND wallpostUid=:wallpost_uid  AND wsocialType=1 ',
							),
							'bind'=>array(
								':user'=>$user_uid,
								':wallpost_uid'=>$wallpost_data['wallpostUid'],
							)
					);
					$wallpost_like=DB::row($this->dbid,$query1);
					$max_need_count++;
					if(!empty($wallpost_like)){$self_pass_count++;$join_tmp['plus_1']=1;}
				}
				if($wallpost_data['wallpost_extend']['missionRequest'] & 4){
					
					$key_wrod=$wallpost_data['wallpost_extend']['missionRequestComment'];
					$query1=array(
							'select'=>'*',
							'from'=>'wcomments',
							'where'=>array(
								'userUid=:user_uid AND wallpostUid=:wallpost_uid  AND wsocialType=1 AND  INSTR(wcommentHtml,:key_word)>0  ',
							),
							'bind'=>array(
								':user_uid'=>$user_uid,
								':wallpost_uid'=>$wallpost_data['wallpostUid'],
								':key_word'=>$key_wrod,
							)
					);
					$wallpost_comment_word=DB::row($this->dbid,$query1);
					$max_need_count++;
					if(!empty($wallpost_comment_word)){$self_pass_count++;$join_tmp['comment']=1;}
				}
				
				$wallpost_data['wallpost_extend']['missionComplete']['need']=$max_need_count;
				$wallpost_data['wallpost_extend']['missionComplete']['pass']=$self_pass_count;
				$wallpost_data['wallpost_extend']['missionComplete']['detail']=$join_tmp;
				$wallpost_data['wallpost_extend']['missionComplete']['detail']['join']=1;
			}
			else{	////揪團無須條件,只要參加就有
				$wallpost_data['wallpost_extend']['missionComplete']['need']=1;
				$wallpost_data['wallpost_extend']['missionComplete']['pass']=1;
				$wallpost_data['wallpost_extend']['missionComplete']['detail']['join']=1;
			}
			return $wallpost_data;
		}
	
	}
	
	
	
	/**
	 * user_event_join_track
	 * 使用者在指定日前7天留下的參加活動log紀錄
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function user_event_join_track($user_uid,$assign_date){
		
		$assign_sdate=date("Y-m-d",strtotime($assign_date." -7days"));

		$query=array(
				'select'=>'*',
				'from'=>'userwallpostjoin',
				'where'=>array(
					'userUid=:user AND ( joinTime BETWEEN :assign_sdate AND :assign_date ) ',
				),
				'order' => 'joinTime desc',
				'bind'=>array(
					':user'=>$user_uid,
					':assign_sdate'=>$assign_sdate,
					':assign_date'=>$assign_date,
				)
		);

		$uwallpost_join=DB::data($this->dbid,$query);

		if($uwallpost_join['args']['num'] >0 ){
			foreach($uwallpost_join['d'] as $uwallpost_join_data){
				
				$join_data=$this->user_join_progressing($uwallpost_join_data['userUid'],$uwallpost_join_data['wallpostUid']);
				
				
				
				$tdate=date("Y-m-d",strtotime($uwallpost_join_data['joinTime']));
				$tmp[$tdate][]=$join_data;
			}
			
			$track=&$tmp;
			return $track;
		}
		
		
	}
	

    public function wsocials_count_getter($user_uid,$startDate,$wsocialType='',$groupby=''){
        $endDate=date("Y-m-d");

        $query=array(
            'select' => 'wsocialType ,count(*)' ,
            'from'   => $this->wsocials_table_name ,
            'where'  => array('userUid=:userUid  AND  wsocialTime >= :startDate AND  wsocialTime <= :endDate ' , 
                            array(
                                    'userUid'=>$user_uid,
                                    'startDate'=>$startDate,
                                    'endDate'=>$endDate,
                                    ),
                    ),
            'group' => array('wsocialType') ,    
        );                                                                 

        if(!empty($wsocialType)){
            $query['where'][0] .= ' AND wsocialType IN ('. $wsocialType.' ) ';
        }

        if(!empty($groupby)){
            $query['select']='wsocialTime ,count(*)';
            $query['group'][0]=$groupby;
        }

        return DB::data($this->dbid,$query);        

    }	
	
	

	/**
	 * plus_1
	 * 使用者 $user_uid 對 $wallpost_uid +1
	 * 處理+1之後給分給點，判斷是否已經+-1過，但不判斷是否可以+1，也不處理通知
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function plus_1($user_uid,$wallpost_uid,$wallpost_user_uid){
		$status_record=$this->uwallpost_status_replace($user_uid,$wallpost_uid,2);
		if($status_record==TRUE){
			
			///使用者個人紀錄
			$record_data=array(
				'uparameterNumLikes' => array('uparameterNumLikes+1'),
			);
			CZ::model('users')->uparameters_update($user_uid,$record_data);
			
			
			
			$data=array(
				'wallpostNumLikes'=>array('wallpostNumLikes+1'),
			);
			DB::update($this->dbid,'wallposts',$data,array('WHERE wallpostUid=:wallpost',array(':wallpost'=>$wallpost_uid)));
			
			
		}
		return $status_record;
	}

	/**
	 * minus_1
	 * 使用者 $user_uid 對 $wallpost_uid -1
	 * 處理-1之後減分減點，判斷是否已經+-1過，但不判斷是否可以-1，也不處理通知
	 * uwallpostStatus 狀態 bit:0:有-1, 1:有+1, 2:有comment過, 3:曾經share, 4:曾經收藏 5:檢舉
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function minus_1($user_uid,$wallpost_uid,$wallpost_user_uid){

		$status_record=$this->uwallpost_status_replace($user_uid,$wallpost_uid,1);
		if($status_record==TRUE){

			///使用者個人紀錄
			$record_data=array(
				'uparameterNumDislikes' => array('uparameterNumLikes+1'),
			);
			CZ::model('users')->uparameters_update($user_uid,$record_data);
			
			
			$data=array(
				'wallpostNumDislikes'=>array('wallpostNumDislikes+1'),
			);
			DB::update($this->dbid,'wallposts',$data,array('WHERE wallpostUid=:wallpost',array(':wallpost'=>$wallpost_uid)));
		}
		
		
		
		return $status_record;
	}

	/**
	 * comment_add 
	 * 在 wallpost 上留言
	 * 不處理權限以及是否可以留言
	 *
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @param mixed $comment 
	 * @access public
	 * @return void
	 */
	public function comment_add($user_uid,$wallpost_uid,$wallpost_user_uid,$comment){
		
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);


		$comment_uid=uid4();
		$data=array(
			'wcommentUid'=>$comment_uid,
			'userUid'=>$user_uid,
			'wallpostUid'=>$wallpost_uid,
			'wcommentHtml'=>$comment,
			'wcommentTime'=>_SYS_DATETIME,
	
		);
		DB::add($this->dbid,'wcomments',$data);
		$status_record=$this->uwallpost_status_replace($user_uid,$wallpost_uid,4);
		if($status_record==TRUE){
			
			
			
			$data=array(
				'wallpostNumComments'=>array('wallpostNumComments+1'),
			);
			DB::update($this->dbid,'wallposts',$data,array('WHERE wallpostUid=:wallpost',array(':wallpost'=>$wallpost_uid)));
		}
		
		return $comment_uid;
	}
	/**
	 * user_share
	 * 在 wallpost 上分享
	 * 不處理權限
	 *
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	public function user_share($user_uid,$public,$wallpost_data,$msg){
		
		$share_wallpost_uid=$this->post_add($user_uid,$public,'',$wallpost_data['wallpostTopic'],$msg,$wallpost_data['wallpostInterests'],$wallpost_data['userUid'],$wallpost_data['wallpostUid']);
		
		if($share_wallpost_uid){
			$status_record=$this->uwallpost_status_replace($user_uid,$wallpost_data['wallpostUid'],8);
			if($status_record==TRUE){
				///使用者個人紀錄 (會有重複紀錄 但先放這)
				$record_data=array(
					'uparameterNumDislikes' => array('uparameterNumShares+1'),
				);
				CZ::model('users')->uparameters_update($user_uid,$record_data);
				
				
			}
			////先不寫入$status_record==TRUE內.....情況有點複雜 XDDDD
			$data=array(
				'wallpostNumShares'=>array('wallpostNumShares+1'),
			);
			DB::update($this->dbid,'wallposts',$data,array('WHERE wallpostUid=:wallpost',array(':wallpost'=>$wallpost_data['wallpostUid'])));
			return $status_record;
		}
		
		return FALSE;
	}
	/**
	 * user_favor 
	 * 在 wallpost 上收藏
	 * 不處理權限以及是否可以收藏
	 *
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @access public
	 * @return void
	 */
	/*
	public function user_favor($user_uid,$wallpost_uid,$wallpost_user_uid,$wallpost_type){
		$data=array(
			'userUid'=>	$user_uid,
			'wallpostUid'=> $wallpost_uid,
			'userUidWallpost'	=> $wallpost_user_uid,
			'wallpostType'=> $wallpost_type,
		);

		$favorsadd=CZ::model('ufavors')->ufavors_add($data);
		
		
		
		if($favorsadd){
			///使用者個人紀錄 (會有重複紀錄 但先放這)
			$record_data=array(
				'uparameterNumFavors' => array('uparameterNumFavors+1'),
			);
			CZ::model('users')->uparameters_update($user_uid,$record_data);
			
			$status_record=$this->uwallpost_status_replace($user_uid,$wallpost_uid,16);
			
			return $status_record;
		}
		
		return FALSE;
	}*/
	/**
	 * uwallpost_status_replace 
	 * 興趣牆
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $wallpost_uid 
	 * @param mixed $uwallpostStatus
	 * @access public
	 * @return void
	 */
	public function uwallpost_status_replace($user_uid,$wallpost_uid,$uwallpostStatus){
		$query=array(
				'select'=>'*',
				'from'=>'uwallposts',
				'where'=>array(
					'userUid=:user AND wallpostUid=:wallpost',
				),
				'bind'=>array(
					':user'=>$user_uid,
					':wallpost'=>$wallpost_uid,
				)
		);
		$uwallpost=DB::row($this->dbid,$query);
		if($uwallpost===NULL){
			$data=array(
				'userUid'=>$user_uid,
				'wallpostUid'=>$wallpost_uid,
				'uwallpostStatus'=>$uwallpostStatus,
				'uwallpostTimeLastUpdate'=>_SYS_DATETIME
			);
			DB::add($this->dbid,'uwallposts',$data);
			return TRUE;
		}
		else{
			
			if($uwallpostStatus==1 || $uwallpostStatus==2)
			{
				if(($uwallpost['uwallpostStatus']&1) || ($uwallpost['uwallpostStatus']&2))	//+1,-1 不能修改狀態
				{
					return FALSE;
				}
			}

			$data=array(
				'uwallpostStatus'=>array('uwallpostStatus | '.$uwallpostStatus),
				'uwallpostTimeLastUpdate'=>_SYS_DATETIME,
			);
			DB::update($this->dbid,'uwallposts',$data,array('WHERE userUid=:user AND wallpostUid=:wallpost',array(':user'=>$user_uid,':wallpost'=>$wallpost_uid)));
			return TRUE;
		}
		return FALSE;
	}
	/**
	 * uwallpost_socials_add  "+1,-1,share,檢舉紀錄"
	 * 興趣牆
	 * 
	 * @param mixed $user_uid 應該會是 ME::user_id();
	 * @param mixed $wallpost_uid 對指定一篇文章做了哪些事 ("+1,-1,share,檢舉紀錄")
	 * @param mixed $uwallpostStatus 同上指定文章作者是誰
	 * @param mixed $wsocial_type 類別 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
	 * @param mixed $wsocial_type 此動作獲得的點數
	 * @access public
	 * @return void
	 */
	public function uwallpost_socials_add($user_uid,$wallpost_uid,$wallpost_user_uid,$wsocial_type,$wsocial_point=0){
		
		$wscocial_uid=uid4();
		$data=array(
			'wscocialUid'=> $wscocial_uid,
			'userUid'=> $user_uid,
			'wallpostUid'=> $wallpost_uid,
			'userUidWallpostAuthor'=> $wallpost_user_uid,
			'wsocialType'=> $wsocial_type,
			'wsociaPoints'=> $wsocial_point,
			'wsocialTime'=> _SYS_DATETIME,
		);
		DB::add($this->dbid,'wsocials',$data);
		return $wscocial_uid;
	}
	
	

	/**
	 * my_watch 
	 * 興趣牆
	 * 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function my_watch($user_uid){



	}

	/**
	 * my_wallpost 
	 * 我的興趣牆，取得自己發表、朋友發表(設為公開)、同好發表(設為公開)的 wallposts
	 * 35000*10% - 30000*5% = 3500 - 1500 = 2000
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $assign_date 
	 * @param mixed $before_or_after 
	 * @param mixed $wall_type 	興趣牆分類 無-全部,active-0,1,event-2,3,4
	 * @param mixed $wall_interests 興趣牆興趣 同興趣分類(ps:目前單選項,但用,可複選項)
	 * @access public
	 * @return void
	 */
	public function my_wallposts($user_uid,$assign_date,$before_or_after,$wall_type='',$wall_interests=''){
		
		
		$query=array(
			'select'=>'*',
			'from'=>$this->table_name.' w',
			'where'=>array(
				'(w.userUid=:uid OR w.userUid IN (SELECT r.userUid2 FROM relations r WHERE  ( (r.relation & 16) OR (r.relation & 32))  AND r.userUid1=:uid) )  AND 	(w.wallpostTemp=0 AND w.wallpostPublic=0 OR w.wallpostPublic=2) AND w.wallpostTimeCreate'.($before_or_after?'<=':'>=').':date',
			),
			'group'=>array(
				'w.wallpostUid',
			),
			'order'=>array(
				'w.wallpostTimeCreate '.($before_or_after?'DESC':'ASC'),
			),
			'bind'=>array(
				':uid'=>$user_uid,
				':date'=>$assign_date,
			),
			'limit'=>20,
		);
		
		if($wall_type!=''){
			if($wall_type=="event"){$wallpostType='2,3,4';}
			else{$wallpostType='0,1';}
			$query['where'][0].='  AND w.wallpostType in ('.$wallpostType.')';
			
		}
		if($wall_interests!=''){
			$tmp=explode(";",$wall_interests);
			if(!empty($tmp)){
				$wallpostInterests='';
				foreach($tmp as $inx => $interest){
					$wallpostInterests.=empty($wallpostInterests) ? ' INSTR(w.wallpostInterests,:interest'.$inx.')>0 ' :'  OR INSTR(w.wallpostInterests,:interest'.$inx.')>0 ';
					$query['bind'][':interest'.$inx]=$interest;
				}
			}
			$query['where'][0].='  AND ('.$wallpostInterests.')';
			
		}
		$data=DB::data($this->dbid,$query);
		
		$cover_images=array();
		$users=array();

		if($data['args']['num']>0){

			foreach($data['d'] as $inx => $data_info){
				///減少sql讀取
				$xuser[$data_info['userUid']]=empty($xuser[$data_info['userUid']]) ?  CZ::model('users')->xuser($data_info['userUid']) : $xuser[$data_info['userUid']] ;
				$users[$inx]=&$xuser[$data_info['userUid']];
				$data['d'][$inx]['userRealname']=$users[$inx]['basic']['userRealname'];
                $data['d'][$inx]['userProfile']=$users[$inx]['basic']['userProfile'];
				$data_info['imageUrl']=empty($data_info['imageUid']) ? '' : CZ::model('images')->imagePath_getter($data_info['imageUid'],'_w');
				if($data_info['imageUrl']==''){
					list($tmpInterest)=explode(";",$data_info['wallpostInterests']);
					if(empty($tmpInterest)){
						$allInterest=CZ::model('interests')->all_getter();
						$rnd=mt_rand ( 0, count($allInterest['d'])-1 );
						$ii=0;
						foreach($allInterest as $key => $val){
							$tmpInterest=$key;
							if($rnd==$ii){break;}
						}
					}
					$data['d'][$inx]['imageUrl']='/css/img/'.$tmpInterest.'.png';
				}
				else{
					$data['d'][$inx]['imageUrl']=$data_info['imageUrl'];
				}
				
				$data['d'][$inx]['uwallpost_track']=$this->user_walltrack(ME::user_uid(),$data_info['wallpostUid']);
				$data['d'][$inx]['wallpost_extend']=$this->wallpost_extend($data_info['wallpostUid'],$data_info['wallpostType']);
				$data['d'][$inx]['wallpost_user']=$users[$inx];
				$defaultPhoto_s='/css/img/default_profile_pic_s.png';
				$user_img_url=CZ::model('images')->imagePath_getter($users[$inx]['basic']['userProfile'],'_s');
				$data['d'][$inx]['wallpost_user_img_url']=($user_img_url) ? $user_img_url : $defaultPhoto_s;
				
				if(!empty($data['d'][$inx]['wallpostUidOriginal'])){$data['d'][$inx]['wallpost_share']=$this->wallpost_get($data['d'][$inx]['wallpostUidOriginal']); }
				
				/////以下這段都擺在程式最後段 (方便處理share文)
				/////wallpost_view 才是處理過的顯示文
				$data['d'][$inx]['wallpost_view']=empty($data['d'][$inx]['wallpost_share']) ? $data['d'][$inx] : $data['d'][$inx]['wallpost_share'];
				
			}
			
		}

		
		return $data;
	}

	/**
	 * my_posts 
	 * 
	 * @param mixed $user_uid 指定的 userUid
	 * @param mixed $assign_date 指定日期
	 * @param mixed $before_or_after 指定日期前或後 0:後 1:前
	 * @access public
	 * @return void
	 */
	public function my_posts($user_uid,$assign_date,$before_or_after,$wall_type=''){
		
		
		if(ME::user_uid()==$user_uid){
			$query=array(
				'select'=>'',
				'from'=>$this->table_name,
				'where'=>array(
					 'wallpostTimeCreate'.($before_or_after?'<=':'>=').':date AND userUid=:user AND wallpostTemp =0',
					//'userUid=:user',
					array(
						':user'=>$user_uid,
						':date'=>$assign_date
					)
				), 
				'order'=>'wallpostTimeCreate DESC',
				'limit'=>20,
			);
		}
		else{
			$relations=CZ::model('users')->relations_getter(ME::user_uid(),$user_uid);
			$query=array(
				'select'=>'',
				'from'=>$this->table_name,
				'where'=>array(
					'wallpostTimeCreate'.($before_or_after?'<=':'>=').':date AND userUid=:user AND wallpostTemp=0',
					//'userUid=:user',
					array(
						':user'=>$user_uid,
						':date'=>$assign_date
					)
				), 
				'order'=>'wallpostTimeCreate DESC',
				'limit'=>20,
			);
			
			
			$relation_str='';
			/*if(!empty($relations['relation_status']['follow'])){
				$relation_str=' OR wallpostPublic=1 ';
			}*/
			if(!empty($relations['relation_status']['friend'])){
				$relation_str=' OR wallpostPublic=2 ';
			}
			$query['where'][0].='  AND 	(wallpostPublic=0 '.$relation_str.' )   ';
			
			
		}
		
		if($wall_type!=''){
			if($wall_type=="event"){$wallpostType='2,3,4';}
			else{$wallpostType='0,1';}
			$query['where'][0].='  AND wallpostType in ('.$wallpostType.')';
			
		}

		$data=DB::data($this->dbid,$query);
		$cover_images=array();
		$users=array();

		if($data['args']['num']>0){
			foreach($data['d'] as $inx => $data_info){
				///減少sql讀取
				$xuser[$data_info['userUid']]=empty($xuser[$data_info['userUid']]) ?  CZ::model('users')->xuser($data_info['userUid']) : $xuser[$data_info['userUid']] ;
				$users[$inx]=&$xuser[$data_info['userUid']];
				$data['d'][$inx]['userRealname']=$users[$inx]['basic']['userRealname'];
				$data_info['imageUrl']=empty($data_info['imageUid']) ? '' : CZ::model('images')->imagePath_getter($data_info['imageUid'],'_w');
				if($data_info['imageUrl']==''){
					list($tmpInterest)=explode(";",$data_info['wallpostInterests']);
					if(empty($tmpInterest)){
						$allInterest=CZ::model('interests')->all_getter();
						$rnd=mt_rand ( 0, count($allInterest['d'])-1 );
						$ii=0;
						foreach($allInterest as $key => $val){
							$tmpInterest=$key;
							if($rnd==$ii){break;}
						}
					}
					$data['d'][$inx]['imageUrl']='/css/img/'.$tmpInterest.'.png';
				}
				else{
					$data['d'][$inx]['imageUrl']=$data_info['imageUrl'];
				}
				
				$data['d'][$inx]['uwallpost_track']=$this->user_walltrack(ME::user_uid(),$data_info['wallpostUid']);
				$data['d'][$inx]['wallpost_extend']=$this->wallpost_extend($data_info['wallpostUid'],$data_info['wallpostType']);
				$defaultPhoto_s='/css/img/default_profile_pic_s.png';
				$user_img_url=CZ::model('images')->imagePath_getter($users[$inx]['basic']['userProfile'],'_s');
				$data['d'][$inx]['wallpost_user_img_url']=($user_img_url) ? $user_img_url : $defaultPhoto_s;
				
				if(!empty($data['d'][$inx]['wallpostUidOriginal'])){$data['d'][$inx]['wallpost_share']=$this->wallpost_get($data['d'][$inx]['wallpostUidOriginal']); }
				
				
				
				/////以下這段都擺在程式最後段 (方便處理share文)
				/////wallpost_view 才是處理過的顯示文
				$data['d'][$inx]['wallpost_view']=empty($data['d'][$inx]['wallpost_share']) ? $data['d'][$inx] : $data['d'][$inx]['wallpost_share'];
				

			}
			
		}

		return $data;
	}
	
	/**
	 * my_wallposts_renew 
	 * 取得一則 wallpost 的最新更新
	 * 
	 * @param str $wallpost_uid_dot 
	 * @access public
	 * @return void
	 */
	public function my_wallposts_renew($user_uid,$wallpost_uid_dot){
			$wallpost_uid_ary=explode(",",$wallpost_uid_dot);
			$wallpost_uid_dot="";
			if(empty($wallpost_uid_ary)){return ;}
			else{
				foreach($wallpost_uid_ary as $wallpost_uid)
				{
					$wallpost_uid_dot.=empty($wallpost_uid_dot) ? '\''.$wallpost_uid.'\'' : ',\''.$wallpost_uid.'\'';
				}
			}
			$count=count($wallpost_uid_ary);
			/*
			$query=array(
				'select'=>'*',
				'from'=>$this->table_name.' w',
				'where'=>array(
				'(w.userUid=:uid OR w.userUid IN (SELECT r.userUid2 FROM relations r WHERE  ((r.relation & 8) OR (r.relation & 16) OR (r.relation & 32))  AND r.userUid1=:uid)) AND wallpostUid in ('.$wallpost_uid_dot.')',
				),
				'group'=>array(
					'w.wallpostUid',
				),
				'order'=>array(
					'w.wallpostTimeCreate DESC',
				),
				'bind'=>array(
					':uid'=>$user_uid,
				),
				'limit'=>$count, 
			);*/
			
			
			$query=array(
				'select' => ' * ',
				'from' => $this->table_name,
				'where' => array(
					'wallpostUid in ('.$wallpost_uid_dot.') ',
				)
			);
			$data=DB::data($this->dbid,$query);
			
			$cover_images=array();
			$users=array();
			if($data['args']['num']>0){
				foreach($data['d'] as $inx => $data_info){
				///減少sql讀取
				$xuser[$data_info['userUid']]=empty($xuser[$data_info['userUid']]) ?  CZ::model('users')->xuser($data_info['userUid']) : $xuser[$data_info['userUid']] ;
				$users[$inx]=&$xuser[$data_info['userUid']];
				$data['d'][$inx]['userRealname']=$users[$inx]['basic']['userRealname'];
				$data['d'][$inx]['xuser']=$users[$inx];
				$data_info['imageUrl']=empty($data_info['imageUid']) ? '' : CZ::model('images')->imagePath_getter($data_info['imageUid'],'_w');
				if($data_info['imageUrl']==''){
					list($tmpInterest)=explode(";",$data_info['wallpostInterests']);
					if(empty($tmpInterest)){
						$allInterest=CZ::model('interests')->all_getter();
						$rnd=mt_rand ( 0, count($allInterest['d'])-1 );
						$ii=0;
						foreach($allInterest as $key => $val){
							$tmpInterest=$key;
							if($rnd==$ii){break;}
						}
					}
					$data['d'][$inx]['imageUrl']='/css/img/'.$tmpInterest.'.png';
				}
				else{
					$data['d'][$inx]['imageUrl']=$data_info['imageUrl'];
				}
				
				$data['d'][$inx]['uwallpost_track']=$this->user_walltrack(ME::user_uid(),$data_info['wallpostUid']);
				$data['d'][$inx]['wallpost_extend']=$this->wallpost_extend($data_info['wallpostUid'],$data_info['wallpostType']);
				
				$defaultPhoto_s='/css/img/default_profile_pic_s.png';
				$user_img_url=CZ::model('images')->imagePath_getter($users[$inx]['basic']['userProfile'],'_s');
				$data['d'][$inx]['wallpost_user_img_url']=($user_img_url) ? $user_img_url : $defaultPhoto_s;
				
				if(!empty($data['d'][$inx]['wallpostUidOriginal'])){$data['d'][$inx]['wallpost_share']=$this->wallpost_get($data['d'][$inx]['wallpostUidOriginal']); }
				
				/////以下這段都擺在程式最後段 (方便處理share文)
				/////wallpost_view 才是處理過的顯示文
				$data['d'][$inx]['wallpost_view']=empty($data['d'][$inx]['wallpost_share']) ? $data['d'][$inx] : $data['d'][$inx]['wallpost_share'];
			}
			
			
				
		}
			
			return $data;
	}
	/**
	 * my_posts_renew 
	 * 
	 * @param mixed $user_uid 指定的 userUid
	 * @access public
	 * @return void
	 */
	public function my_posts_renew($user_uid,$wallpost_uid_dot){
		$wallpost_uid_ary=explode(",",$wallpost_uid_dot);
		$wallpost_uid_dot="";
		
		if(empty($wallpost_uid_ary)){return ;}
		else{
			foreach($wallpost_uid_ary as $wallpost_uid)
			{
				$wallpost_uid_dot.=empty($wallpost_uid_dot) ? '\''.$wallpost_uid.'\'' : ',\''.$wallpost_uid.'\'';
			}
		}
		
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				'userUid=:user AND wallpostUid in ('.$wallpost_uid_dot.')',
				array(
					':user'=>$user_uid,
				)
			), 
			'order'=>'wallpostTimeCreate DESC',
			'limit'=>1000, //如果使用者不更新頁面,只提供傳回文章的前1000則更新
		);

		$data=DB::data($this->dbid,$query);
		$cover_images=array();
		$users=array();

		if($data['args']['num']>0){
			foreach($data['d'] as $inx => $data_info){
				///減少sql讀取
				$xuser[$data_info['userUid']]=empty($xuser[$data_info['userUid']]) ?  CZ::model('users')->xuser($data_info['userUid']) : $xuser[$data_info['userUid']] ;
				$users[$inx]=&$xuser[$data_info['userUid']];
				$data['d'][$inx]['userRealname']=$users[$inx]['basic']['userRealname'];
				$data_info['imageUrl']=empty($data_info['imageUid']) ? '' : CZ::model('images')->imagePath_getter($data_info['imageUid'],'_w');
				if($data_info['imageUrl']==''){
					list($tmpInterest)=explode(";",$data_info['wallpostInterests']);
					if(empty($tmpInterest)){
						$allInterest=CZ::model('interests')->all_getter();
						$rnd=mt_rand ( 0, count($allInterest['d'])-1 );
						$ii=0;
						foreach($allInterest as $key => $val){
							$tmpInterest=$key;
							if($rnd==$ii){break;}
						}
					}
					$data['d'][$inx]['imageUrl']='/css/img/'.$tmpInterest.'.png';
				}
				else{
					$data['d'][$inx]['imageUrl']=$data_info['imageUrl'];
				}
				
				$data['d'][$inx]['uwallpost_track']=$this->user_walltrack(ME::user_uid(),$data_info['wallpostUid']);
				$data['d'][$inx]['wallpost_extend']=$this->wallpost_extend($data_info['wallpostUid'],$data_info['wallpostType']);
				$defaultPhoto_s='/css/img/default_profile_pic_s.png';
				$user_img_url=CZ::model('images')->imagePath_getter($users[$inx]['basic']['userProfile'],'_s');
				$data['d'][$inx]['wallpost_user_img_url']=($user_img_url) ? $user_img_url : $defaultPhoto_s;
				
				if(!empty($data['d'][$inx]['wallpostUidOriginal'])){$data['d'][$inx]['wallpost_share']=$this->wallpost_get($data['d'][$inx]['wallpostUidOriginal']); }
				
				/////以下這段都擺在程式最後段 (方便處理share文)
				/////wallpost_view 才是處理過的顯示文
				$data['d'][$inx]['wallpost_view']=empty($data['d'][$inx]['wallpost_share']) ? $data['d'][$inx] : $data['d'][$inx]['wallpost_share'];
				
			}
		}

		return $data;
	}
	public function wallpost_get($wallpost_uid){
			
			$query=array(
				'select' => ' * ',
				'from' => $this->table_name,
				'where' => array(
					'wallpostUid=:wallpost_uid',
				),
				'bind' =>  array(
					'wallpost_uid'=> $wallpost_uid
				)
			);
			$data=DB::data($this->dbid,$query);
			
			$cover_images=array();
			$users=array();
			if($data['args']['num']>0){
				foreach($data['d'] as $inx => $data_info){
					
					$data_info['imageUrl']=empty($data_info['imageUid']) ? '' : CZ::model('images')->imagePath_getter($data_info['imageUid'],'_w');
					if($data_info['imageUrl']==''){
						list($tmpInterest)=explode(";",$data_info['wallpostInterests']);
						if(empty($tmpInterest)){
							$allInterest=CZ::model('interests')->all_getter();
							$rnd=mt_rand ( 0, count($allInterest['d'])-1 );
							$ii=0;
							foreach($allInterest as $key => $val){
								$tmpInterest=$key;
								if($rnd==$ii){break;}
							}
						}
						$data['d'][$inx]['imageUrl']='/css/img/'.$tmpInterest.'.png';
					}
					else{
						$data['d'][$inx]['imageUrl']=$data_info['imageUrl'];
					}
					
					
					
					$data['d'][$inx]['uwallpost_track']=$this->user_walltrack(ME::user_uid(),$data_info['wallpostUid']);
					$data['d'][$inx]['wallpost_extend']=$this->wallpost_extend($data_info['wallpostUid'],$data_info['wallpostType']);
					$xusers=CZ::model('users')->xuser($data_info['userUid']);
					$data['d'][$inx]['xuser']=$xusers;
					$defaultPhoto_s='/css/img/default_profile_pic_s.png';
					$user_img_url=CZ::model('images')->imagePath_getter($xusers['basic']['userProfile'],'_s');
					$data['d'][$inx]['wallpost_user_img_url']=($user_img_url) ? $user_img_url : $defaultPhoto_s;
					
					if(!empty($data_info['wallpostUidOriginal'])){$data['d'][$inx]['wallpost_share']=$this->wallpost_get($data_info['wallpostUidOriginal']); }
					
				}
			}
			

			if($data['args']['num']==1){
				return $data['d'][0];
			}
			
			return FALSE;
	}

    public function wallpost_editor_getter($user_uid,$wallpost_uid){
        $column='*';
        $query=array( 
            'select' => $column ,
            'from'   => $this->table_name ,
            'where'  => array('userUid=:userUid AND wallpostUid=:wallpostUid',
                                array(  'userUid'=>$user_uid ,
                                        'wallpostUid'=>$wallpost_uid),
                             ),
        );
        
        $wallpost=DB::row($this->dbid,$query);
        $wallpost['extend']=$this->wallpost_extend($wallpost_uid,$wallpost['wallpostType']);

        return $wallpost;
    }
	
	public function wallpost_extend($wallpost_uid,$wallpost_type){
		switch($wallpost_type){
		
			case 0:
				$query=array(
					'select' => '*',
					'from' => 'writings',
					'where' => array(
						'wallpostUid=:wallpost_uid'
					),
					'bind' => array(
						':wallpost_uid' => $wallpost_uid
					)
				);
				$data=DB::row($this->dbid,$query);
				break;
			case 1:
				$query=array(
					'select' => '*',
					'from' => 'articles',
					'where' => array(
						'wallpostUid=:wallpost_uid'
					),
					'bind' => array(
						':wallpost_uid' => $wallpost_uid
					)
				);
				$data=DB::row($this->dbid,$query);
				break;
			case 2:
			case 3:
				$query=array(
					'select' => '*',
					'from' => 'missions',
					'where' => array(
						'wallpostUid=:wallpost_uid'
					),
					'bind' => array(
						':wallpost_uid' => $wallpost_uid
					)
				);
				$data=DB::row($this->dbid,$query);
				if(!empty($data)){
					$query1=array(
						'select' => 'count(*) cnt',
						'from' => 'userwallpostjoin',
						'where' => array(
							'wallpostUid=:wallpost_uid'
						),
						'bind' => array(
							':wallpost_uid' => $wallpost_uid
						)
					);
					$data1=DB::value($this->dbid,$query1);
					$data['joinNumPerson']=$data1;
				}
				break;
			case 4:
				$query=array(
					'select' => '*',
					'from' => 'events',
					'where' => array(
						'wallpostUid=:wallpost_uid'
					),
					'bind' => array(
						':wallpost_uid' => $wallpost_uid
					)
				);
				$data=DB::row($this->dbid,$query);
				if(!empty($data)){
					$query1=array(
						'select' => 'count(*) cnt',
						'from' => 'userwallpostjoin',
						'where' => array(
							'wallpostUid=:wallpost_uid'
						),
						'bind' => array(
							':wallpost_uid' => $wallpost_uid
						)
					);
					$data1=DB::value($this->dbid,$query1);
					$data['joinNumPerson']=$data1;
				}
				break;
		
		}
		
		return $data;
	
	}	
	/**
	 * wallpost_del 
	 * 
	 * 會確認自己本人uid與文章編號
	 * @param mixed 
	 * @access public
	 * @return void
	 */
	public function wallpost_del($user_uid,$wallpost_uid){
		
		////刪除主文
		$query=array(
			'delete'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				'userUid=:user AND wallpostUid=:wallpost_uid ',
			),
			'bind' =>array(
				':user'=>$user_uid,
				':wallpost_uid'=>$wallpost_uid,
			) 
		);
		$wallpost_uid_del=0;
		if($wallpost_uid_del)
		{
			////刪除每個user的互動足跡(需討論!?不知道是否影響點數結算!?)
			$query=array(
				'delete'=>'',
				'from'=>'uwallposts',
				'where'=>array(
					' wallpostUid=:wallpost_uid ',
				),
				'bind' =>array(
					':wallpost_uid'=>$wallpost_uid,
				) 
			);
			////刪除每個user的相關討論(需討論!?不知道是否影響點數結算!?)
			$query=array(
				'delete'=>'',
				'from'=>'wcomments',
				'where'=>array(
					' wallpostUid=:wallpost_uid ',
				),
				'bind' =>array(
					':wallpost_uid'=>$wallpost_uid,
				) 
			);
			
			
			////刪除每個user的收藏(需討論!?不知道是否影響點數結算!?)
			$query=array(
				'delete'=>'',
				'from'=>'ufavors',
				'where'=>array(
					' wallpostUid=:wallpost_uid  AND (wallpostType>=1 AND wallpostType<=4) ',
				),
				'bind' =>array(
					':wallpost_uid'=>$wallpost_uid,
				) 
			);
			return TRUE;
		}

		return FALSE;
	}

    /**
     * wallpost_temp_del 
     * 刪除一筆 wallpost 資料
     *
     * @param mixed $user_uid    
     * @param mixed $wallpost_uid 
     * @access public
     * @return void
     */
    public function wallpost_temp_delete($user_uid,$wallpost_uid){
        $where=array('WHERE userUid=:userUid AND wallpostUid=:wallpostUid AND wallpostTemp = 1',
                        array(  
                                'userUid'=>$user_uid,
                                'wallpostUid'=>$wallpost_uid,
                             ),
                    );
        return DB::del($this->dbid,$this->table_name,$where);
    }

    /**
     * wallpost_temp_del 
     * 依wallpostType 刪除一筆對應資料
     *
     * @param mixed $wallpost_uid 
     * @param mixed $wallpost_type   
     * @access public
     * @return void
     */
    public function wallpost_extend_delete($wallpost_uid,$wallpost_type){

        switch($wallpost_type){
            case 0:
                $result=$this->writing_delete($wallpost_uid);
                break;
            case 1:
                $result=$this->article_delete($wallpost_uid);
                break;
            case 2:
            case 3:
                $result=$this->missions_delete($wallpost_uid);
                break;
            case 4:    
                $result=$this->events_delete($wallpost_uid);
                break;    
        }
        return $result;
    }
    
    public function missions_delete($wallpost_uid){
        $where=array('WHERE wallpostUid=:wallpostUid',
                        array(  
                                'wallpostUid'=>$wallpost_uid,
                             ),
                    );
        return DB::del($this->dbid,$this->missions_table_name,$where);
    }
    
    public function events_delete($wallpost_uid){
        $where=array('WHERE wallpostUid=:wallpostUid',
                        array(  
                                'wallpostUid'=>$wallpost_uid,
                             ),
                    );
        return DB::del($this->dbid,$this->events_table_name,$where);
    }
    
    




	/**
	 * wallpost_comments 
	 * 取得一則 wallpost 的留言
	 * 
	 * @param mixed $wallpost_uid 
	 * @param mixed $assign_date 指定日期
	 * @param mixed $before_or_after 指定日期前或後 0:後 1:前
	 * @access public
	 * @return void
	 */
	public function wallpost_comments($wallpost_uid,$show_row=10,&$page=1){
		
		if($page<=0){$page=1;}
		$offset=($page-1)*$show_row;
		
		
		$query=array(
			'select'=>'count(*) as cnt',
			'from'=>'wcomments',
			'where'=>array(
				'wallpostUid=:wallpost_uid ',
			),
			'order'=>array(
				'wcommentTime DESC'),
			'bind'=>array(
				':wallpost_uid'=>$wallpost_uid,
			)
		);
		$count=DB::row($this->dbid,$query);
		
		
		
		$query['select']='*';
		$query['order']='wcommentTime DESC';
		$query['limit']=$offset.','.$show_row;
		
		
		$comments=DB::data($this->dbid,$query);

	
		
		
		
		if($comments['args']['num']>0){
			$comments['args']['cnt']=$count['cnt'];
			foreach($comments['d'] as $inx => $comments_data)
			{
				$xuser=CZ::model('users')->xuser($comments_data['userUid'],1,0,0);
				$comments['d'][$inx]['xuser']=$xuser;
				$defaultPhoto_s='/css/img/default_profile_pic_s.png';
				$user_img_url=CZ::model('images')->imagePath_getter($xuser['basic']['userProfile'],'_s');
				$comments['d'][$inx]['xuser']['imgUrl']=($user_img_url) ? $user_img_url : $defaultPhoto_s;
			}
		}
		else{
			$comments['args']['cnt']=$count['cnt'];
		}
		
		
		return $comments;
	}

    /**
     * wallpost_getter 
	 * 取得最近數則 wallpost
     * 
     * @param uuid $user_uid 
     * @param string $column 所要取得的欄位
     * @param int $limit 筆數
     * @access public
     * @return void
     */
    public function wallpost_getter($user_uid,$column='*',$limit=3){
        $query=array(
            'select'=>$column,
            'from'=>'wallposts',
            'where'=>array(
                'userUid=:user_uid',
            ),
            'order'=>array(
                'wallpostTimeCreate desc',
            ),
            'bind'=>array(
                ':user_uid'=>$user_uid,
            ),
            'limit'=>$limit,
        ); 
        return DB::data($this->dbid,$query);
    }

    /**
     * explore_getter 
     * 取得探索wallpost資料
     * 
     * @param mixed $orderby 排序方式,  $wallpostType 訊息種類,  $wallpostInterests 興趣標籤 , $spage分頁 , $per 筆數 
     * @access public
     * @return void
     */
    public function explore_getter( $orderby,$wallpostType=0,$wallpostInterests=0,$spage=0,$per=2  ){

        $column='u.userUid,u.userRealname,u.userProfile,w.wallpostUid,w.imageUid,w.wallpostInterests,w.wallpostTimeCreate,w.wallpostTopic,w.wallpostBrief,w.wallpostNumLikes,w.wallpostNumDislikes,w.wallpostNumComments,w.wallpostNumShares'; 

        $query=array( 
            'select' => $column ,
            'from'   => $this->table_name.' w , users u' ,
            'where'  => array('w.userUid=u.userUid AND w.wallpostTemp=0' ),
            'order'  => $orderby.' desc' , 
            'spage'  => $spage ,
            'per'    => $per,
        );

        if( $wallpostType == 'msg' )
            $query['where'][0] .= ' AND (w.wallpostType=0 OR w.wallpostType=1)';

        if( $wallpostType == 'event' )
            $query['where'][0] .= ' AND (w.wallpostType=2 OR w.wallpostType=3 OR w.wallpostType=4 ) ';

        if( !empty($wallpostInterests) ){
            $query['where'][0] .= ' AND w.wallpostInterests LIKE :wallpostInterests '; 
            $query['where'][1]['wallpostInterests'] = "%".$wallpostInterests."%";
        }
	    //_e($query);
        $wallpost_info=DB::data($this->dbid,$query);

        return $wallpost_info;
    }    

    /**
     * explore_hotgetter 
     * 取得探索-熱門動態資料
     * 
     * @param mixed 無
     * @access public
     * @return void
     */
	public function explore_hotgetter(){                                                                                                         
		$column='u.userUid,u.userRealname,u.userProfile,u.userTopbanner,w.wallpostUid,w.wallpostInterests,w.imageUid,w.wallpostTimeCreate,w.wallpostTopic,w.wallpostBrief,w.wallpostNumLikes,w.wallpostNumDislikes,w.wallpostNumComments,w.wallpostNumShares,w.wallpostType';

		$query=array(
			'select' => $column ,
			'from'   => $this->table_name.' w , users u' ,
			'where'  => array('w.userUid=u.userUid AND w.wallpostPublic=0 AND w.wallpostTemp=0 AND TO_DAYS(NOW()) - TO_DAYS(w.wallpostTimeCreate) <= 90'),
			'order'  => 'w.wallpostNumLikes desc' ,
			'limit'  => 1 ,
		);
		return DB::data($this->dbid,$query);
	}

   /**
     * explore_recommgetter 
     * 取得探索-量身推薦資料
     * @param mixed $interest 興趣標籤 ex acg,car,photo ,$orderby 排序方式, $spage分頁 , $per 筆數
     * @access public
     * @return void
     */

    public function explore_recommgetter($interest,$orderby,$spage=0,$per=2 ){

		$column='u.userRealname,u.userProfile,w.wallpostUid,w.imageUid,w.wallpostInterests,w.wallpostTimeCreate,w.wallpostTopic,w.wallpostBrief,w.wallpostNumLikes,w.wallpostNumDislikes,w.wallpostNumComments,w.wallpostNumShares'; 

		$str='';
		$interestArray = explode(";",$interest);
		for($i=0;$i<count($interestArray);$i++){
            if( $i == count($interestArray) -1 ){
				$str .= ' w.wallpostInterests LIKE "%'.$interestArray[$i].'%" ' ;
			}
            else{
				$str .= ' w.wallpostInterests LIKE "%'.$interestArray[$i].'%" OR ' ;
			}
		}

		$query=array(
			'select' =>  $column ,
			'from'   =>  $this->table_name.' w , users u' ,
			'where'  =>  array('w.userUid=u.userUid AND w.wallpostPublic=0 AND w.wallpostTemp=0 AND ( '.$str. ' ) '  ),
			'order'  =>  $orderby.' desc' ,
			'spage'  =>  $spage,
			'per'    =>  $per,
		);  
		$data=DB::data($this->dbid,$query);
		
		return $data; 
    }

    public function postrecord_getter($userUid,$wallpostType=0,$dateBetween='',$spage=0,$per=50 ){
        $query=array(
            'select' =>  '*' ,
            'from'   =>  $this->table_name ,
            'where'  =>  array('userUid=:userUid',
                            array( 
                                    'userUid'=>$userUid,
                                ),
                            ),
            'order'  =>  'wallpostTimeCreate desc' ,
            'spage'  =>  $spage,
            'per'    =>  $per,
        ); 

        if( !empty($dateBetween) )
            $query['where'][0] .= $dateBetween ;     

        switch($wallpostType){
            case 1: 
                // 動態
                $query['where'][0] .= ' AND ( wallpostType = 0 OR wallpostType = 1) AND     wallpostTemp != 1 ' ;     
                break;    
            case 2:
                //活動
                $query['where'][0] .= ' AND ( wallpostType = 2 OR wallpostType = 3 OR wallpostType = 4) AND  wallpostTemp != 1' ;     
                break;
            case 3:
                //草稿
                $query['where'][0] .= ' AND ( wallpostTemp != 0 )' ;     
                break; 
        }
        
//_e($query);

        $data=DB::data($this->dbid,$query);
        
        for($i=0;$i<$data['args']['num'];$i++){
            $data['d'][$i]['extend']=$this->wallpost_extend($data['d'][$i]['wallpostUid'],$data['d'][$i]['wallpostType']) ;   
        }

        return $data; 
    }

		public function wallpost_get_layout($wallpost_data){
		$html="";
		$html.="<div class='box row-fluid box-margin-top wbox' data-uid='{$wallpost_data['userUid']}' data-wuid='{$wallpost_data['wallpostUid']}'  data-wtime='{$wallpost_data['wallpostTimeCreate']}' data-wallpost-type='{$wallpost_data['wallpost_view']['wallpostType']}'  >
							<div class='box-head bottom-bordered'>
								<a href='/user/mywalls/{$wallpost_data['userUid']}'><img src='{$wallpost_data['wallpost_user_img_url']}' /></a>
								<div class='info'>
									<div class='top'>
										<a href='/user/mywalls/{$wallpost_data['userUid']}'>{$wallpost_data['userRealname']}</a>";
										if(!empty($wallpost_data['wallpostUidOriginal'])){
						$html.="<span class='share-box-push'>
											分享了1則的<a href='/user/wallpost_comment?w={$wallpost_data['wallpost_share']['wallpostUid']}'>".(($wallpost_data['wallpost_share']['wallpostType']<2) ? "動態" : "活動")."</a>
										</span>";
										}
					$html.="</div>
									<div class='down'>
										<span class='datetime'>{$wallpost_data['wallpostTimeCreate']}</span>
										<span class='place' style='display:none'>法國巴黎</span>
										<span class='open-stat'>
											<i class='fa ".(($wallpost_data['wallpostPublic']==0) ? "fa-globe" : "fa-group" )." '></i>
											<i class='fa fa-caret-down' style='display:none'></i>
										</span>";
										if($wallpost_data['userUid']==ME::user_uid()){
						$html.="<span class='open-setting'>
											<ul class='nostyle' style='display:none'>
												<li><a href='#'  ><i class='fa fa-globe'></i> 公開</a></li>
												<li><a href='#'  ><i class='fa fa-group'></i>只限好友</a></li>
											</ul>
										</span>";
										}
					$html.="</div>
								</div>
								<span class='fa fa-chevron-down pull-right ul-sw' onclick='ul_sw(this);'></span>
								<div class='ul-bordered' style='display: none;'>
									<ul class='nostyle collect-report'>";
										if( $wallpost_data['userUid']!=ME::user_uid() ){ // 他人的文章 
						$html.="<li><a href='javascript:void(0)' class='favor-this'>".(!empty($wallpost_data['uwallpost_track']['favor']) ? "已收藏" : "加入收藏")."</a></li>
										<li style='display:none;'><a href='javascript:void(0)'>加入追蹤</a></li>
										<li><a href='javascript:void(0)'>檢舉</a></li>";
										}else{ // 自己的文章
											if($wallpost_data['wallpostType']<2) {$js_str='wallpost';}
											if($wallpost_data['wallpostType']==2) {$js_str='lottery';}
											if($wallpost_data['wallpostType']==3){$js_str='pulling';}
											if($wallpost_data['wallpostType']==4){$js_str='spread';}
											
											
						$html.="<li><a href='javascript:void(0)' onclick=\"edit('{$wallpost_data['wallpostUid']}','{$js_str}')\" >編輯修改</a></li>
										<li style='display:none;'><a href='javascript:void(0)'>刪除</a></li>
										<li style='display:none;'><a href='javascript:void(0)'>隱私設定</a></li>";
										}
					$html.="</ul>
								</div>
							</div>";
			if(!empty($wallpost_data['wallpostUidOriginal'])){
			$html.="<div class='share-box'>
								<div class='share-box-body'>
									{$wallpost_data['wallpostBrief']}
								</div>
							</div>";
							}
			$html.="<div class='box-img'>";
								if($wallpost_data['wallpost_view']['wallpostType']>=2 && $wallpost_data['wallpost_view']['wallpostType']<=4) {
									if($wallpost_data['wallpost_view']['wallpostType']==2){$css_str='lottery-post';$title=$wallpost_data['wallpost_view']['wallpost_extend']['missionName'];}
									if($wallpost_data['wallpost_view']['wallpostType']==3){$css_str='pulling-post';$title=$wallpost_data['wallpost_view']['wallpost_extend']['missionName'];}
									if($wallpost_data['wallpost_view']['wallpostType']==4){$css_str='spread-post'; $title=$wallpost_data['wallpost_view']['wallpost_extend']['eventName'];}

				$html.="<div class='head page-commit outside'>
									<div class='{$css_str}'>
										<div class='icon icon-lg'><span class='title comment-this2'>".html($title,false)."</span></div>
									</div>
									<div class='head-buttons wjoin_box' data-wuid='{$wallpost_data['wallpost_view']['wallpostUid']}'>
										<div class='action-button btn-lg wjoin'>我要參加</div>
										<div class='action-button pull-right invite winvite'>邀請</div>
										<div class='invite-nav' style='display: none;'>
											<ul class='nostyle'></ul>
											<div class='invite-submit' style='display: none;'>送出邀請</div>
										</div>
									</div>
								</div>";
								}
				$html.="<img src='{$wallpost_data['wallpost_view']['imageUrl']}' class='comment-this2' />
								<div class='interest-item'>";
								foreach((explode(';',$wallpost_data['wallpost_view']['wallpostInterests'])) as $wallpostInterests){
									if($wallpostInterests!='0' && $wallpostInterests!=''){
					$html.="<div class='icon'><span class='item icon-i27 icon-{$wallpostInterests}'></span></div>";
									}
								}
				$html.="</div>
							</div>";
			if(!empty($wallpost_data['wallpostUidOriginal'])){
			$html.="<div class='share-box'>
								<div class='share-box-body'>
									<a href='/user/mywalls/{$wallpost_data['userUid']}'>{$wallpost_data['wallpost_share']['xuser']['basic']['userRealname']}</a>
								</div>
								<!--<div class='share-box-info'>
									<a href='/user/mywalls/{$wallpost_data['wallpost_share']['userUid']}' ><img src='{$wallpost_data['wallpost_share']['wallpost_user_img_url']}' /></a>
									<div class='share-box-top'>
										<a href='/user/mywalls/{$wallpost_data['userUid']}'>{$wallpost_data['wallpost_share']['xuser']['basic']['userRealname']}</a>
									</div>
									<div class='share-box-down' >
											<span class='datetime'>{$wallpost_data['wallpost_share']['wallpostTimeCreate']}</span>
									</div>
								</div>-->
							</div>";
							}
			$html.="<div class='box-body'>";
							if($wallpost_data['wallpost_view']['wallpostType']==2 || $wallpost_data['wallpost_view']['wallpostType']==3) {
					$html.="<div class='lottery-options'>
										<table>
											<tr>
												<td class='th-width'>活動時間：</td>
												<td>{$wallpost_data['wallpost_view']['wallpost_extend']['missionTimeStart']}~{$wallpost_data['wallpost_view']['wallpost_extend']['missionTimeClose']}</td>
											</tr>";
											if($wallpost_data['wallpost_view']['wallpost_extend']['missionAward']==1) {
							$html.="<tr>
												<td class='th-width'>抽獎名額：</td>
												<td>{$wallpost_data['wallpost_view']['wallpost_extend']['missionNumPerson']}名</td>
											</tr>
											<tr>
												<td class='th-width'>獎品名稱：</td>
												<td>參一腳平台點數 {$wallpost_data['wallpost_view']['wallpost_extend']['missionAwardPoint']}點</td>
											</tr>";
											}
											else if($wallpost_data['wallpost_view']['wallpost_extend']['missionAward']==2) {
							$html.="<tr>
												<td class='th-width'>抽獎名額：</td>
												<td>{$wallpost_data['wallpost_view']['wallpost_extend']['missionNumPerson']}名</td>
											</tr>
											<tr>
												<td class='th-width'>獎品名稱：</td>
												<td>{$wallpost_data['wallpost_view']['wallpost_extend']['missionAwardName']}</td>
											</tr>";
											}
							$html.="<tr>
												<td class='th-width v-top ' >活動介紹：</td>
												<td class='td-introduction comment-this2'>{$wallpost_data['wallpost_view']['wallpostBrief']}</td>
											</tr>
										</table>
									</div>
									<div class='lottery-join pull-right'>
										<div class='top'>".numeric($wallpost_data['wallpost_view']['wallpost_extend']['joinNumPerson'],false)."</div>
										<div class='down'>參加人數</div>
									</div>";
								 }
								 else if($wallpost_data['wallpost_view']['wallpostType']==4) {
					$html.="<div class='lottery-options'>
										<table>
											<tbody>
											<tr>
												<td class='th-width'>活動時間：</td>
												<td>{$wallpost_data['wallpost_view']['wallpost_extend']['eventTimeStart']}~{$wallpost_data['wallpost_view']['wallpost_extend']['eventTimeEnd']}</td>
											</tr>
											<tr>
												<td class='th-width'>活動人數：</td>
												<td>{$wallpost_data['wallpost_view']['wallpost_extend']['eventNumPerson']}名</td>
											</tr>
											<tr>
												<td class='th-width v-top'>活動介紹：</td>
												<td class='td-introduction comment-this2'>{$wallpost_data['wallpost_view']['wallpostBrief']}</td>
											</tr>
											</tbody>
										</table>
									</div>
									<div class='lottery-join pull-right'>
										<div class='top'>".numeric($wallpost_data['wallpost_view']['wallpost_extend']['joinNumPerson'],false)."</div>
										<div class='down'>參加人數</div>
									</div>";
								}
								else{
					$html.="<h4 class='comment-this2'>".html($wallpost_data['wallpost_view']['wallpostTopic'],false)."</h4>
									<article class='comment-this2'>
										{$wallpost_data['wallpost_view']['wallpostBrief']}
									</article>";
								}
			$html.="</div>
							<div class='box-foot'>
								<div class='info'>
									<div class='co12-3 text-center'>喜歡 <span>".numeric($wallpost_data['wallpostNumLikes'],false)."</span>個</div>
									<div class='co12-3 text-center'>不喜歡 <span>".numeric($wallpost_data['wallpostNumDislikes'],false)."</span>個</div>
									<div class='co12-3 text-center'>留言 <span>".numeric($wallpost_data['wallpostNumComments'],false)."</span>則</div>
									<div class='co12-3 text-center'>瀏覽次數 <span>".numeric($wallpost_data['wallpostNumViews'],false)."</span>次</div>
								</div>
								<div class='action-buttons'>
									<div class='co12-3'><span class='like-this ".(!empty($wallpost_data['uwallpost_track']['like']) ? 'did' : '')."'></span></div>
									<div class='co12-3'><span class='dislike-this ".(!empty($wallpost_data['uwallpost_track']['dislike']) ? 'did' : '')."'></span></div>
									<div class='co12-3'><span class='comment-this ".(!empty($wallpost_data['uwallpost_track']['comment']) ? 'did' : '')."'></span></div>
									<div class='co12-3'><span class='fa fa-share-alt ".(!empty($wallpost_data['uwallpost_track']['share']) ? 'did' : '')."' onclick='share_sw(this);'></span></div>
									<div class='share-bordered' style='display: none;'>
										<ul class='nostyle fb-wallpost'>
											<li style='display:none;'><a href='javascript:void(0)' class='share-this'>分享至興趣牆</a></li>
											<li><a href='javascript:void(0)' class='share-wall-this'>分享至興趣牆</a></li>
											<li><a href='javascript:void(0)' class='share-fb-this'>分享至臉書</a></li>
											<li><a href='http://www.countmin.com/mailbox/one/' target='_blank'>私訊分享給好友</a></li>
											<li ><a class='copy-link' data-clipboard-action='copy' data-clipboard-text='http://www.countmin.com/user/wallpost_comment?w={$wallpost_data['wallpostUid']}' >複製此連結</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>";
			return $html;
		}

}



