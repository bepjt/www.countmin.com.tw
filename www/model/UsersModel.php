<?php

class UsersModel extends Model_Base {


    public function __construct(){
        $this->dbid='common';
        $this->table_name='users';
        $this->upoint_table_name='upoint_logs';                                 
        $this->interests_table_name='interests';
        $this->uprolevels_table_name='uprolevels';
        $this->uconnects_table_name='uconnects';
        $this->relations_table_name='relations';
        $this->userdetails_table_name='userdetails';
        $this->contactcc_table_name='contactcc';
        $this->uaccs_table_name='uaccs';
        $this->privacy_table_name='privacy';
        $this->blacklist_table_name='blacklist';
        $this->uacctokens_table_name='uacctokens';
        $this->regions_table_name='regions'; 
        $this->upoint_pays_table_name='upoint_pays';
        $this->userphotoes_table_name='userphotoes';
        $this->goods_table_name='goods';
        $this->usermasters_table_name='usermasters';
        $this->uparameters_table_name='uparameters';
        $this->userexps_table_name='userexps';
        $this->uinvitecodes_table_name='uinvitecodes';
        $this->field_pk='userUid';
        $this->field_pky_charset='uid4';
        $this->fields=array(
                'userUid'=>array('charset'=>'uid4','max'=>32,'req'=>1),
                'userId'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userRealname'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userAliasname'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userGender'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userProfile'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userTimeRegist'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userType'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userEnable'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userPoint'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userLevel'=>array('charset'=>'string','max'=>32,'req'=>0),
                'userTimezone'=>array('charset'=>'string','max'=>32,'req'=>0),
                'langIdInterface'=>array('charset'=>'string','max'=>32,'req'=>0),
                );
        $this->relations_status=array(
                	'interest' => '興趣建議',
                	'inviting' => '邀請中',
                	'response' => '交友回覆',
                	'follow' => '同好',
                	'follower' => '追蹤者',
                	'inviting' => '邀請中',
                	'friend'	=> '好友中',
                );
    }

    /**
     * uid_pwd_check 
     * 驗證 email 帳號密碼是否正確
     * 若正確則回傳 users 資料
     * 若錯誤或不存在則回傳 NULL
     * 
     * @param mixed $uid 
     * @param mixed $pwd 
     * @access public
     * @return void
     */
    public function uid_pwd_check($uid,$pwd){
        $query=array(
                'select'=>'',
                'from'=>'uaccs',
                'where'=>array(
                    'uaccAuth="email" AND uaccId=:uaccId',
                    array(
                        ':uaccId'=>$uid
                        )
                    ),
                );
        $uacc=DB::row($this->dbid,$query);
        if(!$uacc){
            return NULL;
        }
        $query=array(
                'select'=>'',
                'from'=>'uacctokens',
                'where'=>array(
                    'uaccUid=:uaccUid',
                    array(
                        ':uaccUid'=>$uacc['uaccUid'],
                        )
                    ),
                );
        $token=DB::row($this->dbid,$query);
        if(!$token){
            return NULL;
        }
        $pwd=md5('$6G.'.md5($pwd));
        if(!strcmp($pwd,$token['uaccToken'])){
            $query=array(
                    'select'=>'',
                    'from'=>'users',
                    'where'=>array(
                        'userUid=:userUid',
                        array(
                            ':userUid'=>$uacc['userUid'],
                            )
                        ),
                    );
            $cuser=DB::row($this->dbid,$query);
            if(!$cuser){
                // 帳號密碼正確，但卻找不到帳號資料
                return NULL;
            }
            $cuser=array_merge($cuser,$uacc);
            return $cuser;
        }
        return NULL;
    }

    public function interest_getter(){                                                                                                            
        $query=array(
                'select' => '*' ,
                'from'   => $this->interests_table_name ,
                'order'  => 'interestId' ,
                );
        return DB::data($this->dbid,$query);
    }

    public function interest_uprolevels_getter($user_uid){
        $query=array(
                'select'=>'up.*,i.*',
                'from'=>'interests i
                LEFT JOIN uprolevels up ON up.interestId=i.interestId AND up.userUid=:user',
                'order'=>array(
                    'up.userProLevel DESC',
                    ),
                'bind'=>array(
                    ':user'=>$user_uid
                    )
                );
        $my_interests=DB::data($this->dbid,$query);
        //      $interests=CZ::model('interests')->all_getter();
        //      for($i=0;$i<$my_interests['args']['num'];$i++){
        //          if(isset($interests['d'][$my_interests['d'][$i]['interestId']])){
        //              $my_interests['d'][$i]['interestName']=$interests['d'][$my_interests['d'][$i]['interestId']];
        //          }
        //      }
        return $my_interests;
    }

    public function users($uids){
        $query=array(
                'select'=>'userUid,userRealname',
                'from'=>$this->table_name,
                'where'=>array(
                    'userUid IN ('.str_repeat('?,',count($uids)-1).'?)',
                    $uids
                    ),
                'key'=>'userUid',
                );
        $users=DB::data($this->dbid,$query);
        return $users;

    }

    //列出網站使用者列表
    public function users_list($column='*',$spage=0){
        $query=array(
                'select'=> $column ,
                'from'=>$this->table_name,
                'where'=>array('userEnable=1'),
                'spage'=>$spage,
                'per'=>50,
                );
        $users=DB::data($this->dbid,$query);
        return $users;
    }

    public function uparameter($user_uid){
        $query=array(
                'select'=>'*',
                'from'=>'uparameters',
                'where'=>array(
                    'userUid=:uid',
                    ),
                'bind'=>array(
                    ':uid'=>$user_uid,	
                    )
                );
        return DB::row($this->dbid,$query);
    }

    public function uinvitecodes_add($data){
        return DB::add($this->dbid,$this->uinvitecodes_table_name,$data,FALSE);
    }

    public function uinvitecodes_getter($userInviteCode){
        $query=array(
                'select' => '*' ,
                'from'   => $this->uinvitecodes_table_name ,                   
                'where'  => array('userInviteCode=:userInviteCode',
                    array(
                        'userInviteCode'=>$userInviteCode,
                        )
                    )
                );

        return DB::data($this->dbid,$query);
    }


    public function uprolevels_add($data){
        return DB::add($this->dbid,$this->uprolevels_table_name,$data,FALSE);
    }
    
    public function uprolevels_update($user_uid,$interestId,$data){
        $where = array(
                        'WHERE userUid=:userUid AND interestId=:interestId',
                            array(
                                   'userUid'=>$user_uid,
                                   'interestId'=>$interestId,
                            )
                        );
        return DB::update($this->dbid,$this->uprolevels_table_name,$data,$where);    
    }

    public function uprolevels_getter($user_uid,$title=1){
        $query=array(
                'select' => '*' ,
                'from'   => $this->uprolevels_table_name ,                   
                'where'  => array('userUid=:userUid',
                    array(
                        'userUid'=>$user_uid,
                        )
                    ),
                'order'  => 'userGrade desc',
                );

        $users=DB::data($this->dbid,$query);

        if($title){
            $total=0;
            $userTitle='';
            $interest_90=0;
            $interest_70=0;
            $interest_50=0;
            $interest_30=0;
            $interest_15=0;

            for($i=0;$i<$users['args']['num'];$i++){
                $total += $users['d'][$i]['userProLevel']; //加總興趣PRO等級

                if( $users['d'][$i]['userProLevel'] >=90)
                    $interest_90++;
                if( $users['d'][$i]['userProLevel'] >=70)
                    $interest_70++;
                if( $users['d'][$i]['userProLevel'] >=50)
                    $interest_50++;
                if( $users['d'][$i]['userProLevel'] >=30)
                    $interest_30++;
                if( $users['d'][$i]['userProLevel'] >=15)
                    $interest_15++;
            }

            if( $interest_90 >= 10 || $total >= 1000 ){
                $userTitle='神人' ;
                $userTitle2=8;
            }
            elseif( $interest_90 >= 5 || $total >= 700 ){
                $userTitle='巨星' ;
                $userTitle2=7;
            }
            elseif( $interest_90 >= 2 || $total >= 500 ){
                $userTitle='明星' ;
                $userTitle2=6;
            }
            elseif( $interest_70 >= 2 || $total >= 300 ){
                $userTitle='A咖' ;
                $userTitle2=5;
            }
            elseif( $interest_50 >= 2 || $total >= 250 ){
                $userTitle='四星達人' ;
                $userTitle2=4;
            }
            elseif( $interest_30 >= 2 || $total >= 150 ){
                $userTitle='三星達人' ;
                $userTitle2=3;
            }
            elseif( $interest_15 >= 2 || $total >= 100 ){
                $userTitle='二星達人' ;
                $userTitle2=2;
            }
            elseif( $total >= 45 ){
                $userTitle='一星達人' ;
                $userTitle2=1;
            }
            else{
                $userTitle='新手' ;
                $userTitle2=0;
            }
            //TODO remove title2
            $users['title'] = $userTitle ;
            $users['title2'] = $userTitle2 ;    
        }
        return $users;    
    }     

		public function uprolevels_add_pro($userUid,$interestId,$score){
				$query=array(
					'select'	=> '*' ,
					'from'	=> $this->uprolevels_table_name,
					'where'	=> 'userUid=:userUid AND interestId=:interestId',
					'bind'	=> array(
						':userUid' => $userUid,
						':interestId' => $interestId
					)
				);
				$uprolevels=DB::row($this->dbid,$query);
				if(empty($uprolevels)){
					$data=array(
						'userUid' => $userUid,
						'interestId' => $interestId,
						'userGrade' => $score,
						'uprolevelTimeLastUpdate' => _SYS_DATETIME,
					);
					
					return DB::add($this->dbid,$this->uprolevels_table_name,$data,FALSE);
				}
				else{
					$data=array(
						'userGrade' => array('userGrade +'.$score),
						'uprolevelTimeLastUpdate' => _SYS_DATETIME,
					);
					return DB::update($this->dbid,$this->uprolevels_table_name,$data,array('where userUid=:userUid AND interestId=:interestId',array(':userUid' => $userUid,':interestId' => $interestId)));
				}
				
    }
    
    /**
     * users_top_getter 
     * 未登入-你可能感興趣的人
     * 
     * @param mixed $interestId 興趣標籤
     * @param mixed $limit 取出筆數 整數:筆數
     * @access public
     * @return void
     */
    public function users_top_getter($userInterests='',$limit=3){
        
        $query=array(
                'select' => '*' ,
                'from'   => $this->table_name ,                   
                'where'  => array('userType=0 AND userEnable=1'),
                'order' => 'userLevel desc',
                'limit' =>  $limit ,
                );


        if(!empty($userInterests) ){
            $query['where'][0] = 'userInterests LIKE :userInterests';                         
            $query['where'][1]['userInterests'] = "%".$userInterests."%" ;  
        }    

        return DB::data($this->dbid,$query);        
    }

    public function xuser($xuser_uid,$basic=1,$uprolevel=0,$uparameter=0,$userdetails=0){
        $xuser=array(
                'uid'=>'',
                'basic'=>array(),
                'uprolevel'=>array(),
                'uparameter'=>array(),
                'userdetails'=>array(),
                );
        if(strpos($xuser_uid,'-')){// 使用 uuid (系統自動產生，不可自訂)
            $xuser['basic']=$this->one($xuser_uid);
        }
        else{// 使用 userId (這是使用者可以自訂的)
            $xuser['basic']=$this->find_one('userId',$xuser_uid);
        }
        if(!$xuser['basic']){
            return NULL;
        }
        $xuser['uid']=$xuser['basic']['userUid'];
        if($uprolevel){
            $xuser['uprolevel']=$this->interest_uprolevels_getter($xuser_uid);
        }
        if($uparameter){
            $xuser['uparameter']=$this->uparameter($xuser_uid);
        }
        if($userdetails){
            $xuser['userdetails']=$this->userdetails_getter($xuser_uid);          
        }
        return $xuser;
    }

    public function admin_list($spage){
        $query=array(
                'select'=>'*',
                'from'=>$this->table_name,
                'spage'=>$spage,
                );
        $users=DB::data($this->dbid,$query);
        return $users;
    }

    public function uparameters_add($data){
        return DB::add($this->dbid,$this->uparameters_table_name,$data,FALSE);
    }
    
    public function uparameters_update($userUid,$data){
        return DB::update($this->dbid,$this->uparameters_table_name,$data,array('WHERE userUid=:userUid',array(':userUid'=>$userUid)));
    }

    public function users_pro_param_getter($userUid='' , $orderby='up.uparameterNumFollowers' , $limit=8 ){
    //TODO need modify
        $query=array(
                'select'=>'u.* , up.* , upl.*',
                'from'=> $this->table_name  .' u LEFT JOIN uparameters up  ON up.userUid = u.userUid LEFT JOIN uprolevels upl ON up.userUid = upl.userUid ' ,
                'order' => $orderby.' desc',
                'limit' => $limit ,
                );

        if( !empty($userUid) ){
            $query['where'] = array('u.userUid=:userUid' );
            $query['where'][1][':userUid']= $userUid ;                            
        }

        return DB::data($this->dbid,$query);
    }           

    public function users_param_getter($userUid='' , $orderby='up.uparameterNumFollowers' , $limit=8 ){

        $query=array(
                'select'=>'u.* , up.*',
                'from'=> $this->table_name  .' u LEFT JOIN uparameters up  ON up.userUid = u.userUid ' ,
                'order' => $orderby.' desc',
                'limit' => $limit ,
                );

        if( !empty($userUid) ){
            $query['where'] = array('u.userUid=:userUid' );
            $query['where'][1][':userUid']= $userUid ;                            
        }

        return DB::data($this->dbid,$query);
    }     
      
    public function users_add($data){
        return DB::add($this->dbid,$this->table_name,$data,FALSE);
    }

    public function user_getter($userUid,$tableName='users'){
        $query=array(
			'select' => '*',
			'from'   => $tableName ,
			'where'  => array('userUid=:userUid',
				array(
					':userUid'=>$userUid,
					)
				)   
		);
        $users=DB::data($this->dbid,$query);        
        return $users;
    }    

    public function user_update($userUid,$data,$tableName='users'){
        return DB::update($this->dbid,$tableName,$data,array('WHERE userUid=:userUid',array(':userUid'=>$userUid)));  
    }

    /**
     * user_point_update 
     * 更新users userPoint
     * 
     * @access public
     * @input 
     *   '$userUid' : userUid
     *   '$point'   : 要加或減的點數
     *   如果相減後為負,則存入0
     * @return true or false
    */
    public function user_point_update($userUid,$point=0){
        if( empty($userUid) )
            return false;

        $user=$this->user_getter($userUid);

        if( is_array($user) && array_key_exists('userPoint',$user['d'][0]))
            $point = $user['d'][0]['userPoint'] + $point ;
        else
            return false;

        if( $point > 0 )
            $data=array('userPoint'=>$point);
        else
            $data=array('userPoint'=>0); 

        return $this->user_update($userUid,$data);
    }

    /**
     * userwallpost_2p_add 
     * 發表文章活動+1 -1 點數與pro點結算判斷
     * 
     * @access public
     * @input POST 方式
     *   'pro_and_point_settle' : 先到 user_point_settle 取一些精華回來
     *   'wallpost_uid'  : 把對應的wallpost_uid丟進來
     * @return json
    */
    public function userwallpost_2p_add($userUid,$pro_and_point_settle,$wallpost_uid,$note=''){
				
			if(empty($userUid) || empty($pro_and_point_settle) || empty($wallpost_uid)){
				return false;
			}
			///給點數
			if(!empty($pro_and_point_settle['Earn'])){
				$point=$pro_and_point_settle['Earn'];
				
				if($point>=0){
					$edate=date('Y-12-31 23:59:59',strtotime(_SYS_DATETIME.' +1 year '));
					$data=array(
							'upointUid'	=> uid4(),
							'userUid' => $userUid,
							'userPointEarn' => $point,
							'userPointUsed' => 0,
							'userPoint' => $point,
							'userPointTimeLimit' => $edate,
							'upointlogTimeEarn' => _SYS_DATETIME,
							'upointTable' => 'wallposts',
							'upointTableId' => $wallpost_uid,
							'upointlogNote' => $note,
						);
					
					$this->upointlogs_add($data);
				}
				else{
					$less_point=abs($point);
					$this->user_point_less($userUid,$less_point);
				
				}
				
				///使用者點數紀錄
				$record_data=array(
					'userPoint' => array('userPoint+'.$point),
				);
				CZ::model('users')->user_update($userUid,$record_data);
				
				
			}
			///給pro分
			if(!empty($pro_and_point_settle['pro'])){
				foreach($pro_and_point_settle['pro'] as $interestId => $score){
					if(!empty($interestId)){
						if($score!=0){
							$this->uprolevels_add_pro($userUid,$interestId,$score);
						}
					}
				}
			}
		}

    public function upointlogs_wallposts_getter($userUid){
        $column='u.*,w.wallpostTopic';
        $query=array(                                                           
                'select'=>$column,
                'from'=>$this->upoint_table_name.' u LEFT JOIN wallposts w ON u.upointTableId = w.wallpostUid',
                'where'=>array( 'u.userUid=:userUid ',
                            array( 'userUid' =>  $userUid ) ,
                         ),   
                'order'=> 'u.upointlogTimeEarn desc',
                ); 
        $users=DB::data($this->dbid,$query);
        return $users;
    }
    /**
     * user_point_settle 
     * 發表文章活動+1 -1 點數與pro點結算判斷
     * 
     * @access public
     * @input POST 方式
     *   'userUid' :  原作者的useruid
     *   'wallpostType' :  0:writings, 1:articles, 2:抽獎(missions), 3:推廣(missions), 4:揪團(events)
     *   'actionType' : 1發表, 2+, 3-, 4分享, 5活動參加
     *   'interest' : car;photo;3c
     * @return json
     */
    public function user_point_settle($userUid,$wallpostType,$actionType,$interest){

				$xuser=CZ::model('users')->xuser($userUid,1,1,1);
				$xuser_uid=$xuser['uid'];
				if(empty($xuser_uid)){
					$userUid=ME::user_uid();
					$xuser=CZ::model('users')->xuser($userUid,1,1,1);
				}
				$interest_tag=explode(";",$interest);
				
				
				if(  empty($actionType) || empty($userUid) || empty($interest_tag) ){
					return false;
				}
				if($actionType != 5 ){
					foreach($interest_tag as $tag)
					{
						foreach($xuser['uprolevel']['d'] as $prodata)
						{
							if($tag==$prodata['interestId']){
								$level=empty($prodata['userProLevel']) ? 1 : $prodata['userProLevel'] ; ///如果有人問為什麼這樣寫!?這是個黑歷史(茶
								if( $actionType == 4){
									///目前分享一律引用
									$total_post[$tag]=$level*2+$level*2;
									$total_pusher[$tag]=2+2;
								}
								else if( $actionType == 3){
									
									$total_post[$tag]=$level*-1;
									$total_pusher[$tag]=1;
								}
								else{
									$total_post[$tag]=$level*1;
									$total_pusher[$tag]=1;
								}
							}
						}
					}
					switch($actionType){
						case 1: // 發表   
							$data['poster']['Earn'] = 20;//發表者點數
							$data['poster']['pro'] = '';	//pro分
							break;
						case 2: // +1
							$data['participants']['Earn'] = 1; //參與者點數 
							$data['participants']['pro']=$total_pusher;
							$data['poster']['Earn'] = 1; //發表者點數
							$data['poster']['pro'] = $total_post;//pro分
							break;
						case 3: // -1
							$data['participants']['Earn'] = 1; //參與者點數
							$data['participants']['pro']=$total_pusher;
							$data['poster']['Earn'] = -1; //發表者點數
							$data['poster']['pro'] = $total_post; //pro分
							break;
						case 4: // 分享
							$data['participants']['Earn'] = 20; //參與者點數 +引用點數
							$data['participants']['pro'] = $total_pusher;
							$data['poster']['Earn'] = 20; //發表者點數 +引用點數
							$data['poster']['pro'] = $total_post;//pro分
							break;
						default :
							$data = false; 
					}
				}
				if( ($wallpostType == 2 || $wallpostType == 3 || $wallpostType == 4) && $actionType == 5 ){
					//活動 參加
					$data['participants']['Earn'] = 5; //參與者點數
					//TODO 達成活動條件再+10 寫在cron
					$data['poster']['Earn'] = 1; //發表者點數
				}

			return $data;
		}

    /**
     * user_point_less
     * 文章 -1 扣點 
     * 
     * @access public
     * @input 
     *   'user_uid' :  要扣點對象的useruid
     *   'subtration_point' : 要扣的點數
     * @return true;false;2(點數不足不用扣)
     */
    public function user_point_less($user_uid,$subtration_point){
        
        if( empty($user_uid) || empty($subtration_point) || $subtration_point < 1 )
            return false;

        $totalPoint = 0;
        $less_point = $subtration_point;
        $index = array() ;
        $userPoint=$this->upointlogs_getter($user_uid); 
        for($i=0;$i<count($userPoint['d']);$i++){
            $timeLimit = (strtotime($userPoint['d'][$i]['userPointTimeLimit']) >= strtotime(date("Y-m-d")) ) ;                         
            //篩選出剩餘點數 >0 且未過期紀錄
            if( $userPoint['d'][$i]['userPoint'] >0 && $timeLimit ){
                $totalPoint += $userPoint['d'][$i]['userPoint'] ;    
                $less_point -= $userPoint['d'][$i]['userPoint'] ;
                array_push($index,$i);
                if( $less_point <= 0 )
                    break;
            }
        }            
                    
        //點數不足 不用扣了
        if( $totalPoint < $subtration_point )
            return 2 ;

        $less_point = $subtration_point;
    
        for($i=0;$i<count($index);$i++){
            $less_point -= $userPoint['d'][$index[$i]]['userPoint'] ; 
            
            if( $less_point  >= 0  ){  
                //點數全扣光情況
                $data = array(  'userPoint' => 0 ,
                                'userPointUsed' => $userPoint['d'][$index[$i]]['userPointEarn'] ,
                            );
                $userPointPay = $userPoint['d'][$index[$i]]['userPoint'];  
            }
            else{  
                //點數還有剩情況
                $data = array( 'userPoint' => $less_point * (-1) ,
                               'userPointUsed' => $userPoint['d'][$index[$i]]['userPointEarn'] + $less_point ,
                            );
                $userPointPay = $userPoint['d'][$index[$i]]['userPointEarn'] + $less_point ;  
            }

            if(!CZ::model('users')->upointlogs_update($userPoint['d'][$index[$i]]['upointUid'],$data)) 
                 return false;           
        }

        return true ;           
    }

    public function upointlogs_add($data){
        return DB::add($this->dbid,$this->upoint_table_name,$data,FALSE);
    }

    public function upointlogs_getter($userUid){
        $query=array(                                                           
                'select'=>'*',
                'from'=>$this->upoint_table_name,
                'where'=>array( 'userUid=:userUid',
                            array( 'userUid' =>  $userUid ) ,
                         ),   
                'order'=> 'userPointTimeLimit',
                );
        $users=DB::data($this->dbid,$query);
        return $users;
    }

    public function upointlogs_update($upointUid,$data){
        return DB::update($this->dbid,$this->upoint_table_name,$data,array('WHERE upointUid=:upointUid',array(':upointUid'=>$upointUid))); 

    } 

    /**
     * expirePointTotal 
     * 計算今年底到期點數總和
     * @param mixed $uid 
     * @access public
     * @return int
     */
    public function expirePointTotal($user_uid){
        $userPoint=CZ::model('users')->upointlogs_getter($user_uid);
        $totalPoint=0;

        for($i=0;$i<$userPoint['args']['num'];$i++){

            //年底到期日
            $expireDate = date("Y").'-12-31';
            
            //檢查是否超過年底到期日
            $overdue=(strtotime($userPoint['d'][$i]['userPointTimeLimit']) == strtotime($expireDate) ) ;              
            //篩選出剩餘點數 >0 and 點數期限 < 年底到期日
            if( $userPoint['d'][$i]['userPoint'] >0 && $overdue  ){
                echo $userPoint['d'][$i]['userPoint'].'   '.$userPoint['d'][$i]['userPointTimeLimit'].'<br>';
                $totalPoint += $userPoint['d'][$i]['userPoint'];
            }    
        }   
        return $totalPoint;
    }

    public function upoint_pays_add($data){ 
        return DB::add($this->dbid,$this->upoint_pays_table_name,$data,FALSE);
    }       

    public function upoint_pays_update($user_uid,$upointUid,$data){                                                               
        $where = array(
                        'WHERE userUid=:userUid AND upointUid=:upointUid',
                            array(
                                   'userUid'=>$user_uid,
                                   'upointUid'=>$upointUid
                            )
                        );

        return DB::update($this->dbid,$this->upoint_pays_table_name,$data,$where);
    }



    public function upoint_pays_getter($userUid,$startDay='',$endDay=''){
        $column='*';
        $query=array(
                'select'=>$column,
                'from'=>$this->upoint_pays_table_name ,
                'where'=>array( 'userUid=:userUid',
                            array( 'userUid' =>  $userUid ) ,
                         ),   
                'order'=> 'upointlogTimeEarn desc',
                );

        if(!empty($startDay) && !empty($endDay) ){
            $query['where'][0] .= ' AND upointlogTimeEarn <=:startDay AND upointlogTimeEarn >=:endDay';
            $query['where'][1]['startDay'] = $startDay ;
            $query['where'][1]['endDay'] = $endDay ;
        }            

        $users=DB::data($this->dbid,$query);
        return $users;
    }

    public function uconnects_add($data){
        return DB::add($this->dbid,$this->uconnects_table_name,$data,FALSE);
    }              

    /**
     * uconnects_getter 
     * 已登入-你可能感興趣的人
     * 
     * @param uid4 $user_uid 查誰的
     * @param mixed $limit 取出筆數 整數:筆數
     * @access public
     * @return void
     */        
    public function uconnects_getter($userUid,$column='*',$limit=3){
        $query=array(
                'select' => $column ,
                'from'   => $this->uconnects_table_name ,
                'where'  => array('userUid1=:userUid1 ',
                    array(
                        'userUid1'=>$userUid,
                        ),
                    ),
                'order' => 'uconnectLevel desc' ,
                'limit' =>  $limit,
                );  
        return DB::data($this->dbid,$query); 
    }

    public function uconnects_count_getter($userUid){
        $query=array(
                'select' => 'count(*)' ,
                'from'   => $this->uconnects_table_name ,
                'where'  => array('userUid1=:userUid1 ',
                    array(
                        'userUid1'=>$userUid,
                        ),
                    ),
                );  
        return DB::row($this->dbid,$query); 
    }

	
	/**
	 * user_inviting_list 
	 * 取得 user_uid 的邀請名單
	 * 
	 * @param uid4 $user_uid	查誰的
	 * @param mixed $table_uid uid
	 * @param mixed $table 存取的 table_name wallposts,goods
	 * @access public
	 * @return void
	 */
	public function user_inviting_list($user_uid,$table_uid,$table='wallposts'){
		
		if($table=='goods'){$table='goods';}
		else{$table='wallposts';}
		
		
		$query=array(
			'select'=>'*',
			'from'=> 'userinviting',
			'where'=>'targetTable=:targetTable AND  targetTableUid=:targetTableUid  AND userUid1=:user_uid',
			'bind'=>array(
				':user_uid'=>$user_uid,
				':targetTable'=>$table,
				':targetTableUid'=>$table_uid,
			)
		);

		$data=DB::data($this->dbid,$query);
		return $data;
	}
	
	/**
	 * user_inviting_add 
	 *  增加user_uid 的已邀請名單
	 * 
	 * @param uid4 $user_uid	查誰的
	 * @param mixed $table_uid uid
	 * @param mixed $table 存取的 table_name wallposts,goods
	 * @access public
	 * @return void
	 */
	public function user_inviting_add($user_uid1,$user_uid2,$table_uid,$table='wallposts'){
		
		if($table=='goods'){$table='goods';}
		else{$table='wallposts';}
		
		$query=array(
			'select'=>'*',
			'from'=> 'userinviting',
			'where'=>'targetTable=:targetTable AND  targetTableUid=:targetTableUid  AND userUid1=:user_uid1 AND userUid2=:user_uid2 ',
			'bind'=>array(
				':user_uid1'=>$user_uid1,
				':user_uid2'=>$user_uid2,
				':targetTable'=>$table,
				':targetTableUid'=>$table_uid,
			)
		);

		$info=DB::row($this->dbid,$query);
		if(empty($info)){
			$data=array(
				'userinvitingUid' => uid4(),
				'targetTable'	=> $table,
				'targetTableUid'	=>	$table_uid,
				'userUid1'	=>	$user_uid1,
				'userUid2'	=>	$user_uid2,
				'userInvitingTime' 	=>	_SYS_DATETIME,
			);
			return DB::add($this->dbid,'userinviting',$data,FALSE);
		}
	}
  
  
  /**
	 * relation_list 
	 * 取得同好/朋友使用者清單
	 * 
	 * @param uid4 $user_uid	查誰的
	 * @param mixed $relation_type userUid1對userUid2的關係,(不用下bit) 0:無, 2:興趣建議, 3:送出交友邀請, 4:同好(userUid1追蹤userUid2), 5:互為好友
	 * @param mixed $limit 取出筆數 0:全部, 整數:筆數
	 * @param mixed $sort  排序依據 rand:隨機
	 * @param mixed $reverse  ,$relation_type=3或4用.....3:尚未回覆交友邀請,4:追蹤者(userUid2追蹤userUid1)
	 * @access public
	 * @return void
	 */
	public function relation_list($user_uid,$relation_type,$limit,$sort,$reverse=0){
		
		$query=array(
			'select'=>'r.relation,u.*',
			'from'=>'relations r
				LEFT JOIN users u ON r.userUid2=u.userUid',
			'where'=>'r.userUid1=:user_uid',
			'bind'=>array(
				':user_uid'=>$user_uid,
			)
		);

		
		if($relation_type==2 ){}
		else if($relation_type==3 || $relation_type==4){ 
			if($reverse!=0){
				$query['from']='relations r LEFT JOIN users u ON r.userUid1=u.userUid';
				$query['where']=' r.userUid2=:user_uid ';
			}
		}
		else{$relation_type=5;}
		
		$relation_bit_type=pow(2,$relation_type);///relation is bit in DB
		$query['where'].=' AND r.relation &'.$relation_bit_type;
		
		
		if($limit){
			$query['limit']=$limit;
		}
		if($sort){
			$query['order']='RAND()';
		}
		if($relation_type==2 ){
			$query['select'].=',c.uconnectLevel';
			$query['from'].=' LEFT JOIN uconnects c ON r.userUid2=c.userUid2 ';
			$query['order']='c.uconnectLevel desc ';
		}
		
		$relations=DB::data($this->dbid,$query);
		if($relations['args']['num']>0){
			foreach($relations['d'] as $inx => $data_info ){
					$user_uid1=$user_uid;
					$user_uid2=$data_info['userUid'];
					
					$relation_status=$this->relations_getter($user_uid1,$user_uid2);
					$relations['d'][$inx]=array_merge($relations['d'][$inx],$relation_status);
					
					if($relation_type==3){ 
						if($reverse!=0){
							$user_uid1=$data_info['userUid'];
							$user_uid2=$user_uid;
							$target_relation_status=$this->relations_getter($user_uid1,$user_uid2);
							$relations['d'][$inx]['uid2_relation_status']=$target_relation_status['relation_status'];
						}
					}

			}
		}
		
		
		return $relations;
	}
   /**
	 * relations_getter 
	 * 取得目前關係
	 * 
	 * @param uid4 $user_uid1	查誰的
	 * @param uid4 $user_uid2	user_uid1對應$user_uid2的關係
	 * @access public
	 * @return void
	 */
	public function relations_getter($user_uid1,$user_uid2){ 
      $query=array(
				'select' => '*' ,
				'from'   => $this->relations_table_name ,
				'where'  => array('userUid1=:userUid1 AND userUid2=:userUid2  ',
				    array(
				        'userUid1'=>$user_uid1,
				        'userUid2'=>$user_uid2,
				        ),
				    ),
				); 

			$relations=DB::row($this->dbid,$query);
			
			
			
			if($relations){
				$relations['relation_status']['now']='stranger';
				$relations['relation_status']['userUid2']=$user_uid2;
				if($relations['relation']&4){$relations['relation_status']['interest']='interest';}
				if($relations['relation']&8){$relations['relation_status']['inviting']='inviting';}
				if($relations['relation']&16){$relations['relation_status']['follow']='follow';}
				if($relations['relation']&32){$relations['relation_status']['friend']='friend';}
				
				//以下是應該顯示關係
				if(!empty($relations['relation_status']['interest']) ){$relations['relation_status']['now']='interest';}
				if(!empty($relations['relation_status']['interest']) && !empty($relations['relation_status']['follow'])){$relations['relation_status']['now']='follow';}
				if(!empty($relations['relation_status']['interest']) && !empty($relations['relation_status']['inviting'])){$relations['relation_status']['now']='inviting'; }
				if(!empty($relations['relation_status']['follow']) ){$relations['relation_status']['now']='follow';}
				if(!empty($relations['relation_status']['follow']) && !empty($relations['relation_status']['inviting'])){$relations['relation_status']['now']='inviting'; }
				if(!empty($relations['relation_status']['friend']) ){$relations['relation_status']['now']='friend';}
				
				
				
				return $relations;
			}
			$relations['relation_status']['now']='stranger';
			$relations['nodata']=1;
			return $relations;
	}
	/**
	 * relations_count_update 
	 * 取得目前關係
	 * 
	 * @param uid4 $user_uid1	更新誰的
	 * @access public
	 * @return void
	 */
	public function relations_count_update($user_uid1){
		
		/*
		uparameterNumAskFriends		inviting		8
		uparameterNumFollows		follow		16
		uparameterNumFriends		friends		32

		uparameterNumNeedFriends		response	8
		uparameterNumFollowers		follower	16
		
		*/
			
			
			
		//////inviting
		$relation=8;
		$query=array(
			'select' => 'count(*) as cnt' ,
			'from'   => $this->relations_table_name ,
			'where'  => array('userUid1=:userUid1 AND (relation & :relation)',
				array(
					'userUid1'	=>	$user_uid1,
					'relation'	=>	$relation,
					),
				),
		); 
		$data=DB::row($this->dbid,$query);
		
		///使用者個人紀錄
		$record_data=array(
			'uparameterNumAskFriends' => $data['cnt'],
		);
		$re['inviting']=$data['cnt'];
		CZ::model('users')->uparameters_update($user_uid1,$record_data);
		
		//////follow
		$relation=16;
		$query=array(
			'select' => 'count(*) as cnt' ,
			'from'   => $this->relations_table_name ,
			'where'  => array('userUid1=:userUid1 AND (relation & :relation)',
				array(
					'userUid1'	=>	$user_uid1,
					'relation'	=>	$relation,
					),
				),
		); 
		$data=DB::row($this->dbid,$query);
		
		///使用者個人紀錄
		$record_data=array(
			'uparameterNumFollows' => $data['cnt'],
		);
		$re['follow']=$data['cnt'];
		CZ::model('users')->uparameters_update($user_uid1,$record_data);
		
		//////friends
		$relation=32;
		$query=array(
			'select' => 'count(*) as cnt' ,
			'from'   => $this->relations_table_name ,
			'where'  => array('userUid1=:userUid1 AND (relation & :relation)',
				array(
					'userUid1'	=>	$user_uid1,
					'relation'	=>	$relation,
					),
				),
		); 
		$data=DB::row($this->dbid,$query);
		
		///使用者個人紀錄
		$record_data=array(
			'uparameterNumFriends' => $data['cnt'],
		);
		$re['friends']=$data['cnt'];
		CZ::model('users')->uparameters_update($user_uid1,$record_data);
		
		
		//////response
		$relation=8;
		$query=array(
			'select' => 'count(*) as cnt' ,
			'from'   => $this->relations_table_name ,
			'where'  => array('userUid2=:userUid1 AND (relation & :relation)',
				array(
					'userUid1'	=>	$user_uid1,
					'relation'	=>	$relation,
					),
				),
		); 
		$data=DB::row($this->dbid,$query);
		
		///使用者個人紀錄
		$record_data=array(
			'uparameterNumNeedFriends' => $data['cnt'],
		);
		$re['response']=$data['cnt'];
		CZ::model('users')->uparameters_update($user_uid1,$record_data);
		
		//////follower
		$relation=16;
		$query=array(
			'select' => 'count(*) as cnt' ,
			'from'   => $this->relations_table_name ,
			'where'  => array('userUid2=:userUid1 AND (relation & :relation)',
				array(
					'userUid1'	=>	$user_uid1,
					'relation'	=>	$relation,
					),
				),
		); 
		$data=DB::row($this->dbid,$query);
		
		///使用者個人紀錄
		$record_data=array(
			'uparameterNumFollowers' => $data['cnt'],
		);
		$re['follower']=$data['cnt'];
		CZ::model('users')->uparameters_update($user_uid1,$record_data);
		
		
		return $re;
	}
	
	
	
	
		////use  function relation_build
    public function relations_add($data){                                                                                    
        return DB::add($this->dbid,$this->relations_table_name,$data,FALSE);
    }      
		////use  function relation_build
    public function relations_update($user_uid1,$user_uid2,$data){                                                               
        $where = array(
                        'WHERE userUid1=:userUid1 AND userUid2=:userUid2',
                            array(
                                   'userUid1'=>$user_uid1,
                                    'userUid2'=>$user_uid2
                            )
                        );

        return DB::update($this->dbid,$this->relations_table_name,$data,$where);
    }
	/**
	 * relation_build 
	 * 建立會員社群關係
	 * 
	 * @param uid4 $user_uid1	查誰的
	 * @param uid4 $user_uid2	user_uid1對應$user_uid2的關係
	 * @param $action	//可以增加關係為 interest,follow,not_follow,inviting,not_inviting,not_friend,agree_friend
	 * @access public	
	 * @return after_relation
	 */
		public function relation_build($user_uid1,$user_uid2,$action){
			
			$relation=$this->relations_getter($user_uid1,$user_uid2);
			
			if($action=="demo" || $action=="view" ||  $action=="test"){
			
				return $relation;
			}
			
			///這最簡單直接建立關係
			if(!empty($relation['nodata'])){
				
				if($action=='interest'){$relation=4;}
				else if($action=='inviting'){$relation=8;}
				else if($action=='follow'){$relation=16;}
				//else if($action=='friend'){$relation=32;} //暫時不開放,只能對後台使用者開放,ps:要改寫雙方狀態
				else{$relation=0;}///其他關係不可能馬上建立
	
				$data=array(
					'userUid1'	=>	$user_uid1,
					'userUid2'	=>	$user_uid2,
					'relation'	=>	$relation,
					'relationTime'	=>	_SYS_DATETIME,
					'relationTimeResponse'	=>	_SYS_DATETIME,
				);
				$this->relations_add($data);
			}
			else{
				$relation_now=$relation['relation_status']['now'];
				
				////當收到指令是強制收好友
				if($action=='agree_friend' ){
						$targe_relation=$this->relations_getter($user_uid2,$user_uid1);
						////確認對方關係有邀你加好友
						if(!empty($targe_relation['relation_status']['inviting'])){
							$relation=48; // 32 + 16
							$data=array(
								'relation'	=>	$relation,	//成為好友會把雙方關係都變 好友+(同好)
								'relationTime'	=>	_SYS_DATETIME,
								'relationTimeResponse'	=>	_SYS_DATETIME,
							);
							$this->relations_update($user_uid1,$user_uid2,$data);
							$this->relations_update($user_uid2,$user_uid1,$data);
							
							
							
							
							////更新雙方狀態
							$this->relations_count_update($user_uid1);
							$this->relations_count_update($user_uid2);
							
							///通知
							$userUid1=$user_uid1;	
							$userUid2=$user_uid2;
							$xuser1=CZ::model('users')->xuser($userUid1,1,0,0);
							$xuser2=CZ::model('users')->xuser($userUid2,1,0,0);
							
							
							///user_uid1
							$from_uid=$user_uid2;
							$to_uid=$user_uid1;
							$target_user_uid=$user_uid2;
							$target_url='http://www.countmin.com/user/mywalls/'.$target_user_uid;
							$notice_id='Ly_other_make_friendship';
							$notice_data=array(
									'user_name' =>$xuser2['basic']['userRealname'] ,
							);
							$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
							if($notificationSetting['friend']==0 || $notificationSetting['friend']==2){
									$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_user_uid,$notice_id,$notice_data);
							}
							
							///user_uid2
							$from_uid=$user_uid1;
							$to_uid=$user_uid2;
							$target_user_uid=$user_uid1;
							$target_url='http://www.countmin.com/user/mywalls/'.$target_user_uid;
							$notice_id='Ly_other_make_friendship';
							$notice_data=array(
									'user_name' =>$xuser1['basic']['userRealname'] ,
							);
							$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
							if($notificationSetting['friend']==0 || $notificationSetting['friend']==2){
										$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_user_uid,$notice_id,$notice_data);
							}
							
							$after_relation=$this->relations_getter($user_uid1,$user_uid2);
							
							///個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉 ,11-男追蹤者,12-女追蹤者
							if($xuser1['basic']['userGender']==1){
								$follow_gender=11;
							}
							else{
								$follow_gender=12;
							}
							$self_point=0;
							CZ::model('wallposts')->uwallpost_socials_add($userUid2,'',$userUid1,$follow_gender,$self_point);
							
							///個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉 ,11-男追蹤者,12-女追蹤者
							if($xuser2['basic']['userGender']==1){
								$follow_gender=11;
							}
							else{
								$follow_gender=12;
							}
							$self_point=0;
							CZ::model('wallposts')->uwallpost_socials_add($userUid1,'',$userUid2,$follow_gender,$self_point);
							
							
							return $after_relation;
						}
				}
				$work=0;
				$data=array(
					'relationTime'	=>	_SYS_DATETIME,
					'relationTimeResponse'	=>	_SYS_DATETIME,
				);
				switch($relation_now){
					case 'stranger':
						if($action=='interest'){
							$relation=4;
							$work=1;
							$data['relation']=array(' relation + '.$relation);
						}
						if($action=='follow'){
							$relation=16;
							$work=1;
							$data['relation']=array(' relation + '.$relation);
						}
						else if($action=='inviting'){
							$relation=8;
							$work=1;
							$data['relation']=array(' relation + '.$relation);
						}
						else if($action=='not_inviting'){
							$relation=8;
							$work=1;
							$data['relation']=array(' relation - '.$relation);
						}
						if($work==1){$this->relations_update($user_uid1,$user_uid2,$data);}
						break;
					case 'interest':
						if($action=='follow'){
							$relation=16;
							$work=1;
							$data['relation']=array(' relation + '.$relation);
						}
						else if($action=='inviting'){
							$relation=8;
							$work=1;
							$data['relation']=array(' relation + '.$relation);
						}
						if($work==1){$this->relations_update($user_uid1,$user_uid2,$data);}
						break;
					case 'follow':
						if($action=='inviting'){
							$relation=8;
							$work=1;
							$data['relation']=array(' relation + '.$relation);
						}
						if($action=='not_follow' && !empty($relation['relation_status']['follow'])){
							$relation=16;
							$data['relation']=array(' relation - '.$relation);
							$work=1;
						}
						if($work==1){$this->relations_update($user_uid1,$user_uid2,$data);}
						break;	
					case 'inviting':
						if($action=='follow'){
							$relation=16;
							$work=1;
							$data['relation']=array(' relation + '.$relation);
						}
						if($action=='not_follow' && !empty($relation['relation_status']['follow'])){
							$relation=16;
							$data['relation']=array(' relation - '.$relation);
							$work=1;
						}
						if($action=='not_inviting' && !empty($relation['relation_status']['inviting'])){
							$relation=8;
							$data['relation']=array(' relation - '.$relation);
							$work=1;
						}
						if($work==1){$this->relations_update($user_uid1,$user_uid2,$data);}
						break;
					case 'friend':
						if($action=='not_friend' && !empty($relation['relation_status']['friend'])){
							$relation=20;	//取消好友會把雙方關係都改為追蹤(同好)與(興趣)	
							$data=array(
								'relation'	=>	$relation,
								'relationTime'	=>	_SYS_DATETIME,
								'relationTimeResponse'	=>	_SYS_DATETIME,
							);
							$this->relations_update($user_uid1,$user_uid2,$data);
							$this->relations_update($user_uid2,$user_uid1,$data);
						}
						break;
					default:break;
				}
			}
			
			
			////更新雙方狀態
			$this->relations_count_update($user_uid1);
			$this->relations_count_update($user_uid2);
			
			///通知
			$userUid1=$user_uid1;	
			$userUid2=$user_uid2;
			$xuser1=CZ::model('users')->xuser($userUid1,1,0,0);
			$xuser2=CZ::model('users')->xuser($userUid2,1,0,0);
			
			if($action=="follow"){
				$from_uid=$user_uid1;
				$to_uid=$user_uid2;
				$target_user_uid=$user_uid1;
				$target_url='http://www.countmin.com/user/socially?status=follower';
				$notice_id='Ly_other_follow_me';
				$notice_data=array(
						'user_name' =>$xuser1['basic']['userRealname'] ,
				);
				$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
				if($notificationSetting['follow']==0 || $notificationSetting['follow']==2){
							$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_user_uid,$notice_id,$notice_data);
				}
			}
			if($action=="inviting"){
				$from_uid=$user_uid1;
				$to_uid=$user_uid2;
				$target_user_uid=$user_uid1;
				$target_url='http://www.countmin.com/user/socially?status=response';
				$notice_id='Ly_other_invite_friendship';
				$notice_data=array(
						'user_name' =>$xuser1['basic']['userRealname'] ,
				);
				$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
				//if($notificationSetting['inviting']==0 || $notificationSetting['inviting']==2){
							$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_user_uid,$notice_id,$notice_data);
				//}
			}
			
			
			///個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉 ,11-男追蹤者,12-女追蹤者
			if($action=="follow"){
				if($xuser1['basic']['userGender']==1){
					$follow_gender=11;
				}
				else{
					$follow_gender=12;
				}
				$self_point=0;
				CZ::model('wallposts')->uwallpost_socials_add($userUid2,'',$userUid1,$follow_gender,$self_point);
			}
			
			
			$after_relation=$this->relations_getter($user_uid1,$user_uid2);
			
			return $after_relation;
		}

    //計算你可能感興趣的人數量    
    public function relation_list_count($user_uid,$relation_type){
        $query=array(
            'select'=>'count(*)',
            'from'=>'relations', 
            'where'=>'userUid1=:user_uid',
            'bind'=>array(
                ':user_uid'=>$user_uid,
            )
        );

        return DB::row($this->dbid,$query); 
    }

        public function uaccs_add($data){
            return DB::add($this->dbid,$this->uaccs_table_name,$data,FALSE);
        }

        public function uaccs_getter($user_uid,$column='*'){ 
            $query=array(
                'select' => $column,
                'from'   => $this->uaccs_table_name ,
                'where'  => array('userUid=:userUid',
                    array(
                        'userUid'=>$user_uid,
                        ),
                    ),
                ); 
            return DB::row($this->dbid,$query); 
        }                              

        public function uaccs_getter_list($column='*',$spage=0){ 
            $query=array(
                'select' => $column,
                'from'   => $this->uaccs_table_name ,
                'where'  => array(' uaccRegistStep IN ( 0,1) '),
                'spage'  => $spage,
                'per'    => 50,
                );
            return DB::data($this->dbid,$query); 
        }                      


        public function uaccs_uaccId_getter($uaccId,$column='uaccId'){ 
            $query=array(
                'select' => $column,
                'from'   => $this->uaccs_table_name ,
                'where'  => array('uaccId=:uaccId',
                    array(
                        'uaccId'=>$uaccId,
                        ),
                    ),
                ); 
            return DB::data($this->dbid,$query); 
        }      

        public function uaccs_update($user_uid,$data){        
            $attach = array('WHERE userUid=:userUid',
                                array('userUid'=>$user_uid)
                            );
            return DB::update($this->dbid,$this->uaccs_table_name,$data,$attach);
        }        

        public function uacctokens_getter($uacc_uid,$column='*'){ 
            $query=array(
                'select' => $column,
                'from'   => $this->uacctokens_table_name ,
                'where'  => array('uaccUid=:uaccUid',
                    array(
                        'uaccUid'=>$uacc_uid,
                        ),
                    ),
                ); 

            return DB::row($this->dbid,$query); 
        }        

        public function facebook_pwd_check($uaccId,$uaccToken){ 

            $query=array(
                        'select' => 'u.userUid,u.userRealname,u.userType,ua.uaccUid',
                        'from'   => $this->table_name.' u,'.$this->uaccs_table_name.' ua' ,
                        'where'  => array('u.userUid=ua.userUid AND ua.uaccId=:uaccId',
                                        array('uaccId'=>$uaccId),
                                    ),
                    );         
            $facebook=DB::row($this->dbid,$query);
            
            //驗證成功後存 access_token
            if($facebook){
                $data=array('uaccToken'=>$uaccToken);
                $uacctokens=$this->uacctokens_update($facebook['uaccUid'],$data);
            }

            return $facebook;
        }        

        public function uacctokens_add($data){
            return DB::add($this->dbid,$this->uacctokens_table_name,$data,FALSE);
        }

        public function uacctokens_update($uaccUid,$data){        
            $attach = array('WHERE uaccUid=:uaccUid',
                                array('uaccUid'=>$uaccUid)
                            );
            return DB::update($this->dbid,$this->uacctokens_table_name,$data,$attach);
        }        


        public function userdetails_add($data){
            return DB::add($this->dbid,$this->userdetails_table_name,$data,FALSE);
        }
    

        public function userdetails_getter($user_uid,$column='*'){
        $query=array(
                'select' => $column,
                'from'   => $this->userdetails_table_name ,
                'where'  => array('userUid=:userUid',
                    array(
                        'userUid'=>$user_uid,
                        ),
                    ),
                );

        return DB::row($this->dbid,$query);
        }

        public function userdetails_update($user_uid,$data){        
            $attach = array('WHERE userUid=:userUid',
                                array('userUid'=>$user_uid)
                            );

            return DB::update($this->dbid,$this->userdetails_table_name,$data,$attach);
        }

        public function contactcc_add($data){                                                                                    
            return DB::add($this->dbid,$this->contactcc_table_name,$data,FALSE);
        }                  

        public function privacy_add($data){                                                                                    
            return DB::add($this->dbid,$this->privacy_table_name,$data,FALSE);
        }       

        public function privacy_getter($user_uid,$column='*'){                                                                           
            $query=array(
                'select' => $column,
                'from'   => $this->privacy_table_name ,
                'where'  => array('userUid=:userUid',
                    array(
                        'userUid'=>$user_uid,
                        ),
                    ),
                );

            return DB::row($this->dbid,$query);
        }

        public function privacy_update($user_uid,$data){                                                                      
            return DB::update($this->dbid,$this->privacy_table_name,$data,array('WHERE userUid=:userUid',array('userUid'=>$user_uid)));
        }

        public function blacklist_add($data){                                                                                    
            return DB::add($this->dbid,$this->blacklist_table_name,$data,FALSE);
        }       

        public function blacklist_getter($user_uid){ 
        $column='b.userUid,b.blackUserUid,u.userRealname,u.userProfile';
        $query=array(
                'select' => $column,
                'from'   => $this->blacklist_table_name.' b ,'.$this->table_name.' u ',
                'where'  => array('b.blackUserUid=u.userUid AND b.userUid=:userUid',
                    array(
                        'userUid'=>$user_uid,
                        ),
                    ),
                'limit'  => 30,
                );

        return DB::data($this->dbid,$query);
        }

        public function blacklist_delete($user_uid,$blackUserUid){

            $attach=array('WHERE userUid=:userUid and blackUserUid=:blackUserUid',
                        array(  
                                'userUid'=>$user_uid,
                                'blackUserUid'=>$blackUserUid 
                             ),
                    );

            return DB::del($this->dbid,$this->blacklist_table_name,$attach );
        }

        public function regions_getter($regionIsCity,$regionUidRef=''){ 
            $query=array(
                'select' => '*',
                'from'   => $this->regions_table_name,
                'where'  => array(),
                'order'  => 'regionUid',
                );
            $query['where'][0] = '';
            $query['where'][0] .= 'regionIsCity=:regionIsCity' ;
            $query['where'][1]['regionIsCity']= $regionIsCity ;

            if( !empty($regionUidRef) ){
                $query['where'][0] .= ' AND regionUidRef=:regionUidRef' ;
                $query['where'][1]['regionUidRef']= $regionUidRef ;
            }
            return DB::data($this->dbid,$query);
        }

        public function regions_regionuid_getter($regionUid){ 
            $query=array(
                'select' => '*',
                'from'   => $this->regions_table_name,
                'where'  => array('regionUid=:regionUid',
                                    array('regionUid'=>$regionUid),
                                ),
                );
            return DB::row($this->dbid,$query);
        }

        public function userexps_getter($user_uid,$userexpType=1){                                                                           
            $query=array(
                'select' => ' * ',
                'from'   => $this->userexps_table_name ,
                'where'  => array('userUid=:userUid AND userexpType=:userexpType' ,
                    array(
                        'userUid'=>$user_uid,
                        'userexpType'=>$userexpType,
                        ),
                    ),
                'order'  => 'userexpEndYear desc',
                );

            return DB::data($this->dbid,$query);
        }

        public function userexps_add($data){ 
            return DB::add($this->dbid,$this->userexps_table_name,$data,FALSE);
        }

        public function userexps_update($user_uid,$userexpUid,$data){ 
            $attach=array(  'WHERE userUid=:userUid AND userexpUid=:userexpUid',
                        array('userUid'=>$user_uid,
                              'userexpUid'=>$userexpUid,  
                            ),
                        );
            return DB::update($this->dbid,$this->userexps_table_name,$data,$attach);
        }

        public function userexps_delete($user_uid,$userexpUid){
            $attach=array('WHERE userUid=:userUid and userexpUid=:userexpUid',
                        array(
                                'userUid'=>$user_uid,
                                'userexpUid'=>$userexpUid
                             ),
                    ); 
            return DB::del($this->dbid,$this->userexps_table_name,$attach ); 
        }

        public function userphotoes_getter($user_uid){                                                                           
            $query=array(
                'select' => ' * ',
                'from'   => $this->userphotoes_table_name ,
                'where'  => array('userUid=:userUid' ,
                    array(
                        'userUid'=>$user_uid,
                        ),
                    ),
                'order'  => 'userphotoTimeUpload desc',
                );

            return DB::data($this->dbid,$query);
        }

        public function userphotoes_add($data){ 
            return DB::add($this->dbid,$this->userphotoes_table_name,$data,FALSE);           
        }        

        public function userphotoes_update($user_uid,$imageUid='',$data){ 
            $attach=array(  'WHERE userUid=:userUid ' ,
                            array('userUid'=>$user_uid,)
                        );

            if(!empty($imageUid)){
                $attach[0] .= ' AND imageUid=:imageUid ';                
                $attach[1]['imageUid']=$imageUid;    
            }

            return DB::update($this->dbid,$this->userphotoes_table_name,$data,$attach);
        }

        public function userphotoes_delete($user_uid,$imageUid){
            $attach=array('WHERE userUid=:userUid and imageUid=:imageUid',
                        array(
                                'userUid'=>$user_uid,
                                'imageUid'=>$imageUid,
                             ),
                    );
            return DB::del($this->dbid,$this->userphotoes_table_name,$attach ); 
        }
        
        public function usermasters_getter($interestId=''){                                                                           
            $str='' ;
            $query=array(
                'select' => ' m.*,u.userRealname,u.userProfile,u.userTopbanner,u.userInterests ',
                'from'   => $this->usermasters_table_name.' m,'.$this->table_name.' u' ,
                'where'  => array('m.userUid=u.userUid AND u.userType=0 AND u.userEnable=1'),
                'limit'  => 20,
                );

            if( !empty($interestId)){    
                $tag=explode(";",$interestId);
                for($i=0;$i<count($tag);$i++){
                    if( $i == count($tag)-1 )
                        $str .= 'm.interestId LIKE :tag'.$i.' ' ;
                    else
                        $str .= 'm.interestId LIKE :tag'.$i.' OR ';    

                    $bind['tag'.$i]= '%'.$tag[$i].'%';
                }

                $query['where'][0] .= ' AND ('.$str.')' ;    
                $query['where'][1] = $bind;
            }

            return DB::data($this->dbid,$query);
        }

        public function usermasters_add($data){                                           
            return DB::add($this->dbid,$this->usermasters_table_name,$data,FALSE);           
        }

        public function usermasters_update($user_uid,$data){                 
            $attach=array(  'WHERE userUid=:userUid ' ,
                            array('userUid'=>$user_uid,)
                        );                  
            return DB::update($this->dbid,$this->usermasters_table_name,$data,$attach);
        }           


}


