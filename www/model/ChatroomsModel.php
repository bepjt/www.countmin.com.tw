<?php

class ChatroomsModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='chatrooms';
		$this->field_pk='chatroomUid';
		$this->field_pk_charset='';
		$this->fields=array(
			'chatroomUid'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//聊天室UUID
			'chatroomName'=>array('charset'=>'string','max'=>16,'default'=>'','req'=>0),	//聊天室名稱
			'chatroomNumUsers'=>array('charset'=>'uint','max'=>255,'default'=>0,'req'=>0),	//成員人數(上限 100 人)
			'chatroomTimeLastUpdate'=>array('charset'=>'timestamp','req'=>0),	//最後新增訊息時間
			'userUidLastUpdate'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//最後發訊息的帳號UUID
			'chatroomMessageLastUpdate'=>array('charset'=>'string','max'=>20,'default'=>'','req'=>0),	//最後訊息的20個字
		);
		$this->relation_tables=array();
	}

	/**
	 * users_at_chatroom 
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $xusers 所有聊天室內的 userUid
	 * @access public
	 * @return void
	 */
	public function users_at_chatroom($user_uid,$xusers){
		sort($xusers);
		$hash=md5(join(':',$xusers));
		$query=array(
			'select'=>'*',
			'from'=>'chatrooms c
				LEFT JOIN chatroom_user cu ON c.chatroomUid=cu.chatroomUid',
			'where'=>array(
				'c.chatroomUsersHash=:hash AND cu.userUid=:user_uid',
				array(
					':hash'=>$hash,
					':user_uid'=>$user_uid,
				)
			),
		);
		$chatrooms=DB::data($this->dbid,$query);
		if(!$chatrooms['args']['num']) return NULL;
		else{
			return $chatrooms['d'][0]['chatroomUid'];
		}
	}

	/**
	 * chatroom_add 
	 * 新增 chatroom
	 * 
	 * @param mixed $user_uid 發起的 user_uid
	 * @param mixed $users 所有被加入的 user_uid 清單，為自然序列陣列，應由 command 檢查是否有重複
	 * @access public
	 * @return chatroomUid
	 */
	public function chatroom_add($user_uid,$users,$chatroom_name=''){
		if(count($users)<2){
			return FALSE;
		}
		sort($users);
		$hash=md5(join(':',$users));
		$query=array(
			'select'=>'*',
			'from'=>'chatrooms c
				LEFT JOIN chatroom_user cu ON c.chatroomUid=cu.chatroomUid',
			'where'=>array(
				'c.chatroomUsersHash=:hash AND cu.userUid=:user_uid',
			),
			'bind'=>array(
				':user_uid'=>$user_uid,
				':hash'=>$hash,
			)
		);
		$chatroom=DB::row($this->dbid,$query);
		if($chatroom!==NULL){
			return $chatroom['chatroomUid'];
		}
		else{
			$chatroom_uid=uid4();
			$data=array(
				'chatroomUid'=>$chatroom_uid,
				'chatroomName'=>strlen($chatroom_name)?$chatroom_name:mb_substr(join(',',array_values($users)),0,16,'UTF-8'),
				'chatroomNumUsers'=>count($users),
				'chatroomUsersHash'=>$hash,
				'chatroomTimeLastUpdate'=>_SYS_DATETIME,
				'userUidLastUpdate'=>ME::user_uid(),
				'chatroomMessageLastUpdate'=>_SYS_DATETIME,
			);
			DB::add($this->dbid,'chatrooms',$data);
			$data=array('default'=>
				array(
					'chatroomUid'=>$chatroom_uid,
					'userUid'=>'',
					'cuTimeCreate'=>_SYS_DATETIME,
					'cuTimeLastRead'=>'0000-00-00',
				)
			);
			for($i=0,$n=count($users);$i<$n;$i++){
				$data[]=array(
					'userUid'=>$users[$i]
				);
			}
			DB::adds($this->dbid,'chatroom_user',$data);
			return $chatroom_uid;
		}
	}

	/**
	 * chatroom_list 
	 * $user_uid 參加過的 chatrooms
	 * 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function chatroom_list($user_uid){
		$query=array(
			'select'=>'*',
			'from'=>'chatrooms c
				LEFT JOIN chatroom_user cu ON c.chatroomUid=cu.chatroomUid',
			'where'=>array(
				'cu.userUid=:user_uid',
			),
			'order'=>array(
				'c.chatroomTimeLastUpdate DESC',
			),
			'bind'=>array(
				':user_uid'=>$user_uid,
			)
		);
		return DB::data($this->dbid,$query);
	}

	/**
	 * user_listing_at_chatrooms 
	 * $chatroom_uid 的使用者名單
	 * 
	 * @param mixed $chatroom_uid 
	 * @access public
	 * @return void
	 */
	public function user_listing_at_chatrooms($chatroom_uid){
		$query=array(
			'select'=>'cu.userUid,u.userRealname',
			'from'=>'chatroom_user cu
				LEFT JOIN users u ON cu.userUid=u.userUid',
			'where'=>'cu.chatroomUid IN (:chatroom)',
			'bind'=>array(
				':chatroom'=>$chatroom_uid,
			),
		);
		return DB::data($this->dbid,$query);
	}

	/**
	 * chatroom_getter 
	 * 取得聊天室列表
	 * 依最後更新時間反向排序
	 * 
	 * @param mixed $user_uid 
	 * @access public
	 * @return void
	 */
	public function chatroom_getter($user_uid){
		$query=array(
			'select'=>'c.chatroomUid AS uid, c.chatroomName AS name, c.chatroomNumUsers AS num_users, c.chatroomTimeLastUpdate AS time_lastupdate, c.userUidLastUpdate AS user_lastupdate, c.chatroomMessageLastUpdate AS message_lastupdate',
			'from'=>'chatrooms c
				LEFT JOIN chatroom_user cu ON c.chatroomUid=cu.chatroomUid',
			'where'=>array(
				'cu.userUid=:user_uid',
			),
			'order'=>array(
				'c.chatroomTimeLastUpdate DESC',
			),
			'bind'=>array(
				':user_uid'=>$user_uid,
			)
		);
		$chatrooms=DB::data($this->dbid,$query);
		if($chatrooms['args']['num']){
			for($i=0;$i<$chatrooms['args']['num'];$i++){
				$chatrooms_uids[]=$chatrooms['d'][$i]['uid'];
			}
			$query=array(
				'select'=>'cu.chatroomUid,cu.userUid,u.userRealname',
				'from'=>'chatroom_user cu
					LEFT JOIN users u ON cu.userUid=u.userUid',
				'where'=>'cu.chatroomUid IN (:chatrooms)',
				'bind_array'=>array(
					':chatrooms'=>$chatrooms_uids,
				),
			);
			$cu=DB::data($this->dbid,$query);
			$cus=array();
			for($i=0;$i<$cu['args']['num'];$i++){
				if(!isset($cus[$cu['d'][$i]['chatroomUid']])){
					$cus[$cu['d'][$i]['chatroomUid']]=array();
				}
				$cus[$cu['d'][$i]['chatroomUid']][]=array(
					'userUid'=>$cu['d'][$i]['userUid'],
					'userRealname'=>$cu['d'][$i]['userRealname'],
				);
			}
			for($i=0;$i<$chatrooms['args']['num'];$i++){
				$chatrooms['d'][$i]['userListing']=isset($cus[$chatrooms['d'][$i]['uid']])?$cus[$chatrooms['d'][$i]['uid']]:array();
			}
		}
		return $chatrooms;
	}

	/**
	 * chatroom_one_by_one 
	 * 取得一對一聊天的聊天室
	 * 
	 * @param mixed $user_uid_1 
	 * @param mixed $user_uid_2 
	 * @access public
	 * @return void
	 */
	public function chatroom_one_by_one($user_uid_1,$user_uid_2){
		$users=array($user_uid_1,$user_uid_2);
		sort($users);
		$hash=md5(join(':',$user_uids));

		$query=array(
			'select'=>'*',
			'from'=>'chatrooms c
				LEFT JOIN chatroom_user cu ON c.chatroomUid=cu.chatroomUid',
			'where'=>array(
				'c.chatroomUsersHash=:hash AND cu.userUid=:user_uid',
			),
			'bind'=>array(
				':user_uid'=>$user_uid_1,
				':hash'=>$hash,
			)
		);
		$chatroom=DB::row($this->dbid,$query);
		return $chatroom;
	}

	/**
	 * chatroom_view 
	 * 更新 $user_uid 查看 $chatroom_uid 的時間
	 * 
	 * @param mixed $user_uid 
	 * @param mixed $chatroom_uid 
	 * @access public
	 * @return void
	 */
	public function chatroom_view($user_uid,$chatroom_uid){
		$data=array(
			'cuTimeLastRead'=>_SYS_DATETIME,
		);
		DB::update($this->dbid,'chatroom_user',$data,array('WHERE userUid=:user_uid AND chatroomUid=:chatroom_uid',array(':user_uid'=>$user_uid,':chatroom_uid'=>$chatroom_uid)));
	}

	public function message_getter($user_uid,$chatroom_uid,$timeup,$timebefore){
		$query=array(
			'select'=>'cm.*,u.userRealname,u.userProfile',
			'from'=>'chatmessages cm
				LEFT JOIN users u ON cm.userUid=u.userUid',
			'where'=>array(
				'cm.chatroomUid=:chatroom_uid',
			),
			'order'=>array(
				'chatmessageTime',
			),
			'bind'=>array(
				':chatroom_uid'=>$chatroom_uid,
			)
		);
		if($timebefore){
			$query['where'][0].=' AND chatmessageTime<=:time';
			$query['bind'][':time']=$timeup;
		}
		else{
			$query['where'][0].=' AND chatmessageTime>=:time';
			$qyery['order']='chatmessageTime DESC';
			$query['bind'][':time']=$timeup;
		}
		$messages=DB::data($this->dbid,$query);
		return $messages;
	}

	public function message_add($user_uid,$chatroom_uid,$content){
		$chatmessage_uid=uid4();
		$data=array(
			'chatmessageUid'=>$chatmessage_uid,
			'chatroomUid'=>$chatroom_uid,
			'userUid'=>$user_uid,
			'chatmessage'=>$content,
			'chatmessageTime'=>_SYS_DATETIME,
		);
		DB::add($this->dbid,'chatmessages',$data);
	}
}

