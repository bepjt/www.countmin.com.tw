<?php

class ImagesModel extends Model_Base {


    public function __construct(){
        $this->dbid='common';
        $this->table_name='images';
        $this->field_pk='imageUid';
        $this->field_pk_charset='uid4';
        $this->fields=array(
                'imageUid'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0), //圖檔UUID
                'imageSavedname'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0), //儲存的檔名(不含副檔名)
                'imageExtname'=>array('charset'=>'string','max'=>6,'default'=>'','req'=>0), //副檔名
                'imageTable'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0), //屬於哪個項目使用的圖檔
                'imageTableId'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0), //該項目的UUID
                'imageCapacity'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0), //檔案大小(bytes)
                'imageWidth'=>array('charset'=>'uint','max'=>65535,'default'=>0,'req'=>0), //寬度(pixels)
                'imageHeight'=>array('charset'=>'uint','max'=>65535,'default'=>0,'req'=>0), //高度(pixels)
                'imagePublic'=>array('charset'=>'uint','max'=>255,'default'=>0,'req'=>0), //是否為公開圖檔
                'userUidUpload'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0), //上傳者
                'imageTimeUpload'=>array('charset'=>'timestamp','req'=>0), //圖檔上傳時間
                'imageStorageId'=>array('charset'=>'string','max'=>12,'default'=>'','req'=>0), //儲存的伺服器代碼(參見etc/sys.php)
                );
        $this->rels=array();
        $this->imgUploadPath = _DIR_DOCS.'public/images/' ;
    }
		/*
    public function wallpost_covers($images){
        if(!count($images)){
            return DB::empty_data();
        }
        $query=array(
                'select'=>'*',
                'from'=>$this->table_name,
                'where'=>array(
                    'imageTable="wallposts" AND imageUid IN ('.str_repeat('?,',count($images)-1).'?)',
                    $images
                    ),
                'key'=>'imageUid',
                );
        
        $images=DB::data($this->dbid,$query);

        return $images;
    }*/


    /**
     * image_add 
     * 
     * @param mixed $tmp_path 暫存的絕對路徑
     * @param mixed $source_name 原始圖檔完整名稱
     * @param mixed $save_mode 儲存方式
     * @access public
     * @return void
     */
    public function wallpost_image_add($tmp_path,$source_name,$save_mode){
        if(!is_file($tmp_path)){
            return NULL;
        }
        $subname=strtolower(strchr($source_name,'.'));
        if(!in_array($subname,array('.jpg','.png','.gif','.jpeg'))){
            @unlink($tmp_path);
            return NULL;
        }
        $capacity=filesize($tmp_path);
        switch($save_mode){
            case 'wallpost':
                return $this->image_add_wallpost($tmp_path,$source_name,$subname,$capacity);
                break;
        }
    }

    public function image_add($data){
        return DB::add($this->dbid,$this->table_name,$data,FALSE);

    }    

    /**
     * image_convert 
     * 
     * 圖檔類別(convert_type)：
     *	profile: 使用者大頭照		需輸出 75*75(_s) 和 140*140(_m) 兩種尺寸，限制上傳至少要 500*500
     *	topbanner:	使用者橫幅圖	需輸出 960*250(_t)， 228*175(_ts)    限制上傳至少要 1200*250
     *	goods:	    好康圖檔	    需輸出 235*156(_dl) 348*277(_dd)  55*44(_do)
     *  brand:      品牌logo圖      需輸出 36*36(_b)		
     *	wallpost:	發表動態
     *	lottery:	發表抽獎活動
     *	spread:		發表推廣活動
     *	pulling:	發表揪團活動
     *	所有圖檔都要輸出
     *		1.原比例，寬度為 700px 的尺寸(_w)給興趣牆使用
     *		2.寬 180px 高 180px 的尺寸(_g)給相本使用
     *		3.原圖(_o)
     *	輸出圖檔命名方式, ex: 
     *		原圖 => [imageUid]_o.png
     *		topbanner => [imageUid]_t.png
     *		profile => [imageUid]_s.png & [imageUid]_m.png
     *	儲存的路徑為 substr($imageUid,0,3).'/'.substr($imageUid,-3).'/'.[imageUid][圖檔類別].[副檔名]
     * 
     * @param mixed $tmp_path 來源圖檔的路徑，以 / 結尾
     * @param mixed $source_name 來源圖檔的完整暫存檔名，ex: abcdefghijk.jpg, phpNJk129.tmp
     * @param mixed $subname 來源圖檔的副檔名，包含 .，ex: .jpg, .jpeg, .png
     * @param mixed $convert_type 要轉存的圖檔類別
     * @param mixed $storage_id 儲存的區塊，連線方式、路徑等參照 etc/sys.php storage
     *	// 20151022 by brandt: 先不分儲存區塊
     * @access private
     *
     * @return array('uid'=>imageUid, 'tmp'=>array(所暫存的檔名清單，不含路徑))
     */
    public function image_convert($source_path,$source_name,$subname,$convert_type='',$zoom=100 , $x='',$y='',$heigh='',$width='',$storage_id=1){
        $tmp_name=uid4();
        $conf=CZ::config_get('storage.'.$storage_id);
        $image_temp=array();
        if(!is_readable($source_path) || !is_file($source_path.$source_name)){
            return NULL;
        }
        $info=getimagesize($source_path.$source_name);
        if(empty($info[0]) || empty($info[1])){
            return NULL;
        }

        // 原圖
        $image_temp[]=$tmp_name.'_o.png';
        image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_o.png', '100%', substr($subname,1), 'png') ;

        if($convert_type=='profile'){
            //先依zoom 倍率縮放
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_z.png', ($zoom*100).'%', substr($subname,1), 'png') ;

            //再依xy裁切-----------
            $image_temp[]=$tmp_name.'_m.png';
            $this->image_cut('/tmp/'.$tmp_name.'_z.png','/tmp/'.$tmp_name.'_m.png','png',$zoom,$x,$y,$heigh,$width) ;

            $image_temp[]=$tmp_name.'_s.png';
            image_resize('/tmp/'.$tmp_name.'_m.png', '/tmp/'.$tmp_name.'_s.png', '75*75', 'png' );

            // 相本使用
            $image_temp[]=$tmp_name.'_g.png';
            image_resize( '/tmp/'.$tmp_name.'_m.png', '/tmp/'.$tmp_name.'_g.png', '180*180', 'png');
        }
        else if($convert_type=='topbanner'){
            $image_temp[]=$tmp_name.'_t.png';
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_t.png', '960*250', substr($subname,1), 'png');

            $image_temp[]=$tmp_name.'_ts.png';
            image_resize('/tmp/'.$tmp_name.'_t.png', '/tmp/'.$tmp_name.'_ts.png', '228*175', 'png');

        }
        else if( $convert_type=='goods'){
            //商品列表
            $image_temp[]=$tmp_name.'_dl.png';  
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_dl.png', '235*156', substr($subname,1), 'png');

            //商品內容
            $image_temp[]=$tmp_name.'_dd.png';  
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_dd.png', '348*277', substr($subname,1), 'png');

            //訂單頁面
            $image_temp[]=$tmp_name.'_do.png';  
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_do.png', '55*44', substr($subname,1), 'png');

        }
        else if( $convert_type=='brand'){
            $image_temp[]=$tmp_name.'_b.png';  
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_b.png', '36*36', substr($subname,1), 'png');
        }
        else{
            // 興趣牆使用
            $image_temp[]=$tmp_name.'_w.png';
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_w.png', '700', substr($subname,1), 'png');

            // 相本使用
            $image_temp[]=$tmp_name.'_g.png';
            image_resize($source_path.$source_name, '/tmp/'.$tmp_name.'_g.png', '500*400', substr($subname,1), 'png');
        }

        return array('uid'=>$tmp_name,'tmp'=>$image_temp);
    }

    /**
      image_cut
      大頭照 背景裁切用
      $src_filename     來源圖檔檔名
      $dst_filename     裁切後的圖檔檔名  ex: /tmp/xxx._m.png
      $src_extname      來源圖檔副檔名    ex: png
      $x                來源圖檔裁切點 x 座標
      $y                來源圖檔裁切 y 座標
      $heigh            裁切後的圖檔高度
      $width            裁切後的圖檔寬度
     */
    public function image_cut($src_filename,$dst_filename,$src_extname,$zoom,$x,$y,$heigh,$width){
        $image_new = imagecreatetruecolor($width,$heigh);

        switch($src_extname){
            case 'jpg':case 'jpeg':
                $image = imagecreatefromjpeg($src_filename);
                break;
            case 'png':
                $image = imagecreatefrompng($src_filename);
                break;
            case 'gif':
                $image = imagecreatefromgif($src_filename);
                break;
        }

        if( $zoom > 1) 
            imagecopyresampled($image_new, $image, 0, 0, $x*$zoom, $y*$zoom, $width*$zoom, $heigh*$zoom, $width*$zoom, $heigh*$zoom) ; //放大ok
        else
           imagecopyresampled($image_new, $image, 0, 0, $x*$zoom, $y*$zoom, $width, $heigh, $width, $heigh) ; //縮小ok

        imagepng($image_new, $dst_filename)     ;
    }


    /**
      image_move
      圖片檔案搬移並依圖片uid建立目錄
      $imgUid       圖片uid
      $tmp_path     暫存檔路徑 ex: /tmp/ 
      $tmp_file     暫存檔檔名 
      傳入array型態為多檔搬移 ex: array( 'aaa.jpg','bbb.jpg' );
      傳入string型態為單檔搬移

      $target_path  要搬移的路徑   ex: /srv/web/countmin/public/goods/

      return array( 'error' => 錯誤訊息
      'success' => true || false 
      )
     */
    public function image_move($imgUid,$tmp_path,$tmp_file,$target_path){

		$msg=array();
        $msg['success'] = false;
        $msg['error']   = ''; 

        if(strlen($imgUid) != 28  ){
            error_log( 'image uid false') ;
            return $msg;
        }    

        if( is_array($tmp_file) )
            foreach( $tmp_file as $value ){
                if(!is_file( $tmp_path.$value )){
                    error_log( $tmp_path.$value.' file not exists');   
                    return $msg ;
                }        
            }
        elseif( !is_file( $tmp_path.$tmp_file ) ){
            error_log( $tmp_path.$tmp_file.' file not exists');   
            return $msg ;
        }    

        if(!is_dir( $target_path ))
        {
            error_log( $target_path.' target dir not exists');   
            return $msg ;
        }        

        $dir_1 = substr($imgUid,0,3).'/' ;
        $dir_2 = substr($imgUid,-3).'/' ;
        $dirPath = $target_path.$dir_1.$dir_2;

        //檢查目錄是否存在,若無就建立目錄
        if (!is_dir( $target_path.$dir_1 )){ 
            if(!mkdir($target_path.$dir_1))                 
                error_log(  'mkdir level 1 fail need use chmod ') ;                                          
            elseif( !mkdir($dirPath) )
                error_log(  'mkdir level 2 fail need use chmod') ;           
        }                                                                       
        elseif( !is_dir( $dirPath ) ){
            if(!mkdir($dirPath)) ;                                                      
            error_log(  'mkdir level 2 fail need use chmod') ;                               
        }

        if( !is_dir( $dirPath ) )
            return $msg ;        

        //搬移檔案
        if( is_array($tmp_file) ){
            foreach($tmp_file as $value){
                if(!rename( $tmp_path.$value,$dirPath.$value)) 
                    error_log(  'files move to /public/images fail') ;
                else
                    $msg['success']=true ;
            }    
        }
        else{
            
            if(!rename( $tmp_path.$tmp_file,$dirPath.$tmp_file)) 
                error_log(  'file move to /public/images fail') ; 
            else
                $msg['success']=true ;

        }
        return $msg ;                
    }    

    public function image_getter($imageUid,$column='*' ){
        $query=array(
                'select' => $column ,
                'from'   => $this->table_name ,
                'where'  => array('imageUid=:imageUid',
                    array(
                        'imageUid'=>$imageUid,
                        )
                    )
                );
        return DB::data($this->dbid,$query);

    }    

    public function imagePath_getter($imageUid,$imageConvert='_g'){
        if(!empty($imageUid)){
            $dir_1 = substr($imageUid,0,3).'/' ;  
            $dir_2 = substr($imageUid,-3).'/' ;
            $imagePath = '/images/'.$dir_1.$dir_2.$imageUid.$imageConvert.'.png' ;
        }
        else
            $imagePath = '/css/img/default_profile_pic_m.png';            

        return $imagePath;
    }    

    public function imageUpload($img,$imageTableId,$convert_type='',$zoom=100,$x='',$y='',$heigh='',$width='' ){
        //傳入 $_FILES
        $imageMsg=array();
        $move=array();
        if( !is_array($img) )
            return false;

        foreach ($img as $key => $value) {
            $picType = explode('/',$value['type']);
            
            if(  array_key_exists(1,$picType) &&  ( $picType[1] == 'jpeg' || $picType[1] == 'png' || $picType[1] == 'gif' ) ){
                $picTypeCheck = true;
                $picType[1] = '.'.$picType[1] ;//image_convert 傳入變數需為 .jpg .bmp .png
            }    
            else
                $picTypeCheck = false;    

            if($value['error'] == 0  && $picTypeCheck ){

                if( $key == 'imgM' )
                    $Conv = '_g' ;  //縮圖
                else
                    $Conv = '_w' ;

                // 圖檔壓縮搬移                                                                          
                $imgConv=$this->image_convert(dirname($value['tmp_name']).'/',basename($value['tmp_name']),$picType[1],$convert_type,$zoom,$x,$y,$heigh,$width);
                $move = (CZ::model('images')->image_move($imgConv['uid'],'/tmp/',$imgConv['tmp'],$this->imgUploadPath));

                $imageMsg[$key]['filename'] =$imgConv['uid']; 
                list($width,$height,$type,$attr)=getimagesize($value['tmp_name']);

                //TODO 一次要存3個圖檔
                if( array_key_exists('success',$move) && $move['success']  ){
                    $data=array(
                            'imageUid'=>$imgConv['uid'],
                            'imageConvert'=>$Conv,
                            'imageExtname'=>'.png',
                            'imageTable'=>'users',
                            'imageTableId'=>$imageTableId, 
                            'imageCapacity'=>$value['size'],
                            'imageWidth'=>$width,
                            'imageHeight'=>$height,
                            'imagePublic'=>1,
                            'userUidUpload'=>ME::user_uid() ,
                            );
                   if(  !CZ::model('images')->image_add($data) ){
                        error_log( 'insert into images DB fail ');
                        return false ; 
                   }

                }
                else{
                    error_log( 'image move fail ');
                    return false; 
                }
            }
        }
        return $imageMsg ;
    }


}
