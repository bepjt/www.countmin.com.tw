<?php

class TinyurlsModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='tinyurls';
		$this->field_pk='tinyurlUid';
		$this->field_pk_charset='uid4';
		$this->fields=array(
			'tinyurlUid'=>array('charset'=>'pk','req'=>1),	//流水號
			'tinyurlHash'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//md5(tinyurlFull),作為特徵辨識
			'tinyurlFull'=>array('charset'=>'string','max'=>65535,'default'=>'','req'=>0),	//完整網址
		);
		$this->relation_tables=array();
	}

	// sample method
	public function tinyurls_add(...){
	}
}

