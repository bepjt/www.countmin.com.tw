<?php

class InterestsModel extends Model_Base {


	public function __construct(){
		$this->dbid='common';
		$this->table_name='interests';
		$this->field_pk='interestId';
		$this->field_pky_charset='id';
		$this->fields=array(
			'interestId'=>array('charset'=>'string','max'=>6,'req'=>1),
			'interestName'=>array('charset'=>'string','max'=>24,'req'=>1),
		);
	}

	/**
	 * all_getter 
	 * 取得全部的興趣標籤,回傳為interestId=>interestName的陣列
	 * 
	 * @access public
	 * @return void
	 */
	public function all_getter(){
		return array(
			'd'=>array(
				'food'=>'美食',
				'acg'=>'遊戲動漫',
				'artist'=>'名人動態',
				'trend'=>'流行時尚',
				'mkup'=>'美容彩妝',
				'movie'=>'電影戲劇',
				'health'=>'醫療保健',
				'mood'=>'心情抒發',
				'parent'=>'親子互動',
				'bg'=>'感情話題',
        
				'fate'=>'命理星座',
				'pet'=>'寵物',
				'music'=>'音樂',
				'brand'=>'品牌服務',
				'art'=>'文學藝術',
				'photo'=>'攝影',
				'life'=>'居家生活',
				'pub'=>'公眾議題',
				'3c'=>'3C數位',
				'learn'=>'教育學習',
				'invest'=>'投資理財',
				'trip'=>'休閒旅遊',
				'sport'=>'運動',
				'car'=>'車友天地',
				'soul'=>'心靈勵志',
				'pi'=>'公益',
				'sci'=>'自然科普'
			),
			'args'=>array(
				'num'=>27
			)
		);
	}

}


