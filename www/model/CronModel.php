<?php

class CronModel extends Model_Base {

	const CRON_INTER_TXT_ROOTS = '/srv/web/www/cron/';

	const CORN_TYPE_NONE = '0';
	const CORN_TYPE_TIME = '1';
	const CORN_TYPE_CLOCK = '2';
	static $CONF_CORN_TYPE = array(
		array(self::CORN_TYPE_NONE, "無排程"),
		array(self::CORN_TYPE_TIME, "循環排程"),
		array(self::CORN_TYPE_CLOCK, "定時排程"),
	);


	const CORN_TIME_5MINS = '300';
	const CORN_TIME_10MINS = '600';
	const CORN_TIME_20MINS = '1200';
	const CORN_TIME_30MINS = '1800';
	const CORN_TIME_1HOURS = '3600';
	const CORN_TIME_2HOURS = '7200';
	const CORN_TIME_3HOURS = '10800';
	const CORN_TIME_6HOURS = '21600';
	const CORN_TIME_12HOURS = '43200';
	const CORN_TIME_1DAYS = '86400';
	const CORN_TIME_2DAYS = '172800';
	const CORN_TIME_3DAYS = '259200';

	public function __construct(){
		$this->dbid='common';
		$this->table='crons';
		
		$this->cron_init_setting=array(
			'cron_time_demo' => array('測試循環排程',self::CORN_TYPE_TIME,self::CORN_TIME_30MINS),
			'cron_clock_demo' => array('測試定時排程',self::CORN_TYPE_CLOCK,"18:00"),
		);
		
		$this->cron_remove=array();
		
		$this->cron_init();
		//$this->run_corn_time();
		//$this->run_corn_clock();
		
	}
	
	public function cron_init(){
		/*if(!empty($this->cron_remove)){
			foreach($this->cron_remove as $cron_uid ){
				try{
					$this->cron_delete($cron_uid);
					throw  new Exception ($cron_uid.' is delete...');
				}
				catch (Exception $e){
					 error_log( $cron_uid.' delete fail , because: '.$e->getMessage() );
				}
			}
		}*/
		if(!empty($this->cron_init_setting)){
			foreach($this->cron_init_setting as $cron_uid => $data){
				$this->cron_setting($cron_uid,$data['0'],$data['1'],$data['2']);
			}
		}
	}
	
	
	public function cron_setting($cron_uid,$cron_name,$cron_type,$cron_time)
	{
		$query=array(
			'select' => '*',
			'from' => $this->table,
			'where' => array('cronUid=:cron_uid'),
			'bind' => array(':cron_uid' => $cron_uid),
		);
		print_r($query);
		
		$info=DB::row($this->dbid,$query);
		
		
		
		if(empty($info)){
			$data=array(
				'cronUid' => $cron_uid,
				'cronName' => $cron_name,
				//'cronPhp' =>	$data['cronPhp'],
				'cornTime' => 86400,
				'cornClock' => '12:00',
				'cornType' => $cron_type,
				'cornRunTime' => _SYS_DATETIME,
			);
			if($cron_type==self::CORN_TYPE_TIME){$data['cornTime']=$cron_time;}
			else if($cron_type==self::CORN_TYPE_CLOCK){$data['cornClock']=$cron_time;}
			DB::add($this->dbid,$this->table,$data);
		}
		else{
			$data=array(
				'cronUid' => $cron_uid,
				'cronName' => $cron_name,
				'cornType' => $cron_type,
				'cornTime' => 86400,
				'cornClock' => '12:00',
			);
			if($cron_type==self::CORN_TYPE_TIME){$data['cornTime']=$cron_time;}
			else if($cron_type==self::CORN_TYPE_CLOCK){$data['cornClock']=$cron_time;}
			
			DB::update($this->dbid,$this->table,$data,array(" WHERE cronUid=:cron_uid",array(':cron_uid' => $cron_uid)));
		}
		
	}
	public function cron_delete($cron_uid)
	{
			DB::del($this->dbid,$this->table,array(" WHERE cronUid=:cron_uid",array(':cron_uid' => $cron_uid)));
	}
	
	public function run_corn_time()
	{
		/*
		$query=array(
			'select ' => '*',
			'from ' => $this->table,
			'where ' => array(' cornjob_type='.self::CORN_TYPE_TIME.'  AND cornRunTime < NOW()'),
			'order'=> 'cornRunTime ASC',
			'limit' => '2000'
		);
		
		$info=DB::data($this->dbid,$query);
		if($info['args']['num']>0)
		{
			foreach($info['d'] as $mydata)
			{
				$next_time=time()+$mydata['cornTime'];
				$next_date=date("Y-m-d H:i:s",$next_time);
				
				try{
					call_user_func_array(array($this,$mydata['cronUid']), $args);
					throw  new Exception ($mydata['cronUid'].' is runing...');
				}
				catch (Exception $e){
					 error_log( $mydata['cronUid'].' runing fail , because: '.$e->getMessage() );
				}
				

			}
			return $myreader;
		}*/
		
				try{
					call_user_func(array($this,'cron_time_demo'));
					throw  new Exception ('cron_time_demo is runing...');
					
				}
				catch (Exception $e){
					 error_log('cron_time_demo runing fail , because: '.$e->getMessage() );
				}

	}
	public function run_corn_clock()
	{
		$ndate=date("Y-m-d H:i:s",time()+_TIME_ZONE);

		$query=array(
			'select ' => '*',
			'from ' => $this->table,
			'where ' => array(' cornjob_type='.self::CORN_TYPE_CLOCK.'  AND cornRunTime < :cornRunTime'),
			'bind' => array(':cornRunTime' => $ndate ),
			'order'=> 'cornRunTime ASC',
			'limit' => '2000'
		);
		
		$info=DB::data($this->dbid,$query);
		if($info['args']['num']>0)
		{
			foreach($info as $mydata)
			{
				list($cornjob_date_hh,$cornjob_date_mm)=explode(":",$mydata['cornClock']);
				$tmp_time=$cornjob_date_mm*60+$cornjob_date_hh*3600;
				$tmp_date=date("Y-m-d",strtotime("+1 day"));
				$next_time=strtotime($tmp_date)+$tmp_time;
				$next_date=date("Y-m-d H:i:s",$next_time);

				call_user_func(array($this,$mydata['cronUid']));
				
				//$sqlstr2="UPDATE kits_cornjob SET ldate='{$next_date}' WHERE kid={$mydata['kid']}";
				//$kits->sqlPut("kits",$sqlstr2);
			}
		}
	}
	
	public function cron_time_demo()
	{
		
	
	}
	public function cron_clock_demo()
	{
	
	
	}

}
