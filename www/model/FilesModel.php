<?php

class FilesModel extends Model_Base {


	public function __construct(){
		$this->dbid='common';
		$this->table_name='images';
		$this->field_pk='imageUid';
		$this->field_pk_charset='uid4';
		$this->fields=array(
			'imageUid'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),
			'imageExtname'=>array('charset'=>'string','max'=>6,'default'=>'','req'=>0),
			'imageTable'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),
			'imageTableId'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),
			'imageCapacity'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0),
			'imageWidth'=>array('charset'=>'uint','max'=>65535,'default'=>0,'req'=>0),
			'imageHeight'=>array('charset'=>'uint','max'=>65535,'default'=>0,'req'=>0),
			'imagePublic'=>array('charset'=>'uint','max'=>255,'default'=>0,'req'=>0),
			'userUidUpload'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),
			'imageTimeUpload'=>array('charset'=>'timestamp','req'=>0),
		);
		$this->rels=array();
	}

	/**
	 * tempfile_add 
	 * 直接傳入 $_FILES 的個索引值
	 * 
	 * @param mixed $error		$_FILES[xxxx]['error']
	 * @param mixed $name		$_FILES[xxxx]['name']
	 * @param mixed $tmp_name	$_FILES[xxxx]['tmp_name']
	 * @param mixed $type		$_FILES[xxxx]['type']，ex: image/png
	 * @param mixed $size		$_FILES[xxxx]['size']，單位 bytes
	 * @access public
	 * @return void
	 */
	public function tempfile_add($error,$name,$tmp_name,$type,$size){
		if($error!=UPLOAD_ERR_OK){
			return NULL;
		}
		$uid=uid4();
		$extname=strrpos($name,'.');
		$source_name=substr($name,0,strlen($name)-strlen($extname));
		move_uploaded_file($tmp_name, _DIR_TEMPFILE.$uid.$extname);
		$data=array(
			'tempfileUid'=>$uid,
			'tempfileSourceName'=>$source_name,
			'tempfileExtname'=>$extname,
			'tempfileHeaderType'=>$type,
			'tempfileSaveName'=>_DIR_TEMPFILE.$uid,$extname,
			'tempfileCapacity'=>$size,
			'tempfileTimeUpload'=>_SYS_DATETIME,
			'tempfileRemoteUpload'=>$_SERVER['REMOTE_ADDR'],
		);
		DB::add($this->dbid,'tempfiles',$data);
		return $uid;
	}

	/**
	 * tempfile_get 
	 * 
	 * @param mixed $uid 
	 * @access public
	 * @return void
	 */
	public function tempfile_get($uid){
		$query=array(
			'select'=>'*',
			'from'=>'tempfiles',
			'where'=>array(
				'tempfileUid=:uid',
				array(
					':uid'=>$uid,
				)
			),
		);
		$tempfile=DB::row($this->dbid,$query);
		return $tempfile;
	}


	/**
	 * image_add 
	 * 
	 * @param mixed $tmp_path 暫存的絕對路徑
	 * @param mixed $source_name 原始圖檔完整名稱
	 * @param mixed $save_mode 儲存方式
	 * @access public
	 * @return void
	 */
	public function image_add($tmp_path,$source_name,$save_mode){
		if(!is_file($tmp_path)){
			return NULL;
		}
		$subname=strtolower(strchr($source_name,'.'));
		if(!in_array($subname,array('.jpg','.png','.gif','.jpeg'))){
			@unlink($tmp_path);
			return NULL;
		}
		$capacity=filesize($tmp_path);
		switch($save_mode){
			case 'wallpost':
				return $this->image_add_wallpost($tmp_path,$source_name,$subname,$capacity);
				break;
		}
	}

	private function image_convert($tmp_path,$source_name,$subname,$capacity){


	}



}
