<?php

class CounterModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='counter';
        $this->invEncry=array(
                                '0' => 'NZX',
                                '1' => 'RH8',
                                '2' => '6AS',
                                '3' => 'FEC',
                                '4' => '3JM',
                                '5' => 'Q24',
                                '6' => '75',
                                '7' => 'U9',
                                '8' => 'YP',
                                '9' => 'KGT',
                          );

        $this->orderIdEncry=array(
                                '0' => '3',
                                '1' => '9',
                                '2' => '1',
                                '3' => '5',
                                '4' => '2',
                                '5' => '8',
                                '6' => '0',
                                '7' => '6',
                                '8' => '4',
                                '9' => '7',
                          );

	}

    public function counter_getter($counterId){
        $query=array(
                'select' => '*' ,
                'from'   => $this->table_name ,
                'where'  => array('counterId=:counterId',
                    array(
                        'counterId'=>$counterId,
                        )
                    )
                ); 
        return DB::row($this->dbid,$query);                                    
    }        

    public function counter_update($counterId,$data){
        $attach=array('WHERE counterId=:counterId',
                    array(
                            'counterId'=>$counterId,
                        )
                    );
        return DB::update($this->dbid,$this->table_name,$data,$attach); 
    }


    public function invitation_code_getter($num){ 
        $numEncode='';
        $numSplit=str_split($num);
        $mapping='';
        for($i=0;$i<count($numSplit);$i++){
            $mapping=str_split($this->invEncry[$numSplit[$i]]);
            $numEncode.=$mapping[ rand(0,count($mapping) -1 ) ];    
        }
        return $numEncode ;
    }

    public function orderid_getter($num){
        $numEncode=date("Ymd");
        $numSplit=str_split($num);
        $mapping='';
        $checksum=0;

        for($i=0;$i<count($numSplit);$i++){
            $numEncode.=$this->orderIdEncry[$numSplit[$i]] ;
            $checksum+=$this->orderIdEncry[$numSplit[$i]];
        }
    
        $numEncode .= ($checksum+983)%9; 
        return $numEncode ;
    }

    public function encrypt($data, $key){
        $key=md5($key);
        $x=0;
        $len=strlen($data);
        $l=strlen($key);
        $char='';
        $str='';

        for ($i = 0; $i < $len; $i++){
            if($x == $l) 
                $x = 0;
        
            $char .= $key{$x};
            $x++;
        }

        for ($i = 0; $i < $len; $i++)
            $str .= chr(ord($data{$i}) + (ord($char{$i})) % 256);
    
        return base64_encode($str);
    } 
    
    public function decrypt($data, $key){
        $key = md5($key);
        $x = 0;
        $data = base64_decode($data);
        $len = strlen($data);
        $l = strlen($key);
        $char='';
        $str='';
     
        for ($i = 0; $i < $len; $i++){
            if ($x == $l) 
                $x = 0;

            $char .= substr($key, $x, 1);
            $x++;
        }

        for ($i = 0; $i < $len; $i++){
            if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1)))
                $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
            else
                $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
        }
        return $str;
    }


}
