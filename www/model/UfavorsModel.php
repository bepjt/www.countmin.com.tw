<?php

class UfavorsModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='ufavors';
		$this->field_pk='';
		$this->field_pk_charset='';
		//$this->fields=array(
		//	'noticeId'=>array('charset'=>'string','max'=>48,'default'=>'','req'=>0),	//訊息詞彙代碼
		//	'noticeDescription'=>array('charset'=>'string','max'=>64,'default'=>'','req'=>0),	//訊息詞彙說明
	 //	);
		//$this->relation_tables=array();
	}

    public function ufavors_add($data){
        return DB::add($this->dbid,$this->table_name,$data,FALSE);
    }

    public function ufavors_getter_count($userUid,$wallpostType='all'){
        $typeQuery = '' ; 
        if($wallpostType != 'all'){
            $typeQuery = ' AND wallpostType=:wallpostType';
        }

        $query=array(
            'select' => 'count(*)' ,
            'from'   => $this->table_name ,
            'where'  => array('userUid=:userUid'.$typeQuery,
                            array(
                                'userUid'=>$userUid,
                            )
                        )
                );

        if($wallpostType != 'all')
            $query['where'][1]['wallpostType'] = $wallpostType  ;

        return DB::data($this->dbid,$query);                                    
    }

    public function ufavors_getter($userUid,$wallpostType='',$is_dot='0'){
        $query=array(
            'select' => '*',
            'from'   => $this->table_name ,
            'where'  => array('userUid=:userUid',
                            array(                                              
                                'userUid'=>$userUid,
                            )
                        ),
             'order' => 'ufavorTime DESC'
                );
        if(!empty($wallpostType) ){
            if($is_dot==0){
            	$query['where'][0] .= ' AND wallpostType=:wallpostType '; 
            	$query['where'][1]['wallpostType'] = $wallpostType  ;
          	}
          	else{
          		$query['where'][0] .= ' AND wallpostType in('.$wallpostType.') '; 
          	}
        }
        return DB::data($this->dbid,$query);
    }

    public function ufavors_check($userUid,$wallpostUid,$wallpostType){
        $query=array(
            'select' => 'count(*)',
            'from'   => $this->table_name ,
            'where'  => array('userUid=:userUid AND wallpostUid=:wallpostUid AND wallpostType=:wallpostType',
                            array(                                              
                                'userUid'=>$userUid,
                                'wallpostUid'=>$wallpostUid,
                                'wallpostType'=>$wallpostType,
                            )
                        )
                );

        return DB::row($this->dbid,$query);
    }

    public function ufavors_delete($userUid,$wallpostUid,$wallpostType){
            $attach=array('WHERE userUid=:userUid and wallpostUid=:wallpostUid and wallpostType=:wallpostType',
                        array(  
                                'userUid'=>$userUid,
                                'wallpostUid'=>$wallpostUid,
                                'wallpostType'=>$wallpostType, 
                            ),
                    );
            return DB::del($this->dbid,$this->table_name,$attach );
    }


}

