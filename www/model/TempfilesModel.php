<?php

class TempfilesModel extends Model_Base {
	public function __construct(){
		$this->dbid='common';
		$this->table_name='tempfiles';
		$this->field_pk='';
		$this->field_pk_charset='';
		$this->fields=array(
			'tempfileUid'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//暫存檔UUID
			'tempfileSourceName'=>array('charset'=>'string','max'=>64,'default'=>'','req'=>0),	//上傳的原始檔名(不含副檔名)
			'tempfileExtname'=>array('charset'=>'string','max'=>6,'default'=>'','req'=>0),	//上傳的副檔名(小寫)
			'tempfileHeaderType'=>array('charset'=>'string','max'=>32,'default'=>'','req'=>0),	//瀏覽器送過來的header
			'tempfileSaveName'=>array('charset'=>'string','max'=>64,'default'=>'','req'=>0),	//儲存的檔名(完整檔名)
			'tempfileCapacity'=>array('charset'=>'uint','max'=>4294967295,'default'=>0,'req'=>0),	//容量(bytes)
			'tempfileTimeUpload'=>array('charset'=>'timestamp','req'=>0),	//上傳時間
			'tempfileRemoteUpload'=>array('charset'=>'string','max'=>24,'default'=>'','req'=>0),	//上傳者來源IP
		);
		$this->relation_tables=array();
	}

	// sample method
	public function tempfile_add(){
	}
}

