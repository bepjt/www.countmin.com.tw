<?php
class SearchModel extends Model_Base {

    public function __construct(){
        $this->dbid='common';
        $this->goods_table_name='goods';
        $this->users_table_name='users';
        $this->uprolevels_table_name='uprolevels';
        $this->wallposts_table_name='wallposts';
    }

    public function goods_search($keyword,$day=7,$column='*',$limit=10 ){
        $query=array(
                'select' => $column ,
                'from'   => $this->goods_table_name ,                           
                'where'  => array('(goodsName LIKE :keyword  OR goodsBrand LIKE :keyword ) AND goodsNumStorage >0 AND goodsStatus=1 AND TO_DAYS(NOW()) - TO_DAYS(goodsTimeStart)<= :day ',
                    array(
                        'keyword'=>"%".$keyword."%",
                        'day'=>$day,
                        )
                    ),
                'order'  => 'goodsTimeStart desc' ,
                'limit'  => $limit ,
                ); 
        return DB::data($this->dbid,$query);
    }    

    public function users_search($keyword,$userInterests='',$spage=0,$per=50 ){
        
        $query=array(
                'select' => '*' ,
                'from'   => $this->users_table_name,
                'where'  => array('userType=0 AND userEnable=1 AND userRealname LIKE :keyword ',
                    array(
                        'keyword'=> "%".$keyword."%",
                        )
                    ),
                'spage'  => $spage ,
                'per'    => $per ,
                ); 
        if(!empty($userInterests)){
            $query['where'][0] .= ' AND userInterests LIKE :userInterests';
            $query['where'][1]['userInterests'] = "%".$userInterests."%"; 
        }
 
        return DB::data($this->dbid,$query);
    }    

    public function wallposts_search($keyword,$day=7,$interest=0,$orderby=0,$spage=0,$per=200 ){

        $column='u.userRealname,u.userProfile,w.wallpostInterests,w.wallpostTimeCreate,w.wallpostUid,w.wallpostTopic,w.wallpostBrief,w.wallpostNumLikes,w.wallpostNumDislikes,w.wallpostNumComments,w.wallpostNumShares';

        $query=array(
                'select' => '*', //$column ,
                'from'   => $this->wallposts_table_name.' w , '.$this->users_table_name.' u' ,                           
                'where'  => array(' u.userUid=w.userUid AND (w.wallpostBrief LIKE :keyword OR w.wallpostTopic LIKE :keyword ) AND w.wallpostPublic=0   ',

//AND TO_DAYS(NOW()) - TO_DAYS(w.wallpostTimeCreate)<= :day 

                    array(
                        'keyword'=>"%".$keyword."%",
                        //'day'=>$day,
                        )
                    ),
                'spage'  => $spage ,
                'per'    => $per ,
                );                                                              

        if( !empty($interest) ){
            $query['where'][0] .= ' AND w.wallpostInterests LIKE :interest' ;
            $query['where'][1]['interest']= "%".$interest."%" ;
        }

        if( !empty($orderby) ) 
            $query['order'] = 'w.'.$orderby.' desc' ;
        else
            $query['order'] = 'w.wallpostTimeCreate desc' ;

        return DB::data($this->dbid,$query);
    }    


}

?>
