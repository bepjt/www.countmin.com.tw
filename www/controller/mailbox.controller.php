<?php
class Mailbox_Controller extends Controller {
	protected $log=array();

	public function __construct(){
		$this->name='mailbox';
		$this->layout='profile-no-topbanner';
		/**
		 * name => grid method
		 */
		$this->layout_grids=array(
			'page-header'=>array('grid'=>'Header'),
		);
		CZ::grid_setter('topbanner',TRUE);
		CZ::grid_setter('page-header',TRUE);
	}

	public function Command_Private(){
		$user_uid=form('p','string','get');
		$chatroom_uid=form('q','string','get');
		$chatrooms=CZ::model('chatrooms')->chatroom_list(ME::user_uid());

		if(strlen($chatroom_uid) && !strcmp($user_uid,ME::user_uid())){
			$chatmessages=CZ::model('chatrooms')->message_getter($user_uid,$chatroom_uid,_SYS_DATETIME,0);
		}
		else if($chatrooms['args']['num']){
			$chatroom_uid=$chatrooms['d'][0]['chatroomUid'];
			$user_uid=ME::user_uid();
			$chatmessages=CZ::model('chatrooms')->message_getter(ME::user_uid(),$chatrooms['d'][0]['chatroomUid'],_SYS_DATETIME,0);
			$chatroom=$chatrooms['d'][0];
		}
		else{
			$chatmessages=DB::empty_data();
			$chatroom=array(
				'chatroomUid'=>'',
				'chatroomName'=>'',
				'chatroomNumUsers'=>'',
				'chatroomUsersHash'=>'',
				'chatroomTimeLastUpdate'=>'',
				'userUidLastUpdate'=>'',
				'chatroomMessageLastUpdate'=>'',
			);
		}
		$xusers=CZ::model('users')->relation_list(ME::user_uid(),5,0,'rand');
		include($this->layout_path(CZ::controller(),CZ::command()));
	}

	/**
	 * Command_One
	 * 載入聊天室畫面
	 *  當有 mode=one 時表示是載入對指定對象的聊天畫面(紀錄)；若無則表示載入空的聊天紀錄
	 *  當無 q 時，表示不帶入先前的聊天紀錄
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_One(){

		$uid1=form('p','string','get');
		$uid2=form('q','string','get');
		if(strlen($uid1)<10){
			$uid1='';	
		}
		if(strlen($uid2)<10){
			$uid2='';
		}
		$mode='none';
		if(!$uid1){
		}
		else if(!$uid2){
			$mode='chatroom';
		}
		else{
			$mode='1on1';
		}

		/**
		 * 測試 pdo
		$sql=array(
			'select'=>'*',
			'from'=>'users',
			'where'=>array(
				'userUid IN (:user_uid)',
			),
			'bind_array'=>array(
				':user_uid'=>array(1,2,3,4,5,6),
			),
		);
		_e(DB::value('common',$sql));
		die('gg');
		**/

		switch($mode){
			case 'one':
			case '1on1':
				$xuser=CZ::model('users')->one($uid2);
				if(!$xuser){
					CZ::page('not_exists_user');
				}
				$relation=CZ::model('users')->relations_getter(ME::user_uid(), $uid2);
				if(empty($relation['relation_status']['friend'])){
					CZ::page('not_relation');
				}
				else if(!$relation){
					CZ::page('error_404');
				}
				$chat=array(
					'title'=>$xuser['userRealname'],
					'xuser_uid'=>$uid2,
					'chatroom_uid'=>'',
				);
				break;
			case 'chatroom':
			default:
				if(strlen($uid1)<5){
					$chat=array(
						'title'=>'',
						'xuser_uid'=>'',
						'chatroom_uid'=>'',
					);
				}
				else{
					$chatroom=CZ::model('chatrooms')->one($uid1);
					if(!$chatroom){
						$chat=array(
							'title'=>'',
							'xuser_uid'=>'',
							'chatroom_uid'=>'',
						);
					}
					else{
						if(strlen($chatroom['chatroomName'])){
							$chat=array(
								'title'=>$chatroom['chatroomName'],
								'xuser_uid'=>'',
								'chatroom_uid'=>$chatroom['chatroomUid'],
							);
						}
						else{
							$users=CZ::model('chatrooms')->user_listing_at_chatrooms($uid1);
							$user_realnames=array();
							$len=0;
							for($i=0;$i<$users['args']['num'];$i++){
								if($users['d'][$i]['userUid']==ME::user_uid()) continue;
								if(strlen($users['d'][$i]['userRealname'])>(40-$len) && $len>0){
									continue;
								}
								$user_realnames[]=$users['d'][$i]['userRealname'];
							}
							$chat=array(
								'title'=>join(',',$user_realnames),
								'xuser_uid'=>'',
								'chatroom_uid'=>$chatroom['chatroomUid'],
							);
						}
					}
				}
				break;
		}
		$friends=CZ::model('users')->relation_list(ME::user_uid(),5,0,'rand');
		include($this->layout_path(CZ::controller(),CZ::command()));
	}

	/**
	 * Command_Xss_cracker 
	 * 測試各種 XSS 字串，跟 mailbox 無關
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Xss_cracker(){
		if(!strcmp($_SERVER['REMOTE_ADDR'],'127.0.0.1') || !strcmp($_SERVER['REMOTE_ADDR'],'::1')){
			$str=form('str','string','post');
			include($this->layout_path(CZ::controller(),CZ::command()));
		}
		else{
			die('Error');
		}
	}


	/**
	 * Command_Message_getter 
	 * 給ajax用的取得訊息
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Message_getter(){
		$user_uid=form('user','string','post');
		$chatroom_uid=form('chatroom','string','post');
		$timeup=form('timeup','datetime','post');
		$timebefore=form('timebefore','boolean','post');
		if(strlen($chatroom_uid) && !strcmp($user_uid,ME::user_uid())){
			$chatmessages=CZ::model('chatrooms')->message_getter($user_uid,$chatroom_uid,$timeup,$timebefore);
			CZ::ajax_json_output($chatmessages);
		}
		else{
			CZ::ajax_json_output(DB::empty_data());
		}
	}

	/**
	 * Command_Message_add 
	 * 新增聊天室訊息
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Message_add(){
		$err=array('error'=>1);
		$user_uid=form('user','string','post');
		$chatroom_uid=form('chatroom','string','post');// 傳送訊息到一個聊天室
		$xuser_uid=form('xuser','string','post');
		$timeup=form('timeup','datetime','post');
		$timebefore=form('timebefore','boolean','post');
		$content=form('content','string','post');
		if(!strlen($content) || (!strlen($chatroom_uid) && !strlen($xuser_uid)) || strcmp($user_uid,ME::user_uid())){
			$err['error']=2;
			CZ::ajax_json_output($err);
		}
		if(!$chatroom_uid && $xuser_uid){
			$has_chatroom=CZ::model('chatrooms')->users_at_chatroom(ME::user_uid(),array($xuser_uid,$user_uid));
			if(!$has_chatroom){
				$chatroom_uid=CZ::model('chatrooms')->chatroom_add(ME::user_uid(),array($xuser_uid,$user_uid));
			}
			else{
				$chatroom_uid=$has_chatroom;
			}
			$err['chatroom_uid']=$chatroom_uid;
		}
		if(strlen($content) && strlen($chatroom_uid)){
			CZ::model('chatrooms')->message_add(ME::user_uid(),$chatroom_uid,$content);
			$err['error']=0;
			CZ::ajax_json_output($err);
		}
	}

	/**
	 * Command_Message_add_with 
	 * 新增一對一的聊天室訊息
	 * 
	 * @param string POST['xuser_uid'] 必要
	 * @access public
	 * @return void
	 */
	public function Command_Message_add_with(){
		$user_uid=form('user','string','post');
		$xuser_uid=form('xuser','string','post');// 傳送訊息給一個對象
		$chatroom_uid=form('chatroom','string','post');// 傳送訊息到一個聊天室
		$timeup=form('timeup','datetime','post');
		$timebefore=form('timebefore','boolean','post');
		$content=form('content','string','post');
		if(strlen($content) && strlen($xuser_uid)){
			$chatroom=CZ::model('chatrooms')->chatroom_one_by_one(ME::user_uid(),$xuser_uid);			
			if(!$chatroom){
				$chatroom_uid=CZ::model('chatrooms')->chatroom_add(ME::user_uid(),array(ME::user_uid,$xuser_uid),'');
			}
			else{
				$chatroom_uid=$chatroom['chatroomUid'];
			}
			CZ::model('chatrooms')->message_add(ME::user_uid(),$chatroom_uid,$content);
			CZ::ajax_json_output(array('error'=>0));
		}
		else if(strlen($content) && strlen($chatroom_uid)){
		}
		CZ::ajax_json_output(array('error'=>1));
	}

	/**
	 * Command_Chatroom_getter 
	 * 取得聊天室列表
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Chatroom_getter(){
		$user_uid=ME::user_uid();
		$chatrooms=CZ::model('chatrooms')->chatroom_getter($user_uid);
		$chatrooms_uids=array();
		CZ::ajax_json_output($chatrooms);
	}

	/**
	 * Command_Chatroom_add 
	 * 新增聊天室
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Chatroom_add(){
		$chatroom_name=form('name','string','post');
		$users=form('users.','string','post');

		$num=count($users);
		if($num<1 || ($num==1 && !strcmp($users[0],ME::user_uid()))){
			CZ::ajax_json_output(array('error'=>1));
		}
		$xusers=array();
		for($i=0;$i<$num;$i++){
			$xusers[$users[$i]]='A';
		}
		$xusers[ME::user_uid()]='A';
		$xusers=array_keys($xusers);

		$chatroom_uid=CZ::model('chatrooms')->chatroom_add(ME::user_uid(),$xusers,$chatroom_name);
		
		CZ::ajax_json_output(array('error'=>0,'chatroom_uid'=>$chatroom_uid));
	}

}
