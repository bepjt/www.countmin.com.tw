<?php
use Mailgun\Mailgun;
class Setting_Controller extends Controller {
	protected $log=array();

	public function __construct(){
		$this->name='setting';
		$this->layout='setting';
		/**
		 * name => grid method
		 */
       $this->cc_email='service@countmin.com';
       $this->layout_grids=array(
                'page-header'=>array('grid'=>'Header'),
                'user-basic'=>array('grid'=>'UserBasic'),
                'user-info'=>array('grid'=>'UserInfo'),
                'setting-menu'=>array('grid'=>'Menu'),
                );
        CZ::grid_setter('page-header',TRUE);
        CZ::grid_setter('user-basic',TRUE);
        CZ::grid_setter('user-info',TRUE);
        CZ::grid_setter('setting-menu',TRUE);      
	}

    public function Command_Account(){
        $userUid=ME::user_uid();

        $uaccs=CZ::model('users')->uaccs_getter($userUid,'uaccUid,uaccAuth,uaccId');
        $xuser=CZ::model('users')->xuser($userUid,1,0,0,1);
        $uacctokens=CZ::model('users')->uacctokens_getter($uaccs['uaccUid'],'uacctokenTimeLastUpdate');

        $deliveryinfo=CZ::model('goods')->deliveryinfo_getter($userUid);
        $showCity=CZ::model('users')->regions_regionuid_getter($deliveryinfo['d'][0]['city']);
        $showDist=CZ::model('users')->regions_regionuid_getter($deliveryinfo['d'][0]['dist']);
       
        if(!empty($showCity['regionUid']) )
            $showDistOption=CZ::model('users')->regions_getter(6,$showCity['regionUid']);

        $city=CZ::model('users')->regions_getter(5); 
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_City(){
        //ajax used
        $city=form('city','string','post');
        if(!empty($city))
            $section=CZ::model('users')->regions_getter(6,$city); //取得鄉鎮區

        for($i=0;$i<$section['args']['num'];$i++){
            echo '<option zip='.$section['d'][$i]['regionZipcode'].' value="'.$section['d'][$i]['regionUid'].'">'.$section['d'][$i]['regionName'].'</option>';

        }

    }

    public function Command_Accountsave(){
        //ajax used
        $userUid=ME::user_uid();
        $groupId=form('group','int','post');
        $groupname=form('groupname','string','post');
        $data=array(
                    'groupName'=>$groupname,
                    'regionUid'=>form('zip','string','post'),
                    'city'=>form('city','string','post'),
                    'dist'=>form('dist','string','post'),
                    'address'=>form('address','string','post'),
                    'note'=>form('note','string','post'),
                    );
        //$info=CZ::model('goods')->deliveryinfo_getter($userUid,$groupId);

        //if($info['args']['num'] >0)
        $save=CZ::model('goods')->deliveryinfo_update($userUid,$groupId,$data);
        //else{
        //    $data['userUid']=$userUid;
        //    $data['groupid']=$groupId;
        //    $insert=CZ::model('goods')->deliveryinfo_add($data);
        //}
        echo ($save) ? '收貨地址儲存成功' : '收貨地址儲存失敗';
    }

    public function Command_changepwd(){
        $userUid=ME::user_uid();
        $oldpassword=form('oldpassword','string','post');     
        $newpassword=form('newpassword','string','post');
        $checkpassword=form('checkpassword','string','post');

        if( $newpassword != $checkpassword ){
            echo '新密碼不相符';
            exit;
        }

        if( strlen($newpassword) < 7 ){
            echo '新密碼最少要8個英文或數字'.strlen($newpassword);
            exit;
        }
        
        if($oldpassword == $newpassword ){
            echo '目前的密碼與新密碼不可相同';
            exit;
        }

        $uaccId=CZ::model('users')->uaccs_getter($userUid,'uaccId');
        $password=CZ::model('users')->uid_pwd_check($uaccId['uaccId'],$oldpassword);
        
        if(!$password){
            echo '目前的密碼不正確';
            exit;
        }

        $uaccToken=md5('$6G.'.md5($newpassword));
        $data=array('uaccToken'=>$uaccToken );
        $passwordUpdate=CZ::model('users')->uacctokens_update($password['uaccUid'],$data);

        if($passwordUpdate){
            echo '密碼更新成功,登出後請以新密碼登入';
        }
    }

    public function Command_Friends(){
        $userUid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($userUid); 
        $email=form('email','string','post');
    
        if( !empty($email) ){
        //    include($this->layout_path(CZ::controller(),CZ::command()));
        //    exit;
            require '/srv/web/CoreBase/Firm/include/vendor/autoload.php';
            //use Mailgun\Mailgun;

            # Instantiate the client.
            $mgClient = new Mailgun('key-c171575a3231adfa8b44ed0f577f6a4d');
            $domain = "sandbox76b99712de564f3b909cb1aa4e8f55c2.mailgun.org";

            # Make the call to the client.
            $result = $mgClient->sendMessage($domain, array(
            'from'    => '參一腳countmin.com <server@countmin.com>',
            'to'      => '<'.$email.'>',
            'subject' => 'countmin參一腳邀請信',
            'text'    => '
                        您好:
                        這是'.$xuser['basic']['userRealname'].'在countmin參一腳寄出的邀請信
                        邀請碼為：'.$xuser['basic']['userInviteCode'].'
                        請您馬上前往 
                        http://www.countmin.com/index/regist?code='.$xuser['basic']['userInviteCode'].'  
                        註冊帳號
                        你和你的朋友每人可獲得各100點唷！
                        '
                        ,
            ));
            echo '<script>alert("邀請信已成功寄出");</script>';

        }

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_Photo(){
        /*
        $userUid=ME::user_uid();
        $userProfile='';
        $userTopbanner='';
        $data=array();
        $xuser=CZ::model('users')->xuser($userUid);
        $images=CZ::model('images')->imageUpload($_FILES,$xuser['basic']['userUid'],'topbanner' ); //封面照 
        $userProfile=( array_key_exists('photo' ,$images   ) ) ?  $images['photo']['filename']  :  '';
        $userTopbanner=( array_key_exists('banner' ,$images   ) ) ?  $images['banner']['filename']  :  '';

        if( !empty($userProfile))    
            $data['userProfile'] = $userProfile ;

        if( !empty($userTopbanner))
            $data['userTopbanner']=$userTopbanner;
                        
        if( !empty($userProfile) || !empty($userTopbanner) ){
            $users = CZ::model('users')->user_update($userUid,$data);
            $xuser=CZ::model('users')->xuser($userUid);        
        }

        include($this->layout_path(CZ::controller(),CZ::command()));
        */
    }

    public function Command_Privacy(){
        $userUid=ME::user_uid();
        $privacy=CZ::model('users')->privacy_getter($userUid);
        $blacklist=CZ::model('users')->blacklist_getter($userUid);
//_e($blacklist);
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_blacklistdel(){
        $userUid2=form('id','string','post');
        $userUid=ME::user_uid();
        $del=CZ::model('users')->blacklist_delete($userUid,$userUid2);
        echo $del;
    }
    
    public function Command_PrivacyUpdate(){
        //ajax used
        $data=array();
        $userUid=ME::user_uid();
        $group=form('group','string','post');

        switch($group){
            case 'a':
                $myPost=form('open','int','post');
                $data=array('myPost'=>$myPost);
                break;
            case 'b':
                $findMe=form('find','int','post');
                $data=array('findMe'=>$findMe);
                break;      
            case 'c':
                $location=form('agree','int','post');
                $data=array('location'=>$location);
                break;      
            case 'e':
                $sendMsg=form('msg','int','post');
                $data=array('sendMsg'=>$sendMsg);
                break;
            case 'd': 
                $blackUserUid=form('black','string','post');
                break;     
        }
        
        if($group == 'd')
            $privacy = CZ::model('users')->blacklist_delete($userUid,$blackUserUid);
        else        
            $privacy = CZ::model('users')->privacy_update($userUid,$data);

        if($privacy)
            echo '更新成功' ;
        else
            echo '更新失敗';
    }

    public function Command_Notify(){
        $userUid=ME::user_uid(); 
        $submitcheck=form('submitcheck','int','post');
        $notifyUpdate=false;
        if(!$submitcheck){
            $notify=CZ::model('notifications')->notificationSetting_getter($userUid);
            include($this->layout_path(CZ::controller(),CZ::command()));
            exit;
        }

        $likes=form('add','int','post'); 
        $dislikes=form('reduce','int','post');  
        $comments=form('msg','int','post');  
        $afterComments=form('after','int','post');  
        $shares=form('share','int','post');  
        $favors=form('love','int','post');  
        $abuses=form('report','int','post');  
        $wallposts=form('new','int','post');  
        $friend=form('addfriend','int','post');  
        $follow=form('follow','int','post');  
        $memberPaper=form('epaper','int','post');  
        $activityPaper=form('epaper2','int','post');  
        $newFunction=form('epaper1','int','post');  
        $recommendActivity=form('epaper3','int','post');  
        $recommendInfo=form('epaper4','int','post');  
        $submit=form('submit','int','post'); 

        $data=array(
            'likes'=>$likes,
            'dislikes'=>$dislikes,
            'comments'=>$comments,
            'afterComments'=>$afterComments,
            'shares'=>$shares,
            'favors'=>$favors,
            'abuses'=>$abuses,
            'wallposts'=>$wallposts,
            'friend'=>$friend,
            'follow'=>$follow,
            'memberPaper'=>$memberPaper,
            'activityPaper'=>$activityPaper,
            'newFunction'=>$newFunction,
            'recommendActivity'=>$recommendActivity,
            'recommendInfo'=>$recommendInfo,
        );
        $notifyUpdate=CZ::model('notifications')->notificationSetting_update($userUid,$data);
        $notify=CZ::model('notifications')->notificationSetting_getter($userUid);

        include($this->layout_path(CZ::controller(),CZ::command()));        
    }

	public function Command_CustomerService(){
        $contactcc=false; 
        $type=form('type','string','post');
        $contents=form('depiction','string','post');
        $goodsorderId=form('orderid','string','post');
        $userUid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($userUid,1,0,0,1);
        $uaccs=CZ::model('users')->uaccs_getter($userUid,'uaccId');       
 
        switch($type){
            case 'point':
                $questionType=0;
                $emailQuestionType='點數問題' ;
                break;
            case 'sys':
                $questionType=1;
                $emailQuestionType='系統問題' ; 
                break;
            case 'goods':
                $questionType=2;
                $emailQuestionType='商品問題' ; 
                break;
            case 'order':
                $questionType=3;
                $emailQuestionType='訂單問題' ; 
                break;
            case 'other':
                $questionType=4;
                $emailQuestionType='其他' ; 
                break;
            default:
                $questionType=4;
                $emailQuestionType='其他' ; 
                break;
        }
        
        if( !empty($contents)){
            $data=array(
                        'contactccId'=>uid4(),
                        'userUid'=>$userUid,
                        'questionType'=>$questionType,
                        'contents'=>$contents,
                        'goodsorderId'=>$goodsorderId,
                        'ccUid'=>'',
                        'answer'=>'',
                        ); 
            $contactcc=CZ::model('users')->contactcc_add($data);

            $title='countmin.com 聯絡客服信件' ;
            $content=
                     '  會員名稱: '.$xuser['basic']['userRealname'].'<p>
                        會員信箱: '.$uaccs['uaccId'].'<p> 
                        問題類型: '.$emailQuestionType.'<p> 
                        訂單編號:'.$goodsorderId.'<p> 
                        問題: '.$contents .'<p> '
                    ;                               
            $sendAuthEmail=CZ::model('mail')->send_mail('service@countmin.com',$title,$content);
            $sendAuthEmail=CZ::model('mail')->send_mail('jasline@gelivable.biz',$title,$content);                
            $sendAuthEmail=CZ::model('mail')->send_mail('emma@gelivable.biz',$title,$content); 
        } 

		include($this->layout_path(CZ::controller(),CZ::command()));
	}

        public function Command_Import(){
            $interests=form('interest','string','post');
            $selectOption=CZ::model('interests')->all_getter();
        
            if( count($_FILES) ==0 ){
                include($this->layout_path(CZ::controller(),CZ::command()));
                exit();
            }

            $mt = "";
            if(file_exists($_FILES['filename']['tmp_name'])){
                $file = fopen($_FILES['filename']['tmp_name'], "r");
                if($file != NULL){
                    while (!feof($file)) {
                        $mt .= fgets($file);
                    }
                    fclose($file);
                }
            }

            $mtArray = explode("--------",$mt); //分出文章
            for($i=0;$i<count($mtArray) -1 ;$i++)
            {
                //第n篇文章
                $article = explode("-----",$mtArray[$i]);
                //文章資訊
                $authorPos = strpos($article[0], "AUTHOR:");
                //文章標題
                $topic = substr($article[0],7,$authorPos - 8);
                preg_match_all('@src="([^"]+)"@',$article[1],$pic);

                $picUid=$this->fetchPic($pic[1]); //抓圖片取圖片uid                
                //替換<img src>網址
                for($j=0;$j<count($picUid);$j++)
                    $article[1] = str_replace($picUid[$j]['orgFile'],$picUid[$j]['newFile'],$article[1]);                
           
                $content=strip_tags($article[1], '<p><br><img>');  //文章內容

                $user_uid=ME::user_uid();
                $public=0;
               
                if( is_array($picUid) && array_key_exists('0',$picUid))
                    $cover_image = $picUid[0]['uid']; 
                else
                    $cover_image = '';

                //文章 insert into db
                $wallpostsId=@CZ::model('Wallposts')->post_add($user_uid,$public,$cover_image,$topic,$content,$interests,'','');


                $data=array(
                            'imageUid'=>$picUid[$i]['uid'],
                            'imageConvert'=>'_w',
                            'imageExtname'=>'.png',
                            'imageTable'=>'wallposts',
                            'imageTableId'=>$wallpostsId, 
                            'imageCapacity'=>$picUid[$i]['size'],
                            'imageWidth'=>$picUid[$i]['width'],
                            'imageHeight'=>$picUid[$i]['height'],
                            'imagePublic'=>1,
                            'userUidUpload'=>ME::user_uid() ,
                            );

                if(  !CZ::model('images')->image_add($data) )
                        $imageMsg[$i]['error'] = $picUid[$i]['uid'].'圖片存入資料庫失敗';                  
            }

            include($this->layout_path(CZ::controller(),CZ::command()));
        }
    
    public function fetchPic($picLink){

        if( !is_array($picLink))
            return false;

        for($i=0;$i<count($picLink);$i++)
        {
            if(file($picLink[$i])){
                $source='/tmp/'.ME::user_uid().time().rand(100,999) ;
                //$source='/srv/web/countmin/public/images/'.ME::user_uid().time().rand(100,999);
                copy($picLink[$i], $source );
            }

            if(file_exists($source )){
                //echo $picLink[$i].' copy ok <br>';
                    
                $picFile['filename']=array(
                                            'type'=>'image/jpeg',
                                            'tmp_name'=>$source,
                                            'error'=>0,
                                            'size'=>getimagesize($source),
                                            );

                // 圖檔壓縮搬移
                //TODO 判斷圖檔類型
                $imgConv=CZ::model('images')->image_convert(dirname($source).'/',basename($source),'.jpg','png');

                $move = (CZ::model('images')->image_move($imgConv['uid'],'/tmp/',$imgConv['tmp'],$this->imgUploadPath));


                $newFile='http://www.countmin.com'.CZ::model('images')->imagePath_getter($imgConv['uid']);
                list($width,$height,$type,$attr)=getimagesize($source);
                $imageMsg[$i]=array(
                                    'orgFile'=>$picLink[$i],
                                    'newFile'=>$newFile,
                                    'uid'=>$imgConv['uid'],
                                    'size'=>filesize($source),
                                    'height'=>$height,
                                    'width'=>$width,
                                    );    
            }
        }
    
        return $imageMsg ;
    }

	public function Grid_Menu(){
     include($this->grid_path($this->name,'menu'));		
	}

	/**
	 * Command_Message_getter 
	 * 給ajax用的取得訊息
	 * 
	 * @access public
	 * @return void
	 */

}
