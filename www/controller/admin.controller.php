<?php
class Admin_Controller extends Controller {

    public function __construct(){
        $this->name='admin';
        /**
         * name => grid method
         */
        $this->layout='admin';
        $this->layout_grids=array(
                'admin-menu'=>array('grid'=>'Menu'),
                );
        $this->usersTable     = 'users';
        $this->ulevelTable    = 'uprolevels';
        $this->uparamTable    = 'uparameters';
        $this->upointlogTable = 'upoint_logs';
        $this->imagesTable    = 'images'     ;
        $this->goodorders     = 'goodorders' ;
        $this->goods          = 'goods'      ;
        $this->rowNum         =  50          ; //每頁幾筆資料
        $this->imgUpload      = _DIR_DOCS.'public/images/' ;
    
        if(!ME::is_admin())
            exit;

        CZ::grid_setter('admin-menu',TRUE);
    }

    public function Command_Index(){     
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_Users(){
        $userUid=form('userUid','string','post'); 
        $users=CZ::model('users')->users_pro_param_getter($userUid); //用uid查會員       
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_cron(){
 
        //$users=CZ::model('users')->user_point_less('76gkyeba-4rggwow4-ko40k4kk8g',3);
        //var_dump($users);
         
       include($this->layout_path(CZ::controller(),CZ::command()));
    }


    public function Command_UsersModify(){
        $userUid=form('userUid','string','post');
        $userId=form('userId','string','post');
        $userRealname=form('userRealname','string','post');
        $userAliasname=form('userAliasname','string','post');
        $userGender=form('userGender','string','post');
        $userProfile=form('userProfile','string','post');
        $userTimeRegist=form('userTimeRegist','string','post');
        $userType=form('userType','string','post');
        $userEnable=form('userEnable','string','post');
        $userPoint=form('userPoint','string','post');
        $userLevel=form('userLevel','string','post');
        $userTimezone=form('userTimezone','string','post');
        $langIdInterface=form('langIdInterface','string','post');

        $uparameterNumFriends=form('uparameterNumFriends','string','post');
        $uparameterNumFollows=form('uparameterNumFollows','string','post');
        $uparameterNumFollowers=form('uparameterNumFollowers','string','post');
        $uparameterNumPosts=form('uparameterNumPosts','string','post');
        $uparameterNumEvents=form('uparameterNumEvents','string','post');
        $uparameterNumFavors=form('uparameterNumFavors','string','post');
        $uparameterNumLikes=form('uparameterNumLikes','string','post');
        $uparameterNumDislikes=form('uparameterNumDislikes','string','post');
        $uparameterNumShares=form('uparameterNumShares','string','post');
        $uparameterNumJoins=form('uparameterNumJoins','string','post');
        $uparameterNumAskFriends=form('uparameterNumAskFriends','string','post');
        $uparameterNumNeedFriends=form('uparameterNumNeedFriends','string','post');

        $interestId=form('interestId','string','post');
        $userGrade=form('userGrade','uint','post');
        $userProLevel=form('userProLevel','uint','post');
        $uprolevelTimeLastUpdate=form('uprolevelTimeLastUpdate','string','post');

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_UsersPut(){
        $table=form('table','string','post'); //區分不同db table 更新
        $userUid=form('userUid','string','post');  
        switch ($table) 
        {
            case 'users':

                $userId=form('userId','string','post');
                $userRealname=form('userRealname','string','post');
                $userAliasname=form('userAliasname','string','post');
                $userGender=form('userGender','string','post');
                $userProfile=form('userProfile','string','post');
                $userTimeRegist=form('userTimeRegist','string','post');
                $userType=form('userType','string','post');
                $userEnable=form('userEnable','string','post');
                $userPoint=form('userPoint','string','post');
                $userLevel=form('userLevel','string','post');
                $userTimezone=form('userTimezone','string','post');
                $langIdInterface=form('langIdInterface','string','post');

                $data=array( 'userId'=>$userId,
                        'userRealname'=>$userRealname,
                        'userAliasname'=>$userAliasname,
                        'userGender'=>$userGender,
                        'userProfile'=>$userProfile,
                        'userTimeRegist'=>$userTimeRegist,
                        'userType'=>$userType,
                        'userEnable'=>$userEnable,
                        'userPoint'=>$userPoint,
                        'userLevel'=>$userLevel,
                        'userTimezone'=>$userTimezone,
                        'langIdInterface'=>$langIdInterface,
                        );
                break;

            case 'uparameters':
                $uparameterNumFriends=form('uparameterNumFriends','uint','post');
                $uparameterNumFollows=form('uparameterNumFollows','uint','post');
                $uparameterNumFollowers=form('uparameterNumFollowers','uint','post');
                $uparameterNumPosts=form('uparameterNumPosts','uint','post');
                $uparameterNumEvents=form('uparameterNumEvents','uint','post');
                $uparameterNumFavors=form('uparameterNumFavors','uint','post');
                $uparameterNumLikes=form('uparameterNumLikes','uint','post');
                $uparameterNumDislikes=form('uparameterNumDislikes','uint','post');
                $uparameterNumShares=form('uparameterNumShares','uint','post');
                $uparameterNumJoins=form('uparameterNumJoins','uint','post');
                $uparameterNumAskFriends=form('uparameterNumAskFriends','uint','post');
                $uparameterNumNeedFriends=form('uparameterNumNeedFriends','uint','post');

                $data=array('uparameterNumFriends'=>$uparameterNumFriends,
                        'uparameterNumFollows'=>$uparameterNumFollows,
                        'uparameterNumFollowers'=>$uparameterNumFollowers,
                        'uparameterNumPosts'=>$uparameterNumPosts,
                        'uparameterNumEvents'=>$uparameterNumEvents,
                        'uparameterNumFavors'=>$uparameterNumFavors,
                        'uparameterNumLikes'=>$uparameterNumLikes,
                        'uparameterNumDislikes'=>$uparameterNumDislikes,
                        'uparameterNumShares'=>$uparameterNumShares,
                        'uparameterNumJoins'=>$uparameterNumJoins,
                        'uparameterNumAskFriends'=>$uparameterNumAskFriends,
                        'uparameterNumNeedFriends'=>$uparameterNumNeedFriends,
                        );
                break;

            case 'uprolevels': 

                $interestId=form('interestId','string','post');
                $userGrade=form('userGrade','uint','post');
                $userProLevel=form('userProLevel','uint','post');
                $uprolevelTimeLastUpdate=form('uprolevelTimeLastUpdate','string','post');
                $data = array( 'interestId'=>$interestId,
                        'userGrade'=>$userGrade,
                        'userProLevel'=>$userProLevel,
                        'uprolevelTimeLastUpdate'=>$uprolevelTimeLastUpdate
                        );


                break;    
        }

        switch($table){
            case 'users' :
                $tableName=$this->usersTable ;
                break; 
            case 'uparameters':
                $tableName=$this->uparamTable;
                break;
            case 'uprolevels':
                $tableName=$this->ulevelTable ;
                break;
        }     

        $users=CZ::model('users')->user_update($userUid,$data,$tableName);     
        include($this->layout_path(CZ::controller(),CZ::command()));
    }    

    public function Command_UsersPoint(){
        $userUid=form('userUid','string','post');
        $users=CZ::model('users')->user_getter($userUid,$this->upointlogTable);      
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_UsersPointModify(){
        $userUid=form('userUid','string','post');
        $upointUid=form('upointUid','string','post');
        $userUid=form('userUid','string','post');
        $userPointEarn=form('userPointEarn','uint','post');
        $userPointUsed=form('userPointUsed','uint','post');
        $userPoint=form('userPoint','uint','post');
        $userPointTimeLimit=form('userPointTimeLimit','datetime','post');
        $upointlogTimeEarn=form('upointlogTimeEarn','datetime','post');
        $upointlogNote=form('upointlogNote','string','post');

        $data=array(
                'upointUid'=>$upointUid,
                'userUid'=>$userUid,
                'userPointEarn'=>$userPointEarn,
                'userPointUsed'=>$userPointUsed,
                'userPoint'=>$userPoint,
                'userPointTimeLimit'=>$userPointTimeLimit,
                'upointlogTimeEarn'=>$upointlogTimeEarn,
                'upointlogNote'=>$upointlogNote,
                );       

        $users=CZ::model('users')->user_update($userUid,$data,$this->upointlogTable);      
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_UsersGoods(){

        $page=form('p','int','get') ;

        if( empty($page) ) 
            $page = 1 ;

        $queryPage = $page -1 ;
        $prePage = '' ;
        $nextPage= '' ;
        $userUid=form('u','string','get');

        $goodsCount=CZ::model('goods')->goods_orderCount_getter($userUid);
        $goods=CZ::model('goods')->goods_order_by_user_getter($userUid,'','','',$queryPage,$this->rowNum);      
        $totalPage = ceil($goodsCount['d'][0]['count(*)'] / $rowNum );

        if($page > 1 )
            $prePage = $page -1 ; 
        else
            $prePage = '';   

        if($page < $totalPage )
            $nextPage = $page +1 ;
        else
            $nextPage = '' ;  

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_GoodsOrder(){

        $p=form('p','int','get') ;
        $oid=form('oid','string','get') ;
        $goodsName=form('goodsName','string','get') ;
        $goodsType=form('goodsType','int','get') ;
        $status=form('status','int','get') ;
        $brandName=form('brands','string','get') ;
        $stime=form('stime','date','get') ;
        $etime=form('etime','date','get') ;

        if( empty($p) )
            $p = 1 ;
        
        $queryPage = $p -1 ;

        $goods=CZ::model('goods')->goods_order_query($oid,$goodsName,$goodsType,$brandName,$status,$stime,$etime,'',$queryPage,$this->rowNum);

        for($i=0;$i<$goods['args']['num'];$i++){
            $suppliers=CZ::model('goods')->suppliers_getter('all',$goods['d'][$i]['supplierUid']);
            $brands=CZ::model('goods')->brands_getter('all',$goods['d'][$i]['brandUid']);
               
            $goods['d'][$i]['brandName'] = $brands['d'][0]['brandName'];
            $goods['d'][$i]['supplierName']=$suppliers['d'][0]['supplierName'];
        }

        $goodsCount=CZ::model('goods')->goods_order_query($oid,$goodsName,$goodsType,$brandName,$status,$stime,$etime,'','','','count');
        include($this->layout_path(CZ::controller(),CZ::command()));     
    }    

    public function Command_GoodsOrderModify(){
        $oid=form('oid','string','get') ;
        $oid2=form('oid','string','post') ; 
        $orderUpdate=false;

        if( !empty($oid2) ){

            $data=array(
                        'goodsorderStatus'=>form('status','int','post'),
                        'goodsorderTimeDelivery'=>form('sendgoodsdate','date','post'),
                        'goodslogistics'=>form('logistics','string','post'),
                        'goodsOtherLogistics'=>form('otherlogistics','string','post'),
                        'goodslogisticsId'=>form('logisticsId','string','post'),
                        'goodsorderNote'=>form('goodsorderNote','string','post'),
                        ); 
            $orderUpdate=CZ::model('goods')->goods_order_modify($oid2,$data);
        }
        
        $order=CZ::model('goods')->goods_order_getter($oid); 
        $suppliers=CZ::model('goods')->suppliers_getter('all',$order['supplierUid']);
        $brands=CZ::model('goods')->brands_getter('all',$order['brandUid']);
        $xuser=CZ::model('users')->xuser($order['userUid']);
        include($this->layout_path(CZ::controller(),CZ::command()));     
    }  

    public function Command_GoodsNew(){
        $goodsMsg = array();
        $interests=CZ::model('interests')->all_getter(); 
        $goodsName=form('goodsName','string','post');
        $brand=CZ::model('goods')->brands_getter('all','','',0,2000); 
        $suppliers=CZ::model('goods')->suppliers_getter('all','','',0,2000);
        $goodsUid = uid4();
        if(empty($goodsName)) {
            include($this->layout_path(CZ::controller(),CZ::command()));
            exit();
        }
        
        $images=CZ::model('images')->imageUpload($_FILES,$goodsUid,'goods' ); //圖檔上傳

        $imgM = ( array_key_exists('imgM' ,$images   ) ) ?  $images['imgM']['filename']  :  '';
        $img1 = ( array_key_exists('img1' ,$images   ) ) ?  $images['img1']['filename']  :  ''; 
        $img2 = ( array_key_exists('img2' ,$images   ) ) ?  $images['img2']['filename']  :  '';
        $img3 = ( array_key_exists('img3' ,$images   ) ) ?  $images['img3']['filename']  :  '';
        $img4 = ( array_key_exists('img4' ,$images   ) ) ?  $images['img4']['filename']  :  '';
        $img5 = ( array_key_exists('img5' ,$images   ) ) ?  $images['img5']['filename']  :  '';

        $goodsInterests = '';
        $goodsType=form('goodsType','uint','post');
        $goodsPricePoint=form('goodsPricePoint','uint','post');
        $goodsCost=form('goodsCost','uint','post');
        $goodsPriceMarket=form('goodsPriceMarket','uint','post');
        $goodsNumStorage=form('goodsNumStorage','uint','post');
        $goodsIntro=form('goodsIntro','string','post');
        $goodsNote=form('goodsNote','string','post');
        $brandUid=form('brandUid','string','post');
        $supplierUid=form('supplierUid','string','post');
        $goodsStatus=form('goodsStatus','string','post');
        $goodsInterestsArray=form('goodsInterests.','boolean','post');
        $goodsStore=form('goodsStore','string','post');
        $goodsFeature=form('goodsFeature','string','post');        
        $goodsTimeStart=form('goodsTimeStart','string','post');
        $goodsTimeClose=form('goodsTimeClose','string','post');
        $goodsDateLimitClose=form('goodsDateLimitClose','string','post');

        $goodsTimeStart .= ' '.date("H:i:s");

        if( $goodsTimeClose == '' )
            $goodsTimeClose = '2100-12-31 00:00:00';

        if( $goodsDateLimitClose == '' )
            $goodsDateLimitClose = '2100-12-31 00:00:00';

        $i=1;
        $counts = count($goodsInterestsArray);
        foreach($goodsInterestsArray as $key => $value)
        {
            if( $i == $counts)
                $goodsInterests .= $key;
            else{
                $goodsInterests .= $key.";";  
                $i++; 
            }  
        }   

        $data=array( 
                'goodsUid'=>$goodsUid,
                'goodsName'=>$goodsName,
                'goodsTimeStart'=>$goodsTimeStart,
                'goodsTimeClose'=>$goodsTimeClose,
                'goodsDateLimitStart'=>'2000-01-01 00:00:00',
                'goodsDateLimitClose'=>$goodsDateLimitClose,
                'goodsType'=>$goodsType,
                'imageUidMaster'=>$imgM,
                'imageUid1'=>$img1,
                'imageUid2'=>$img2,
                'imageUid3'=>$img3,
                'imageUid4'=>$img4,
                'imageUid5'=>$img5,
                'goodsPricePoint'=>$goodsPricePoint,
                'goodsCost'=>$goodsCost,
                'goodsPriceMarket'=>$goodsPriceMarket,
                'goodsNumStorage'=>$goodsNumStorage,
                'goodsIntro'=>$goodsIntro,
                'goodsNote'=>$goodsNote,
                'brandUid'=>$brandUid,
                'supplierUid'=>$supplierUid,
                'goodsInterests'=>$goodsInterests,
                'goodsExchangeFlow'=>'',
                'goodsStatus'=>$goodsStatus,
                'goodsStore'=>$goodsStore,
                'goodsFeature'=>$goodsFeature,
                );

        if( CZ::model('goods')->goods_new($data) ){
            $goodsMsg['success'] = $goodsName ;     

        }  
        else
            $goodsMsg['error'] = $goodsName ;     

        include($this->layout_path(CZ::controller(),CZ::command()));   
    }

    public function Command_imgUpload_ajax(){
       
        $img = $_FILES ;
        $imageMsg=array();
        $move=array();
        $goodsUid=form('goodsUid','string','post');
        foreach ($img as $key => $value) {
            $picType = explode('/',$value['type']);
            
            if(  array_key_exists(1,$picType) &&  ( $picType[1] == 'jpeg' || $picType[1] == 'png' || $picType[1] == 'gif' ) ){
                $picTypeCheck = true;
                $picType[1] = '.'.$picType[1] ;//image_convert 傳入變數需為 .jpg .bmp .png
            }    
            else
                $picTypeCheck = false;    

            if($value['error'] == 0  && $picTypeCheck  )
            {                  

                if( $key == 'imgM' )
                    $Conv = '_g' ;  //縮圖
                else
                    $Conv = '_w' ;    

                // 圖檔壓縮搬移
                $imgConv=CZ::model('images')->image_convert(dirname($value['tmp_name']).'/',basename($value['tmp_name']),$picType[1],'png');
                $move = (CZ::model('images')->image_move($imgConv['uid'],'/tmp/',$imgConv['tmp'],$this->imgUpload));

                $imageMsg[$key]['filename'] =$imgConv['uid']; 
                list($width,$height,$type,$attr)=getimagesize($value['tmp_name']);

                if( $move['success']  ){
                    $data=array(
                            'imageUid'=>$imgConv['uid'],
                            'imageConvert'=>$Conv,
                            'imageExtname'=>'.png',
                            'imageTable'=>$this->goods,
                            'imageTableId'=>$goodsUid, 
                            'imageCapacity'=>$value['size'],
                            'imageWidth'=>$width,
                            'imageHeight'=>$height,
                            'imagePublic'=>1,
                            'userUidUpload'=>ME::user_uid() ,
                            );
                    if(  !CZ::model('images')->image_add($data) )
                        $imageMsg[$key]['error'] = '圖片存入資料庫失敗';  
                }
                else
                    $imageMsg[$key]['error'] = '圖片搬移失敗';                    
            }
        } 
        echo '圖片網址 ： http://www.countmin.com'.CZ::model('images')->imagePath_getter($imgConv['uid']) ;

    }

    public function Command_Goodslist(){
        $data=array();
        $p=form('p','int','get') ;                                                                                      $gid=form('gid','string','get') ;
        $keyword=form('keyword','string','get') ;
        $brands=form('brands','string','get') ;    
        $status=form('status','int','get') ;
        $goodsType=form('goodsType','int','get') ;

        if( empty($p) )
            $p = 1 ;

        if( empty($desc))
            $desc = 'desc' ;

        $queryPage = $p -1 ;

        $goods=CZ::model('goods')->goods_list($gid,$keyword,$brands,$status,$goodsType,'','','',$desc,$queryPage,$this->rowNum);
        
        for($i=0;$i<$goods['args']['num'];$i++){
            $brand=CZ::model('goods')->brands_getter('all',$goods['d'][$i]['brandUid']);
            $suppliers=CZ::model('goods')->suppliers_getter('all',$goods['d'][$i]['supplierUid']); 
            $goods['d'][$i]['brandName'] = $brand['d'][0]['brandName'];
            $goods['d'][$i]['supplierName']= $suppliers['d'][0]['supplierName'];
        }

        $goodsCount=CZ::model('goods')->goods_list($gid,$keyword,$brands,$status,$goodsType,'','','',$desc,$queryPage,$this->rowNum,'count');
        include($this->layout_path(CZ::controller(),CZ::command())  )   ;
    }    

    public function Command_GoodsModify(){
        $goodsMsg = array();
        $goodsInterests='';
        $gid=form('gid','string','get') ;
        $goodsName=form('goodsName','string','post');
        $returnLink=form('returnLink','string','get') ;        
        $interests=CZ::model('interests')->all_getter();
        $goodsUid=form('goodsUid','string','post');

        $goodsTimeStart=form('goodsTimeStart','string','post');
        $goodsTimeClose=form('goodsTimeClose','string','post');
        $goodsDateLimitClose=form('goodsDateLimitClose','string','post');

        if( $goodsTimeClose == '' )
            $goodsTimeClose = '2100-12-31 00:00:00';

        if( $goodsDateLimitClose == '' )
            $goodsDateLimitClose = '2100-12-31 00:00:00';

        $modifyImg = false ;
        foreach ($_FILES as $key => $value) {
            if( !empty($value['name']) )
                $modifyImg = true; 
        }
        if($modifyImg){
            $images=CZ::model('images')->imageUpload($_FILES,$goodsUid,'goods' ); //圖檔上傳
            $imgM = ( array_key_exists('imgM' ,$images   ) ) ?  $images['imgM']['filename']  :  '';
            $img1 = ( array_key_exists('img1' ,$images   ) ) ?  $images['img1']['filename']  :  ''; 
            $img2 = ( array_key_exists('img2' ,$images   ) ) ?  $images['img2']['filename']  :  '';
            $img3 = ( array_key_exists('img3' ,$images   ) ) ?  $images['img3']['filename']  :  '';
            $img4 = ( array_key_exists('img4' ,$images   ) ) ?  $images['img4']['filename']  :  '';
            $img5 = ( array_key_exists('img5' ,$images   ) ) ?  $images['img5']['filename']  :  '';
        }
        $goodsInterestsArray=form('goodsInterests.','boolean','post');
        $i=1;
        $counts = count($goodsInterestsArray);
        foreach($goodsInterestsArray as $key => $value)
        {                                                                                                                                                   
            if( $i == $counts)
                $goodsInterests .= $key;
            else{
                $goodsInterests .= $key.";";  
                $i++; 
            }  
        }   

        $data=array( 
                'goodsName'=>form('goodsName','string','post'),
                'goodsTimeStart'=>$goodsTimeStart,
                'goodsTimeClose'=>$goodsTimeClose,
                'goodsDateLimitStart'=>'2000-01-01 00:00:00',
                'goodsDateLimitClose'=>$goodsDateLimitClose,
                'goodsType'=>form('goodsType','uint','post'),
                'goodsPricePoint'=>form('goodsPricePoint','uint','post'),
                'goodsCost'=>form('goodsCost','uint','post'),
                'goodsPriceMarket'=>form('goodsPriceMarket','uint','post'),
                'goodsNumStorage'=>form('goodsNumStorage','uint','post'),
                'goodsIntro'=>form('goodsIntro','string','post'),
                'goodsNote'=>form('goodsNote','string','post'),
                'supplierUid'=>form('supplierUid','string','post'),
                'goodsInterests'=>$goodsInterests,
                'goodsStatus'=>form('goodsStatus','uint','post'),    
                );
        
        if( !empty($imgM) )
            $data['imageUidMaster'] = $imgM ;

        if( !empty($img1))
            $data['imageUid1'] = $img1 ;

        if( !empty($img2))
            $data['imageUid2'] = $img2 ;

        if( !empty($img3))
            $data['imageUid3'] = $img3 ;

        if( !empty($img4))
            $data['imageUid4'] = $img4 ;

        if( !empty($img5))
            $data['imageUid5'] = $img5 ;         

        if( !empty($goodsUid)){
           $goods=CZ::model('goods')->goods_modify($gid,$data);
           if($goods){
                $goodsMsg['success'] = $goodsName ; 
           }
           else{
                $goodsMsg['error'] = $goodsName ; 
           }      
        }  

        $goods=CZ::model('goods')->goods_getter($gid);
        $brand=CZ::model('goods')->brands_getter('all',$goods['brandUid']);
        $suppliers=CZ::model('goods')->suppliers_getter('all',$goods['supplierUid']);        
        $brandAll=CZ::model('goods')->brands_getter('all','','','',2000);
        $suppliersAll=CZ::model('goods')->suppliers_getter('all','','','',2000);   

        include($this->layout_path(CZ::controller(),CZ::command())  )   ;
    }    

    public function Command_Supply(){
        $add=false;
        $update=false;
        $sid=form('sid','string','post');
        $sidName=form('sidName','string','post');
        $supplierName=form('supplierName','string','post');      
        $p=form('p','int','get');
        $spage=0;
        $supplyAdd=false;
        $supplyUpdate=false;    

        //新增供應商
        if( !empty($supplierName)){
            $supplierUid=uid4();
            $data=array( 
                        'supplierUid'=>$supplierUid,
                        'supplierName'=>$supplierName,
                        );
            $supplyAdd=CZ::model('goods')->suppliers_add($data);
        } 

        //修改供應商
        if($sid){
            $data=array('supplierName'=>$sidName);
            $supplyUpdate=CZ::model('goods')->suppliers_update($sid,$data);
        }
       
        if($p >0 )
            $spage = (int)$p - 1 ;

     
        $suppliers=CZ::model('goods')->suppliers_getter('all','','',$spage);
        $suppliersCount=CZ::model('goods')->suppliers_getter('count');

        include($this->layout_path(CZ::controller(),CZ::command()));            
    }

    public function Command_Brand(){
        $add=false;
        $update=false;
        $bid=form('bid','string','post');
        $p=form('p','int','get');
        $bidName=form('bidName','string','post');
        $brandName=form('brandName','string','post');      
        $spage=0;
        $brandsAdd=false;
        $brandsUpdate=false;

        //新增品牌
        if( !empty($brandName)){
            $images=CZ::model('images')->imageUpload($_FILES,$bid,'brand' ); //圖檔上傳
            $brandUid=uid4();
            $data=array(
                        'brandUid'=>$brandUid,
                        'brandName'=>$brandName,
                        );
            if(array_key_exists('imgM',$images)){
                $data['imageUid'] = $images['imgM']['filename'];
                $brandsAdd=CZ::model('goods')->brands_add($data);
            }
                
        }

        //修改品牌
        if($bid){
            $data=array('brandName'=>$bidName);
            if($_FILES['imgM2']['error'] == 0){
                $images=CZ::model('images')->imageUpload($_FILES,$bid,'brand' );
                $data['imageUid']=$images['imgM2']['filename'];
            }
                    

            $brandsUpdate=CZ::model('goods')->brands_update($bid,$data);
        }
 
        if($p >0 )
            $spage = (int)$p - 1 ;

        $brand=CZ::model('goods')->brands_getter('all','','',$spage);
        $brandCount=CZ::model('goods')->brands_getter('count');

        include($this->layout_path(CZ::controller(),CZ::command()));
    }


    public function Grid_Menu($args){
        include($this->grid_path($this->name,'menu'));
    }

}

