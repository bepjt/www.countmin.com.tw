<?php
class Editor_Controller extends Controller {
	public function __construct(){
		$this->name='editor';
		/**
		 * name => grid method
		 */
		$this->layout_grids=array(
		);
	
	}

	// 動態
	public function Command_Wallpost_save(){
        $wallpostUid = form('wid','string','post');
        $wtype = form('wtype','int','post');
		$topic = form('topic','string','post');
		$content = form('content', 'string', 'post');
		$privacy = form('privacy', 'int', 'post');
        $imgid   = form('imgid','string', 'post');
        $exe     = form('exe','string', 'post');

		// interest-list
		$interest_1 = form('interest_1','string','post');
		$interest_2 = form('interest_2','string','post');
		$interest_3 = form('interest_3','string','post');
		$interests = $interest_1.';'.$interest_2.';'.$interest_3;
		
        //由content內容抓第一張圖
        $content2=strip_tags($content,'<img>');
        preg_match_all('@src="([^"]+)"@',$content2,$pic);
        $imgUrl=explode("/",$pic[1][0]);
        $imgid=substr($imgUrl[4],0,28);

        //TODO 須考慮使用者從外部直接拉圖的情況
        
        switch($exe){
            case 'add': //新文章
                $wallpost_uid=CZ::model('wallposts')->post_add(ME::user_uid(), $privacy, $imgid, $topic, $content, $interests, '', '');

                //個人小成就點數
                $self_point=0;
                $give_point=true;////是否給點!?  防洪之後再用
        
                if($give_point){
                    //贈與點數與pro分
                    //這邊先擋任何自己洗自己動作
                    //$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
                    $action_type=1; ///這邊用1
                    $user1_note='你發表了'.$topic;
                    $usettle=CZ::model('users')->user_point_settle(ME::user_uid(),1,$action_type,$interests);

                    if(!empty($usettle['poster']))
                        CZ::model('users')->userwallpost_2p_add(ME::user_uid(),$usettle['poster'],$wallpost_uid,$user1_note);

                    //個人小成就點數
                    $self_point=20;
                }

                //個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
                CZ::model('wallposts')->uwallpost_socials_add(ME::user_uid(),$wallpost_uid,'',0,$self_point);
                break;
            case 'edit': //編輯
                $wallpost_edit=CZ::model('wallposts')->post_update($wallpostUid,ME::user_uid(),$wtype,$privacy,$imgid,$topic,$content,$interests);
                break;
            case 'tempedit': //草稿編輯
                $wallpost_edit=CZ::model('wallposts')->post_update($wallpostUid,ME::user_uid(),$wtype,$privacy,$imgid,$topic,$content,'');
                break;
            case 'temp': //新增草稿
                $wallpost_temp=CZ::model('wallposts')->post_add(ME::user_uid(), $privacy, $imgid, $topic, $content, '', '', '',1);
                break;
            default:
    
        }
	}

	// 抽獎
	public function Command_Lottery_save(){
		// 基本訊息
        $exe = form('exe','string','post');
        $wallpostUid = form('wid','string','post');
		$topic = form('topic','string','post');
		$content = form('content', 'string', 'post');
		$privacy = form('privacy', 'int', 'post');
		$imgid   = form('imgid','string', 'post');
		$date_s = form('lottery-date-s', 'string', 'post');
		$date_e = form('lottery-date-e', 'string', 'post');
		$go_time = form('lottery-gotime', 'string', 'post');
		$qty = form('quota', 'integer', 'post');
		$in_mesg = form('in_mesg', 'string', 'post');
		$url = form('url', 'string', 'post');
		//獎項
		$active_awards = form('active_awards', 'integer', 'post');
		$awards_quota = form('awards_quota', 'integer', 'post');
		$awards_name = form('awards_name', 'string', 'post');
		$awards_introduction = form('awards_introduction', 'string', 'post');
		$awards_img = form('awards_img', 'string', 'post');

		$pldate1 = form('pldate1', 'string', 'post');
		$pldate2 = form('pldate2', 'string', 'post');
		$pldate3 = form('pldate3', 'string', 'post');
		
		// interest-list
		$interest_1 = form('interest_1','string','post');
		$interest_2 = form('interest_2','string','post');
		$interest_3 = form('interest_3','string','post');
		$interests = $interest_1.';'.$interest_2.';'.$interest_3;
        $request = form('request','int','post');	

        $user_uid=ME::user_uid();

        //由content內容抓第一張圖
        $content2=strip_tags($content,'<img>');
        preg_match_all('@src="([^"]+)"@',$content2,$pic);
        $imgUrl=explode("/",$pic[1][0]);
        $imgid=substr($imgUrl[4],0,28);

        switch($exe){
            case 'add': //新增抽獎
                if( $active_awards == 1 ){
                    //先扣除抽獎活動點數
                    $less_point=CZ::model('users')->user_point_less($user_uid,$awards_quota);
                    if( !$less_point ){
                        echo '<script>alert("您的點數不足,無法新增抽獎活動")';
                        exit;    
                    }
                    $upointUid=uid4();   
                    $totalPoint = $awards_quota * $qty ; 
                    $data=array(
                                'upointUid'=>$upointUid,
                                'userUid'=>$user_uid,                                    
                                'userPointPay'=>$totalPoint,
                                'upointlogTimeEarn'=>_SYS_DATETIME,
                                'upointTable'=>'lottery',
                                'upointTableId'=>'',
                                'upointTableId2'=>'',
                                'upointlogNote'=>'發起 '.$topic.' 抽獎活動預扣點數'
                            );    
                   $pays_add= CZ::model('users')->upoint_pays_add($data);
                }
	
		        // 寫入資料庫
		        $mission_mid=CZ::model('wallposts')->mission_add($user_uid,$privacy,$imgid,$topic,$content,$interests,2,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img, $awards_name,$awards_introduction,$pldate3,$url,$awards_quota);
	
                
                if( $active_awards == 1 && $less_point){	
                    //update upoint_pays.upointTableId
                    $data=array('upointTableId'=>$mission_mid);
                    $updateUpointid=CZ::model('users')->upoint_pays_update($user_uid,$upointUid,$data);        

                    //更新 users.userPoint
                    $userPointUpdate=CZ::model('users')->user_point_update($user_uid,$totalPoint * (-1) );
                }

		        //個人小成就點數
		        $self_point=0;
		        $give_point=true;////是否給點!? 防洪之後再用

		        if($give_point){
			        //個人小成就點數
			        $self_point=20;
			
			        //贈與點數與pro分
			        //這邊先擋任何自己洗自己動作
			        //$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
			        $action_type=1; ///這邊用1
			        $user1_note='你發表了'.$topic.'任務';
			        $usettle=CZ::model('users')->user_point_settle($user_uid,2,$action_type,$interests);
			
			        if(!empty($usettle['poster']))
				        CZ::model('users')->userwallpost_2p_add($user_uid,$usettle['poster'],$mission_mid,$user1_note);
		        }
		
		        //個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
		        CZ::model('wallposts')->uwallpost_socials_add($user_uid,$mission_mid,'',0,$self_point);
	            break;
            case 'edit': //編輯
                $mission_edit=CZ::model('wallposts')->mission_update($wallpostUid,$user_uid,$privacy,$imgid,$topic,$content,$interests,2,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img, $awards_name,$awards_introduction,$pldate3,$url,$awards_quota);
                break;
            case 'tempedit': //草稿編輯
                $mission_edit=CZ::model('wallposts')->mission_update($wallpostUid,$user_uid,$privacy,$imgid,$topic,$content,'',2,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img, $awards_name,$awards_introduction,$pldate3,$url,$awards_quota);
                break;
            case 'temp': //新增草稿
                $mission_temp=CZ::model('wallposts')->mission_add($user_uid,$privacy,$imgid,$topic,$content,'',2,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img, $awards_name,$awards_introduction,$pldate3,$url,$awards_quota,1);
                break;
            default:
        }
	}

	//推廣
	public function Command_Pulling_save(){
		// 基本訊息
        $exe = form('exe','string','post');
        $wallpostUid = form('wid','string','post');
		$topic = form('topic','string','post');
		$content = form('content', 'string', 'post');
		$privacy = form('privacy', 'int', 'post');
		$imgid   = form('imgid','string', 'post');

		// interest-list
		$interest_1 = form('interest_1','string','post');
		$interest_2 = form('interest_2','string','post');
		$interest_3 = form('interest_3','string','post');
		$qty = form('quota', 'integer', 'post');

		$interests = $interest_1.';'.$interest_2.';'.$interest_3;
		//獎項
		$active_awards = form('product_sel', 'integer', 'post');
		$in_mesg = form('in_mesg', 'string', 'post');

		$active_awards = form('active_awards', 'integer', 'post');
		$awards_name = form('awards_name', 'string', 'post');
		$awards_introduction = form('awards_introduction', 'string', 'post');
		$awards_quota = form('awards_quota', 'integer', 'post');
		$awards_img = form('awards_img', 'string', 'post');
	    $request = form('request','int','post'); 
    	
		$pldate1 = form('pldate1', 'string', 'post');
		$pldate2 = form('pldate2', 'string', 'post');
		$pldate3 = form('pldate3', 'string', 'post');

        $user_uid = ME::user_uid() ;
		$url = 	form('url', 'string', 'post');		
		
		$view_img_id=!empty($awards_img) ? $awards_img : $imgid;
		
        //由content內容抓第一張圖
        $content2=strip_tags($content,'<img>');
        preg_match_all('@src="([^"]+)"@',$content2,$pic);
        $imgUrl=explode("/",$pic[1][0]);
        $imgid=substr($imgUrl[4],0,28);

        switch($exe){
            case 'add': //新文章   
                if( $active_awards == 1 ){                                                                                                                  
                    //先扣除抽獎活動點數
                    $less_point=CZ::model('users')->user_point_less($user_uid,$awards_quota);
                    if( !$less_point ){
                        echo '<script>alert("您的點數不足,無法新增抽獎活動")';
                        exit;
                    }
                    $upointUid=uid4();
                    $totalPoint = $awards_quota * $qty ;
                    $data=array(
                                'upointUid'=>$upointUid,
                                'userUid'=>$user_uid,
                                'userPointPay'=>$totalPoint,
                                'upointlogTimeEarn'=>_SYS_DATETIME,
                                'upointTable'=>'lottery',
                                'upointTableId'=>'',
                                'upointTableId2'=>'',
                                'upointlogNote'=>'發起 '.$topic.' 推廣活動預扣點數'
                            );
                   $pays_add= CZ::model('users')->upoint_pays_add($data);
                }

		        $mission_mid=CZ::model('wallposts')->mission_add(ME::user_uid(),$privacy,$imgid,$topic,$content,$interests,3,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img,$awards_name,$awards_introduction,$pldate3,$url,$awards_quota);

                if( $active_awards == 1 && $less_point){    
                    //update upoint_pays.upointTableId
                    $data=array('upointTableId'=>$mission_mid);
                    $updateUpointid=CZ::model('users')->upoint_pays_update($user_uid,$upointUid,$data);        

                    //更新 users.userPoint
                    $userPointUpdate=CZ::model('users')->user_point_update($user_uid,$totalPoint * (-1) );
                }

		        //個人小成就點數
			    $self_point=0;
		        $give_point=true;////是否給點!?  防洪之後再用
		        if($give_point){
			        //個人小成就點數
			        $self_point=20;
			        //贈與點數與pro分
			        //這邊先擋任何自己洗自己動作
			        //$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
			        $action_type=1; ///這邊用1
			        $user1_note='你推廣了'.$topic;
			        $usettle=CZ::model('users')->user_point_settle(ME::user_uid(),3,$action_type,$interests);
			
			        if(!empty($usettle['poster']))
				        CZ::model('users')->userwallpost_2p_add(ME::user_uid(),$usettle['poster'],$mission_mid,$user1_note);
			
		        }
		        //個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
		        CZ::model('wallposts')->uwallpost_socials_add(ME::user_uid(),$mission_mid,'',0,$self_point);
                break;
            case 'edit': //編輯
                $mission_edit=CZ::model('wallposts')->mission_update($wallpostUid,ME::user_uid(),$privacy,$imgid,$topic,$content,$interests,3,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img, $awards_name,$awards_introduction,$pldate3,$url,$awards_quota);
                break;
            case 'tempedit': //草稿編輯
                $mission_edit=CZ::model('wallposts')->mission_update($wallpostUid,ME::user_uid(),$privacy,$imgid,$topic,$content,'',3,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img, $awards_name,$awards_introduction,$pldate3,$url,$awards_quota);                                                                                                   
                break;
            case 'temp': //新增草稿
                $mission_temp=CZ::model('wallposts')->mission_add(ME::user_uid(),$privacy,$imgid,$topic,$content,'',3,$pldate1,$pldate2,$qty,$request,$in_mesg,$active_awards, $awards_img, $awards_name,$awards_introduction,$pldate3,$url,$awards_quota,1);
                break;

            default:
        }

	}

	// 揪團
	public function Command_Spread_save(){
		// 基本訊息
        $exe = form('exe','string','post');
        $wallpostUid = form('wid','string','post');
		$topic = form('topic','string','post');
		$pldate1 = form('pldate1', 'string', 'post');
		$pldate2 = form('pldate2', 'string', 'post');		
		$qty = form('qty', 'integer', 'post');
		$price = form('price', 'integer', 'post');
		$url = form('url', 'string', 'post');
		$content = form('content', 'string', 'post');
		$privacy = form('privacy', 'integer', 'post');
		$imgid= form('imgid','string', 'post'); ///插入(第一張)圖檔


		// interest-list
		$interest_1 = form('interest_1','string','post');
		$interest_2 = form('interest_2','string','post');
		$interest_3 = form('interest_3','string','post');
		$interests = $interest_1.';'.$interest_2.';'.$interest_3;

        //由content內容抓第一張圖
        $content2=strip_tags($content,'<img>');
        preg_match_all('@src="([^"]+)"@',$content2,$pic);
        $imgUrl=explode("/",$pic[1][0]);
        $imgid=substr($imgUrl[4],0,28);


        switch($exe){
            case 'add': //新文章
		        $event_mid=CZ::model('wallposts')->event_add(ME::user_uid(),$privacy,$imgid,$topic,$content,$interests,$pldate1,$pldate2,$qty,$url,$price);
		        //個人小成就點數
	            $self_point=0;
		        $give_point=true;////是否給點!?  防洪之後再用
		        if($give_point){
			        //個人小成就點數
			        $self_point=20;
			        //贈與點數與pro分
			        //這邊先擋任何自己洗自己動作
			        //$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
			        $action_type=1; ///這邊用1
			        $user1_note='你揪了一團'.$topic;
			        $usettle=CZ::model('users')->user_point_settle(ME::user_uid(),4,$action_type,$interests);
			
			        if(!empty($usettle['poster']))
				        CZ::model('users')->userwallpost_2p_add(ME::user_uid(),$usettle['poster'],$event_mid,$user1_note);
		        }
		        //個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
		        CZ::model('wallposts')->uwallpost_socials_add(ME::user_uid(),$event_mid,'',0,$self_point);
	            break;
            case 'edit': //編輯
                $event_edit=CZ::model('wallposts')->event_update($wallpostUid,ME::user_uid(),$privacy,$imgid,$topic,$content,$interests,$pldate1,$pldate2,$qty,$url,$price);
                break;
            case 'tempedit': //草稿編輯
                $event_tempedit=CZ::model('wallposts')->event_update($wallpostUid,ME::user_uid(),$privacy,$imgid,$topic,$content,'',$pldate1,$pldate2,$qty,$url,$price);
                break;
            case 'temp': //新增草稿
                $event_temp=CZ::model('wallposts')->event_add(ME::user_uid(),$privacy,$imgid,$topic,$content,'',$pldate1,$pldate2,$qty,$url,$price,1);
                break;
            default:
        }
	
	}

	public function Command_Uploader(){

        $funcNum = form('CKEditorFuncNum','string','get') ;
        $message='';
		$type=form('type','string','get');

		switch($type){
            case 'img':
		        //圖片壓縮 
                $wallpostUid='5566'; //TODO 需將wallpostUid傳入   
                $images=CZ::model('images')->imageUpload($_FILES,$wallpostUid );
				//顯示圖片網址   
				if($images){
			        $data=array(
					    'uid'=> $images['fileupload']['filename'] ,
						'url'=> CZ::model('images')->imagePath_getter($images['fileupload']['filename']),
					);
                    $url=CZ::model('images')->imagePath_getter($images['fileupload']['filename']);
                    //echo "<script type='text/javascript'>
                    //        window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');
                    //      </script>";
                    echo $url;    
                    //die(json_encode($data));
				}                
			break;			
			case 'media-link':
				include(_DIR_FRAMEWORK.'include/lib_http.php');
				$resu_youtube = http_youtube_watch($_POST["link"]);
				die(json_encode($resu_youtube));
				// echo $resu_youtube['image_small'];
				break;
			case 'pdtimg':
				//圖片壓縮 
                $wallpostUid='1234'; //TODO 需將wallpostUid傳入   
                $images=CZ::model('images')->imageUpload($_FILES,$wallpostUid );
				//顯示圖片網址   
				if($images){   
					$data=array(
						'uid'=> $images['pdtupload']['filename'] ,
						'url'=> CZ::model('images')->imagePath_getter($images['pdtupload']['filename']),
					);
					die(json_encode($data));
				}                
				break;	
		}
		die('');
	
	}

    public function Command_getUserPoint(){
        //ajax used 發表抽獎取user點數
        $user_uid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($user_uid,1);
        
        if($xuser)
            echo $xuser['basic']['userPoint']; 
        else
            echo false;
    }

    public function Command_Wallpost_getter(){
        $wallpostUid=form('wid','string','get');
        $user_uid=ME::user_uid();
        $wallpost=CZ::model('wallposts')->wallpost_editor_getter($user_uid,$wallpostUid);        
        echo json_encode( $wallpost );
    }
}
