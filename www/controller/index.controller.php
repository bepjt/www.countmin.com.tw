<?php
use Mailgun\Mailgun;
class Index_Controller extends Controller {

    public function __construct(){
        $this->name='index';
        $this->key = '=-0-f0if4ff9fj94jntb32yt[]ponpghgdsddsk#$2!'; //重設密碼編碼用
        $this->checkKey = '$%#@$HTR^JDQWVSD##Rdvvbs44221';          //重設密碼驗證用
        $this->emailAuthKey = 'cKhyPLOyFR87&(_+^^%%az';             //email認證用
        $this->loginTokenKey = 'n4n%H&$fbtokenjy328b66#%^&8P{gHejED' ; //fb login token編碼用
        $this->profile_width=180; //大頭貼照片寬度,需與前端縮放框相符 
        $this->profile_heigh=180; //大頭貼照片高度
        /**
         * name => grid method
         */
        $this->layout_grids=array(
                'page-header'=>array('grid'=>'Header'),
                'user-basic'=>array('grid'=>'UserBasic'),
                'user-info'=>array('grid'=>'UserInfo'),
                'masterpeople'=>array('grid'=>'masterpeople'),
                'interestpeople'=>array('grid'=>'interestPeople'),
                'hotwrite'=>array('grid'=>'hotWrite'),
                'registform'=>array('grid'=>'registform'),
                );
        
        CZ::grid_setter('page-header',TRUE);
        CZ::grid_setter('user-basic',TRUE);
        CZ::grid_setter('user-info',TRUE);
        CZ::grid_setter('masterpeople',TRUE);
	    CZ::grid_setter('interestpeople',TRUE);
	    CZ::grid_setter('hotwrite',TRUE);
        CZ::grid_setter('registform',TRUE);
    }

    public function Command_Login(){
		$this->layout='login';
        $showLoginErr=false;        

        CZ::grid_setter('user-basic',TRUE);
        CZ::grid_setter('user-info',FALSE);

        $exe=form('exe','strip_unsafe','post');
        if($exe=='login'){
            $uid=form('uid','strip_unsafe','post');
            $pwd=form('pwd','strip_unsafe','post');
            $cuser=CZ::model('users')->uid_pwd_check($uid,$pwd);
            if($cuser){
                ME::become($cuser['userUid'],$cuser['userRealname'],1,$cuser['userType']==9?1:0);  
                CZ::page('login_then_redirect'); 
            }
            else{
                $showLoginErr=true;
                //CZ::page('login_failed');
            }    
        }

		$redirect_uri='http://'.$_SERVER['HTTP_HOST'].'/index/login_facebook_confirm/';
		$request_params=array(
			'client_id'=>CZ::config_get('sns.facebook.app_id'),
			'response_type'=>'token',
			'scope'=>join(',',array('public_profile','user_friends','email','user_about_me')),
		);

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

	public function Command_Login_facebook_confirm(){
        $this->layout='regist';
		$error=form('error','strip_unsafe','get');
		$error_code=form('error_code','uint','get');
		$error_description=form('error_description','strip_unsafe','get');
		$error_reason=form('error_reason','strip_unsafe','get');
        $fbtoken=array();
        $app_id='';

		if($error_code){
			CZ::page('fblogin_failed',array('error'=>$error,'code'=>$error_code,'description'=>$error_description,'reason'=>$error_reason));
		}
		$d=form('d','strip_unsafe','post');

		if(strlen($d)){
			parse_str($d,$d);
			if(!isset($d['name']) || empty($d['id'])){
				CZ::page('error_404');
			}
			if(!isset($d['access_token'])){
				$d['access_token']='';
			} 
			$cuser=CZ::model('users')->facebook_pwd_check($d['id'],$d['access_token']);
			if(!$cuser){
                $fbtoken=$d;
			}
			else{
				ME::become($cuser['userUid'],$cuser['userRealname'],1,$cuser['userType']==9?1:0);
                CZ::page('login_then_redirect');
                exit;
			}
		}
		else{
			$app_id=CZ::config_get('sns.facebook.app_id');
		}
        include($this->layout_path(CZ::controller(),CZ::command()));
	}

    public function Command_Logout(){

        ME::user_set('uid','');
        ME::user_set('name','');
        ME::user_set('perm','');
        ME::user_set('admin','');
        CZ::page('logout_then_redirect');
    }

	public function Command_Index(){

	}

    public function Command_Registauth(){
        $authid=form('authid','strip_unsafe','get');
        $decode=explode(';',CZ::model('counter')->decrypt($authid,$this->emailAuthKey));

        $userUid=htmlspecialchars(strip_tags($decode[0]));        
        $email=$decode[1];    
        $uaccs=CZ::model('users')->uaccs_getter($userUid,'uaccRegistStep');
        /*  0  註冊完成
            1  email驗證通過
            10 基本資料填寫完成
            11 email驗證通過 及 基本資料填寫完成
        */
        $registCheck=( $uaccs['uaccRegistStep'] != 1 && $uaccs['uaccRegistStep'] != 11 );    

        if( $decode[2] == md5($userUid.$this->emailAuthKey) && $registCheck ){
            //給點數
            $data=array(
                        'upointUid'=>uid4(),
                        'userUid'=>$userUid,
                        'userPointEarn'=>300,
                        'userPointUsed'=>0,
                        'userPoint'=>300,
                        'userPointTimeLimit'=>date("Y",strtotime('+1 year')).'-12-31',
                        'upointlogTimeEarn'=>_SYS_DATETIME,
                        'upointTable'=>'emailauth',
                        'upointTableId'=>'',
                        'upointlogNote'=>'E-mail 認證完成',
                        );
         
            $upointlogs=CZ::model('users')->upointlogs_add($data);                

            //更新user.userpoint
            $user_point=CZ::model('users')->user_point_update($userUid,300);

            //更改註冊進度
            switch($uaccs['uaccRegistStep']){
                case 0:
                    $data=array('uaccRegistStep'=>1);
                    break;
                case 10:
                    $data=array('uaccRegistStep'=>11);
                    break;
                default:
                    $data=array('uaccRegistStep'=>1);
            }

            $uaccsUpdate=CZ::model('users')->uaccs_update($decode[0],$data);

            //通知
            CZ::model('notifications')->notifications_add('countmin-system',$userUid,'/user/about','','Ly_regist_authemail_completed','');

            //email
            $subtitle='完成認證信';
            $content='
                        恭喜你!已完成驗證信，獲得點數300點 <p>
                    
                        完成基本資料填寫後再送500點 <p>
    
                        立即前往 http://www.countmin.com/user/about <p>

                    ';
            $sendEmail=CZ::model('mail')->send_mail($email,$subtitle,$content);            
        }
        include($this->layout_path(CZ::controller(),CZ::command()));
    }
    
    /**
     * Command_Explore 
     * 探索頁面
     * 
     * @access public
     * @return void
     */
    public function Command_Explore(){
        $orderby='';
        $type='';
        $wallpostType='';
        $interest='';
        $score=array(); 
        $moreType='';
        $highestIndex=0;
        $users=array();
        $userUid=ME::user_uid(); 
        $sort=form('sort','strip_unsafe','get');
        $type=form('type','strip_unsafe','get');
        $keyword=form('keyword','strip_unsafe','get');
        $searchType=form('searchType','strip_unsafe','get');
        $wallpostInterests=form('interest','strip_unsafe','get');
 
        $defaultPhoto_m='/css/img/default_profile_pic_m.png';
        $defaultPhoto_s='/css/img/default_profile_pic_s.png';
				
	    CZ::grid_setter('interestpeople',TRUE);
		CZ::grid_setter('hotwrite',TRUE);

        switch($sort){
            case 'time' : //時間
                $orderby = 'wallpostTimeCreate';
                break;
            case 'hot' :  //熱門 最受歡迎
                $orderby = 'wallpostNumLikes' ;
                break;
            case 'view' : //瀏覽量
                $orderby = 'wallpostNumViews' ;  
                break;
            default:
                $orderby = 'wallpostTimeCreate'; 
        }
        
        switch($type){
            case 'msg' :
                $wallpostType = 'msg';
                break;
            case 'event':  
                $wallpostType = 'event';
                break;
            default:
                $wallpostType = 'all' ;  
        }    

        //27興趣標籤
        $internetTag=CZ::model('users')->interest_getter();

        //關鍵字商品搜尋
        if(!empty($keyword) && $searchType=='goods'    ){
            header('Location:/index/goods?key='.$keyword);
            exit;
        }

        //會員搜尋 
        if(!empty($keyword) && $searchType=='member'  ){ 
            header('Location:/index/exploremore?type=key&keyword='.$keyword);
            exit;
        }

        //已登入 您可能感興趣的人
        if(ME::is_login() && empty($keyword) ){
            $relation_from=2;//與您興趣相似的人
            $hotPeople=CZ::model('users')->relation_list($userUid,$relation_from,3,0);                
            $hotPeopleCount=CZ::model('users')->relation_list_count($userUid,$relation_from); 
            
            $moreType='conn'; 
            for($i=0;$i<$hotPeople['args']['num'];$i++){
                $tmp = CZ::model('users')->user_getter( $hotPeople['d'][$i]['userUid2']  )  ;
                $title= CZ::model('users')->uprolevels_getter( $hotPeople['d'][$i]['userUid2']  )  ;    
                $users['d'][$i] = $tmp['d'][0];
                $users['d'][$i]['title'] = $title['title'];             
                $users['args']['num'] = $hotPeopleCount['count(*)'];
            }
        }

        //未登入 您可能感興趣的人
        if(!ME::is_login() && empty($keyword) ){        
            $users=CZ::model('users')->users_top_getter($wallpostInterests); 
            for($i=0;$i<$users['args']['num'];$i++){
                $title= CZ::model('users')->uprolevels_getter( $users['d'][$i]['userUid']  )  ;
                $users['d'][$i]['title'] = $title['title'];
            }
            $moreType='pro';    
        }
        
        //追蹤判斷
		if(!empty($users['d'])){
		    foreach($users['d'] as $inx => $user_data){
				$user_uid1=$userUid;
				$user_uid2=$user_data['userUid'];
				if(!ME::is_login()){$user_uid1='';}
						
				$relation=CZ::model('users')->relations_getter($user_uid1,$user_uid2);
				$users['d'][$inx]['relation_status']=$relation['relation_status'];
				$imgUrl=CZ::model('images')->imagePath_getter($user_data['userProfile']) ;
						
                if(empty($imgUrl)){$imgUrl=$defaultPhoto_s;}
				    
		        $users['d'][$inx]['imgUrl']=$imgUrl;
			}
		}
				
        //熱門動態
        //計算方式  (+1次數 + 留言次數+被分享次數) / (今天日期-發文日期)
        $hotWrite=CZ::model('wallposts')->explore_hotgetter();
        $today =  strtotime(date("Y-m-d 23:59:59")) ;
        for($i=0;$i<$hotWrite['args']['num'];$i++){
            $diffDay = $today - strtotime($hotWrite['d'][$i]['wallpostTimeCreate'] );
            $num = $hotWrite['d'][$i]['wallpostNumLikes'] + $hotWrite['d'][$i]['wallpostNumComments'] + $hotWrite['d'][$i]['wallpostNumShares'];
            $score[$i] = ($num/$diffDay);
        }

        if( $hotWrite['args']['num'] > 0){
            if ( is_array($score) && count($score) > 0)
                $highestIndex = array_search(max($score),$score);
       
            $hotWrite=$hotWrite['d'][$highestIndex];
        }
      
        include($this->layout_path(CZ::controller(),CZ::command()));
    }
   
    public function Command_ExploreAjax(){
        $userUid=ME::user_uid();
        $spage=form('page','int','get');
        $sort=form('sort','strip_unsafe','get');                                                                                                            
        $type=form('type','strip_unsafe','get');
        $keyword=form('keyword','strip_unsafe','get');
        $searchType=form('searchType','strip_unsafe','get');
        $wallpostInterests=form('interest','strip_unsafe','get');

        switch($sort){
            case 'time' : //時間
                $orderby = 'wallpostTimeCreate';
                break;
            case 'hot' :  //熱門 最受歡迎
                $orderby = 'wallpostNumLikes' ;
                break;
            case 'view' : //瀏覽量
                $orderby = 'wallpostNumViews' ;  
                break;
            default:
                $orderby = 'wallpostTimeCreate';
        }

        switch($type){
            case 'msg' :
                $wallpostType = 'msg';
                break;
            case 'event':  
                $wallpostType = 'event';
                break;
            default:
                $wallpostType = 'all' ;
        }    


        //量身推薦 
        if(ME::is_login() && $type=='recomm' && empty($keyword)  ){
            $interest_str='';
            $userInterest=CZ::model('users')->uprolevels_getter($userUid); 

            for($i=0;$i<3;$i++){
                if( $i == 2)
                    $interest_str .= $userInterest['d'][$i]['interestId'];
                else
                    $interest_str .= $userInterest['d'][$i]['interestId'].";";
            }
            $explore=CZ::model('wallposts')->explore_recommgetter($interest_str,$orderby,$spage); //量身推薦
        }                      
        elseif(empty($keyword) && $type !='recomm' ) //一般探索 動態,活動
            $explore=CZ::model('wallposts')->explore_getter( $orderby,$wallpostType,$wallpostInterests,$spage); 
        elseif(!empty($keyword) && $type !='recomm') //關鍵字搜尋
            $explore=CZ::model('search')->wallposts_search($keyword,7,$wallpostInterests,$orderby,$spage);
 
        if($explore['args']['num']>0){
            foreach($explore['d'] as $inx => $data_info){
                $explore['d'][$inx]['uwallpost_track']=CZ::model('wallposts')->user_walltrack(ME::user_uid(),$data_info['wallpostUid']);
            }
        }

        include($this->grid_path($this->name,'exploreajax'));
    }
 
    public function Grid_interestPeople($users){
		include($this->grid_path($this->name,'interestpeople'));
	}
    
    public function Grid_hotWrite($hotWrite){
		include($this->grid_path($this->name,'hotwrite'));
	}

    public function Grid_registform($fbtoken){
        include($this->grid_path($this->name,'registform'));
    }


    public function Command_Exploremore(){
        $userUid=ME::user_uid();
        $type=form('type','strip_unsafe','get');   
        $keyword=form('keyword','strip_unsafe','get');
        $interest=form('interest','strip_unsafe','get');
        $users['args']['num']=0;

        if(empty($type))
            $type='people';

        //27興趣標籤
        $internetTag=CZ::model('users')->interest_getter();

        //已登入您可能感興趣的人 移到社交圈?
        if(ME::is_login() && $type=='conn' ){
            $hotPeople=CZ::model('users')->uconnects_getter($userUid ); 
            for($i=0;$i<$hotPeople['args']['num'];$i++){
                $tmp = CZ::model('users')->user_getter( $hotPeople['d'][$i]['userUid2']  )  ;
                $users['d'][$i] = $tmp['d'][0];
            }
            $users['args']['num']=$hotPeople['args']['num'];
        }

        //未登入-你可能感興趣的人
        if(!ME::is_login() && $type=='pro' )
            $users=CZ::model('users')->users_top_getter($interest,16); 

        //關鍵字搜尋會員
        if( $type=='key' && !empty($keyword))
            $users=CZ::model('search')->users_search($keyword,$interest); 
        
        //推薦達人
        if( $type=='people')
            $users=CZ::model('users')->usermasters_getter($interest); 
      
        //取得user的興趣標籤  
        for($i=0;$i<$users['args']['num'];$i++) 
            $upro[$i] = CZ::model('users')->uprolevels_getter($users['d'][$i]['userUid'],1); 

        //取得文章
        for($i=0;$i<$users['args']['num'];$i++)
            $wallposts[$i]=CZ::model('wallposts')->wallpost_getter($users['d'][$i]['userUid']);            
        //追蹤判斷
        if(!empty($users['d'])){
            foreach($users['d'] as $inx => $user_data){
		        $user_uid1=$userUid;
				$user_uid2=$user_data['userUid'];

				if(!ME::is_login()){$user_uid1='';}
						
				$relation=CZ::model('users')->relations_getter($user_uid1,$user_uid2);
				$users['d'][$inx]['relation_status']=$relation['relation_status'];
				$imgUrl=CZ::model('images')->imagePath_getter($user_data['userProfile']) ;
				
                if(empty($imgUrl)){$imgUrl=$defaultPhoto_s;}
						
                $users['d'][$inx]['imgUrl']=$imgUrl;
			}
		}
        
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_Goods(){	
        
        $sixMon='';
		$sort=form('sort','strip_unsafe','get');
		$status=form('status','strip_unsafe','get');
		$key=form('key','strip_unsafe','get');
		$goodsChoose=form('goodsChoose','strip_unsafe','get');
        $myfavors=form('myfavors','int','get');
		$interest=form('interest','strip_unsafe','get');
        $goodsFree=false;        
        $gid='';
        $expirePoint=0;
   
        switch($goodsChoose){
            case 'all' :
                $goodsType = '';
                break;
            case 'mail' :
                $goodsType = 1;
                break;
            case 'pc' :
                $goodsType = 2;
                break;
            default: 
                $goodsType = '';  
        }    
        
        $userUid=ME::user_uid();
        $tag=CZ::model('users')->interest_getter();  
        $xuser=CZ::model('users')->xuser($userUid);
        $favorsCount=CZ::model('ufavors')->ufavors_getter_count(ME::user_uid(),5  );         
        $sixMon = date("Y").'-12-31';

        //點數到期前半年再做顯示告知 
        if( date("m") >= 6 )
            $expirePoint=CZ::model('users')->expirePointTotal($userUid);       

        include($this->layout_path(CZ::controller(),CZ::command()));    
    }

    public function Command_Goodsajax(){
        $spage=form('page','int','get');
        //_e($_GET);
        $orderby='';
        $sort=form('sort','strip_unsafe','get');
        $status=form('status','strip_unsafe','get');
        $key=form('key','strip_unsafe','get');
        $goodsChoose=form('goodsChoose','strip_unsafe','get');
        $myfavors=form('myfavors','int','get');
        $interest=form('interest','strip_unsafe','get');
        $goodsFree=false;
        $gid='';
        $expirePoint=0;

        switch($sort){
            case 'hot' :
                $orderby = 'goodsNumExchange desc';
                break;
            case 'new' :
                $orderby='goodsTimeStart desc';
                break;
            case 'lpoint' :
                $orderby='goodsPricePoint';
                break;
            case 'hpoint' :
                $orderby='goodsPricePoint desc';
                break; 
            default: 
                $orderby='goodsTimeStart desc';
        }              
        
        switch($goodsChoose){
            case 'all' :
                $goodsType = '';
                break;
            case 'mail' :
                $goodsType = 1;
                break;
            case 'pc' :
                $goodsType = 2;
                break;
            default: 
                $goodsType = '';  
        }    
        $userUid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($userUid);

        //點數到期前半年再做顯示告知 
        if( date("m") >= 6 )  
            $expirePoint=CZ::model('users')->expirePointTotal($userUid);

        if($myfavors){
            //我的好康收藏
            $favors=CZ::model('ufavors')->ufavors_getter(ME::user_uid(),5);
            for($i=0;$i<$favors['args']['num'];$i++){
                if($i == $favors['args']['num'] - 1)
                    $gid.="'".$favors['d'][$i]['wallpostUid']."'";
                else
                    $gid.="'".$favors['d'][$i]['wallpostUid']."',";
            }
            $goods=CZ::model('goods')->goods_indexlist($key,$orderby,$goodsType,$interest,$gid,$spage);
        }
        else
            $goods=CZ::model('goods')->goods_indexlist($key,$orderby,$goodsType,$interest,'',$spage);

        for($i=0;$i<$goods['args']['num'];$i++){
           $favorsCheck=CZ::model('ufavors')->ufavors_check(ME::user_uid(),$goods['d'][$i]['goodsUid'],5);
           $goods['d'][$i]['addfavors']=$favorsCheck['count(*)'] ;
        }
        

        include($this->grid_path($this->name,'goodsajax'));
    }

    public function Command_GoodsDetail(){
        $xuser_uid=ME::user_uid();                                              
        $sixMon = date("Y").'-12-31';
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);   
        $goodsUid=form('gid','strip_unsafe','get');  
        $expirePoint=form('point','int','get');
        $goods=CZ::model('goods')->goods_getter($goodsUid);    
        $brands=CZ::model('goods')->brands_getter('all',$goods['brandUid']);        
        $imgM = CZ::model('images')->imagePath_getter($goods['imageUidMaster']) ;   
        $favorsCount=CZ::model('ufavors')->ufavors_getter_count(ME::user_uid(),5  ); 
        $favorsCheck=CZ::model('ufavors')->ufavors_check(ME::user_uid(),$goodsUid,5); 

        $upointCheck = $this->upoint_check($goods);
    
        include($this->layout_path(CZ::controller(),CZ::command()));    
    }    

    public function Command_GoodsBuy(){
        $xuser_uid=ME::user_uid(); 
        $users=CZ::model('users')->xuser($xuser_uid,1,0,0,1); 
        $goodsUid=form('gid','strip_unsafe','post');
        $deliveryinfo=CZ::model('goods')->deliveryinfo_getter($xuser_uid);

        $showCity=CZ::model('users')->regions_regionuid_getter($deliveryinfo['d'][0]['city']);
        $showDist=CZ::model('users')->regions_regionuid_getter($deliveryinfo['d'][0]['dist']);
       
        if(!empty($showCity['regionUid']) )
            $showDistOption=CZ::model('users')->regions_getter(6,$showCity['regionUid']);

        $city=CZ::model('users')->regions_getter(5); 

        $uaccs=CZ::model('users')->uaccs_getter($xuser_uid,'uaccId');

        if( array_key_exists('gid',$_GET))  
            $goodsUid=form('gid','strip_unsafe','get');
         
        $goods=CZ::model('goods')->goods_getter($goodsUid); 
        $brands=CZ::model('goods')->brands_getter('all',$goods['brandUid']); 

        $username=form('username','strip_unsafe','post');
        $email=form('email','strip_unsafe','post');  
        $tel=form('tel','strip_unsafe','post');  
        $mobile=form('mobile','strip_unsafe','post');  
        $address=form('address','strip_unsafe','post');  
        $notation=form('notation','strip_unsafe','post');  

        include($this->layout_path(CZ::controller(),CZ::command()));    
    }        

    public function Command_GoodsOrder(){
        $xuser_uid=ME::user_uid(); 
        $goodsUid=form('gid','strip_unsafe','post');
        $username=form('username','strip_unsafe','post');
        $email=form('email','strip_unsafe','post');  
        $tel=form('tel','strip_unsafe','post');  
        $mobile=form('mobile','strip_unsafe','post');
        $city=form('city2','strip_unsafe','post');  
        $dist=form('dist2','strip_unsafe','post');
        $zip=form('zip2','strip_unsafe','post');
        $address=form('address','strip_unsafe','post');
        $notation=form('notation','strip_unsafe','post');
        $today = date("Y-m-d H:i:s") ;
        $select = 'goodsUid,goodsName,brandUid,imageUidMaster,goodsPricePoint,goodsType,goodsNumStorage,goodsNumExchange,goodsStatus,goodsTimeStart,goodsTimeClose,goodsDateLimitStart,goodsDateLimitClose';
        
        $goods=CZ::model('goods')->goods_getter($goodsUid,$select);
        $brands=CZ::model('goods')->brands_getter('all',$goods['brandUid']);
        $users = CZ::model('users')->xuser($xuser_uid,1,0,0,1);
        $errPage = false;

        //檢查好康庫存數量
        try{
            if( $goods['goodsStatus'] == 0 || $goods['goodsNumStorage'] < 1 )
                throw new Exception("好康數量不足");     

            if( strtotime($goods['goodsTimeStart']) > strtotime($today)   )    
                throw new Exception("好康尚未開始兌換");

            if( strtotime($goods['goodsTimeClose']) < strtotime($today) )
                throw new Exception("好康尚未開始兌換");   

            if( $goods['goodsDateLimitStart'] != '0000-00-00'   && strtotime($goods['goodsDateLimitStart']) > strtotime($today) )
                throw new Exception("好康兌換期限有誤"); 
             
            if( $goods['goodsDateLimitClose'] != '0000-00-00' && strtotime($goods['goodsDateLimitClose']) < strtotime($today) )        
                throw new Exception("好康兌換期限有誤");   

            //檢查會員點數 upoint_logs

            $userPoint=CZ::model('users')->upointlogs_getter($xuser_uid); 
            $totalPoint = 0 ;
            $goodsPrice = (int)$goods['goodsPricePoint'] ;
            $index = array() ;

            for($i=0;$i<count($userPoint['d']);$i++){
                $timeLimit = (strtotime($userPoint['d'][$i]['userPointTimeLimit']) >= strtotime(date("Y-m-d")) ) ;
                //篩選出剩餘點數 >0 且未過期紀錄
                if( $userPoint['d'][$i]['userPoint'] >0 && $timeLimit ){
                    $totalPoint += $userPoint['d'][$i]['userPoint'] ;    
                    $goodsPrice -= $userPoint['d'][$i]['userPoint'] ;
                    array_push($index,$i);
                    if( $goodsPrice  <= 0 )
                        break;
                }
            }    
                  
            $goodsPrice = (int)$goods['goodsPricePoint'] ;

            //檢查會員點數 upoint_logs            
            if( $this->upoint_check($goods) ){
                $unotifications=CZ::model('notifications')->notifications_add('countmin-system',$xuser_uid,'','','Ly_i_exchange_failed','');
                throw new Exception("兌換點數不足");           
            }

            //取得訂單編號
            $counterId='order'.date("Ym");   
            $counter=CZ::model('counter')->counter_getter($counterId);
            if(!is_array($counter) || !array_key_exists('counterId',$counter))
                throw new Exception("取得訂單編號失敗");

            $data=array('counterNumber' => $counter['counterNumber']+1) ;         
            $counterUpdate=CZ::model('counter')->counter_update($counterId,$data);
            $orderId=CZ::model('counter')->orderid_getter($counter['counterNumber']);

            //開始扣點數
            for($i=0;$i<count($index);$i++){
                $goodsPrice -= $userPoint['d'][$index[$i]]['userPoint'] ; //echo $goodsPrice."      ".
                if( $goodsPrice  >= 0  ){  //點數全扣光情況
                    $data = array( 'userPoint' => 0 ,
                                  'userPointUsed' => $userPoint['d'][$index[$i]]['userPointEarn'] ,
                                );
                    $userPointPay = $userPoint['d'][$index[$i]]['userPoint'];  
                }
                else{  //點數還有剩情況
                   $data = array( 'userPoint' => $goodsPrice * (-1) ,
                                  'userPointUsed' => $userPoint['d'][$index[$i]]['userPointEarn'] + $goodsPrice ,
                                );
                   $userPointPay = $userPoint['d'][$index[$i]]['userPointEarn'] + $goodsPrice;  
                }
              
                if(!CZ::model('users')->upointlogs_update($userPoint['d'][$index[$i]]['upointUid'],$data)) 
                    throw new Exception("兌換點數更新失敗-1");           
            }     

            //寫入upoint_pays
            $paydata=array(
                            'upointUid'=>uid4(),
                            'userUid'=>$xuser_uid,
                            'userPointPay'=>$goods['goodsPricePoint'],
                            'upointlogTimeEarn'=>date("Y-m-d H:i:s",strtotime('+8 hour')),
                            'upointTable'=>'goodsorders',
                            'upointTableId'=>$orderId,
                            'upointTableId2'=>$goods['goodsUid'],
                            'upointlogNote'=>'您兌換了 '.$goods['goodsName'].' 商品', 
                    );                                 

        if(!CZ::model('users')->upoint_pays_add($paydata) ) 
            throw new Exception("兌換點數更新失敗-2");      

        //新增實體好康兌換紀錄 goodsorders
            $goodsOrderData = array(
                                    'goodsUid'=>$goods['goodsUid'],
                                    'goodsorderId'=>$orderId,
                                    'goodsorderTime'=>date("Y-m-d H:i:s",strtotime('+8 hour') ),
                                    'userUid'=>$xuser_uid,
                                    'goodsorderPoint'=>$goods['goodsPricePoint'],
                                    'goodsorderStatus'=>0,
                                    //'goodsorderTimeDelivery'=>
                                    'goodsorderReciverName'=>$username,
                                    'goodsorderReciverEmail'=>$email,
                                    'goodsorderReciverTel'=>$tel,
                                    'goodsorderReciverCity'=>$city,
                                    'goodsorderReciverDist'=>$dist,
                                    'goodsorderReciverZip'=>$zip,  
                                    'goodsorderReciverAddress'=>$address,
                                    'goodsorderTimeLastUpdate'=>date("Y-m-d H:i:s"),
                                    'goodsorderNote'=>$notation,
                                    'goodsLogistics'=>1,
                                    'goodsOtherLogistics'=>1,
                                    'goodsLogisticsId'=>1,      
                                );

            if(!CZ::model('goods')->goods_order_new($goodsOrderData))
                throw new Exception("訂單更新失敗-1");  

            // goods  更新庫存 
            $select = 'goodsNumStorage,goodsNumExchange'; 
            $stockData = array(
                                'goodsNumStorage'=>$goods['goodsNumStorage']-1,
                                'goodsNumExchange'=>$goods['goodsNumExchange']+1,
                            );
            if(!CZ::model('goods')->goods_modify($goods['goodsUid'], $stockData ))
                throw new Exception("訂單更新失敗-2");   

            // 更新 users userPoint
      
            $userData = array(
                             'userPoint'=>$users['basic']['userPoint'] - $goods['goodsPricePoint'],
                            );

            if(!CZ::model('users')->user_update($xuser_uid, $userData ))
                throw new Exception("訂單更新失敗-3");

            //通知
            $noticeData=array('goods_name'=>$goods['goodsName']);
            $unotifications=CZ::model('notifications')->notifications_add('countmin-system',$xuser_uid,'','','Ly_i_exchange_goods',$noticeData);    
            $unotifications=CZ::model('notifications')->notifications_add($xuser_uid,$xuser_uid,'/user/orderlist','','Ly_i_exchange_completed',''); 
        }
        catch(Exception $e){
            $errPage = true;
            $unotifications=CZ::model('notifications')->notifications_add('countmin-system',$xuser_uid,'','','Ly_i_exchange_failed',''); 
            error_log( $xuser_uid.' buy '.$goods['goodsUid'].' fail , because: '.$e->getMessage() );
                        
            include($this->layout_path(CZ::controller(),CZ::command()));
            exit;
        }     
      
        include($this->layout_path(CZ::controller(),CZ::command()));
    }    

    public function Command_Regist(){
        $this->layout='regist';
        $userUid=uid4();
        $timeCreate=date("Y-m-d H:i:s",strtotime('+8 hour')); //註冊時間
        $userPointTimeLimit=date("Y",strtotime('+1 year')).'-12-31';  //點數期限
        $userPoint=0;
        $userProfile='';
        $email=form('email','email','post');
        $password=form('password','strip_unsafe','post');
        $username=form('username','strip_unsafe','post');

        $fbEncode=form('fb','strip_unsafe','post'); 
        $tokenEncode=form('token','strip_unsafe','post');
        $checkCode=form('key','strip_unsafe','post');

        if( !empty($fbencode) || !empty($tokenEncode) ){
            if( md5($this->checkKey.$tokenEncode) == $checkCode ){
                $fb=CZ::model('counter')->decrypt($fbEncode,$this->loginTokenKey);
                $access_token=CZ::model('counter')->decrypt($tokenEncode,$this->loginTokenKey);
            }
            else{
                header('Location:/index/login');
                exit;    
            }
        }

        $birth_year=form('birth_year','int','post');
        $birth_mon=form('birth_mon','int','post');
        $birth_day=form('birth_day','int','post');
        $sex=form('sex','int','post');
        $country=form('country','strip_unsafe','post');    
        $city=form('city','strip_unsafe','post');
        $urlCode=form('code','strip_unsafe','get');
        $code=form('code','strip_unsafe','post');
        $interests=form('interests','strip_unsafe','post');
        $master=form('master.','strip_unsafe','post');  

        $x=form('x1','int','post');
        $y=form('y1','int','post');
        $zoom=form('zoom','float','post') ;
 
        if( empty($birth_year) ){
            include($this->layout_path(CZ::controller(),CZ::command()));
            exit;
        }

        //檢查該email是否有註冊過       
        if( !empty($email)){ 
            $emailCheck=CZ::model('users')->uaccs_uaccId_getter($email);
            if( $emailCheck['args']['num'] > 0 ){
                echo '<script>alert("電子信箱 ：'.$email.' 已經註冊過了")</script>';
                //include($this->layout_path(CZ::controller(),CZ::command()));
                exit;
            }
        }

        //檢查邀請碼是否正確
        $invitecodes=CZ::model('users')->uinvitecodes_getter($code);
        if($invitecodes['args']['num'] > 0 ){
            $userPoint=100;
            $data=array(
                        'upointUid'=>uid4(),
                        'userUid'=>$userUid,
                        'userPointEarn'=>100,
                        'userPointUsed'=>0,
                        'userPoint'=>100,
                        'userPointTimeLimit'=>$userPointTimeLimit,
                        'upointlogTimeEarn'=>$timeCreate,
                        'upointTable'=>'invite',
                        'upointTableId'=>'',
                        'upointlogNote'=>'註冊時輸入邀請碼獲得',
                        );
         
            //註冊者加100點數
            $upointlogs=CZ::model('users')->upointlogs_add($data);
      
            //邀請碼擁有者加100點數
            $data['upointUid']=uid4();
            $data['userUid']=$invitecodes['d'][0]['userUid'];
            $data['upointTableId']=$userUid;
            $data['upointlogNote']=$username.' 註冊時輸入您的邀請碼獲得';
            $upointlogs=CZ::model('users')->upointlogs_add($data);
            CZ::model('notifications')->notifications_add('countmin-system',$xuser_uid,'','','Ly_regist_invite_friends_completed',''); 
            
            //更新users userPoint
            $userpoint=CZ::model('users')->user_point_update($invitecodes['d'][0]['userUid'],100);

        }

        //推薦達人加入追蹤 relations
        for($i=0;$i<count($master);$i++){
            $data=array(
                        'userUid1'=> $userUid,
                        'userUid2'=> $master[$i], 
                        'relation'=> 16, 
                        'relationTime'=>$timeCreate , 
                        );
            $relations=CZ::model('users')->relations_add($data);
        }

        //大頭貼
        if( array_key_exists('photo',$_FILES) &&  $_FILES['photo']['error'] == 0){ 
            $images=CZ::model('images')->imageUpload($_FILES,$userUid,'profile',$zoom,$x,$y,$this->profile_heigh,$this->profile_width );
            $data=array(                                                               
                        'userUid'=>$userUid,
                        'imageUid'=>$images['photo']['filename'],
                        'userphotoUsed'=>1,
                        'userphotoTimeUpload'=>_SYS_DATETIME,
                       );
            $photoadd=CZ::model('users')->userphotoes_add($data);
            $userProfile=$images['photo']['filename'];
            error_log( $userProfile );
        }


        //deliveryinfo table   
        for($i=0;$i<3;$i++){
            $data=array(
                        'userUid'=>$userUid,
                        'groupId'=>$i,
                        'groupName'=>'群組'.$i,
                        'name'=>'',
                        'email'=>'',
                        'tel'=>'',
                        'mobile'=>'',
                        'regionUid'=>'',
                        'city'=>'',
                        'dist'=>'',
                        'address'=>'',
                        'note'=>'', 
                   );   
                $deliveryinfo=CZ::model('goods')->deliveryinfo_add($data);
            }

        //notificationsetting table
        $data=array(
                    'userUid'=>$userUid,    
                    'likes'=>0,
                    'dislikes'=>0,
                    'comments'=>0,
                    'afterComments'=>0,
                    'shares'=>0,
                    'favors'=>0,
                    'abuses'=>0,
                    'wallposts'=>0,
                    'friend'=>0,
                    'follow'=>0,
                    'memberPaper'=>1,
                    'activityPaper'=>1,
                    'newFunction'=>1,
                    'recommendActivity'=>1,
                    'recommendInfo'=>1,                    
                );
        $notificationsetting=CZ::model('notifications')->notificationSetting_add($data);

        //privacy table
        $data=array(
                    'userUid'=>$userUid,
                    'myPost'=>1,
                    'findMe'=>2,
                    'location'=>0,
                    'sendMsg'=>1,
                );                    
        $privacy=CZ::model('users')->privacy_add($data);

        //uaccs table
        $uaccUid=uid4();
        if( !empty($fb))
            $uaccAuth='facebook';
        else
            $uaccAuth='email';

        $data=array(
                    'uaccUid'=>$uaccUid,
                    'userUid'=>$userUid,
                    'uaccAuth'=>$uaccAuth,
                    'uaccId'=>$email,
                    'uaccRegistStep'=>0,
                    );                        

        //facebook註冊
        if( !empty($fb))
            $data['uaccId']=$fb; 

        $uaccs=CZ::model('users')->uaccs_add($data);

        //uacctokens table
        $uaccToken=md5('$6G.'.md5($password));    

        //facebook註冊
        if( !empty($fb))
            $uaccToken=$access_token;

        $data=array(
                    'uaccUid'=>$uaccUid,
                    'uaccToken'=>$uaccToken,
                    );
        $uacctokens=CZ::model('users')->uacctokens_add($data);

        //uinvitecodes 
        //產生邀請碼
        $counter=CZ::model('counter')->counter_getter('invitation');            
        if(!is_array($counter) || !array_key_exists('counterId',$counter)){
                // throw new Exception("取得編號失敗");
                //TODO errlog
        }

        $data=array('counterNumber' => $counter['counterNumber']+1) ;         
        $counterUpdate=CZ::model('counter')->counter_update('goodsorder',$data);
        $invitationCode=CZ::model('counter')->invitation_code_getter($counter['counterNumber']);

        $data=array(
                    'userInviteCode'=>$invitationCode,
                    'userUid'=>$userUid,
                    'uinvitecodeTimeCreate'=>$timeCreate,
                    );                        
        $uinvitecodes=CZ::model('users')->uinvitecodes_add($data);

        //uparameters
        $data=array(
                    'userUid'=>$userUid,
                    'uparameterNumFriends'=>0,
                    'uparameterNumFollows'=>0, 
                    'uparameterNumFollowers'=>0, 
                    'uparameterNumPosts'=>0, 
                    'uparameterNumEvents'=>0, 
                    'uparameterNumFavors'=>0, 
                    'uparameterNumLikes'=>0, 
                    'uparameterNumDislikes'=>0, 
                    'uparameterNumShares'=>0, 
                    'uparameterNumJoins'=>0, 
                    'uparameterNumAskFriends'=>0, 
                    'uparameterNumNeedFriends'=>0, 
                    );    
        $uparameters=CZ::model('users')->uparameters_add($data);

        //uprolevels
        $allInterest=CZ::model('users')->interest_getter();
        $interestArray=explode(";",$interests);
        for($i=0;$i<$allInterest['args']['num'];$i++){
            $data=array(
                        'userUid'=>$userUid,
                        'interestId'=>$allInterest['d'][$i]['interestId'],
                        'userGrade'=>0,
                        'userProLevel'=>1,
                        );
            for($j=0;$j<count($interestArray);$j++){
                if( $interestArray[$j] == $allInterest['d'][$i]['interestId'] ){
                    $data['userGrade']=200;
                    $data['userProLevel']=2;
                }
            }    

            $uprolevels=CZ::model('users')->uprolevels_add($data);
        }

        //userdetails
        $data=array(
                    'userUid'=>$userUid,
                    'userAstrology'=>0,
                    'userBlood'=>0,
                    'userAptitude'=>0,
                    'userRelationship'=>0,
                    'userContactEmail'=>'',
                    'userContactPhone'=>'',
                    'userShowAstrology'=>1,
                    'userShowBlood'=>1,
                    'userShowAptitude'=>1,
                    'userShowRelationship'=>1,
                    'userShowContactEmail'=>1,
                    'userShowContactPhone'=>1,
                    );
        $userdetails=CZ::model('users')->userdetails_add($data);

        //users

        //避免圖形上傳失敗無法insert into
        if( is_null($userProfile) )
            $userProfile='';

        $data=array(
                    'userUid'=>$userUid,
                    'userId'=>NULL,
                    'userRealname'=>$username,
                    'userAliasname'=>'',
                    'userGender'=>$sex,
                    'userProfile'=>$userProfile,
                    'userTopbanner'=>'',
                    'userTimeRegist'=>$timeCreate,
                    'userType'=>0,
                    'userEnable'=>1,
                    'userPoint'=>$userPoint,
                    'userLevel'=>1,
                    'userBirthYear'=>$birth_year,
                    'userBirthMonth'=>$birth_mon,
                    'userBirthDay'=>$birth_day,
                    'userShowBirthYear'=>1,
                    'userShowBirthMonthDay'=>1,    
                    'userShowGender'=>1,
                    'userTimezone'=>480,
                    'userInviteCode'=>$invitationCode,
                    'userInterests'=>$interests,
                    'langIdInterface'=>'tw',
                    );
        $users=CZ::model('users')->users_add($data);

        //寄認證信 TODO 測試暫用
        if(!empty($email)){
            $authId=CZ::model('counter')->encrypt($userUid.';'.$email.';'.md5($userUid.$this->emailAuthKey),$this->emailAuthKey);    
            error_log($authId);
            $subtitle='參一腳countmin.com 加入會員認證信';
            $content='            
                        親愛的【'.$username.'】您好：<p>
                            
                        完成認證後，立即獲得 參一腳點數300點<p>

                        非常感謝您加入 參一腳平台！ 我們想要確認您所輸入的 email 地址是正確的。<p>
                        請 點擊這裡 或下方的連結來認證您的帳號：<p>
                        http://www.countmin.com/index/registauth?authid='.$authId.' <p>

                        認證帳號後，您將可以開始在參一腳上發表動態與活動並參與更多有趣的事物，<p>
                        平台依據不同興趣類別成就不同領域的達人，自媒體的時代已來臨，下一個達人就是你。<p>
                        同時也會開通即時的提醒，讓您不會錯過的您參與或互動的任何訊息！<p>

                         -- Team 參一腳Countmin<p>
                         P.S.如果您沒有在 參一腳Countmin 上註冊，請忽略此郵件．<p>
                    ';

            $subtitle2='成為參一腳會員通知信';    
            $content2='
                        親愛的【會員暱稱】您好：<p>

                        感謝您成為參一腳社群平台的會員，<p>
                        未來您可以使用下列資料登入<p>
                        帳號：'.$email.' <p>
                        （為確保您的個人資料安全，本信件將不顯示您的登入密碼。）<p>

                        您可點選此處查看會員條款，進一步了解本平台會員服務細節，<p>
                        或至參一腳平台即刻體驗。若有任何疑問，歡迎您與我們的客服連繫。<p>
                        參一腳社群平台全體同仁將竭誠為您服務。<p>

                        敬祝您：使用愉快！ <p>
                        現在就前往參一腳，享樂分享生活大小事~<p>

                        參一腳社群平台 敬啟<p>

                        ■此信件是由系統自動發信，請勿直接回覆，謝謝。<p>
                    ';
            
            $subtitle3='恭喜你，已完成註冊，可開始使用參一腳囉!';
            $content3='
                        目前資料完整度為30%，<p>
                        
                        填寫詳細個人資料可讓人更快認識你<p>

                        還可獲得參一腳平台的獎勵點數500點唷!!!<p>

                        快來填寫吧!!<p>

                        立即前往  http://www.countmin.com/user/about   <p>

                        之後再填寫 http://www.countmin.com/index/explore <p>
                    ';                        

            $sendAuthEmail=CZ::model('mail')->send_mail($email,$subtitle,$content);
            $sendAuthEmail=CZ::model('mail')->send_mail($email,$subtitle2,$content2);     
            $sendAuthEmail=CZ::model('mail')->send_mail($email,$subtitle3,$content3);
        }

        //unotifications
        CZ::model('notifications')->notifications_add('countmin-system',$userUid,'','','Ly_regist_not_completed',array('email'=>$email));
        //CZ::model('notifications')->notifications_add('countmin-system',$userUid,'','','Ly_regist_not_completed',array('email'=>$email));

        //login
        if( !empty($fb))
            $cuser=CZ::model('users')->facebook_pwd_check($fb,$access_token);
        else
            $cuser=CZ::model('users')->uid_pwd_check($email,$password);


        if(!$cuser){
            header('Location:/index/login');
            exit;
        }

        ME::become($cuser['userUid'],$cuser['userRealname'],1,$cuser['userType']==9?1:0);
        CZ::page('login_then_redirect');

    }

    public function Command_forget(){
        $email=form('email','email','post'); 
        $uaccs=array();

        if( !empty($email) )
            $uaccs=CZ::model('users')->uaccs_uaccId_getter($email);

        //檢查email是否存在
        if( array_key_exists('args',$uaccs) && $uaccs['args']['num'] > 0){
            $str=uid4().';'.$email.';'.date("Y-m-d H:i:s");
            $authId = CZ::model('counter')->encrypt($str, $this->key); 
            $checkCode=md5($this->checkKey.$authId); 
            $authId = $checkCode.$authId;           

            $subtitle='參一腳忘記密碼申請信';
            $content='親愛的會員您好：<p>
                      請於6小時內<p>
                      點選信件中的連結重設密碼<p>
                      http://www.countmin.com/index/resetpassword?id='.$authId
                     ;
            $sendAuthEmail=CZ::model('mail')->send_mail($email,$subtitle,$content);
        }

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_resetpassword(){
        $authId=form('id','strip_unsafe','get');                
        $email=form('email','email','post');
        $newpassword=form('password','strip_unsafe','post');
        $passwordUpdate=false;

        if(!empty($authId)){
            //驗證碼
            $verifyCode=substr($authId,0,32);
            $encode=substr($authId,32,strlen($authId)-32); 
            $decode=explode(";",CZ::model('counter')->decrypt($encode, $this->key));    

            if(count($decode) != 3){
                header('Location:/index/login'); 
                exit;   
            }
 
            //6小時內檢查
            $timeCkeck = ( strtotime(date("Y-m-d H:i:s")) - strtotime($decode[2]) < 21600 );            
            //驗證碼檢查
            $verifyCheck = (md5($this->checkKey.$encode) == $verifyCode) ;
        }

        if( $email == $decode[1] && $verifyCheck && $timeCkeck ){
            //重設密碼
            $uaccs=CZ::model('users')->uaccs_uaccId_getter($email,'uaccUid');

            if( $uaccs['args']['num'] ==0)
                return ;

            $uaccToken=md5('$6G.'.md5($newpassword));
            $data=array(
                        'uaccToken'=>$uaccToken,
                        'uacctokenTimeLastUpdate'=>date("Y-m-d H:i:s",strtotime('+8 hour') ) 
                  );
            $passwordUpdate=CZ::model('users')->uacctokens_update($uaccs['d'][0]['uaccUid'],$data);    

        }
                
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    //邀請碼確認是否正確
    public function Command_codecheck(){
        //ajax used
        $code=form('code','strip_unsafe','post');
        $check=CZ::model('users')->uinvitecodes_getter($code);
        if( $check['args']['num'] > 0)
            echo true ;
        else
            echo false;
    }

    public function Command_registpeople(){
        //ajax used
        //依註冊選的興趣撈出推薦達人
        $interests=form('interests','strip_unsafe','post');
        $users=CZ::model('users')->usermasters_getter($interests); 
        for($i=0;$i<$users['args']['num'];$i++) //取得user的興趣標籤
            $upro[$i] = CZ::model('users')->uprolevels_getter($users['d'][$i]['userUid'],1); 
         include($this->grid_path($this->name,'masterpeople'));
    }
    
    public function Command_addfavors(){
        $userUid=ME::user_uid();
        $wallpostUid=form('gid','strip_unsafe','post'); 
        $wallpostType=form('type','int','post');
        $favorsCheck=CZ::model('ufavors')->ufavors_check($userUid,$wallpostUid,$wallpostType);
        $favors=false;

        if( empty($userUid) || empty($wallpostType) || empty($wallpostType) )
            return false;

        if( $favorsCheck['count(*)'] == 0){
            //新增
            $data=array(
                        'userUid'=>$userUid,
                        'wallpostUid'=>$wallpostUid,
                        'wallpostType'=>$wallpostType,
            );
            $favors=CZ::model('ufavors')->ufavors_add($data);    
        }
        else{
            //刪除
            $favors=CZ::model('ufavors')->ufavors_delete($userUid,$wallpostUid,$wallpostType); 

        }

        $favorsCount=CZ::model('ufavors')->ufavors_getter_count($userUid,$wallpostType);

        if($favors) 
            echo $favorsCount['d'][0]['count(*)']  ; 
    }

    public function Command_emailCheck(){
        //ajax used
        $email=form('email','email','post');
        if( empty($email) ){
            echo false;
            exit;
        }
    
        $check=CZ::model('users')->uaccs_uaccId_getter($email);

        if($check['args']['num'] > 0) 
            echo true ; 
        else
            echo false ;
    }

    public function Command_saveInfo(){
        $user_uid=ME::user_uid();    
        $data=array(
                    'name'=>form('username','strip_unsafe','post'),
                    'email'=>form('email','strip_unsafe','post'),
                    'tel'=>form('tel','strip_unsafe','post'),
                    'regionUid'=>form('zip','strip_unsafe','post'),    
                    'mobile'=>form('mobile','strip_unsafe','post'),
                    'city'=>form('city','strip_unsafe','post'),
                    'dist'=>form('dist','strip_unsafe','post'),
                    'address'=>form('address','strip_unsafe','post'),
                    'note'=>form('notation','strip_unsafe','post'),
                   );

        $deliveryinfo=CZ::model('goods')->deliveryinfo_update($user_uid,0,$data);

        if($deliveryinfo)
            echo '新增收件人資料成功';
        else
            echo '新增收件人資料失敗';
    
    }

    private function upoint_check($goods){
        //檢查會員點數 upoint_logs
        $userPoint=CZ::model('users')->upointlogs_getter(ME::user_uid());
        $totalPoint = 0 ;
        $goodsPrice = (int)$goods['goodsPricePoint'] ;
        $index = array() ;
                                    
        for($i=0;$i<count($userPoint['d']);$i++){
            $timeLimit = (strtotime($userPoint['d'][$i]['userPointTimeLimit']) >= strtotime(date("Y-m-d")) ) ;
            //篩選出剩餘點數 >0 且未過期紀錄
                if( $userPoint['d'][$i]['userPoint'] >0 && $timeLimit ){
                    $totalPoint += $userPoint['d'][$i]['userPoint'] ;    
                    $goodsPrice -= $userPoint['d'][$i]['userPoint'] ;
                    array_push($index,$i);
                    if( $goodsPrice  <= 0 )
                        break;
                }
        }    
      
        $goodsPrice = (int)$goods['goodsPricePoint'] ;
                 
        return ( $totalPoint < $goodsPrice );
    }

    public function Grid_Header($args){
        $user_uid=ME::user_uid();
        $notifyMsg=CZ::model('notifications')->list_getter($user_uid);
        for($i=0;$i<$notifyMsg['args']['num'];$i++){
            if($notifyMsg['d'][$i]['userUidFrom'] != 'countmin-system' ){
                $xuser=CZ::model('users')->xuser($notifyMsg['d'][$i]['userUidFrom']);
                $notifyMsg['d'][$i]['userRealname'] = $xuser['basic']['userRealname']; 
                $notifyMsg['d'][$i]['userProfile']=$xuser['basic']['userProfile']; 
            }
        }
        
        include($this->grid_path($this->name,'header'));
    }

    public function Grid_MasterPeople(){
        include($this->grid_path($this->name,'masterpeople'));
    }

}
