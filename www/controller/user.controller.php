<?php
class User_Controller extends Controller {

	public function __construct(){
		$this->name='user';
		$this->layout='profile';
        $this->profile_heigh=180;    //大頭貼照片寬度,需與前端縮放框相符
        $this->profile_width=180;    //大頭貼照片高度
        $this->topbanner_heigh=250;  //封面照片高度
        $this->topbanner_heigh=1625; //封面照片寬度
		/**
		 * name => grid method
		 */
		$this->layout_grids=array(
			'user-basic'=>array('grid'=>'UserBasic'),
			'user-menu'=>array('grid'=>'UserMenu'),
			'topbanner'=>array('grid'=>'Topbanner'),
			'user-points'=>array('grid'=>'UserPoints'),
			'postrecordmenu'=>array('grid'=>'PostRecordmenu'),
			'interestpeople'=>array('grid'=>'interestPeople'),
			'hotwrite'=>array('grid'=>'hotWrite'),
		);
		CZ::grid_setter('user-basic',TRUE);
		CZ::grid_setter('page-header',TRUE);
		CZ::grid_setter('topbanner',TRUE);
		CZ::grid_setter('user-menu',TRUE);
		CZ::grid_setter('user-points',TRUE);
		CZ::grid_setter('postrecordmenu',TRUE);
		CZ::grid_setter('interestpeople',TRUE);
		CZ::grid_setter('hotwrite',TRUE);
	}
		
	/**
	 * wallpost_over 
	 * 在興趣牆上加入任何活動
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_wallpost_over(){
		$wallpost_uid=form('wuid','string','get');
		$key=form('key.','string','get');//自動結算 key
		$ntime=strtotime(_SYS_DATETIME)+_TIMEZONE*60;
		$ndate=date("Y-m-d H:i:s",$ntime);
		$power_run=0;
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);
		if(!empty($wallpost_data)){
			$this_power_key=md5(md5($wallpost_data['wallpostUid']).md5($wallpost_data['userUid']));
			$power_run=($this_power_key==$key) ? 1 : 0; ///上面改用get就是可以讓系統傳網址自動結算
			
			/////手動結算限制
			if($power_run==0){
				if(!ME::is_login()){die();}
				$user_uid=ME::user_uid();
				$json_data['error']=1;
				$errMsg='';
				if(empty($errMsg)){
					if($user_uid!=$wallpost_data['userUid']){$errMsg='你是誰!?(害怕)(抖~~)';}
				}
				if(empty($errMsg)){
					if($wallpost_data['wallpostType']>1){
						if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
							$esdate=$wallpost_data['wallpost_extend']['missionTimeStart'];
							$eedate=$wallpost_data['wallpost_extend']['missionTimeClose'];
							
							$game_rule=$wallpost_data['wallpost_extend'];
							$game_rule['wallpostType']=$wallpost_data['wallpostType'];
							
						}
						if($wallpost_data['wallpostType']==4){
							$esdate=$wallpost_data['wallpost_extend']['eventTimeStart'];
							$eedate=$wallpost_data['wallpost_extend']['eventTimeEnd'];
						}
					}
					else{$errMsg='未知的活動!?';}
				}
				if(empty($errMsg)){
					if($ntime < strtotime($esdate) ){$errMsg='活動尚未開始';}
				}
				if(empty($errMsg)){
					if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
						if($wallpost_data['wallpost_extend']['missionAward']==0){$errMsg='此活動無獎勵';}
					}
				}
				if(empty($errMsg)){
					if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
						if($wallpost_data['wallpost_extend']['missionOver']!=0){$errMsg='此活動已結算';}
					}
				}
				if(empty($errMsg)){
					if($wallpost_data['wallpostType']==4){
						if($wallpost_data['wallpost_extend']['eventOver']!=0){$errMsg='此活動已結算';}
					}
				}
				////有開始就好
				//if(empty($errMsg)){
				//	if(strtotime(_SYS_DATETIME) > strtotime($eedate) ){$errMsg='活動已結束';}
				//}
				///////其他的限制等企劃開來需求再增加
				if(empty($errMsg)){
					$power_run=1;
				}
			}
			////結算
			if($power_run==1){
				////無論手動或自動
				////至少給我1個人結算也好...否則也跑不出來
				if(empty($errMsg)){
					if($wallpost_data['wallpost_extend']['joinNumPerson']<=0 ){$errMsg='無法結算，至少要1人參加活動';}
				}
				if(empty($errMsg)){
					$json_data['error']=0;
					$json_data['msg']='結算完成(無結果)';
					if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
						$active='mission';
					}	
					else{$active='event';}
					$people=CZ::model('wallposts')->user_wallpost_eventover($wallpost_uid,$active,$game_rule);
					
					//////有跑出得獎名單,活動結束
					if($people['args']['num']>0){
						
						$json_data['msg']='結算完成(已結算得獎名單及贈點)';
						
						/*
						////完成活動的給10點
						$give_point=10;
						$note='恭喜你完成['.html($wallpost_data['wallpostTopic'],false).']活動，點數增加'.$give_point.'點';
						foreach($people['d'] as $pinx => $data){
							$edate=date('Y-12-31 23:59:59',strtotime(_SYS_DATETIME.' +1 year '));
							$data=array(
									'upointUid'	=> uid4(),
									'userUid' => $data['userUid'],
									'userPointEarn' => $give_point,
									'userPointUsed' => 0,
									'userPoint' => $give_point,
									'userPointTimeLimit' => $edate,
									'upointlogTimeEarn' => _SYS_DATETIME,
									'upointTable' => 'wallposts',
									'upointTableId' => $wallpost_uid,
									'upointlogNote' => $note,
							);
							
							CZ::model('users')->upointlogs_add($data);
							///使用者點數紀錄
							$record_data=array(
								'userPoint' => array('userPoint+'.$give_point),
							);
							CZ::model('users')->user_update($data['userUid'],$record_data);
						}
						*/
						
						////有送點活動直接給予獎勵
						if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
							if($wallpost_data['wallpost_extend']['missionAward']==1){
								$give_point=$wallpost_data['wallpost_extend']['missionAwardPoint'];
								$note='恭喜你在['.html($wallpost_data['wallpostTopic'],false).']中獲得'.$give_point.'點活動獎勵';
								foreach($people['d'] as $pinx => $data){
									$edate=date('Y-12-31 23:59:59',strtotime(_SYS_DATETIME.' +1 year '));
									$data=array(
											'upointUid'	=> uid4(),
											'userUid' => $data['userUid'],
											'userPointEarn' => $give_point,
											'userPointUsed' => 0,
											'userPoint' => $give_point,
											'userPointTimeLimit' => $edate,
											'upointlogTimeEarn' => _SYS_DATETIME,
											'upointTable' => 'wallposts',
											'upointTableId' => $wallpost_uid,
											'upointlogNote' => $note,
										);
									
									CZ::model('users')->upointlogs_add($data);
									
									
									///使用者點數紀錄
									$record_data=array(
										'userPoint' => array('userPoint+'.$give_point),
									);
									CZ::model('users')->user_update($data['userUid'],$record_data);
									
								}
							}
						}
						//////活動結束
						if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
							$over_data=array(
								'missionOver' =>	2,
							);
							DB::update('common','missions',$over_data,array('WHERE wallpostUid=:wallpost_uid',array(':wallpost_uid'=> $wallpost_uid)));
						}
						else if($wallpost_data['wallpostType']==4){
							$over_data=array(
								'eventOver' =>	2,
							);
							DB::update('common','events',$over_data,array('WHERE wallpostUid=:wallpost_uid',array(':wallpost_uid'=> $wallpost_uid)));
						}
					}
					
				}
				
			}
		}
		else{
			$errMsg='未知的活動!?';
		}
		if(!empty($errMsg)){
			$json_data['msg']=$errMsg;
		}
		
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}
/**
	 * Command_wallpost_join_list 
	 * 在興趣牆上加入任何活動的名單
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_wallpost_join_list(){
		$wallpost_uid=form('wuid','string','post');
		$type=form('type.','string','post');
		$ntime=strtotime(_SYS_DATETIME)+_TIMEZONE*60;
		$ndate=date("Y-m-d H:i:s",$ntime);
		
		if(!ME::is_login()){die();}
		$user_uid=ME::user_uid();
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);
		$json_data['error']=1;
		$errMsg='';
		if(!empty($wallpost_data)){//文章是否存在
			
			if(empty($errMsg)){
				if($wallpost_data['wallpostType']>1){
					if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
						$esdate=$wallpost_data['wallpost_extend']['missionTimeStart'];
						$eedate=$wallpost_data['wallpost_extend']['missionTimeClose'];
					}
					if($wallpost_data['wallpostType']==4){
						$esdate=$wallpost_data['wallpost_extend']['eventTimeStart'];
						$eedate=$wallpost_data['wallpost_extend']['eventTimeEnd'];
					}
				}
				else{$errMsg='未知的活動!?';}
			}
			if(empty($errMsg)){
				if($ntime < strtotime($esdate) ){$errMsg='活動尚未開始 '.$ndate;}
			}
			/*if(empty($errMsg)){
				if($ntime > strtotime($eedate) ){$errMsg='活動已結束';}
			}*/
			if(empty($errMsg)){
				$json_data['error']=0;
				$order='';
				if($type=='win'){$order='reward';}
				
				$list=CZ::model('wallposts')->user_wallpost_join_list($wallpost_uid,$order);
				
				if($list['args']['num']>0){
					foreach($list['d'] as $inx => $data){
						$xuser=CZ::model('users')->xuser($data['userUid'],1,1,1);
						$list['d'][$inx]['xuser']=$xuser;
					}
				}
				$json_data['data']=$list;
			}
		}
		else{
			$errMsg='未知的活動!?';
		}
		if(!empty($errMsg)){
			$json_data['msg']=$errMsg;
		}
		
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}	
	
	
	/**
	 * Command_wallpost_invite 
	 * 在興趣牆上加入任何活動
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_wallpost_invite(){
		$wallpost_uid=form('wuid','string','post');
		$invite_user=form('invite_user.','string','post');
		$ntime=strtotime(_SYS_DATETIME)+_TIMEZONE*60;
		$ndate=date("Y-m-d H:i:s",$ntime);
		
		if(!ME::is_login()){die();}
		$user_uid=ME::user_uid();
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);
		$json_data['error']=1;
		$errMsg='';
		if(!empty($wallpost_data)){//文章是否存在
			
			if(empty($errMsg)){
				if($wallpost_data['wallpostType']>1){
					if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
						$esdate=$wallpost_data['wallpost_extend']['missionTimeStart'];
						$eedate=$wallpost_data['wallpost_extend']['missionTimeClose'];
					}
					if($wallpost_data['wallpostType']==4){
						$esdate=$wallpost_data['wallpost_extend']['eventTimeStart'];
						$eedate=$wallpost_data['wallpost_extend']['eventTimeEnd'];
					}
				}
				else{$errMsg='未知的活動!?';}
			}
			if(empty($errMsg)){
				if(empty($invite_user)){$errMsg='無邀請對象';}
			}
			if(empty($errMsg)){
				if($ntime < strtotime($esdate) ){$errMsg='活動尚未開始 '.$ndate;}
			}
			if(empty($errMsg)){
				if($ntime > strtotime($eedate) ){$errMsg='活動已結束';}
			}
			if(empty($errMsg)){
				$json_data['error']=0;
				if(!empty($invite_user)){
					foreach($invite_user as $user2){
						$userUid1=ME::user_uid();	
						$userUid2=$user2;
						$xuser1=CZ::model('users')->xuser($userUid1,1,1,1);
						$xuser2=CZ::model('users')->xuser($userUid2,1,1,1);
						
						///增加邀請名單
						CZ::model('users')->user_inviting_add($userUid1,$userUid2,$wallpost_data['wallpostUid'],'wallposts');
						
						
						///通知
						$from_uid=$userUid1;
						$to_uid=$userUid2;
						$target_wuid=$wallpost_data['wallpostUid'];
						$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
						$notice_id='Ly_other_invite_me';
						$notice_data=array(
								'user_name' => $xuser1['basic']['userRealname'],
								'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
						);
						$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
						if($notificationSetting['recommendActivity']==1){
									$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
						}
					}
				}
			}
		}
		else{
			$errMsg='未知的活動!?';
		}
		if(!empty($errMsg)){
			$json_data['msg']=$errMsg;
		}
		
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}

	/**
	 * Command_Wallpost_join 
	 * 在興趣牆上加入任何活動
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Wallpost_join(){
		$wallpost_uid=form('wuid','string','post');
		$ntime=strtotime(_SYS_DATETIME)+_TIMEZONE*60;
		$ndate=date("Y-m-d H:i:s",$ntime);
		if(!ME::is_login()){die();}
		$user_uid=ME::user_uid();
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);
		$json_data['error']=1;
		$errMsg='';
		if(!empty($wallpost_data)){//文章是否存在
			
			if(empty($errMsg)){
				if($wallpost_data['wallpostType']>1){
					if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
						$esdate=$wallpost_data['wallpost_extend']['missionTimeStart'];
						$eedate=$wallpost_data['wallpost_extend']['missionTimeClose'];
					}
					if($wallpost_data['wallpostType']==4){
						$esdate=$wallpost_data['wallpost_extend']['eventTimeStart'];
						$eedate=$wallpost_data['wallpost_extend']['eventTimeEnd'];
					}
				}
				else{$errMsg='未知的活動!?';}
			}
			if(empty($errMsg)){
				if($ntime < strtotime($esdate) ){$errMsg='活動尚未開始 '.$ndate;}
			}
			if(empty($errMsg)){
				if($ntime > strtotime($eedate) ){$errMsg='活動已結束';}
			}
			/*  ////不知道是抽獎名額限定人數!? 
			if(empty($errMsg)){
				if($wallpost_data['wallpostType']==2 || $wallpost_data['wallpostType']==3){
					if($wallpost_data['wallpost_extend']['joinNumPerson']>=$wallpost_data['wallpost_extend']['missionNumPerson']){
						$errMsg='參加人數已額滿';
					}
				}
				if($wallpost_data['wallpostType']==4){
					if($wallpost_data['wallpost_extend']['joinNumPerson']>=$wallpost_data['wallpost_extend']['eventNumPerson']){
						$errMsg='參加人數已額滿';
					}
				}
			}*/
			if(empty($errMsg)){
				
				
				$join_data=CZ::model('wallposts')->user_wallpost_join_get($user_uid,$wallpost_uid);
				if(empty($join_data)){
					$json_data['error']=0;
					CZ::model('wallposts')->user_wallpost_join($user_uid,$wallpost_uid);

					$userUid1=ME::user_uid();	
					$userUid2=$wallpost_data['userUid'];
					$xuser1=CZ::model('users')->xuser($userUid1,1,1,1);
					$xuser2=CZ::model('users')->xuser($userUid2,1,1,1);
					
					
					$give_point=true;////是否給點!?  上面已經防再次參加
					if($give_point){
						///贈與點數與pro分
						///這邊先擋任何自己洗自己動作
						///$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
						$action_type=5;
						$user1_note='你參加了'.$wallpost_data['wallpostTopic'];
						$user2_note=$xuser1['basic']['userRealname'].'已經參加你的'.$wallpost_data['wallpostTopic'];
						if($userUid1!=$userUid2){
							$usettle=CZ::model('users')->user_point_settle($userUid2,$wallpost_data['wallpostType'],$action_type,$wallpost_data['wallpostInterests']);
							
							if(!empty($usettle['participants'])){
								CZ::model('users')->userwallpost_2p_add($userUid1,$usettle['participants'],$wallpost_data['wallpostUid'],$user1_note);
								
							}
							if(!empty($usettle['poster'])){
								CZ::model('users')->userwallpost_2p_add($userUid2,$usettle['poster'],$wallpost_data['wallpostUid'],$user2_note);
							}
						}
					}
					
						
					///通知 userUid1
					if($userUid1!=$userUid2){
						$from_uid='countmin-system';
						$to_uid=$userUid1;
						$target_wuid=$wallpost_data['wallpostUid'];
						$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
						$notice_id='Ly_i_join';
						$notice_data=array(
								'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
						);
						$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
						if($notificationSetting['recommendActivity']==1){
									$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
						}
					}
					
					///通知 userUid2
					if($userUid1!=$userUid2){
						$from_uid=$userUid1;
						$to_uid=$userUid2;
						$target_wuid=$wallpost_data['wallpostUid'];
						$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
						$notice_id='Ly_other_join_my_event';
						$notice_data=array(
								'user_name' => $xuser1['basic']['userRealname'],
								'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
						);
						$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
						if($notificationSetting['recommendActivity']==1){
									$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
						}
					}
					
			
					
					
				}
				else{
					$errMsg='你已經參與過此活動!?';
				}
			}
		}
		else{
			$errMsg='未知的活動!?';
		}
		if(!empty($errMsg)){
			$json_data['msg']=$errMsg;
		}
		
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}
	
	
	 /**
	 * Command_relation_build 
	 * 建立關係
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_relation_build(){
		$user_uid1=ME::user_uid();
		$user_uid2=form('user_uid2','string','get');
		$action=form('action','string','get');	//interest,follow,not_follow,inviting,not_inviting,not_friend,agree_friend
		
		$xuser1=CZ::model('users')->xuser($user_uid1,1,1,1);
		$xuser2=CZ::model('users')->xuser($user_uid2,1,1,1);
		if($xuser1==$xuser2){die();}
		if(empty($xuser1) || empty($xuser2)){die();}
		
		$relation=CZ::model('users')->relation_build($user_uid1,$user_uid2,$action);
		
		$json_data['error']=0;
		$json_data['data']=$relation;
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
		
		
		
	}
	
	
	
	/**
	 * Command_comment_save 
	 * 在興趣牆上留言
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_comment_save(){
		$wallpost_uid=form('wuid','string','post');
		$comment=form('comment','string','post');
		if(!ME::is_login()){die();}
		
		$user_uid=ME::user_uid();
		$json_data['error']=1;
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);
		
		if(!empty($wallpost_data)){//文章是否存在文章是否存在
		
			$userUid1=ME::user_uid();	
			$userUid2=$wallpost_data['userUid'];
			$xuser1=CZ::model('users')->xuser($userUid1,1,1,1);
			$xuser2=CZ::model('users')->xuser($userUid2,1,1,1);
			$comment=CZ::model('wallposts')->comment_add($user_uid,$wallpost_uid,$wallpost_data['userUid'],$comment);
			$json_data['error']=0;
			$json_data['data']=$comment;
			
			
			///通知
			if($userUid1!=$userUid2){
				$from_uid=$userUid1;
				$to_uid=$userUid2;
				$target_wuid=$wallpost_data['wallpostUid'];
				$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
				$notice_id='Ly_reply_comment_by_other';
				$notice_data=array(
						'user_name' => $xuser1['basic']['userRealname'],
						'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
				);
				$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
				if($notificationSetting['comments']==0 || $notificationSetting['comments']==2){
							$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
				}
			}
			
			
			/////偵測使用這是否完成活動並給點數
			CZ::model('wallposts')->give_user_event_join(ME::user_uid(),$wallpost_uid);
		}
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}
	
	/**
	 * Command_wallpost_share 
	 * 在興趣牆上留言
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Wallpost_share(){
		$wallpost_uid=form('wuid','string','post');
		$msg=form('msg','strip_tag','post');
		$public=form('public','integer','post');
		
		if(!ME::is_login()){die();}
		
		$user_uid=ME::user_uid();
		$json_data['error']=1;
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);
		
		if(!empty($wallpost_data)){//文章是否存在文章是否存在
		
			if($wallpost_data['wallpostUidOriginal']){
				$wallpost_share_data=$wallpost_data['wallpost_share'];
			}
			$wallpost_real_data=empty($wallpost_share_data) ?  $wallpost_data: $wallpost_share_data;
		
		
			$userUid1=ME::user_uid();	
			$userUid2=$wallpost_real_data['userUid'];
			$xuser1=CZ::model('users')->xuser($userUid1,1,1,1);
			$xuser2=CZ::model('users')->xuser($userUid2,1,1,1);
			
			$msg=nl2br($msg);
			
			
			$share_wallpost_uid=CZ::model('wallposts')->user_share($userUid1,$public,$wallpost_real_data,$msg);
			$json_data['error']=0;
			$json_data['data']=$share_wallpost_uid;
			
			$give_point=TRUE;
			
			
			////是某給點!?  已分享!?防洪!?
			//// PS: 以下如果是分享文.應該用分享文(一般文)做計算 拿 $wallpost_real_data 或 $wallpost_share_data...會導致算錯
			if($give_point){
				///贈與點數與pro分
				///這邊先擋任何自己洗自己動作
				///$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
				$action_type=4;
				$user1_note='你分享了'.$xuser2['basic']['userRealname'].'文章';
				$user2_note=$xuser1['basic']['userRealname'].'分享了你的文章';
				if($userUid1!=$userUid2){
					$usettle=CZ::model('users')->user_point_settle($userUid2,$wallpost_data['wallpostType'],$action_type,$wallpost_data['wallpostInterests']);
					
					if(!empty($usettle['participants'])){
						CZ::model('users')->userwallpost_2p_add($userUid1,$usettle['participants'],$wallpost_data['wallpostUid'],$user1_note);
					}
					if(!empty($usettle['poster'])){
						CZ::model('users')->userwallpost_2p_add($userUid2,$usettle['poster'],$wallpost_data['wallpostUid'],$user2_note);
					}
				}
			}
			
			///個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
			$self_point=!(empty($usettle['participants']['Earn'])) ? $usettle['participants']['Earn'] : 0;
			CZ::model('wallposts')->uwallpost_socials_add($userUid1,$wallpost_data['wallpostUid'],$userUid2,3,$self_point);
			
			
			///通知 userUid1
			if($userUid1!=$userUid2){
				$from_uid='countmin-system';
				$to_uid=$userUid1;
				$target_wuid=$wallpost_data['wallpostUid'];
				$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
				$notice_id='Ly_i_share_this';
				$notice_data=array(
						'point' =>	empty($usettle['participants']['Earn']) ? 0 : $usettle['poster']['Earn'] ,
				);
				
				$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
				if($notificationSetting['shares']==0 || $notificationSetting['shares']==2){
							$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
				}
			}
			
			
			///通知 userUid2
			$from_uid=$userUid1;
			$to_uid=$userUid2;
			$target_wuid=$wallpost_data['wallpostUid'];
			$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
			$notice_id='Ly_other_share_my_post';
			$notice_data=array(
					'user_name' => $xuser1['basic']['userRealname'],
					'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
					'point' =>	empty($usettle['poster']['Earn']) ? 0 : $usettle['poster']['Earn'] ,
			);
			
			$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
			if($notificationSetting['shares']==0 || $notificationSetting['shares']==2){
						$exe=CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
			}
			
		}
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}
	
	
	/**
	 * Command_Wallpost_comment 
	 * 單則的文章含留言
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Wallpost_comment(){
		$this->layout='comment';
		$ui=form('ui','integer','get');
		$way=form('way','string','get');
		$wallpost_uid=form('w','string','get');
		$timeset=form('timeset','datetime','post');
		$timebefore=form('timebefore','boolean','post');
		$json=form('json','boolean','post');
		$comment_json=form('comment_json','boolean','post');
		$page=form('page','integer','post');
		
		///頁數控制
		$show_row=10;
		if($page<=0){$page=1;}
		
		$interests='';
		

		$defaultPhoto_m='/css/img/default_profile_pic_m.png';
		$defaultPhoto_s='/css/img/default_profile_pic_s.png';
		
		//CZ::model('mail')->add_mail_queue("m370460@gmail.com","超給力快報!!超給力快報~~","hello world!!<br/><b>hello world!!</b><i>哈囉!!</i>");
		//CZ::model('mail')->send_mail_queue();
		//CZ::model('mail')->send_mail("m370460@gmail.com","超給力快報!!超給力快報~~","hello world!!<br/><b>hello world!!</b><i>哈囉!!</i>");
		
		//CZ::model('cron')->run_corn_time();
		
		//這邊控管哪些人可以看這文章
		$wallposts_info=CZ::model('wallposts')->my_wallposts_renew(ME::user_uid(),$wallpost_uid,1);	///先帶自己登入uid看看 (是否可以觀看 興趣牆與留言狀態!!)
		
		$show_post=0;
		if($wallposts_info['args']['num']>0){
			$wallposts=$wallposts_info['d'][0];
			
			$relations_status=CZ::model('users')->relations_getter(ME::user_uid(),$wallposts['userUid']);
			////確認是朋友或同好關係
			if($wallposts['wallpostPublic']>0){
				
				
				if(!empty($relations_status['relation_status'])){
					foreach($relations_status['relation_status'] as $status => $relations_status_data){
						//if($status=='follow' && $wallposts['wallpostPublic']==1){$show_post=1;}
						if($status=='friend' && $wallposts['wallpostPublic']==2){$show_post=1;}
					}
				}
			}

			////自己可以看自己(請勿刪除  )
			if(ME::user_uid()==$wallposts['userUid']){$show_post=1;}
			////公開最大
			if($wallposts['wallpostPublic']==0){$show_post=1;}

		}
		
		
		if($show_post==0){$wallposts='';}
		
		if($show_post){
			$fb_share['title']=$wallposts['wallpostTopic'];
			$fb_share['img']=!empty($wallposts['imageUrl']) ? $wallposts['imageUrl'] : "";
			
			switch($wallposts['wallpostType']){
				case 0:
					$wallposts['wallpostContent']=$wallposts['wallpost_extend']['writingContent'];
					break;
				case 1:
					$wallposts['wallpostContent']=$wallposts['wallpost_extend']['articleContent'];
					break;
				case 2:
				case 3:
					$wallposts['wallpostContent']=$wallposts['wallpost_extend']['missionContent'];
					break;
				case 4:
					$wallposts['wallpostContent']=$wallposts['wallpost_extend']['eventContent'];
					break;
			}
			switch($wallposts['wallpost_view']['wallpostType']){
				case 0:
					$wallposts['wallpost_view']['wallpostContent']=empty($wallposts['wallpost_view']['wallpost_extend']['writingContent']) ? '' : $wallposts['wallpost_view']['wallpost_extend']['writingContent'];
					break;
				case 1:
					$wallposts['wallpost_view']['wallpostContent']=$wallposts['wallpost_view']['wallpost_extend']['articleContent'];
					break;
				case 2:
				case 3:
					$wallposts['wallpost_view']['wallpostContent']=$wallposts['wallpost_view']['wallpost_extend']['missionContent'];
					break;
				case 4:
					$wallposts['wallpost_view']['wallpostContent']=$wallposts['wallpost_view']['wallpost_extend']['eventContent'];
					break;
			}
			
			$fb_share['description']=$wallposts['wallpostBrief'];
			//$fb_share['description']=htmlspecialchars_decode($fb_share['description'],ENT_QUOTES);
			//$fb_share['description']=str_replace("\r\n","",strip_tags($fb_share['description']));
	
			if($way=="fbshare")
			{
				if(ME::is_login()){
					CZ::model('wallposts')->user_wallpost_fbshare(ME::user_uid(),$wallpost_uid);
					/////偵測使用這是否完成活動並給點數
					CZ::model('wallposts')->give_user_event_join(ME::user_uid(),$wallpost_uid);
				}
				$fb_link=urlencode('http://www.countmin.com/user/wallpost_comment?w='.$wallpost_uid);
				$nextUrl='https://www.facebook.com/sharer/sharer.php?u='.$fb_link;
				header('location:'.$nextUrl);
				exit();
			}
			if($json)
			{
				$json_data['error']=0;
				$json_data['data']=$wallposts; 
				echo json_encode($json_data);
				die();
				//CZ::ajax_json_output($json_data);
			}
			if($comment_json){
				$comments=CZ::model('wallposts')->wallpost_comments($wallpost_uid,$show_row,$page);
				$json_data['error']=0;
				$json_data['data']=$comments; 
				echo json_encode($json_data);
				die();
				//CZ::ajax_json_output($json_data);
			}
			else{
				$comments=CZ::model('wallposts')->wallpost_comments($wallpost_uid,$show_row,$page);
			}
			
			
			/////帶入文章上的uid (有可能在別人牆上留言)
			$xuser_uid=$wallposts['userUid'];
			$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
			
			
			if( !empty($xuser['basic']['userProfile']) ){
				$userPhoto=CZ::model('images')->imagePath_getter($xuser['basic']['userProfile'],'_s') ;
			}
			else{
				$userPhoto=$defaultPhoto_s;
			}
			$userTopbanner=CZ::model('images')->imagePath_getter($xuser['basic']['userTopbanner'],'_t') ;
			
			
			
			/////自己的uid
			$meuser=CZ::model('users')->xuser(ME::user_uid(),1,0,0);
			if( !empty($meuser['basic']['userProfile']) ){
				$mePhoto=CZ::model('images')->imagePath_getter($meuser['basic']['userProfile'],'_s') ;
			}
			else{
				$mePhoto=$defaultPhoto_s;
			}
			
			//////歷史發文
			$tmp_wallposts=CZ::model('wallposts')->my_posts($xuser_uid,$wallposts['wallpostTimeCreate'],1,'active');
			if($tmp_wallposts['args']['num']>0){
				foreach($tmp_wallposts['d'] as $inx =>  $tmp_wallposts_data){
					$history_wallposts[$tmp_wallposts_data['wallpostUid']]=$tmp_wallposts_data;
				}
				unset($history_wallposts[$wallposts['wallpostUid']]);////濾掉目前這篇
				shuffle($history_wallposts);////隨機排序
				
			}
			
			
			
			$data=array(
				'wallpostNumViews'=>array('wallpostNumViews+1'),
			);
			DB::update('common','wallposts',$data,array('WHERE wallpostUid=:wallpost',array(':wallpost'=>$wallpost_uid)));
			
			
			include($this->layout_path(CZ::controller(),CZ::command()));
		}
		else{

			include($this->layout_path(CZ::controller(),CZ::command()));
		}
		
		
	}
	
	
	/**
	 * Command_Wallpost_like 
	 * 在訊息上 like 或 dislike
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Wallpost_like(){
		$exe=form('exe','string','post');
		$self_uid=ME::user_uid(); //發表文章或按讚的自身user_uid
		$wallpost_uid=form('wuid','string','post');
		if(!ME::is_login()){die();}
	
		$json_data['error']=1;
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);

		if(!empty($wallpost_data)){//文章是否存在
			
			$userUid1=ME::user_uid();	
			$userUid2=$wallpost_data['userUid'];
			
			
			$xuser1=CZ::model('users')->xuser($userUid1,1,1,1);
			$xuser2=CZ::model('users')->xuser($userUid2,1,1,1);
			
			if($exe==='like'){
				if(CZ::model('wallposts')->plus_1($self_uid,$wallpost_uid,$wallpost_data['userUid'])){
					$json_data['error']=0;
					$wallpost_data['wallpostNumLikes']++;///減少sql再讀一次如果要即時訊息只好改sql
					$json_data['data']=$wallpost_data;
					
					
					///贈與點數與pro分
					///這邊先擋任何自己洗自己動作
					///$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
					$action_type=2;
					
					$user1_note='你在'.$xuser2['basic']['userRealname'].'上+1';
					$user2_note=$xuser1['basic']['userRealname'].'在你文章上+1';
					
					
					if($userUid1!=$userUid2){
						
						$usettle=CZ::model('users')->user_point_settle($userUid2,$wallpost_data['wallpostType'],$action_type,$wallpost_data['wallpostInterests']);
						
						if(!empty($usettle['participants'])){
							CZ::model('users')->userwallpost_2p_add($userUid1,$usettle['participants'],$wallpost_data['wallpostUid'],$user1_note);

						}
						if(!empty($usettle['poster'])){
							CZ::model('users')->userwallpost_2p_add($userUid2,$usettle['poster'],$wallpost_data['wallpostUid'],$user2_note);
						}
					}
					
					///個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
					$self_point=!empty($usettle['participants']['Earn']) ? $usettle['participants']['Earn'] : 0;
					CZ::model('wallposts')->uwallpost_socials_add($userUid1,$wallpost_data['wallpostUid'],$userUid2,1,$self_point);
					
					
					///通知 userUid1
					if($userUid1!=$userUid2){
						$from_uid='countmin-system';
						$to_uid=$userUid1;
						$notice_id='Ly_i_like_this';
						$target_wuid=$wallpost_data['wallpostUid'];
						$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
						$notice_data=array(
							'point' => empty($usettle['participants']['Earn']) ? 0 : $usettle['participants']['Earn'],
						);
						//載入通知設定
						$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
						if($notificationSetting['likes']==0 || $notificationSetting['likes']==2){
							CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
						}
					}

					///通知 userUid2
					if($userUid1!=$userUid2){
						$from_uid=$userUid1;
						$to_uid=$userUid2;
						$notice_id='Ly_other_like_my_post';
						$target_wuid=$wallpost_data['wallpostUid'];
						$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
						$notice_data=array(
							'user_name' => $xuser1['basic']['userRealname'],
							'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
						);
						//載入通知設定
						$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
						if($notificationSetting['likes']==0 || $notificationSetting['likes']==2){
							CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
						}
					}
					
					
					
				}
				/////偵測使用這是否完成活動並給點數
				CZ::model('wallposts')->give_user_event_join(ME::user_uid(),$wallpost_uid);
			}
			else if($exe==='dislike'){
				if(CZ::model('wallposts')->minus_1($self_uid,$wallpost_uid,$wallpost_data['userUid'])){
					$json_data['error']=0;
					$wallpost_data['wallpostNumDislikes']++;
					$json_data['data']=$wallpost_data; 
					
					///贈與點數與pro分
					///這邊先擋任何自己洗自己動作
					///$action_type  --1發表, 2+, 3-, 4分享, 5活動參加
					$action_type=3;
					$user1_note='你在'.$xuser2['basic']['userRealname'].'上-1';
					$user2_note=$xuser1['basic']['userRealname'].'在你文章上-1';
					if($userUid1!=$userUid2){
						$usettle=CZ::model('users')->user_point_settle($userUid2,$wallpost_data['wallpostType'],$action_type,$wallpost_data['wallpostInterests']);
						if(!empty($usettle['participants'])){
							CZ::model('users')->userwallpost_2p_add($userUid1,$usettle['participants'],$wallpost_data['wallpostUid'],$user1_note);
						}
						if(!empty($usettle['poster'])){
							CZ::model('users')->userwallpost_2p_add($userUid2,$usettle['poster'],$wallpost_data['wallpostUid'],$user2_note);
						}
					}
					
					///個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
					$self_point=!empty($usettle['participants']['Earn']) ? $usettle['participants']['Earn'] : 0;
					CZ::model('wallposts')->uwallpost_socials_add($userUid1,$wallpost_data['wallpostUid'],$userUid2,2,$self_point);
					
					///通知 userUid1
					if($userUid1!=$userUid2){
						$from_uid='countmin-system';
						$to_uid=$userUid1;
						$target_wuid=$wallpost_data['wallpostUid'];
						$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
						$notice_id='Ly_i_dislike_this';
						$notice_data=array(
							'point' => empty($usettle['participants']['Earn']) ? 0 : $usettle['participants']['Earn'],
						);
						//載入通知設定
						$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
						if($notificationSetting['dislikes']==0 || $notificationSetting['dislikes']==2){
							CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
						}
					}
					
					///通知 userUid2
					if($userUid1!=$userUid2){
						$from_uid=$userUid1;
						$to_uid=$userUid2;
						$target_wuid=$wallpost_data['wallpostUid'];
						$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
						$notice_id='Ly_other_dislike_my_post';
						$notice_data=array(
							'user_name' => $xuser1['basic']['userRealname'],
							'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
						);
						//載入通知設定
						$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
						if($notificationSetting['dislikes']==0 || $notificationSetting['dislikes']==2){
							CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
						}
					}
					
				}
			}
		}
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}
	/**
	 * Command_Wallpost_favor 
	 * 在訊息上 like 或 dislike
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Wallpost_favor(){
		$user_uid=ME::user_uid();
		$wallpost_uid=form('wuid','string','post'); 
		
		$json_data['error']=1;
		$wallpost_data=CZ::model('wallposts')->wallpost_get($wallpost_uid);
		
		
		
		if(!empty($wallpost_data)){
			
			$favor_data=CZ::model('ufavors')->ufavors_check($user_uid,$wallpost_uid,$wallpost_data['wallpostType']);
			
			
			if($favor_data['count(*)']==0){
				
				$data=array(
					'userUid'=>	$user_uid,
					'wallpostUid'=> $wallpost_uid,
					'userUidWallpost'	=> $wallpost_data['userUid'],
					'wallpostType'=> $wallpost_data['wallpostType'],
				);
		
				$favorsadd=CZ::model('ufavors')->ufavors_add($data);
				
				if($favorsadd){
					///使用者個人紀錄 (會有重複紀錄 但先放這)
					$record_data=array(
						'uparameterNumFavors' => array('uparameterNumFavors+1'),
					);
					CZ::model('users')->uparameters_update($user_uid,$record_data);
					
					$status_record=CZ::model('wallposts')->uwallpost_status_replace($user_uid,$wallpost_uid,16);
				}
				
				$json_data['error']=0;
				$json_data['data']=TRUE;
				
				$userUid1=ME::user_uid();	
				$userUid2=$wallpost_data['userUid'];
				$xuser1=CZ::model('users')->xuser($userUid1,1,1,1);
				$xuser2=CZ::model('users')->xuser($userUid2,1,1,1);
				
				
				///通知 userUid1
				if($userUid1!=$userUid2){
					$from_uid='countmin-system';
					$to_uid=$userUid1;
					$target_wuid=$wallpost_data['wallpostUid'];
					$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
					$notice_id='Ly_i_favor_this';
					$notice_data=array(
						'user_name' => $xuser2['basic']['userRealname'],
						'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
					);
					//載入通知設定
					$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
					if($notificationSetting['favors']==0 || $notificationSetting['favors']==2){
						CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
					}
				}
				
				
				///通知 userUid2
				if($userUid1!=$userUid2){
					$from_uid=$userUid1;
					$to_uid=$userUid2;
					$target_wuid=$wallpost_data['wallpostUid'];
					$target_url='http://www.countmin.com/user/wallpost_comment?w='.$target_wuid;
					$notice_id='Ly_other_favor_my_post';
					$notice_data=array(
						'user_name' => $xuser1['basic']['userRealname'],
						'wallpost_topic' =>$wallpost_data['wallpostTopic'] ,
					);
					
					//載入通知設定
					$notificationSetting=CZ::model('notifications')->notificationSetting_getter($to_uid);
					if($notificationSetting['favors']==0 || $notificationSetting['favors']==2){
						CZ::model('notifications')->notifications_add($from_uid,$to_uid,$target_url,$target_wuid,$notice_id,$notice_data);
					}
				}
				
				///個人小成就 0:留言, 1:+1, 2:-1, 3:share, 4:收藏, 5:檢舉
				CZ::model('wallposts')->uwallpost_socials_add($userUid1,$wallpost_data['wallpostUid'],$userUid2,4,0);

			}
			else
			{
				
				CZ::model('ufavors')->ufavors_delete($user_uid,$wallpost_data['wallpostUid'],$wallpost_data['wallpostType']);
				///使用者個人紀錄 (會有重複紀錄 但先放這)
				$record_data=array(
					'uparameterNumFavors' => array('uparameterNumFavors-1'),
				);
				CZ::model('users')->uparameters_update($user_uid,$record_data);
				
				
				$json_data['error']=0;
				$json_data['data']=false;
			}
		}
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);
	}

	/**
	 * Command_Wallposts 
	 * 興趣牆，自己的
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Wallposts(){
		$interests='';
		$this->layout='profile-no-topbanner';
		$xuser_uid=ME::user_uid();
		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
		$timeset=form('timeset','datetime','post');
		$timebefore=form('timebefore','boolean','post');
		$box=form('box','boolean','post');
		
		$wall_type=form('t','string','get');
		$wall_interests=form('i','string','get');
		
		$defaultPhoto_m='/css/img/default_profile_pic_m.png';
        $defaultPhoto_s='/css/img/default_profile_pic_s.png';

        CZ::grid_setter('interestpeople',TRUE);

    if( !empty($xuser['basic']['userProfile']) ){
			$userPhoto=CZ::model('images')->imagePath_getter($xuser['basic']['userProfile'],'_s') ;
    }
    else{
			$userPhoto=$defaultPhoto_m;
		}
		
		if($box){
			if($timeset){
				$wallposts=CZ::model('wallposts')->my_wallposts(ME::user_uid(),$timeset,$timebefore,$wall_type,$wall_interests);
			}
			else{
				$wallposts=CZ::model('wallposts')->my_wallposts(ME::user_uid(),_SYS_DATETIME,1,$wall_type,$wall_interests);
			}
			$json_data['error']=0;
			for($i=0;$i<$wallposts['args']['num'];$i++){
				////因為JS 排序會看陣列 key值...所以無法過濾資料結構
				$json_data['data'][$i]['wuid']=$wallposts['d'][$i]['wallpostUid'];
				$json_data['data'][$i]['box']=CZ::model('wallposts')->wallpost_get_layout($wallposts['d'][$i]);
			}
			
			//CZ::ajax_json_output($wallposts);
			echo json_encode($json_data);
			
			die();
		}
		else{
			$wallposts=CZ::model('wallposts')->my_wallposts(ME::user_uid(),_SYS_DATETIME,1,$wall_type,$wall_interests);
		}
		for($i=0;$i<3;$i++){
				if($i==2) //取user前3個興趣
					$interests.= $xuser['uprolevel']['d'][$i]['interestId'] ;
				else
					$interests.= $xuser['uprolevel']['d'][$i]['interestId'].';'   ;
		}
		$goods=CZ::model('goods')->goods_interest_getter($interests); //你可能有興趣的好康 
		
		$relation_from=2;//與您興趣相似的人
		$people=CZ::model('users')->relation_list($xuser_uid,$relation_from,3,0);
		
		
		if($people['args']['num']>0){
			foreach($people['d'] as $inx => $people_data){
				$users=CZ::model('users')->uprolevels_getter($people_data['userUid']);
				$people['d'][$inx]['title']=$users['title'];
				$imgUrl=CZ::model('images')->imagePath_getter($people_data['userProfile'],'_s') ;
				if(empty($imgUrl)){$imgUrl=$defaultPhoto_s;}
				$people['d'][$inx]['imgUrl']=$imgUrl;
			}
		}
	  
		
		include($this->layout_path(CZ::controller(),CZ::command()));
	}
	/**
	 * Command_Wallposts_renew 
	 * 興趣牆，自己的即時狀態 (內文,讚,分享)
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Wallposts_renew(){
		$user_uid=ME::user_uid();
		$wallpost_uid_dot=form('wuid_dot','string','post');
		
		$wallposts=CZ::model('wallposts')->my_wallposts_renew($user_uid,$wallpost_uid_dot);
		$json_data['error']=1;

		if(!empty($wallposts)){
			$json_data['error']=0;
			$json_data['data']=$wallposts;
		}
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);

	}


    public function Command_Wallposts_delete(){
        $user_uid=ME::user_uid();
        $wallpostUid=form('wid','string','post');
        $token=form('token','string','post');

        //TODO token check

        $wallposts=CZ::model('wallposts')->wallpost_editor_getter($user_uid,$wallpostUid);
        if( !array_key_exists('wallpostUid',$wallposts)){ 
            echo json_encode(false);
            exit;
        }

        $wallposts_del=CZ::model('wallposts')->wallpost_temp_delete($user_uid,$wallpostUid);

        if( $wallposts_del )
            $wallpost_extend_delete = CZ::model('wallposts')->wallpost_extend_delete($wallpostUid,$wallposts['wallpostType']);
        else{
            echo json_encode(false);
            exit;
        }

        if( $wallpost_extend_delete)
            echo json_encode(true);
        else
            echo json_encode(false);

    }

	public function Command_about(){
		$this->layout='profile-my';
		$xuser_uid=form('p','string','get');
        $relations=array();    
        $friend=false;

		if(!$xuser_uid){
			$xuser_uid=ME::user_uid();
        }
        else{
            //是否為好友
            $relations=CZ::model('users')->relations_getter(ME::user_uid(),$xuser_uid);
            if( array_key_exists('friend',$relations['relation_status']) && $relations['relation_status']['friend'] =='friend' )
                $friend=true;
        }

		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1,1);
        $xuser['p']=$xuser_uid;

        //uid不正確導回首頁        
        if(!$xuser)
            header('Location:/'); 
 
		$xuser['selected']='about';
		$xuser['sidebar_selected']='about';
		include($this->layout_path(CZ::controller(),CZ::command()));
	}

    public function Command_aboutsave(){
        //ajax used
        $data=array();
        $user_data=array();
        $save=false;

        $xuser_uid=ME::user_uid();

        if( array_key_exists('blood',$_POST) )
            $data['userBlood']=form('blood','int','post');

        if( array_key_exists('aptitude',$_POST) )
            $data['userAptitude']=form('aptitude','int','post');

        if( array_key_exists('relationship',$_POST) )
            $data['userRelationship']=form('relationship','int','post');

        if( array_key_exists('email',$_POST) ){
            $data['userContactEmail']=form('email','email','post');
            if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$data['userContactEmail']))
                exit;
        }

        if( array_key_exists('phone',$_POST) )
            $data['userContactPhone']=form('phone','tel','post');

        if( array_key_exists('showastrology',$_POST) )
            $data['userShowAstrology']=form('showastrology','int','post');

        if( array_key_exists('bloodpublic',$_POST) ){
            $data['userShowBlood']=form('bloodpublic','int','post');
        }

        if( array_key_exists('aptitudepublic',$_POST) )
            $data['userShowAptitude']=form('aptitudepublic','int','post');

        if( array_key_exists('relationshippublic',$_POST) )
            $data['userShowRelationship']=form('relationshippublic','int','post');

        if( array_key_exists('emailpublic',$_POST) )
            $data['userShowContactEmail']=form('emailpublic','int','post');

        if( array_key_exists('showphone',$_POST) )
            $data['userShowContactPhone']=form('showphone','int','post');

        if( array_key_exists('name',$_POST) ){
            $user_data['userRealname']=form('name','strip_unsafe','post');
            $user_data['userRealname']=htmlspecialchars($user_data['userRealname']);
            if( empty($user_data['userRealname'])){
                exit;
            }    
        }

        if( array_key_exists('gender',$_POST) ){
            $user_data['userGender']=form('gender','int','post'); 
        }

        if( array_key_exists('genderpublic',$_POST) )
            $user_data['userShowGender']=form('genderpublic','int','post');       

        if( array_key_exists('year',$_POST) )
            $user_data['userBirthYear']=form('year','int','post'); 

        if( array_key_exists('yearpublic',$_POST) )
            $user_data['userShowBirthYear']=form('yearpublic','int','post');
    
        if( array_key_exists('mon',$_POST) )
            $user_data['userBirthMonth']=form('mon','int','post');         

        if( array_key_exists('day',$_POST) )
            $user_data['userBirthDay']=form('day','int','post');

        if( array_key_exists('birdaypublic',$_POST) )
            $user_data['userShowBirthMonthDay']=form('birdaypublic','int','post');

        //星座
        if( array_key_exists('mon',$_POST) && array_key_exists('day',$_POST)  )
            $data['userAstrology']=$this->getStarSignsName($user_data['userBirthMonth'],$user_data['userBirthDay']);  

        if( count($user_data) > 0){
           
            $save1=CZ::model('users')->user_update($xuser_uid,$user_data);
        }

        if( count($data) > 0)
            $save=CZ::model('users')->userdetails_update($xuser_uid,$data);

        //TODO check return 
        echo json_encode($save);
    }    

    public function Command_postrecord(){
        $xuser_uid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1,1); 
        $xuser['selected']='postrecord';
        $xuser['sidebar_selected']='postrecord';
        $wallpostType=form('type','int','get');

        $this->layout='postrecord-menu';

        $relation_from=2;//與您興趣相似的人
        $hotPeople=CZ::model('users')->relation_list($xuser_uid,$relation_from,3,0);                
        $hotPeopleCount=CZ::model('users')->relation_list_count($xuser_uid,$relation_from);

        $users=array();
        $moreType='conn'; 
        for($i=0;$i<$hotPeople['args']['num'];$i++){
            $tmp = CZ::model('users')->user_getter( $hotPeople['d'][$i]['userUid2']  )  ;
            $title= CZ::model('users')->uprolevels_getter( $hotPeople['d'][$i]['userUid2']  ) ;
            $users['d'][$i] = $tmp['d'][0];
            $users['d'][$i]['title'] = $title['title'];             
            $users['args']['num'] = $hotPeopleCount['count(*)'];
        }

        //追蹤判斷
        if(!empty($users['d'])){
            foreach($users['d'] as $inx => $user_data){
                $user_uid1=$xuser_uid;
                $user_uid2=$user_data['userUid'];
                if(!ME::is_login()){$user_uid1='';}
                        
                $relation=CZ::model('users')->relations_getter($user_uid1,$user_uid2);
                $users['d'][$inx]['relation_status']=$relation['relation_status'];
                $imgUrl=CZ::model('images')->imagePath_getter($user_data['userProfile']) ;
                        
                if(empty($imgUrl)){$imgUrl=$defaultPhoto_s;}
                    
                $users['d'][$inx]['imgUrl']=$imgUrl;
            }
        }



        //熱門動態
        //計算方式  (+1次數 + 留言次數+被分享次數) / (今天日期-發文日期)
        $hotWrite=CZ::model('wallposts')->explore_hotgetter();
        $today =  strtotime(date("Y-m-d 23:59:59")) ;
        for($i=0;$i<$hotWrite['args']['num'];$i++){
            $diffDay = $today - strtotime($hotWrite['d'][$i]['wallpostTimeCreate'] );
            $num = $hotWrite['d'][$i]['wallpostNumLikes'] + $hotWrite['d'][$i]['wallpostNumComments'] + $hotWrite['d'][$i]['wallpostNumShares'];
            $score[$i] = ($num/$diffDay);
        }

        if( $hotWrite['args']['num'] > 0){
            if ( is_array($score) && count($score) > 0)
                $highestIndex = array_search(max($score),$score);

            $hotWrite=$hotWrite['d'][$highestIndex];
        }

        $wallposts=CZ::model('wallposts')->postrecord_getter($xuser_uid,$wallpostType);

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_Points(){
        $this->layout='profile-points';
        $xuser_uid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1,1); 
        $xuser['selected']='points';
        $xuser['sidebar_selected']='points';
        $pointsused=form('pointsused','int','get');
        $period=form('period','int','get');
        $startDay=date("Y-m-d",strtotime('+1 day'));

        switch($period){
            case 7 :
                $endDay=date("Y-m-d",strtotime('-7 day'));
                break;
            case 30 :
                $endDay=date("Y-m-d",strtotime('-30 day'));
                break;
            case 90 :
                $endDay=date("Y-m-d",strtotime('-90 day'));
                break;
            case 180:
                $endDay=date("Y-m-d",strtotime('-180 day'));
                break;
            case 365:
                $endDay=date("Y-m-d",strtotime('-365 day'));
                break;
            default :
                $endDay=date("Y-m-d",strtotime('-7 day'));
        }


        if($pointsused==2)
            $point=CZ::model('users')->upoint_pays_getter($xuser_uid,$startDay,$endDay);
        else
            $point=CZ::model('users')->upointlogs_wallposts_getter($xuser_uid);

        include($this->layout_path(CZ::controller(),CZ::command()));
    }
    
    public function Command_OrderList(){
        $this->layout='profile-points';
        $xuser_uid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1,1); 
        $xuser['selected']='points';
        $xuser['sidebar_selected']='points';
        $goodsType=form('goodssend','int','get');
        $period=form('period','int','get');
        $startDay=date("Y-m-d",strtotime('+1 day'));    

        if($goodsType==0)
            $goodsType=3;

        switch($period){
            case 7 :
                $endDay=date("Y-m-d",strtotime('-7 day'));
                break;
            case 30 :
                $endDay=date("Y-m-d",strtotime('-30 day'));
                break;
            case 90 :
                $endDay=date("Y-m-d",strtotime('-90 day'));
                break;
            case 180:
                $endDay=date("Y-m-d",strtotime('-180 day'));
                break;
            case 365:
                $endDay=date("Y-m-d",strtotime('-365 day'));
                break;
            default :
                $endDay=date("Y-m-d",strtotime('-7 day'));
        }

        $order=CZ::model('goods')->goods_order_by_user_getter($xuser_uid,$goodsType,$startDay,$endDay);

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_OrderDetail(){
        $this->layout='profile-points';
        $xuser_uid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1,1); 
        $xuser['selected']='points';
        $xuser['sidebar_selected']='points';
        $oid=form('oid','string','get');
        $order=CZ::model('goods')->goods_order_getter($oid);
        $brands=CZ::model('goods')->brands_getter('all',$order['brandUid']);
        include($this->layout_path(CZ::controller(),CZ::command()));
    }   

	public function Command_Prolevel(){
		$this->layout='profile-my';
		$xuser_uid=form('p','string','get');
		if(!$xuser_uid){
			$xuser_uid=ME::user_uid();
		}
		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1); 
		$xuser['selected'] = 'about';
        $xuser['p']=$xuser_uid;

		$selected='prolevel';
		$xuser['sidebar_selected']='prolevel';

		include($this->layout_path(CZ::controller(),CZ::command()));
	}

	public function Command_Exp(){
		$this->layout='profile-my';
		$xuser_uid=form('p','string','get');
		if(!$xuser_uid){
			$xuser_uid=ME::user_uid();
		}
		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
        $xuser['p']=$xuser_uid;

		$xuser['selected'] = 'about';
		$xuser['sidebar_selected']='exp';
		$selected='exp';
        $expSchool=CZ::model('users')->userexps_getter($xuser_uid,1);
        $expJob=CZ::model('users')->userexps_getter($xuser_uid,2);

		include($this->layout_path(CZ::controller(),CZ::command()));
	}

    public function Command_ExpSave(){
        $xuser_uid=ME::user_uid();
        $userexpUid=form('expid','string','post');
        $add=form('add','int','post');
        $del=form('del','int','post');
        $data=array(
                    'userexpStartYear'=>form('year','int','post'),
                    'userexpStartMonth'=>1,
                    'userexpStartDay'=>1,                        
                    'userexpEndYear'=>form('endyear','int','post'),
                    'userexpEndMonth'=>1,
                    'userexpEndDay'=>1,
                    'userexpType'=>form('type','int','post'),
                    'userexpOrg'=>form('org','string','post'),
                    'userexpTitle'=>form('title','string','post'),
                    'userexpRegion'=>form('region','string','post'),
                    'userexpShow'=>form('show','int','post'),
                    );
        //update
        if( !empty($userexpUid)){
            return CZ::model('users')->userexps_update($xuser_uid,$userexpUid,$data);
        }

        //add
        if( $add ){
            $data['userUid']=$xuser_uid; 
            $data['userexpUid']=uid4();
            $expAdd=CZ::model('users')->userexps_add($data);
            echo $data['userexpUid'];
        }

        if($del){
            return CZ::model('users')->userexps_delete($xuser_uid,$userexpUid);
        }
    }


    public function Command_Album(){
        $this->layout='profile-my';
        $xuser_uid=form('p','string','get');
        $imgid=form('imgid','string','post');
        $del_id=form('delid','string','post');

        if(!$xuser_uid){
            $xuser_uid=ME::user_uid();
        }

        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
        $xuser['selected'] = 'about';
        $xuser['sidebar_selected']='album';
        $xuser['p']=$xuser_uid;

        $selected='album';
        $userUpdate=false;

        //設定為使用中大頭照
        if( !empty($imgid) ){
            //清除之前設定
            $data=array('userphotoUsed'=>0);
            $photoUpdate=CZ::model('users')->userphotoes_update($xuser_uid,'',$data);

            $data=array('userphotoUsed'=>1);
            $photoUpdate=CZ::model('users')->userphotoes_update($xuser_uid,$imgid,$data);

            $data=array('userProfile'=>$imgid);
            $userUpdate=CZ::model('users')->user_update($xuser_uid,$data);
        }

        //刪除大頭照
        if( !empty($del_id) ){
            $delete=CZ::model('users')->userphotoes_delete(ME::user_uid(),$del_id);

        }

        $photo=CZ::model('users')->userphotoes_getter($xuser_uid);

        include($this->layout_path(CZ::controller(),CZ::command()));
    }

    public function Command_uploadphoto(){    
        //ajax user 
        $xuser_uid=ME::user_uid();
        $x=form('x1','int','post');
        $y=form('y1','int','post');
        $zoom=form('zoom','float','post') ;          
        $photoType=form('photoType','int','post') ; //1 大頭照 2封面
        $data=array();
        if( !array_key_exists('photo',$_FILES) ||  $_FILES['photo']['error'] != 0 ){
            echo false;
            return;
        }
        
        switch($photoType){
            case 1 :
                $convType='profile';
                $width=$this->profile_width;
                $heigh=$this->profile_heigh;    
                $images=CZ::model('images')->imageUpload($_FILES,$xuser_uid,$convType,$zoom,$x,$y,$heigh,$width );

                //清空使用中大頭照設定值
                $data['userphotoUsed']=0;
                $photoadd=CZ::model('users')->userphotoes_update($xuser_uid,'',$data);
                //新增大頭照相本    
                $data=array(
                            'userUid'=>$xuser_uid,
                            'imageUid'=>$images['photo']['filename'],
                            'userphotoUsed'=>1,
                            'userphotoTimeUpload'=>_SYS_DATETIME,
                            );
                $photoadd=CZ::model('users')->userphotoes_add($data);
                
                $userData=array('userProfile'=>$images['photo']['filename']);
                break;
            case 2 :
                $convType='topbanner';
                $images=CZ::model('images')->imageUpload($_FILES,$xuser_uid,$convType);
                $userData=array('userTopbanner'=>$images['photo']['filename']);
                break;
            default:    
                $convType='topbanner';
                $images=CZ::model('images')->imageUpload($_FILES,$xuser_uid,$convType);
                break;
        }

        if( !empty($userData['userProfile']) || !empty($userData['userTopbanner']) )
            echo CZ::model('users')->user_update($xuser_uid,$userData);
        else
            echo false;
    }

	public function Command_Activity(){
		$this->layout='profile';
		$xuser_uid=form('p','string','get');
		
		
		if(!$xuser_uid){
			$xuser_uid=ME::user_uid();
		}
		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
		$xuser['selected']='activity';
		$xuser['sidebar_selected']='activity';
		
		
		$socials_track=CZ::model('wallposts')->user_socials_track($xuser_uid,_SYS_DATETIME);
		$tmpInterests=CZ::model('interests')->all_getter();
		$join_track=CZ::model('wallposts')->user_event_join_track($xuser_uid,_SYS_DATETIME);
		
		
		for($day=-7;$day<=0;$day++)
		{
			$assign_sdate=date("Y-m-d",strtotime(_SYS_DATETIME." ".$day."days"));
		
			if(!empty($socials_track[$assign_sdate])){
				$img_list[$assign_sdate]['action']=count($socials_track[$assign_sdate]);
			}
			else{
				$img_list[$assign_sdate]['action']=0;
			}
			
			
			if(!empty($join_track[$assign_sdate])){
				$img_list[$assign_sdate]['event']=count($join_track[$assign_sdate]);
			}
			else{
				$img_list[$assign_sdate]['event']=0;
			}
			
		}
			
		
		
		
		
		include($this->layout_path(CZ::controller(),CZ::command()));
	}

    public function Command_Socially(){
        $this->layout='profile';
        $status=form('status','string','get');
        $xuser_uid=form('p','string','get');
        $wallpost_uid=form('wuid','string','request');
        $format=form('format','string','request');
        if(!$xuser_uid){
					$xuser_uid=ME::user_uid();
				}
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
        $xuser['selected']='activity';
        $xuser['sidebar_selected']='activity';
        $reverse=0;
        
        switch( $status){
        	case 'interest':$relation_type=2;break;
        	case 'inviting':$relation_type=3;break;
        	case 'response':$relation_type=3;$reverse=1;break;
        	case 'follow':$relation_type=4;break;
        	case 'follower':$relation_type=4;$reverse=1;break;
        	case 'friend':$relation_type=5;break;
        	default:$relation_type=5;$status='friend';break;
        }

        $defaultPhoto_m='/css/img/default_profile_pic_m.png';
				$defaultPhoto_s='/css/img/default_profile_pic_s.png'; 
        
        $relation=CZ::model('users')->relation_list($xuser_uid,$relation_type,0,1,$reverse);
				
        $xuser_data='';
        if($relation['args']['num']>0){
        	$xuser_data['args']['num']=$relation['args']['num'];
        	foreach($relation['d'] as $inx => $data_info){
        		$xuser_data['d'][$inx]=CZ::model('users')->xuser($data_info['userUid'],1,1,1);
        		$title=CZ::model('users')->uprolevels_getter($data_info['userUid']);
        		
        		if( !empty($xuser_data['d'][$inx]['basic']['userProfile']) ){
        			$userPhoto=CZ::model('images')->imagePath_getter($xuser_data['d'][$inx]['basic']['userProfile'],'_s') ;
						}
						else{
							$userPhoto=$defaultPhoto_m;
						}
						$xuser_data['d'][$inx]['userImgUrl']=$userPhoto;
						$xuser_data['d'][$inx]['userTitle']=$title['title'];
						$relation['d'][$inx]['xuser']=$xuser_data['d'][$inx];
        	}
        }
        
        if($format=='json'){
        	///已邀請名單
        	$inviting_list='';
        	$tmp_inviting_list=CZ::model('users')->user_inviting_list(ME::user_uid(),$wallpost_uid);
        	if($tmp_inviting_list['args']['num']>0){
        		foreach($tmp_inviting_list as $tmp_inviting_data){
        			$inviting_list[$tmp_inviting_data['userUid2']]=$tmp_inviting_data;
        		}
        	}
        	
        	if($relation['args']['num']>0){
        		foreach($relation['d'] as $inx => $data_info){
        			if(!empty($inviting_list[$data_info['userUid2']])){
        				$relation['d'][$inx]['inviting']=true;
        			}
        			else{
        				$relation['d'][$inx]['inviting']=false;
        			}
        		}	
        	}	
        	$json_data['error']=0;
        	$json_data['data']=$relation;
        	$json_data['inviting_list']=$inviting_list;
        	echo json_encode($json_data);
					die();
					//CZ::ajax_json_output($json_data);
        
        }
       
        
        include($this->layout_path(CZ::controller(),CZ::command()));
    }



	/**
	 * Command_Mywalls 
	 * 個人主頁
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Mywalls(){
		$this->layout='profile';
		$xuser_uid=form('p','string','get');
		$wall_type=form('t','string','get');
		$box=form('box','boolean','post');
		$timeset=form('timeset','datetime','post');
		$timebefore=form('timebefore','boolean','post');
        $interests='';	
        CZ::grid_setter('interestpeople',TRUE);
	
		if(!$xuser_uid){
			$xuser_uid=ME::user_uid();
		}
		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
		$xuser['selected']='mywalls';
		
		$defaultPhoto_m='/css/img/default_profile_pic_m.png';
		$defaultPhoto_s='/css/img/default_profile_pic_s.png';    

		if( !empty($xuser['basic']['userProfile']) ){
			$userPhoto=CZ::model('images')->imagePath_getter($xuser['basic']['userProfile'],'_s') ;
		}
		else{
			$userPhoto=$defaultPhoto_m;
		}
		
		
		if($box){

			if($timeset){
				$wallposts=CZ::model('wallposts')->my_posts($xuser['uid'],$timeset,$timebefore,$wall_type);
			}
			else{
				$wallposts=CZ::model('wallposts')->my_posts($xuser['uid'],_SYS_DATETIME,1,$wall_type);	
			}
			
			for($i=0;$i<$wallposts['args']['num'];$i++){
				echo CZ::model('wallposts')->wallpost_get_layout($wallposts['d'][$i]);
			}
			
			
			
			//CZ::ajax_json_output($wallposts);
			//echo json_encode($wallposts);
			die();
			
		}
		$wallposts=CZ::model('wallposts')->my_posts($xuser['uid'],_SYS_DATETIME,1,$wall_type);
		$relation_from=2;//與您興趣相似的人
		$people=CZ::model('users')->relation_list($xuser_uid,$relation_from,3,0);

		
		if($people['args']['num']>0){
			foreach($people['d'] as $inx => $people_data){
				
                $title=CZ::model('users')->uprolevels_getter($people_data['userUid']);
                $people['d'][$inx]['title']=$title['title'];

				$imgUrl=CZ::model('images')->imagePath_getter($people_data['userProfile'],'_s') ;
				if(empty($imgUrl)){$imgUrl=$defaultPhoto_s;}
				$people['d'][$inx]['imgUrl']=$imgUrl;
				
			}
		}

		$friends=CZ::model('users')->relation_list($xuser_uid,5,4,0);

        for($i=0;$i<3;$i++){
                if($i==2) //取user前3個興趣
                    $interests.= $xuser['uprolevel']['d'][$i]['interestId'] ;
                else
                    $interests.= $xuser['uprolevel']['d'][$i]['interestId'].';'   ;
        }

        $goods=CZ::model('goods')->goods_interest_getter($interests); //你可能有興趣的好康		

		include($this->layout_path(CZ::controller(),CZ::command()));
	}

   /**
     * Command_Mywalls_regist 
     * 新註冊完成後的假頁面
     * 
     * @access public
     * @return void
     */
    public function Command_Mywalls_regist(){
        $this->layout='profile';
        $xuser_uid=form('p','string','get');
        $wall_type=form('t','string','get');
        $timeset=form('timeset','datetime','post');
        $timebefore=form('timebefore','boolean','post');

        if(!$xuser_uid){
            $xuser_uid=ME::user_uid();
        }
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
        $xuser['selected']='mywalls';

        if($timeset){
            $wallposts=CZ::model('wallposts')->my_posts($xuser['uid'],$timeset,$timebefore,$wall_type);
            //CZ::ajax_json_output($wallposts);
            echo json_encode($wallposts);
            die();
        }
        $wallposts=CZ::model('wallposts')->my_posts($xuser['uid'],_SYS_DATETIME,1,$wall_type);
        $relation_from=2;//與您興趣相似的人
        $people=CZ::model('users')->relation_list($xuser_uid,$relation_from,4,0);
        
        
        if($people['args']['num']>0){
            foreach($people['d'] as $inx => $people_data){

                $people['d'][$inx]['pro_level']=CZ::model('users')->uprolevels_getter($people_data['userUid']);
            }
        }
        //$friends=CZ::model('users')->relation_list($xuser_uid,5,4,0);
        $friends=array(
            'd'=>array(
                array(
                    'userUid'=>'jkwerr-1231vf-129vfdfb-wefw',
                    'userRealname'=>'陸公紀',
                ),
                array(
                    'userUid'=>'b0k10-n6okw844-go44skkwg',
                    'userRealname'=>'桓伯緒',
                ),
                array(
                    'userUid'=>'diwkw-h40kgog4-kko4k4so8',
                    'userRealname'=>'鍾離子幹',
                ),
                array(
                    'userUid'=>'apceu-9i0ckww4-8o8ckgo44',
                    'userRealname'=>'鄧伯苗',
                ),

            ),
            'args'=>array(
                'num'=>4
            )
        );
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

	/**
	 * Command_Mywalls 
	 * 個人主頁 即時更新
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Mywalls_renew(){
		
		$xuser_uid=form('p','string','get');
		$wallpost_uid_dot=form('wuid_dot','string','post');
		if(!$xuser_uid){
			$xuser_uid=ME::user_uid();
		}
		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
		$xuser['selected']='mywalls';
		
		$wallposts=CZ::model('wallposts')->my_posts_renew($xuser['uid'],$wallpost_uid_dot);
		$json_data['error']=1;

		if(!empty($wallposts))
		{
			$json_data['error']=0;
			$json_data['data']=$wallposts;
		}
		
		echo json_encode($json_data);
		die();
		//CZ::ajax_json_output($json_data);

	}
	

	/**
	 * Command_Notification 
	 * 近期通知瀏覽
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Notification(){
		$this->layout='profile-no-topbanner';
        $user_uid=ME::user_uid();
        $notify=CZ::model('notifications')->list_all_getter($user_uid);
        for($i=0;$i<$notify['args']['num'];$i++){
            if($notify['d'][$i]['userUidFrom'] != 'countmin-system' ){
                $xuser=CZ::model('users')->xuser($notify['d'][$i]['userUidFrom']);
                $notify['d'][$i]['userRealname'] = $xuser['basic']['userRealname'];
                $notify['d'][$i]['userProfile']=$xuser['basic']['userProfile']; 
            }
        }
        
 		
        include($this->layout_path(CZ::controller(),CZ::command()));
	}

	/**
	 * Command_My_notifications 
	 * 取得使用者尚未讀取的通知
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Notification_realtime_get(){
		if(ME::is_login()){
			$user_uid=ME::user_uid();
			$notifications=CZ::model('notifications')->unread_getter($user_uid);
			//$counter=CZ::model('notifications')->counter($user_uid);
			$counter=$notifications['args']['cnt'];
			if($notifications['args']['num']>0){
				foreach($notifications['d'] as $inx => $notifications_data){
					$msg_info=json_decode($notifications_data['unotificationData'],true);
					
					if(!empty($msg_info)){
						foreach($msg_info as $key => $text){
							$msg_info[$key]=html($text,false);
						}
					}
					$msg_info['_output']=0;
					
					$notifications['d'][$inx]['msg']=_tl($notifications_data['noticeId'],$msg_info,'text');
				}
			}
			
		
			$resp=array(
				'realtimes'=>$notifications,
				'counter'=>$counter,
				'error'=>0,
			);
			CZ::ajax_json_output($resp);

		}
		else{
			CZ::ajax_json_output(array('error'=>1));
		}
	}

    public function Command_Favourite(){
       // $this->layout='profile-my';
        CZ::grid_setter('interestpeople',TRUE);
        CZ::grid_setter('hotwrite',TRUE);
        
        
        $xuser_uid=form('p','string','get');
        if(!$xuser_uid){
            $xuser_uid=ME::user_uid();
        }
        $xuser=CZ::model('users')->xuser($xuser_uid,1,1,1,1); 
        $xuser['selected']='about';
        $xuser['sidebar_selected']='about';
        ///好康要濾開
        $data=CZ::model('ufavors')->ufavors_getter($xuser_uid,'0,1,2,3,4',1);
        
        $favors_count['total']=$data['args']['num'];
        $favors_count['active']=0;
        $favors_count['event']=0;
        //不用 my_wallposts_renew 是因為有鎖好友及公開資訊
        //改用 wallpost_get是收藏後取消好友及公開資訊
        if($data['args']['num']>0){
        	foreach($data['d'] as $inx => $favors_data){
        		
        		if($favors_data['wallpostType']<2){
        			$favors_count['active']++;
        		}
        		else{
        			$favors_count['event']++;
        		}
        		
        		$wallpost_data=CZ::model('wallposts')->wallpost_get($favors_data['wallpostUid']);
        		
        		if(!empty($wallpost_data)){
        			$wallposts['d'][$inx]=$wallpost_data;
        			$wallposts['d'][$inx]['userRealname']=$wallpost_data['xuser']['basic']['userRealname'];
        			unset($wallpost_data['wallpost_share']);
        			$wallposts['d'][$inx]['wallpost_view']=$wallpost_data;
        		}
        	}
        	

        	$wallposts['args']['num']=count($wallposts['d']);
        	
					
        }
        $relation_from=2;//與您興趣相似的人
        $users=CZ::model('users')->relation_list($xuser_uid,$relation_from,3,0);

				if($users['args']['num']>0){
					foreach($users['d'] as $inx => $people_data){
						$users['d'][$inx]['pro_level']=CZ::model('users')->uprolevels_getter($people_data['userUid']);
							
						$users['d'][$inx]['title']=$users['d'][$inx]['pro_level']['title'];
					}
				}
        
        //熱門動態
        //計算方式  (+1次數 + 留言次數+被分享次數) / (今天日期-發文日期)
        $hotWrite=CZ::model('wallposts')->explore_hotgetter();
        $today =  strtotime(date("Y-m-d 23:59:59")) ;
        for($i=0;$i<$hotWrite['args']['num'];$i++){
            $diffDay = $today - strtotime($hotWrite['d'][$i]['wallpostTimeCreate'] );
            $num = $hotWrite['d'][$i]['wallpostNumLikes'] + $hotWrite['d'][$i]['wallpostNumComments'] + $hotWrite['d'][$i]['wallpostNumShares'];
            $score[$i] = ($num/$diffDay);
        }

        if( $hotWrite['args']['num'] > 0){
            if ( is_array($score) && count($score) > 0)
                $highestIndex = array_search(max($score),$score);
       
            $hotWrite=$hotWrite['d'][$highestIndex];
        }
        
        include($this->layout_path(CZ::controller(),CZ::command()));
    }

	public function Grid_UserMenu($xuser=NULL){
		include($this->grid_path($this->name,'user_menu'));
	}

	public function Grid_UserBasic($xuser){
        $user_title=CZ::model('users')->uprolevels_getter($xuser['uid']);
        $xuser['title'] = $user_title['title2'];
		include($this->grid_path($this->name,'user_basic'));
	}

	/**
	 * Grid_Topbanner 
	 * 
	 * @param mixed $xuser 
	 * @access public
	 * @return void
	 */

    public function Grid_UserPoints(){
        $xuser_uid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($xuser_uid);
        $userPoint=CZ::model('users')->upointlogs_getter($xuser_uid);
        $totalPoint=0;

        //計算半年後到期的點數總和
        for($i=0;$i<count($userPoint['d']);$i++){
            $sixMon =date("Y/m/d", mktime(0, 0, 0, date('m')+6, date('d'), date('Y'))); 
            $sixLimit=(strtotime($userPoint['d'][$i]['userPointTimeLimit']) <= strtotime($sixMon) ) ; 
            $todayLimit=(strtotime($userPoint['d'][$i]['userPointTimeLimit']) >= strtotime( date("Y-m-d")  ) ) ;  
        //篩選出剩餘點數 >0 and 點數期限 > 今天 and 點數期限 < 6個月後
            if( $userPoint['d'][$i]['userPoint'] >0 && $sixLimit && $todayLimit){
                $totalPoint += $userPoint['d'][$i]['userPoint'];
            }    
        } 

        include($this->grid_path($this->name,'user_points'));
    }



    public function Grid_PostRecordmenu(){
        $xuser_uid=ME::user_uid();
        $xuser=CZ::model('users')->xuser($xuser_uid);
        include($this->grid_path($this->name,'user_postrecord'));
    }



    private function getStarSignsName($month, $day) {  
        $list=array(  
            array('name'=>12,'min'=>'12-22','max'=>'01-19'), //摩羯    
            array('name'=>1,'min'=>'01-20','max'=>'02-18'), //水瓶 
            array('name'=>2,'min'=>'02-19','max'=>'03-20'), //雙魚 
            array('name'=>4,'min'=>'03-21','max'=>'04-19'), //牡羊 
            array('name'=>3,'min'=>'04-20','max'=>'05-20'), //金牛 
            array('name'=>5,'min'=>'05-21','max'=>'06-21'), //雙子 
            array('name'=>6,'min'=>'06-22','max'=>'07-22'), //巨蟹 
            array('name'=>7,'min'=>'07-23','max'=>'08-22'), //獅子 
            array('name'=>8,'min'=>'08-23','max'=>'09-22'), //處女 
            array('name'=>9,'min'=>'09-23','max'=>'10-23'), //天秤 
            array('name'=>10,'min'=>'10-24','max'=>'11-22'), //天蠍 
            array('name'=>11,'min'=>'11-23','max'=>'12-21'), //射手 
        );   
         
        $time=strtotime("1970-$month-$day");  
        foreach ($list as $row){  
            $min=strtotime("1970-".$row['min']);  
            $max=strtotime("1970-".$row['max']);  
            if($min<=$time && $time<=$max)
                return $row['name'];  
             
       }  
       /*other to 摩羯座*/  
       return $list[0]['name'];  
    }  


    /**
     * checkPrivacy 
     * 依照隱私設定判斷是否要呈現資料
     * 
     * @access privacy
     * @return true/false
     * xuser_uid : 網址傳入的p值 uid
     * privacyValue :  隱私設定 0本人 1公開 2朋友
     * friend : 是否為好友 true/false
     */
    private function checkPrivacy($xuser_uid,$privacyValue,$friend){ 
        switch($privacyValue){
            case 0: //本人
                return ( $xuser_uid == ME::user_uid()  ) ? true : false ;
                break;
            case 1: //公開
                return true;                
                break;
            case 2: //朋友
                return ( $xuser_uid == ME::user_uid() || $friend) ? true : false ;
                break;
            default:
                return false;                
                break;
        }
    }

	public function Grid_Topbanner($xuser){
		// 使用者等級名詞表
		// /**神人**/'Ly_userlevel_9'
		// /**超級A咖**/'Ly_userlevel_8'
		// /**A咖**/'Ly_userlevel_7'
		// /**B咖**/'Ly_userlevel_6'
		// /**C咖**/'Ly_userlevel_5'
		// /**D咖**/'Ly_userlevel_4'
		// /**E咖**/'Ly_userlevel_3'
		// /**F咖**/'Ly_userlevel_2'
		// /**小咖**/'Ly_userlevel_1'
		// /**小兵**/'Ly_userlevel_0'


		$topbanner=1; 
        $user_title=CZ::model('users')->uprolevels_getter($xuser['uid']);

		include($this->grid_path($this->name,'topbanner'));
	}

}

