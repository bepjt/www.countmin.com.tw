<?php
class Personal_Controller extends Controller {

	public function __construct(){
		$this->name='personal';
		/**
		 * name => grid method
		 */
		$this->layout_grids=array(
			'page-header'=>array('grid'=>'Header'),
		);
		CZ::grid_setter('topbanner',TRUE);
		CZ::grid_setter('page-header',TRUE);
	}

	/**
	 * Command_Stat 
	 * 數據中心
	 * 
	 * @access public
	 * @return void
	 */
	public function Command_Stat(){
		$this->layout='profile';
		$xuser_uid=ME::user_uid();
		$xuser=CZ::model('users')->xuser($xuser_uid,1,1,1);
		$xuser['selected']='stat';
        $period=form('period','int','get'); 

        switch($period){
            case 7:
                $day = date("Y-m-d",strtotime('-7 day') );
                break;
            case 30:
                $day = date("Y-m-d",strtotime('-30 day') );
                break;
            case 365:
                $day = date("Y-m-d",strtotime('-365 day') );
                break;
            default:
                $day = date("Y-m-d",strtotime('-7 day') );
        }
    
        //log data
        $wscoials=CZ::model('wallposts')->wsocials_count_getter($xuser_uid,$day);
        
        $active=0;//活躍度
        $follow=0;//追蹤數(傳播力)
        $fascinate=0;//吸引力        
        $total=0;//我的影響力

        $week=$this->get_week(date("Y"));
        $perWeekStart=$week[date("W")-1][0].' 00:00:00'; //上一週開始日期
        $perWeekEnd=$week[date("W")-1][1].' 23:59:59';  //上一週結束日期
        $dateBetween = " AND (wallpostTimeCreate BETWEEN '".$perWeekStart."' AND '".$perWeekEnd."')"; 

        for($i=0;$i<$wscoials['args']['num'];$i++){    
            if( array_key_exists(1,$wscoials['d']) || array_key_exists(2,$wscoials['d']) )        
                $active+=$wscoials['d'][$i]['count(*)'];
            
            //11:男追蹤  12:女追蹤
            if( array_key_exists(11,$wscoials['d']) || array_key_exists(12,$wscoials['d']) )        
                $follow+=$wscoials['d'][$i]['count(*)'];    
        }

        $fascinateData=CZ::model('wallposts')->postrecord_getter($xuser_uid,0,$dateBetween);
        for($i=0;$i<$fascinateData['args']['num'];$i++){
            $fascinate += $fascinateData['d'][$i]['wallpostNumLikes'] + 
                            $fascinateData['d'][$i]['wallpostNumDislikes'] +
                            $fascinateData['d'][$i]['wallpostNumComments'] +
                            $fascinateData['d'][$i]['wallpostNumShares'] ;
        }

        $total=$active+$follow+$fascinate;

        $look=CZ::model('wallposts')->wsocials_count_getter($xuser_uid,$day,'0,1');
        $lookbyDay=CZ::model('wallposts')->wsocials_count_getter($xuser_uid,$day,'0,1','wsocialTime');

        $wallposts=CZ::model('wallposts')->postrecord_getter($xuser_uid,0);
       
		include($this->layout_path(CZ::controller(),CZ::command()));
	}

    // 計算第n週的起始及結束日期
    private function get_week($year){ 
        $year_start = $year . "-01-01"; 
        $year_end = $year . "-12-31"; 
        $startday = strtotime($year_start); 

        if(intval(date('N', $startday)) != '1') 
            $startday = strtotime("next monday", strtotime($year_start)); //獲取年第一周的日期 

        $year_mondy = date("Y-m-d", $startday); //獲取年第一周的日期 
        $endday = strtotime($year_end); 

        if(intval(date('W', $endday)) == '7') 
            $endday = strtotime("last sunday", strtotime($year_end)); 

        $num = intval(date('W', $endday)); 

        for($i = 1; $i <= $num; $i++){ 
            $j = $i -1; 
            $start_date = date("Y-m-d", strtotime("$year_mondy $j week ")); 
            $end_day = date("Y-m-d", strtotime("$start_date +6 day")); 
            $week_array[$i] = array ( $start_date,$end_day);
        } 
        return $week_array; 
    }    

}

