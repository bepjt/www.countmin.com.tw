<?php
class Cron_Controller extends Controller {
	protected $log=array();

	public function __construct(){

		$this->Command_Update_js_css_mtime();


	}

	public function Command_Update_js_css_mtime(){

		$crlf="\n";
		$timestamp_20150601=mktime(0,0,0,6,1,2015);
		$timeup=include(_DIR_DOCS.'etc/css_js_timeup.php');
		if(!isset($timeup['js'])){
		   	$timeup['js']=0;
		}
		if(!isset($timeup['css'])){
		   	$timeup['css']=0;
		}
		$css_dir=_DIR_DOCS.'public/css/';
		$js_dir=_DIR_DOCS.'public/js/';
		$update=FALSE;
		$d=opendir($css_dir);
		while(($f=readdir($d))!=FALSE){
			if(is_file($css_dir.$f)){
				$t=filemtime($css_dir.$f)-$timestamp_20150601;
				if($t>$timeup['css']){
					$timeup['css']=$t;
					$update=TRUE;
				}
			}	
		}
		closedir($d);

		$d=opendir($js_dir);
		while(($f=readdir($d))!=FALSE){
			if(is_file($js_dir.$f)){
				$t=filemtime($js_dir.$f)-$timestamp_20150601;
				if($t>$timeup['js']){
					$timeup['js']=$t;
					$update=TRUE;
				}
			}	
		}
		closedir($d);

		if($update){
			$str ='<'.'?php'.$crlf;
			$str.='return array(\'js\'=>'.$timeup['js'].',\'css\'=>'.$timeup['css'].');';
			file_put_contents(_DIR_DOCS.'etc/css_js_timeup.php',$str);
			echo 'js_cs_mtime_updated'.$crlf;
		}

	}

    /*
    Command_Users_userLevel_update
    功能：1.興趣等級更新
          2.會員頭銜(新手 一星達人..) 更新
          3.發出通知  
    每日4點0分執行一次
    可重覆執行
    */
    public function Command_Users_userLevel_update(){
        $column='userUid,userRealname,userLevel';
        $usersTotal=CZ::model('users')->users_list($column);
        $page=ceil($usersTotal['args']['total']/$usersTotal['args']['per']);
        
        for($i=0;$i<$page;$i++){
            //分頁執行
            $users_list=CZ::model('users')->users_list($column,$i);
            //更新 uprolevels.userProLevel 興趣標籤等級
            for($j=0;$j<$users_list['args']['num'];$j++){
                $uprolevels=CZ::model('users')->uprolevels_getter($users_list['d'][$j]['userUid']);
                for($k=0;$k<$uprolevels['args']['num'];$k++){
                    $pro_level=$this->userProLevel_count($uprolevels['d'][$k]['userGrade']);  
                    if( $uprolevels['d'][$k]['userProLevel'] != $pro_level ){
                        $data=array('userProLevel'=>$pro_level);
                        $level_update=CZ::model('users')->uprolevels_update($users_list['d'][$j]['userUid'],$uprolevels['d'][$k]['interestId'],$data);
                        //通知
                        $type=($pro_level > $uprolevels['d'][$k]['userProLevel']) ? 'Ly_my_interest_prolevel_up' : 'Ly_my_interest_prolevel_down' ;
                        $this->sendNotify($users_list['d'][$j]['userUid'],$type,$users_list['d'][$j]['userRealname'],'','Ly_'.$uprolevels['d'][$k]['interestId'],$pro_level);
                    }

                }            

                //更新 users.userLevel 會員頭銜
                $uprolevels=CZ::model('users')->uprolevels_getter($users_list['d'][$j]['userUid']);
                $user_level=$this->userLevel_count($uprolevels);

                if( $users_list['d'][$j]['userLevel'] != $user_level ){
                    $data=array('userLevel'=>$user_level);
                    $user_update=CZ::model('users')->user_update($users_list['d'][$j]['userUid'],$data);
                    //通知
                    $type=($user_level > $users_list['d'][$j]['userLevel']) ? 'Ly_my_level_up' : 'Ly_my_level_down' ;
                    $this->sendNotify($users_list['d'][$j]['userUid'],$type,$users_list['d'][$j]['userRealname'],'Ly_userlevel_'.$user_level);
                }
            }
        }
    }

    /*
    Command_Users_points_update
    功能：1.依upoint_logs記錄校正users.userPoint
    每日4點20分執行一次
    可重覆執行
    */
    public function Command_Users_points_update(){
        $column='userUid,userPoint';
        $usersTotal=CZ::model('users')->users_list($column);
        $page=ceil($usersTotal['args']['total']/$usersTotal['args']['per']);
        
        for($i=0;$i<$page;$i++){
            //分頁執行
            $users_list=CZ::model('users')->users_list($column,$i);
            for($j=0;$j<$users_list['args']['num'];$j++){    
                $userPoint=CZ::model('users')->upointlogs_getter($users_list['d'][$j]['userUid']); 
                $totalPoint = 0 ;

                for($k=0;$k<$userPoint['args']['num'];$k++){
                    $timeLimit = (strtotime($userPoint['d'][$k]['userPointTimeLimit']) >= strtotime(date("Y-m-d")) ) ;
                    //篩選出剩餘點數 >0 且未過期紀錄
                    if( $userPoint['d'][$k]['userPoint'] >0 && $timeLimit )
                        $totalPoint += $userPoint['d'][$k]['userPoint'] ; 
                }
                //echo $users_list['d'][$j]['userUid'].'  '.$users_list['d'][$j]['userPoint'].'  '.$totalPoint.'<br>';
                
                //更新users.userPoint
                if($users_list['d'][$j]['userPoint'] != $totalPoint){
                    $userData = array('userPoint'=>$totalPoint);
                    if(!CZ::model('users')->user_update($users_list['d'][$j]['userUid'], $userData ))
                        error_log('cron fail');
                }   
            }
        }
    }

    /*
    Command_Users_details_check
    功能：1.檢查使用者是否已填寫基本資料
          2.如完成給500點  
    每日4點30分執行一次
    可重覆執行
    */
    public function Command_Users_details_check(){
        $column='userUid,uaccRegistStep';
        $usersTotal=CZ::model('users')->uaccs_getter_list($column);
        $page=ceil($usersTotal['args']['total']/$usersTotal['args']['per']);
        
        for($i=0;$i<$page;$i++){
            //分頁執行
            $users_list=CZ::model('users')->uaccs_getter_list($column,$i);
            for($j=0;$j<$users_list['args']['num'];$j++){
                $userdetails=CZ::model('users')->userdetails_getter($users_list['d'][$j]['userUid']);
                // TODO //userContactPhone
                if( $userdetails['userBlood'] != '' && $userdetails['userContactEmail'] != ''  && $userdetails['userAptitude'] != '' && $userdetails['userRelationship'] != '' ){

                    //給點數
                    $data=array(
                                'upointUid'=>uid4(),
                                'userUid'=>$users_list['d'][$j]['userUid'],
                                'userPointEarn'=>500,
                                'userPointUsed'=>0,
                                'userPoint'=>500,
                                'userPointTimeLimit'=>date("Y",strtotime('+1 year')).'-12-31',
                                'upointlogTimeEarn'=>_SYS_DATETIME,
                                'upointTable'=>'userdetails',
                                'upointTableId'=>'',
                                'upointlogNote'=>'使用者基本資料填寫完成',
                                );
                    $upointlogs=CZ::model('users')->upointlogs_add($data);

                    //更新user.userpoint
                    $user_point=CZ::model('users')->user_point_update($users_list['d'][$j]['userUid'],500);

                    //更改註冊進度
                    switch( $uaccs['uaccRegistStep'] ){
                        case 0 :
                            $data=array('uaccRegistStep'=>10);
                            break;
                        case 1 :
                            $data=array('uaccRegistStep'=>11);
                            break;
                        default:
                            $data=array('uaccRegistStep'=>10);
                    }
                   
                    $uaccsUpdate=CZ::model('users')->uaccs_update($users_list['d'][$j]['userUid'],$data);  


                    //通知
                    CZ::model('notifications')->notifications_add('countmin-system',$users_list['d'][$j]['userUid'],'','','Ly_regist_memberinfo_completed','');

                }  
            }
        }
    }

    /*
    Command_Users_expire_point_notify
    功能：1.即將到期點數通知
          2.email寄送 
    每年6/1 ~ 12/31日執行    
    每日4點40分執行一次
    不可重覆執行
    */
    public function Command_Users_expire_point_notify(){
        $column='userUid,userRealname';
        $usersTotal=CZ::model('users')->users_list($column);
        $page=ceil($usersTotal['args']['total']/$usersTotal['args']['per']);
        
        for($i=0;$i<$page;$i++){
            //分頁執行
            $users_list=CZ::model('users')->users_list($column,$i);
            $point=0;
            for($j=0;$j<$users_list['args']['num'];$j++){
                $point=CZ::model('users')->expirePointTotal( $users_list['d'][$j]['userUid']  );
                if( $point > 0 ){
                    //email
                    $uaccs=CZ::model('users')->uaccs_getter($users_list['d'][$j]['userUid'],'uaccAuth,uaccId');

                    $subtitle='點數到期通知';
                    $content='
                                親愛的【 '.$users_list['d'][$j]['userRealname'] .'】 您好：<p>

                                感謝您對參一腳的支持，<p>

                                您去年累積的點數【 '.$point.' 點】將在【 '.date("Y").' 年12月31日23點59分】歸零，<p>

                                提醒您紅利點數儘早兌換。謝謝您! <p>

                                現在就前往參一腳 好康區 兌換 <p>
                                http://www.countmin.com/index/goods   <p>

                                敬祝您使用愉快！ <p>
                                參一腳 敬啟
                            ';
                     //TODO facebook
                    //if( $uaccs['uaccAuth'] == 'email')    
                    //    $sendEmail=CZ::model('mail')->send_mail($uaccs['uaccId'],$subtitle,$content);                                          
                }
            }
        }
    }   

    //由pro分算pro等級
    private function userProLevel_count($grade){
        $lv=0;
        if($grade >= 40311000)
            $lv=100;
        elseif( $grade >= 34333000 )
            $lv=99;
        elseif( $grade >= 29355000  )
            $lv=98;
        elseif( $grade >= 25377000   )
            $lv=97;
        elseif( $grade >= 21399000   )
            $lv=96;
        elseif( $grade >= 18421000   )
            $lv=95;
        elseif( $grade >= 15443000   )
            $lv=94;
        elseif( $grade >= 13265000   )
            $lv=93;
        elseif( $grade >= 11087000   )
            $lv=92;
        elseif( $grade >= 9609000   )
            $lv=91;
        elseif( $grade >= 8131000   )
            $lv=90;
        elseif( $grade >= 3741000    ){ //lv85~89
            $diff= (($grade-3741000)/878000);
            $lv=85 + floor($diff);
        }
        elseif( $grade >= 1851000   ){
            $diff= (($grade-1851000)/378000);
            $lv=80 + floor($diff);
        }            
        elseif( $grade >= 961000   ){
            $diff= (($grade-961000)/178000);
            $lv=75 + floor($diff);
        }   
        elseif( $grade >= 571000   ){
            $diff= (($grade-571000)/78000);
            $lv=70 + floor($diff);
        }   
        elseif( $grade >= 291000   ){
            $diff= (($grade-291000)/28000);
            $lv=60 + floor($diff);
        }   
        elseif( $grade >= 201000   ){
            $diff= (($grade-201000)/18000);
            $lv=55 + floor($diff);
        }   
        elseif( $grade >= 136000   ){
            $diff= (($grade-136000)/13000);                                                        
            $lv=50 + floor($diff);
        }  
        elseif( $grade >= 91000   ){
            $diff= (($grade-91000)/9000);                                                        
            $lv=45 + floor($diff);
        }  
        elseif( $grade >= 61000   ){
            $diff= (($grade-61000)/6000);                                                        
            $lv=40 + floor($diff);
        }
        elseif( $grade >= 41000   ){
            $diff= (($grade-41000)/4000);                                                        
            $lv=35 + floor($diff);
        }
        elseif( $grade >= 26000   ){
            $diff= (($grade-26000)/3000);                                                        
            $lv=30 + floor($diff);
        }
        elseif( $grade >= 15000   ){
            $diff= (($grade-15000)/2200);                                                        
            $lv=25 + floor($diff);
        }
        elseif( $grade >= 7500   ){
            $diff= (($grade-7500)/1500);                                                        
            $lv=20 + floor($diff);
        }
        elseif( $grade >= 3000   ){
            $diff= (($grade-3000)/900);                                                        
            $lv=15 + floor($diff);
        }
        elseif( $grade >= 1000   ){
            $diff= (($grade-1000)/400);                                                        
            $lv=10 + floor($diff);
        }
        elseif( $grade >= 100   ){
            $diff= (($grade)/100);                                                        
            $lv= floor($diff);
        }
        else
            $lv = 1;     

        return $lv ;    
    }    

    //計算頭銜等級
    private function userLevel_count($uprolevels){
        $total=0;
        $userTitle=0;
        $interest_90=0;
        $interest_70=0;
        $interest_50=0;
        $interest_30=0;
        $interest_15=0;

        for($i=0;$i<$uprolevels['args']['num'];$i++){
            $total += $uprolevels['d'][$i]['userProLevel']; //加總興趣PRO等級

            if( $uprolevels['d'][$i]['userProLevel'] >=90)
                $interest_90++;
            if( $uprolevels['d'][$i]['userProLevel'] >=70)
                $interest_70++;
            if( $uprolevels['d'][$i]['userProLevel'] >=50)
                $interest_50++;
            if( $uprolevels['d'][$i]['userProLevel'] >=30)
                $interest_30++;
            if( $uprolevels['d'][$i]['userProLevel'] >=15)
                $interest_15++;
        }

        if( $interest_90 >= 10 || $total >= 1000 )
            $userTitle=8 ;
        elseif( $interest_90 >= 5 || $total >= 700 )
            $userTitle=7 ;
        elseif( $interest_90 >= 2 || $total >= 500 )
            $userTitle=6 ;
        elseif( $interest_70 >= 2 || $total >= 300 )
            $userTitle=5 ;
        elseif( $interest_50 >= 2 || $total >= 250 )
            $userTitle=4 ;
        elseif( $interest_30 >= 2 || $total >= 150 )
            $userTitle=3 ;
        elseif( $interest_15 >= 2 || $total >= 100 )
            $userTitle=2 ;
        elseif( $total >= 45 )
            $userTitle=1 ;
        else
            $userTitle=0 ;
      
        return $userTitle;                 
    }

    private function sendNotify($userUid,$type,$userName='',$title='',$interest='',$prolevel=''){
        $msg_info['_output']=0; 
        $title=_tl($title,$msg_info); 
        $interest=_tl($interest,$msg_info);

        switch($type){
            case 'Ly_my_level_up':
                $message=json_encode(array('user_name'=>$userName,'user_level'=>$title));    
                break;
            case 'Ly_my_level_down':
                $message=json_encode(array('user_name'=>$userName,'user_level'=>$title));    
                break;
            case 'Ly_my_interest_prolevel_up':
                $message=json_encode(array('user_name'=>$userName,'interest'=>$interest,'prolevel'=>$prolevel));    
                break;     
            case 'Ly_my_interest_prolevel_down':
                $message=json_encode(array('user_name'=>$userName,'interest'=>$interest,'prolevel'=>$prolevel));    
                break;    
        }
        
        $data=array(
                    'unotificationUid'=>uid4(),
                    'userUidFrom'=>'countmin-system',
                    'userUidTo'=>$userUid,
                    'noticeId'=>$type, 
                    'unotificationTarget'=>'',
                    'unotificationTargetUid'=>'',
                    'unotificationData'=>$message,
                    'unotificationTime'=>_SYS_DATETIME,
                    'unotificationStatusRead'=>0,
                    );                     

        $unotifications=CZ::model('notifications')->notifications_byarray_add($data);        
    }
}
