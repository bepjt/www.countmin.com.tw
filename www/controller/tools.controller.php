<?php
class Tools_Controller extends Controller {
	public $basedir='/';

	public $avaible=FALSE;

	public $controller_base='www/controller/';
	public $model_base='www/model/';
	public $js_base='www/model/';
	public $template_base='www/template/';

	public $controller_call_models=array();
	public $template_call_controllers=array();

	public function __construct(){
		$this->name='tools';
		$this->layout='admin';
	}
		
	public function Command_Index(){
		$type=form('type','string','get');
		if(!in_array($type,array('controller','model','js','template'))){
			$type=NULL;
		}
		$files=CZ::model('tools')->files_getter($type);

        include($this->layout_path(CZ::controller(),CZ::command()));
	}

	public function Command_Info(){
		$func_id=form('func','string','get');

		$func=CZ::model('tools')->func_getter($func_id);
		if(!empty($func['funcIdRefer'])){
			$rels=CZ::model('tools')->func_relationship_from_getter($func['funcIdRefer']);
			$calls=CZ::model('tools')->func_relationship_call_getter($func_id);
		}
		else{
			$rels=CZ::model('tools')->func_relationship_from_getter($func_id);
			$calls=CZ::model('tools')->func_relationship_call_getter($func_id);
		}
        include($this->layout_path(CZ::controller(),CZ::command()));
	}

	public function Command_Refresh(){
		$this->basedir_setter('/srv/web/countmin/');
		$model=$this->find_in_model();

		$data_model=array();
		foreach($model as $model_class => $methods){
			$data_model[]=array(
				'funcId'=>'model/'.$model_class,
				'funcType'=>'model',
			);
			foreach($methods as $method_name => $method_v){
				$data_model[]=array(
					'funcId'=>'method/'.$model_class.'/'.$method_name,
					'funcType'=>'method',
					'funcIdRefer'=>'model/'.$model_class,
					'funcProtected'=>$method_v['comment'],
				);
			}
		}
		//CZ::model('tools')->model_add($data_model);

		$controller=$this->find_in_controller();

		$data_controller=array();
		$data_relation=array();
		foreach($controller as $controller_name => $funcs){
			$data_controller[]=array(
				'funcId'=>'controller/'.$controller_name,
				'funcType'=>'controller',
			);
			foreach($funcs['funcs'] as $func => $v){
				$data_controller[]=array(
					'funcId'=>strtolower($v['type']).'/'.$controller_name.'/'.$v['name'],
					'funcType'=>strtolower($v['type']),
					'funcIdRefer'=>'controller/'.$controller_name,
					'funcProtected'=>$v['mode'],
				);
			}

			for($i=0,$n=count($funcs['models']);$i<$n;$i++){
				$data_relation[]=array(
					'funcIdFrom'=>'controller/'.$controller_name,
					'funcIdCall'=>$funcs['models'][$i],
				);
			}
		}
		//CZ::model('tools')->controller_add($data_controller);
		//CZ::model('tools')->relationship_add($data_relation);

		$template=$this->find_in_template_and_js();
		$data_template=array();
		$data_relation=array();
		foreach($template as $template_name => $funcs){
			$data_template[]=array(
				'funcId'=>$template_name,
				'funcType'=>'template',
				'funcIdRefer'=>'',
				'funcProtected'=>'',
			);
			foreach($funcs as $func => $v){
				$data_relation[]=array(
					'funcIdFrom'=>$template_name,
					'funcIdCall'=>$v['api'],
				);
			}
		}
		CZ::model('tools')->template_add($data_template);
		CZ::model('tools')->relationship_add($data_relation);

        include($this->layout_path(CZ::controller(),CZ::command()));
	}

	public function basedir_setter($absolute_path='/'){
		if(substr($absolute_path,-1)=='/'){
			$this->avaible=TRUE;
			$this->basedir=$absolute_path;
		}
	}

	public function find_controller($fpath){
		$c=file_get_contents($fpath);
		$items=array(
			'funcs'=>array(),
			'models'=>array(),
		);
		
		if(preg_match_all('/([static|private|public|protected]+)\s+function\s+(:?(Command|Grid)_([A-Z][a-zA-Z0-9_]+))\(/',$c,$m)){
			for($i=0,$n=count($m[1]);$i<$n;$i++){
				$items['funcs'][$m[2][$i]]=array(
					'mode'=>$m[1][$i],
					'type'=>$m[3][$i],
					'name'=>$m[2][$i],
				);
			}
		}
		$models=array();
		if(preg_match_all('/CZ::model\(\'([^\']+)\'\)\->([^\(\s]+)/',$c,$m)){
			for($i=0,$n=count($m[1]);$i<$n;$i++){
				$models['method/'.ucfirst($m[1][$i]).'Model/'.$m[2][$i]]=1;
			}
			$items['models']=array_keys($models);
		}
		return $items;
	}

	public function find_model($fpath){
		$c=file_get_contents($fpath);
		$funcs=array();

		if(preg_match_all('/(public|private|protected)\s+function\s+([a-zA-Z_][a-zA-Z0-9_]+)\s*\(/si',$c,$m)){
			for($i=0,$n=count($m[2]);$i<$n;$i++){
				$funcs[$m[2][$i]]=array(
					'comment'=>$m[1][$i],
					'name'=>$m[2][$i]
				);
			}
		}
		return $funcs;
	}

	public function find_js($fpath){
		$c=file_get_contents($fpath);
		$api_needs=array();
			
		if(preg_match_all('/[a-z0-9]\s*=\s*\'\/([a-z0-9_]+)\/([a-z0-9_]+)[\'\/]/',$c,$m)){
			// ex:
			// url='/controller/command/
			// url = '/controller/command'+... 
			for($i=0,$n=count($m[1]);$i<$n;$i++){
				$api_needs['command/'.$m[1][$i].'/Command_'.ucfirst($m[2][$i])]=array(
					'mode'=>'unknown',
					'api'=>'command/'.$m[1][$i].'/Command_'.ucfirst($m[2][$i]),
				);
			}
		}
		if(preg_match_all('/\.(:?post|get)\(\'\/([a-z0-9_]+)\/([a-z0-9_]+)[\'\/]/',$c,$m)){
			// ex:
			// .post('/controller/command/
			// .get('/controller/command'+... 
			for($i=0,$n=count($m[2]);$i<$n;$i++){
				$api_needs['command/'.$m[2][$i].'/Command_'.ucfirst($m[3][$i])]=array(
					'mode'=>$m[1][$i],
					'api'=>'command/'.$m[2][$i].'/Command_'.ucfirst($m[3][$i]),
				);
			}
		}
		return $api_needs;
	}

	public function find_in_controller(){
		if(!$this->avaible){
			echo 'No path set';
			die('');
		}
		$controller=array();
		$d=opendir($this->basedir.$this->controller_base);
		while(($f=readdir($d))!=FALSE){
			if(is_file($this->basedir.$this->controller_base.$f) && substr($f,-4)=='.php'){
				$controller[substr($f,0,-15)]=$this->find_controller($this->basedir.$this->controller_base.$f);
			}
		}
		closedir($d);
		return $controller;
	}


	public function find_in_model(){
		if(!$this->avaible){
			echo 'No path set';
			die('');
		}
		$model=array();
		$d=opendir($this->basedir.$this->model_base);
		while(($f=readdir($d))!=FALSE){
			if(is_file($this->basedir.$this->model_base.$f) && substr($f,-4)=='.php'){
				$model[substr($f,0,-4)]=$this->find_model($this->basedir.$this->model_base.$f);
			}
		}
		closedir($d);
		return $model;
	}

	public function find_in_template_and_js(){
		if(!$this->avaible){
			echo 'No path set';
			die('');
		}
		$template=array();
		$d=opendir($this->basedir.$this->template_base);
		while(($f=readdir($d))!=FALSE){
			if(is_dir($this->basedir.$this->template_base.$f)){
				$d2=opendir($this->basedir.$this->template_base.$f.'/');
				while(($f2=readdir($d2))!=FALSE){
					if(is_file($this->basedir.$this->template_base.$f.'/'.$f2) && substr($f2,-6)=='.phtml'){
						$template['template/'.$f.'/'.substr($f2,0,-6)]=$this->find_js($this->basedir.$this->template_base.$f.'/'.$f2);
					}
				}
				closedir($d2);
			}
		}
		closedir($d);

		$template['js/onload']=$this->find_js($this->basedir.'public/js/onload.js');
		$template['js/editor']=$this->find_js($this->basedir.'public/js/editor.min.js');
		$template['js/wallposts']=$this->find_js($this->basedir.'public/js/wallposts.js');
		return $template;
	}
}
