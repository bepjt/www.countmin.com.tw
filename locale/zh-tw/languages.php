<?php
/** 性別 **/
$GLOBALS['string']['Ly_gender_0']='未知';
$GLOBALS['string']['Ly_gender_1']='男';
$GLOBALS['string']['Ly_gender_2']='女';

/** 頭銜等級 **/
$GLOBALS['string']['Ly_userlevel_9']='神人';
$GLOBALS['string']['Ly_userlevel_8']='神人';
$GLOBALS['string']['Ly_userlevel_7']='巨星';
$GLOBALS['string']['Ly_userlevel_6']='明星';
$GLOBALS['string']['Ly_userlevel_5']='A咖';
$GLOBALS['string']['Ly_userlevel_4']='四星達人';
$GLOBALS['string']['Ly_userlevel_3']='三星達人';
$GLOBALS['string']['Ly_userlevel_2']='二星達人';
$GLOBALS['string']['Ly_userlevel_1']='一星達人';                                
$GLOBALS['string']['Ly_userlevel_0']='新手';

/** 興趣標籤 **/
$GLOBALS['string']['Ly_food']	='美食';
$GLOBALS['string']['Ly_acg']	='遊戲動漫';
$GLOBALS['string']['Ly_artist']	='名人動態';
$GLOBALS['string']['Ly_trend']	='流行時尚';
$GLOBALS['string']['Ly_mkup']	='美容彩妝';
$GLOBALS['string']['Ly_movie']	='電影戲劇';
$GLOBALS['string']['Ly_health']	='醫療保健';
$GLOBALS['string']['Ly_mood']	='心情抒發';
$GLOBALS['string']['Ly_parent']	='親子互動';
$GLOBALS['string']['Ly_bg']		='感情話題';
$GLOBALS['string']['Ly_fate']	='命理星座';
$GLOBALS['string']['Ly_pet']	='寵物';
$GLOBALS['string']['Ly_music']	='音樂';
$GLOBALS['string']['Ly_brand']	='品牌服務';
$GLOBALS['string']['Ly_art']	='文學藝術';
$GLOBALS['string']['Ly_photo']	='攝影';
$GLOBALS['string']['Ly_life']	='居家生活';
$GLOBALS['string']['Ly_pub']	='公眾議題';
$GLOBALS['string']['Ly_3c']		='3C數位';
$GLOBALS['string']['Ly_learn']	='教育學習';
$GLOBALS['string']['Ly_invest']	='投資理財';
$GLOBALS['string']['Ly_trip']	='休閒旅遊';
$GLOBALS['string']['Ly_sport']	='運動';
$GLOBALS['string']['Ly_car']	='車友天地';
$GLOBALS['string']['Ly_soul']	='心靈勵志';
$GLOBALS['string']['Ly_pi']		='公益';
$GLOBALS['string']['Ly_sci']	='自然科普';

/** 通知 **/
$GLOBALS['string']['Ly_i_like_this']='點數增加[#point]點，Pro分上升';
$GLOBALS['string']['Ly_i_dislike_this']='點數增加[#point]點，Pro分上升';
$GLOBALS['string']['Ly_i_share_this']='點數增加[#point]點，Pro分上升';
$GLOBALS['string']['Ly_i_post_one']='點數增加[#point]點，Pro分上升';
$GLOBALS['string']['Ly_i_join']='我參與了【<b>[#wallpost_topic]</b>】';
$GLOBALS['string']['Ly_i_favor_this']='收藏成功!';
$GLOBALS['string']['Ly_i_abuse_this']='檢舉完畢，感謝您的回報!';
$GLOBALS['string']['Ly_reply_comment_by_other']='<b>[#user_name]</b>回應了【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_arrive_event_condition']='達成 【<b>[#wallpost_topic]</b>】 的活動條件，點數增加[#point]點';
$GLOBALS['string']['Ly_event_announce']='你參加的【<b>[#wallpost_topic]</b>】已公布得獎名單。 ';
$GLOBALS['string']['Ly_event_timesup']='你參加的【<b>[#wallpost_topic]</b>】已公布得獎名單。';
$GLOBALS['string']['Ly_other_like_my_post']='<b>[#user_name]</b>喜歡你發表的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_like_my_share']='<b>[#user_name]</b>喜歡你分享的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_2_like_my_post']='<b>[#user_name1]</b>、<b>[#user_name2]</b>等2個人喜歡你發表的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_2_like_my_share']='<b>[#user_name1]</b>、<b>[#user_name2]</b>等2個人喜歡你分享的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_more_like_my_post']='<b>[#user_name1]</b>、<b>[#user_name2]</b>、<b>[#user_name3]</b>等[#num_user]個人喜歡你發表的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_more_like_my_share']='<b>[#user_name1]</b>、<b>[#user_name2]</b>、<b>[#user_name3]</b>等[#num_user]個人喜歡你分享的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_dislike_my_post']='<b>[#user_name]</b>不喜歡你發表的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_dislike_my_share']='<b>[#user_name]</b>不喜歡你分享的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_2_dislike_my_post']='<b>[#user_name1]</b>、<b>[#user_name2]</b>等2個人不喜歡你發表的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_2_dislike_my_share']='<b>[#user_name1]</b>、<b>[#user_name2]</b>等2個人不喜歡你分享的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_more_dislike_my_post']='<b>[#user_name1]</b>、<b>[#user_name2]</b>、<b>[#user_name3]</b>等[#num_user]個人不喜歡你發表的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_more_dislike_my_share']='<b>[#user_name1]</b>、<b>[#user_name2]</b>、<b>[#user_name3]</b>等[#num_user]個人不喜歡你分享的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_i_earn_point_last_week']='上週你的發表累計共獲得[#point]點，Pro分上升。';
$GLOBALS['string']['Ly_i_deduct_point_last_week']='上週你的發表累計共減少[#point]點，Pro分下降。';
$GLOBALS['string']['Ly_other_favor_my_post']='<b>[#user_name]</b>收藏你發表的【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_abuse_my_post']='你發表的【<b>[#wallpost_topic]</b>】被檢舉。';
$GLOBALS['string']['Ly_other_share_my_post']='<b>[#user_name]</b>分享了你發表的【<b>[#wallpost_topic]</b>】。點數增加[#point]點，Pro分上升';
$GLOBALS['string']['Ly_other_invite_me']='<b>[#user_name]</b>邀請你參加【<b>[#wallpost_topic]</b>】。';
$GLOBALS['string']['Ly_other_comment_my_post']='<b>[#user_name]</b>在你發表的【<b>[#wallpost_topic]</b>】留言。';
$GLOBALS['string']['Ly_other_join_my_event']='<b>[#user_name]</b>參加了你發起的【<b>[#wallpost_topic]</b>】';
$GLOBALS['string']['Ly_i_earn_point_be_shared_post']='你的發表被分享後之評價分上升[#grade]分，點數共增加[#point]點，Pro分上升。';
$GLOBALS['string']['Ly_i_deduct_point_be_shared_post']='你的發表被分享後之評價分下降[#grade]分，點數共減少[#point]點，Pro分下降。';
$GLOBALS['string']['Ly_other_invite_friendship']='<b>[#user_name]</b>對你發出了交友邀請。';
$GLOBALS['string']['Ly_other_follow_me']='<b>[#user_name]</b>追蹤你成為同好。';
$GLOBALS['string']['Ly_other_make_friendship']='你和<b>[#user_name]</b>已經成為好友!';
$GLOBALS['string']['Ly_my_level_up']='<b>[#user_name]</b>恭喜您 ! 您的頭銜提升到[#user_level]!';
$GLOBALS['string']['Ly_my_level_down']='<b>[#user_name]</b>很可惜 ! 您的頭銜修改為[#user_level]!';
$GLOBALS['string']['Ly_my_interest_prolevel_up']='<b>[#user_name]</b>太棒了！您的[#interest]興趣Pro等級提升到[#prolevel]級! ';
$GLOBALS['string']['Ly_my_interest_prolevel_down']='<b>[#user_name]</b>很可惜！您的[#interest]興趣Pro等級修改為[#prolevel]級! ';
$GLOBALS['string']['Ly_i_exchange_goods']='恭喜您! 成功兌換【[#goods_name]】~';
$GLOBALS['string']['Ly_user_abuse_post']='【會員暱稱】檢舉了【文章主旨】_理由:【會員選填】';
$GLOBALS['string']['Ly_user_ask_question']='【會員暱稱】詢問關於【會員選擇】';
$GLOBALS['string']['Ly_unknow_warning']='無法兌換、無法發表、無法評價….系統錯誤次數過多無法操作時的錯誤報告';
$GLOBALS['string']['Ly_i_exchange_completed']='兌換成功，點數帳戶中可查詢您的訂單唷!!';
$GLOBALS['string']['Ly_i_exchange_failed']='兌換失敗，請聯絡客服';
$GLOBALS['string']['Ly_no_enough_point']='點數不足';
$GLOBALS['string']['Ly_regist_completed']='恭喜你，已完成註冊，可開始使用參一腳囉!目前資料完整度為30%，填寫詳細個人資料可讓人更快認識你還可獲得參一腳平台的獎勵點數500點唷!!!快來填寫吧!!立即前往';
$GLOBALS['string']['Ly_regist_not_completed']='提醒你，驗證信已送至你的信箱[#email]，請盡速至信箱點入連結來確認你的郵件地址唷!! ';
$GLOBALS['string']['Ly_regist_authemail_completed']='恭喜你!已完成驗證信，獲得點數300點，完成基本資料填寫後再送500點';
$GLOBALS['string']['Ly_regist_memberinfo_completed']='恭喜你!已完成資料填寫，獲得點數500點';
$GLOBALS['string']['Ly_regist_invite_friends_completed']='您成功推薦朋友加入參一腳，共獲得點數【100】！推薦回饋無上限，快拉更多朋友加入吧~';

$GLOBALS['string']['Ly_astrology_0']='未知';
$GLOBALS['string']['Ly_astrology_1']='水瓶';
$GLOBALS['string']['Ly_astrology_2']='雙魚';
$GLOBALS['string']['Ly_astrology_3']='金牛';
$GLOBALS['string']['Ly_astrology_4']='牡羊';
$GLOBALS['string']['Ly_astrology_5']='雙子';
$GLOBALS['string']['Ly_astrology_6']='巨蟹';
$GLOBALS['string']['Ly_astrology_7']='獅子';
$GLOBALS['string']['Ly_astrology_8']='處女';
$GLOBALS['string']['Ly_astrology_9']='天秤';
$GLOBALS['string']['Ly_astrology_10']='天蠍';
$GLOBALS['string']['Ly_astrology_11']='射手';
$GLOBALS['string']['Ly_astrology_12']='魔羯';

$GLOBALS['string']['Ly_bloodtype_0']='未知';
$GLOBALS['string']['Ly_bloodtype_1']='A型';
$GLOBALS['string']['Ly_bloodtype_2']='B型';
$GLOBALS['string']['Ly_bloodtype_3']='O型';
$GLOBALS['string']['Ly_bloodtype_4']='AB型';

$GLOBALS['string']['Ly_gender_0']='未知';
$GLOBALS['string']['Ly_gender_1']='男';
$GLOBALS['string']['Ly_gender_2']='女';

$GLOBALS['string']['Ly_lovestatus_0']='未知';
$GLOBALS['string']['Ly_lovestatus_1']='單身';
$GLOBALS['string']['Ly_lovestatus_2']='暗戀中';
$GLOBALS['string']['Ly_lovestatus_3']='穩定交往中';
$GLOBALS['string']['Ly_lovestatus_4']='已婚';
$GLOBALS['string']['Ly_lovestatus_5']='一言難盡';


