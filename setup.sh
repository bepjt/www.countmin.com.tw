#!/bin/sh
chmod -R 644 /srv/web/countmin/
chmod -R 644 /srv/web/CoreBase/
find /srv/web/countmin/ -type d -exec chmod 755 * \;
find /srv/web/CoreBase/ -type d -exec chmod 755 * \;
chmod 755 /srv/web/countmin
chmod 755 /srv/web/CoreBase
chmod -R 777 public/images/
chmod -R 775 public/user_profiles/
chmod -R 775 public/wallpost_images/
chmod -R 775 public/user_banners/
chmod -R 775 public/image_temps/
chmod -R 775 public/css/img/