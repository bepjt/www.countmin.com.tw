<?php
define('_DIR_FRAMEWORK','/srv/web/CoreBase/Firm/');
define('_DIR_VIEW',_DIR_DOCS.'www/template/');

define('_DIR_TEMPFILE','/tmp/');
if(!strcmp(substr($_SERVER['REMOTE_ADDR'],0,10),'10.10.100.') || in_array($_SERVER['REMOTE_ADDR'],array('127.0.0.1','::1'))){
	define('_DEBUG',1);
}
else{
	define('_DEBUG',0);
}

if(!strcasecmp($_SERVER['HTTP_HOST'],'countmin.com') || !strcasecmp(strchr($_SERVER['SERVER_NAME'],'.'),'.countmin.com')){
	define('_DOMAIN','.countmin.com');
}
else{
	define('_DOMAIN',$_SERVER['HTTP_HOST']);
}

if(!strcmp($_SERVER['SERVER_ADDR'],'10.10.100.5') || in_array($_SERVER['REMOTE_ADDR'],array('127.0.0.1','::1'))){
	define('_DEV',1);
}
else{
	define('_DEV',0);
}

